#!/bin/bash
#---------------------------------------------------------------------------------------------------------
#   RICOMPILA PACCHETTO SORGENTE
#---------------------------------------------------------------------------------------------------------

# Variabili di servizio
#---------------------------------------------------------------------------------------------------------
set -e
data=$(date +"%Y.%^b.%d %a %H.%M.%S")                       # variabile impostazione data, ora
shopt -s extglob
pkg="$1"

# X Org configure singoli pacchetti
#---------------------------------------------------------------------------------------------------------
# export MAKEFLAGS='-j 2'                                   # CPU utilizzate
case $(uname -r) in
  x86_64 ) export CFLAGS="-02 -fPIC" ;;
  x?86   ) export CFLAGS="-O2 -march=i686 -mtune=i686" ;;
esac
export CXXFLAGS="${CFLAGS}"
export XORG_PREFIX="/usr"
export XORG_CONFIG="--prefix=$XORG_PREFIX --sysconfdir=/etc --mandir=/usr/man --localstatedir=/var --disable-static"

# Variabili impostazione localizzazione lingua e archivi desktop
#---------------------------------------------------------------------------------------------------------
loc_set=${LANG:0:2}                                         # Impostazioni localizzazione lingua di sistema
nls_set=""                                                  # ${loc_set} lingua di sistema o es. 'it' lingua italiana
man_set=""                                                  # ${loc_set} lingua di sistema o es. 'it' lingua italiana
dsk_set=""                                                  # Impostato a '1' installa archivi desktop

# Repository, Librerie script
#---------------------------------------------------------------------------------------------------------
_rep='/usr/src/pack'                                        # Directory script pacchetti
_lib='/usr/bin'                                             # Directory funzioni accessorie
_dep="${_rep}/00-depend"                                    # Elenco pacchetti e dipendenze
_hlp="${_lib}/pack-hlp"                                     # Help
_col="${_lib}/pack-col"                                     # Colori
. "${_col}"                                                 # Caricamento varibili colore

if [[ ! -n "${pkg}" ]]; then clear; . "${_hlp}"; exit; fi   # Uscita per opzione pacchetto nulla

# File / Directory di lavoro
#---------------------------------------------------------------------------------------------------------
IFS='_' read -r name vers <<< "${pkg}"                      # Nome e Versione pacchetto estrapolati dalla variabile 'pkg'
workdir="${_rep}/${pkg}"                                    # Directory singolo pacchetto
scripts="${workdir}/${name}.build"                          # Script configura, compila, installa pacchetto
tracpkg="${workdir}/${pkg}.trc"                             # Archivio traccia componenti pacchetto
logspkg="${workdir}/${pkg}.log"                             # Archivio log installazione sorgenti
ante_fs='/tmp/00_antefs'                                    # Archivio scansione filesystem ante-installazione
post_fs='/tmp/00_postfs'                                    # Archivio scansione filesystem post-installazione
last_fs='/tmp/00_lastfs'                                    # Archivio scansione filesystem ripristino fallimento
_dsktop='/usr/share/applications'                           # Directory archivi desktop installati
_locale='/usr/share/locale'                                 # Directory archivi localizzazione lingua
_mandoc='/usr/man'                                          # Directory pagine man

# Funzione scansione filesystem ante e post installazione
#---------------------------------------------------------------------------------------------------------
SCAN () {
find / \
-path /proc -prune -o \
-path /tmp -prune -o \
-path /sys -prune -o \
-path /dev -prune -o \
-path /home -prune -o \
-path /root -prune -o \
-path /mnt -prune -o \
-path /media -prune -o \
-path /usr/src -prune -o \
-path /srv/www -prune -o \
-path /var -prune -o \
-path /run -prune -o \
-path /boot -prune -o \
-print ;
find /home -maxdepth 1 -type d -print ;
find /var -type d -print ;
}

# Ricerca pacchetto in 00-depend
#---------------------------------------------------------------------------------------------------------
while IFS=$'\n' read -r ldep ; do                           # Operazioni di verifica sul file 00-depend a partire dalla variabile 'pkg'
  if [[ ${ldep} =~ \*${name}_* ]]; then                     # Verifica la presenza del pacchetto in 00-depend
    flag=${ldep:0:2}                                        # Status pacchetto (installato/non installato)
    lpak=${ldep:2:43}                                       # Campo pacchetto grezzo (con caratteri di completamento e numero gruppo)
    chrn=${ldep:2:38}                                       # Idem come 'lpak', senza numero gruppo
    IFS=' ' read -r dpkg tttt <<< "${lpak}"                 # nome, versione pacchetto con trattini e numero gruppo appartenenza
    IFS=' ' read -r trat numg <<< "${tttt}"                 # Trattini di completameto per l'archivio cronlog
    IFS='_' read -r _nam _ver <<< "${dpkg}"                 # nome, versione pacchetto riscontrati in 00-depend
    _NG=${numg:0:2}                                         # Prime due cifre del numero gruppo
    _R0=${_nam:0:1}                                         # Eventuale flag di ricompilazione '@'
    break                                                   # Interruzione. Trovato nome pacchetto
  fi
done < "${_dep}"
unset IFS

# Varibili pacchetto da ricompilare
#---------------------------------------------------------------------------------------------------------
rpkg=${pkg/@/}

while IFS=$'\n' read -r ldep ; do                           # Operazioni di verifica sul file 00-depend a partire dalla variabile 'pkg'
  if [[ ${ldep} =~ \*"${rpkg}"* ]]; then                    # Verifica la presenza del pacchetto in 00-depend
    rflg=${ldep:0:2}                                        # Status pacchetto (installato/non installato)
    break                                                   # Interruzione. Trovato nome pacchetto
  fi
done < "${_dep}"
unset IFS

IFS='_' read -r _rna _rve <<< "${rpkg}"                     # Nome e Versione pacchetto estrapolati dalla variabile 'pkg'

_wrkdir="${_rep}/${rpkg}"                                   # Directory singolo pacchetto pre-ricompilazione
_source="${_wrkdir}/sources"                                # Directory sorgenti
_script="${_wrkdir}/${_rna}.build"                          # Script configura, compila, installa pacchetto
_trcpkg="${_wrkdir}/${rpkg}.trc"                            # Archivio traccia componenti pacchetto pre-ricompilazione
_srcpkg="${_source}/00-sources.txt"                         # Archivio url sorgenti
_reclnk=$(readlink ${pkg})
_recomp=${_reclnk///}

if [[ ! -s "${scripts}" ]]; then scripts="${_script}" ]]; fi

# Funzione messaggi
#---------------------------------------------------------------------------------------------------------
err () { echo -ne "\r[ ${b_ros}**${z} ] ${pkg} ${trat} ${msg_i}${b_ros}${msg_e}${z}"; echo ""; }
adv () {
case "${msg_x}" in
  '_E1'  )  msg_i="Installazione sorgente impossibile      "
            msg_e="*** ERROR: archivio '${scripts}' assente ***                      "; err ;;
  '_R1'  )  msg_i="Impossibile procedere                   "
            msg_e="*** Warning: flag ricompilazione '@' assente in '${_dep}' ***     "; err ;;
  '_R2'  )  msg_i="Impossibile procedere                   "
            msg_e="*** Warning: pacchetto da ricompilare ${rpkg} non installato ***  "; err ;;
  '_R3'  )  msg_i="Impossibile procedere                   "
            msg_e="*** Warning: pacchetto da ricompilare ${rpkg} assente ***         "; err ;;
  '_R4'  )  msg_i="Impossibile procedere                   "
            msg_e="*** Warning: script ricompilazione '${script}' assente ***        "; err ;;
  '_W1'  )  msg_i="Impossibile procedere                   "
            msg_e="*** Warning: pacchetto assente in '${_dep}' ***                   "; err ;;
  '_W2'  )  msg_i="Impossibile procedere                   "
            msg_e="*** Warning: versione pacchetto differente in '${_dep}' ***       "; err ;;
  '_W3'  )  msg_i="Impossibile procedere                   "
            msg_e="*** Warning: pacchetto precedentemente installato in '${_dep}' ***"; err ;;
esac
}

# Condizioni di uscita dallo script:
# pacchetto assente in 00-depend, differente versione, precedentemente installato
#---------------------------------------------------------------------------------------------------------
if [[ "${_R0}" != '@' ]]; then msg_x="_R1"; . "${_adv}"; exit; fi
if [[ "${rflg}" = '**' ]]; then msg_x="_R2"; . "${_adv}"; exit; fi
if [[ ! -n "${_recomp}" ]]; then msg_x="_R3"; . "${_adv}"; exit; fi
if [[ ! -s "${_script}" ]]; then
  if [[ ! -s "${scripts}" ]]; then
     msg_x="_R4"; . "${_adv}"; exit
  fi
fi
if [[ "${_nam}" != "${name}" ]]; then msg_x="_W1"; . "${_adv}"; exit; fi
if [[ "${_ver}" != "${vers}" ]]; then msg_x="_W2"; . "${_adv}"; exit; fi
if [[ "${flag}" = '**' ]]; then msg_x="_W3"; . "${_adv}"; exit; fi
# if [[ ! -s "${scripts}" ]]; then msg_x="_E1"; . "${_adv}"; exit; fi

# Modifica flag pacchetto a installato in 00-depend
#---------------------------------------------------------------------------------------------------------
sed -i "s/^ \*${pkg}/**${pkg}/g" "${_dep}"

# Scansiona filesystem ante installazione
#---------------------------------------------------------------------------------------------------------
SCAN | sort > "${ante_fs}"

# Funzione scarica e scompatta archivi sorgenti
#---------------------------------------------------------------------------------------------------------
if [[ -e "${_srcpkg}" ]]; then
  while read -r; do
    if [[ "${REPLY}" = wget* ]]; then
      read -r _ s_get _ <<< "${REPLY}"
    else
      s_get="${rpkg}.txz"
    fi
    dwn=$(basename ${s_get})
    if [[ "${_source}/${dwn}" != *deb ]]; then
      s_dir=$(tar tf "${_source}/${dwn}" | sed -n '2 { s|\./||; s|/.*||; p; q}')
      cd "${_source}"
      tar xfv "${dwn}"
      cd "${s_dir}"; chown -Rv root:root .; chmod -Rv u+w,go+r-w,a-s .
    fi
    break
  done < "${_srcpkg}"
fi

# Esecuzione istruzioni installazione
#---------------------------------------------------------------------------------------------------------
case "${_pgm}" in
  'pack-add' ) { time . "${scripts}" ; } 2>&1 | tee "${logspkg}"  ;;
  'pack-grp' ) . "${scripts}"                                     ;;
esac
# { time . "${scripts}"; } > "${logspkg}" 2>&1
# { time . "${scripts}" ; } 2>&1 | tee "${logspkg}"

# Rimuove directory sorgenti
#---------------------------------------------------------------------------------------------------------
if [[ -n "${s_dir}" ]] && [[ -d "${_source}/${s_dir}" ]]; then rm -rv "${_source}/${s_dir}"; fi

# Elimina pagine info
#---------------------------------------------------------------------------------------------------------
if [[ -d /usr/share/info ]]; then rm -rv /usr/share/info; fi

# Salva ed elimina eventuali file '.desktop' in /usr/share/applications
#---------------------------------------------------------------------------------------------------------
# if [[ -d "${_dsktop}" ]]; then
#   if [[ ! -n "${dsk_set}" ]]; then
#     rm -rv "${_dsktop}"
#   fi
# fi

# Salva ed elimina archivi di localizzazione della lingua
#---------------------------------------------------------------------------------------------------------
if [[ "${name}" = 'fluxbox' ]]; then
  cd /usr/share/fluxbox/nls
else
  cd "${_locale}"
fi
for _lan in *; do
  _nls={_lan:0:2}
  if [[ -d "${_nls}" ]]; then
    if [[ "${_nls}" != "${nls_set}" ]]; then
      rm -rv "${_lan}"
    fi
  fi
done

# Salva ed elimina pagine man con localizzazione della lingua
#---------------------------------------------------------------------------------------------------------
cd "${_mandir}"
for _mpg in *; do
  _man={_mpg:0:2}
  if [[ "${_mpg}" != man* ]]; then
    if [[ "${_man}" != "${man_set}" ]]; then
      rm -rv "${_mpg}"
    fi
  fi
done

# Scansiona filesystem post Installazione e crea archivio traccia
#---------------------------------------------------------------------------------------------------------
SCAN | sort > "${post_fs}"; comm --nocheck-order -13 "${ante_fs}" "${post_fs}" | sort -r > "${tracpkg}"

#---------------------------------------------------------------------------------------------------------
# Trattamento pagine man per la ricompilazione
#---------------------------------------------------------------------------------------------------------
if [[ -s "${tracpkg}" ]]; then
  while read -r; do
    if [[ "${REPLY}" = *${_mandoc}* ]] && [[ ! -d "${REPLY}" ]]; then
      if [[ "${REPLY}" != *.gz ]]; then
        find "${REPLY}" | xargs /bin/rm -fv 2> /dev/null
      fi
    fi
  done < "${tracpkg}"
  sed -i '/\/usr\/man/d' "${tracpkg}"
  sed -i '/^$/d' "${tracpkg}"
fi
if [[ -s "${tracpkg}" ]]; then
  cat "${_trcpkg}" >> "${tracpkg}"
  mv -v "${_trcpkg}" "${_trcpkg}.bak"
  sort -r "${tracpkg}" > "${_trcpkg}"
  mv -v "${tracpkg}" "${tracpkg}.bak"
fi

# Stripping binari e librerie
#---------------------------------------------------------------------------------------------------------
while read -r; do
  if [[ "${REPLY}" = *bin/* ]]; then strip --strip-unneeded "${REPLY}" 2> /dev/null || true; fi
  if [[ "${REPLY}" = *lib/* ]]; then strip --strip-debug "${REPLY}" 2> /dev/null || true; fi
  if [[ "${REPLY}" = *sbin/* ]]; then strip --strip-debug "${REPLY}" 2> /dev/null || true; fi
done < "${_trcpkg}"
