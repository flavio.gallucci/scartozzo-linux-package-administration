#!/bin/bash
#---------------------------------------------------------------------------------------------------------
#   Logs Analisi del file di log del pacchetto
#---------------------------------------------------------------------------------------------------------
# set -e

opt="$1"
pak="$2"


#    VARIABILI DA MODIFICARE
# ------------------------------------------------------------------------------
sys="/usr/src/pack/01-system"
rep="${SL}${sys}"

LOG="${rep}/service"

col="03-m_col"
ord="05-pk_az"
. "${ord}"
. "${col}"


HELP () {
echo ""
echo -e "${b_cia}$0${z} [${b_blu}-a|-t|-s|-p|-o|-e|-k|-f|-i|-w|*${z}]"
echo "--------------------------------------------------------------------------------"
echo -e "  ${b_blu}-t${z}  Analisi log pacchetti sistema temporaneo"
echo -e "  ${b_blu}-s${z}  Analisi log pacchetti sistema base"
echo -e "  ${b_blu}-a${z}  Analisi log pacchetti sistema completo"
echo -e "  ${b_blu}-p${z}  Analisi log pacchetto singolo"
echo -e "  ${b_blu}-e${z}  Analisi globale log per error / errore"
echo -e "  ${b_blu}-k${z}  Analisi globale log per fail / failed / fallito"
echo -e "  ${b_blu}-f${z}  Analisi globale log per not found / non trovato"
echo -e "  ${b_blu}-i${z}  Analisi globale log per impossible / impossibile"
echo -e "  ${b_blu}-w${z}  Analisi globale log per warning"
echo -e "  ${b_blu}-o${z}  Analisi globale log per -e -k -f -i -w"
echo -e "  ${b_blu} *${z}  Help"
echo ""
}

TESTATA () {
echo "--------------------------------------------------------------------------------"
echo -e "   ${b_cia}${pkg}${z}"
echo "--------------------------------------------------------------------------------"
}

ERROR () {
if grep -qiw 'error' "${logspkg}" || grep -qiw 'errore' "${logspkg}"; then
  TESTATA
  grep --color -iw ' error' "${logspkg}"
  grep --color -iw ' errore' "${logspkg}"
  grep --color -iw 'error:' "${logspkg}"
  grep --color -iw 'errore:' "${logspkg}"
fi
}

FAIL () {
if grep -qiw 'fail' "${logspkg}" || grep -qiw 'failed' "${logspkg}" || grep -qiw 'fallito' "${logspkg}"; then
  TESTATA
  grep --color -iw ' fail' "${logspkg}"
  grep --color -iw ' failed' "${logspkg}"
  grep --color -iw ' fallito' "${logspkg}"
  grep --color -iw 'fail:' "${logspkg}"
  grep --color -iw 'failed:' "${logspkg}"
  grep --color -iw 'fallito:' "${logspkg}"
fi
}

FOUND () {
if grep -qi 'not found' "${logspkg}" || grep -qi 'non trovato' "${logspkg}"; then
  TESTATA
  grep --color -i ' not found' "${logspkg}"
  grep --color -i ' non trovato' "${logspkg}"
  grep --color -i 'not found:' "${logspkg}"
  grep --color -i 'non trovato:' "${logspkg}"
fi
}

IMPOSSIBLE () {
if grep -qiw 'impossible' "${logspkg}" || grep -qiw 'impossibile' "${logspkg}"; then
  TESTATA
  grep --color -iw ' impossibile' "${logspkg}"
  grep --color -iw ' impossible' "${logspkg}"
  grep --color -iw 'impossibile:' "${logspkg}"
  grep --color -iw 'impossible:' "${logspkg}"
fi
}

WARNING () {
if grep -qiw 'warning' "${logspkg}"; then
  TESTATA
  grep --color -iw ' warning' "${logspkg}"
  grep --color -iw 'warning:' "${logspkg}"
fi
}

CK_LOG () {
for pkg in ${lpk}; do
  lck=""
  cke=""
  ckf=""
  ckn=""
  cki=""
  ckw=""
  logspkg="${LOG}/${pkg}.log"
  if [[ -e "${logspkg}" ]]; then
    clear
    if grep -iwq 'error' "${logspkg}" || grep -iwq 'errore' "${logspkg}"; then cke='1'; fi
    if grep -iwq 'fail' "${logspkg}" || grep -iwq 'failed' "${logspkg}" || grep -iwq 'fallito' "${logspkg}"; then ckf='1'; fi
    if grep -iq 'not found' "${logspkg}" || grep -iq 'non trovato' "${logspkg}"; then ckn='1'; fi
    if grep -iwq 'impossibile' "${logspkg}" || grep -iwq 'impossible' "${logspkg}"; then cki='1'; fi
    if grep -iwq 'warning' "${logspkg}"; then ckw='1'; fi
    lck="${cke}${ckf}${ckn}${cki}${ckw}"

    if [[ -n ${lck} ]]; then
      echo ""
      echo "--------------------------------------------------------------------------------"
      echo -e "   ${b_blu}Analsi file di log${z} ${b_cia}${pkg}${z}"
      echo "--------------------------------------------------------------------------------"
      echo ""
    fi
    if [[ ${cke} = '1' ]]; then
      echo -e "   ${b_blu}Ricerca per${z} ${b_ros}ERRORE${z}"
      echo "--------------------------------------------------------------------------------"
      grep --color -iw ' error' "${logspkg}"
      grep --color -iw ' errore' "${logspkg}"
      grep --color -iw 'error:' "${logspkg}"
      grep --color -iw 'errore:' "${logspkg}"
      echo ""
    fi
    if [[ ${ckf} = '1' ]]; then
      echo -e "   ${b_blu}Ricerca per${z} ${b_ros}FALLIMENTO${z}"
      echo "--------------------------------------------------------------------------------"
      grep --color -iw ' fail' "${logspkg}"
      grep --color -iw ' failed' "${logspkg}"
      grep --color -iw ' fallito' "${logspkg}"
      grep --color -iw 'fail:' "${logspkg}"
      grep --color -iw 'failed:' "${logspkg}"
      grep --color -iw 'fallito:' "${logspkg}"
      echo ""
    fi
    if [[ ${ckn} = '1' ]]; then
      echo -e "   ${b_blu}Ricerca per${z} ${b_ros}NON TROVATO${z}"
      echo "--------------------------------------------------------------------------------"
      grep --color -i ' not found' "${logspkg}"
      grep --color -i ' non trovato' "${logspkg}"
      grep --color -i 'not found:' "${logspkg}"
      grep --color -i 'non trovato:' "${logspkg}"
      echo ""
    fi
    if [[ ${cki} = '1' ]]; then
      echo -e "   ${b_blu}Ricerca per${z} ${b_ros}IMPOSSIBILE${z}"
      echo "--------------------------------------------------------------------------------"
      grep --color -iw ' impossibile' "${logspkg}"
      grep --color -iw ' impossible' "${logspkg}"
      grep --color -iw 'impossibile:' "${logspkg}"
      grep --color -iw 'impossible:' "${logspkg}"
      echo ""
    fi
    if [[ ${ckw} = '1' ]]; then
      echo -e "   ${b_blu}Ricerca per${z} ${b_ros}AVVERTIMENTI${z}"
      echo "--------------------------------------------------------------------------------"
      grep --color -iw ' warning' "${logspkg}"
      grep --color -iw 'warning:' "${logspkg}"
      echo ""
    fi
    if [[ -n ${lck} ]]; then
      read -r -s -n 1 conf
      if [[ "${conf}" = 'x' ]]; then exit; fi
    fi
  fi
done
}

CK_GOL () {
clear
echo ""
echo "--------------------------------------------------------------------------------"
echo -e "   ${b_blu}Analsi file di log per${z} ${b_cia}${titolo}${z}"
echo "--------------------------------------------------------------------------------"
echo ""
for pkg in ${lpk}; do
  logspkg="${LOG}/${pkg}.log"
  if [[ -e "${logspkg}" ]]; then ${SLOG}; fi
done
echo ""
if [[ ${opt} = '-o' ]]; then
  read -r -s -n 1 conf
  if [[ "${conf}" = 'x' ]]; then exit; fi
fi
}

if [[ ! -n "${SL}" ]]; then HELP && exit; fi

case ${opt} in
  -a ) lpk="${tlch} ${base}"; CK_LOG ;;
  -s ) lpk="${base}"; CK_LOG ;;
  -t ) lpk="${tlch}"; CK_LOG ;;
  -p ) if [[ "${tlch} ${base}" != *${pak}* ]]; then HELP && exit; else lpk="${pak}"; CK_LOG; fi ;;
  -e ) lpk="${tlch} ${base}"; SLOG="ERROR"; titolo='error'; CK_GOL ;;
  -k ) lpk="${tlch} ${base}"; SLOG="FAIL"; titolo='fail'; CK_GOL ;;
  -f ) lpk="${tlch} ${base}"; SLOG="FOUND"; titolo='not found'; CK_GOL ;;
  -i ) lpk="${tlch} ${base}"; SLOG="IMPOSSIBLE"; titolo='impossible'; CK_GOL ;;
  -w ) lpk="${tlch} ${base}"; SLOG="WARNING"; titolo='warnings'; CK_GOL ;;
  -o ) lpk="${tlch} ${base}"; SLOG="ERROR"; titolo='error'; CK_GOL
       lpk="${tlch} ${base}"; SLOG="FAIL"; titolo='fail'; CK_GOL
       lpk="${tlch} ${base}"; SLOG="FOUND"; titolo='not found'; CK_GOL
       lpk="${tlch} ${base}"; SLOG="IMPOSSIBLE"; titolo='impossible'; CK_GOL
       lpk="${tlch} ${base}"; SLOG="WARNING"; titolo='warnings'CK_GOL ;;
   * ) HELP && exit ;;
esac
