Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 11. General Utilities

     * [1]Prev
       Hd2u-1.0.4
     * [2]Next
       ibus-1.5.21
     * [3]Up
     * [4]Home

Highlight-3.55

Introduction to Highlight

   Highlight is an utility that converts source code to formatted text
   with syntax highlighting.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]http://www.andre-simon.de/zip/highlight-3.55.tar.bz2
     * Download MD5 sum: 33a0d901c8750bedfe9103e6ccaeec1e
     * Download size: 1.3 MB
     * Estimated disk space required: 22 MB (with gui)
     * Estimated build time: 0.3 SBU (Using paralllelism=4; with gui)

Highlight Dependencies

Required

   [6]Boost-1.72.0 and [7]Lua-5.3.5

Optional

   [8]Qt-5.14.1 (to build the GUI front-end)

   User Notes: [9]http://wiki.linuxfromscratch.org/blfs/wiki/highlight

Installation of Highlight

   For consistency, do not compress man pages.
sed -i '/GZIP/s/^/#/' makefile

   To build Highlight run the following command:
make

   To build the Qt5 GUI front-end, run the following command:
make gui

   This package does not come with a test suite.

   To install Highlight, run the following command as the root user:
make install

   To install the GUI program, run the following command as the root user:
make install-gui

Contents

   Installed Programs: highlight
   Installed Libraries: None
   Installed Directories: /etc/highlight, /usr/share/doc/highlight, and
   /usr/share/highlight

Short Descriptions

   highlight

   is a universal source code to formatted text converter.

   Last updated on 2020-02-17 12:03:00 -0800

     * [10]Prev
       Hd2u-1.0.4
     * [11]Next
       ibus-1.5.21
     * [12]Up
     * [13]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/hd2u.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/ibus.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genutils.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. http://www.andre-simon.de/zip/highlight-3.55.tar.bz2
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/boost.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lua.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/qt5.html
   9. http://wiki.linuxfromscratch.org/blfs/wiki/highlight
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/hd2u.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/ibus.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genutils.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
