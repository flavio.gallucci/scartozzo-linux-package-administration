Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 4. Security

     * [1]Prev
       GPGME-1.13.1
     * [2]Next
       iptables-1.8.4
     * [3]Up
     * [4]Home

Haveged-1.9.2

Introduction to Haveged

   The Haveged package contains a daemon that generates an unpredictable
   stream of random numbers and feeds the /dev/random device.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://downloads.sourceforge.net/haveged/haveged-1.9.2.tar.gz
     * Download MD5 sum: fb1d8b3dcbb9d06b30eccd8aa500fd31
     * Download size: 484 KB
     * Estimated disk space required: 20 MB (with tests)
     * Estimated build time: 0.1 SBU (with tests)

   User Notes: [6]http://wiki.linuxfromscratch.org/blfs/wiki/haveged

Installation of Haveged

   Install Haveged by running the following commands:
./configure --prefix=/usr &&
make

   To test the results, issue: make check.

   Now, as the root user:
make install &&
mkdir -pv    /usr/share/doc/haveged-1.9.2 &&
cp -v README /usr/share/doc/haveged-1.9.2

Configuring haveged

Boot Script

   If you want the Haveged daemon to start automatically when the system
   is booted, install the /etc/rc.d/init.d/haveged init script included in
   the [7]blfs-bootscripts-20191204 package (as the root user):
make install-haveged

Contents

   Installed Programs: haveged
   Installed Libraries: libhavege.so
   Installed Directory: /usr/include/haveged

Short Descriptions

   haveged

   is a daemon that generates an unpredictable stream of random numbers
   harvested from the indirect effects of hardware events based on hidden
   processor states (caches, branch predictors, memory translation tables,
   etc).

   Last updated on 2020-02-15 08:54:30 -0800

     * [8]Prev
       GPGME-1.13.1
     * [9]Next
       iptables-1.8.4
     * [10]Up
     * [11]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gpgme.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/iptables.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/security.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://downloads.sourceforge.net/haveged/haveged-1.9.2.tar.gz
   6. http://wiki.linuxfromscratch.org/blfs/wiki/haveged
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/introduction/bootscripts.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gpgme.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/iptables.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/security.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
