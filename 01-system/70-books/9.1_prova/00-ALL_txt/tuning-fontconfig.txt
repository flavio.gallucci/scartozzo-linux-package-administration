Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 24. X Window System Environment

     * [1]Prev
       Xorg-7 Testing and Configuration
     * [2]Next
       TTF and OTF fonts
     * [3]Up
     * [4]Home

Tuning Fontconfig

Overview of Fontconfig

   If you only read text in English, and are happy with the common libre
   fonts listed on the next page, you may never need to worry about the
   details of how fontconfig works. But there are many things which can be
   altered if they do not suit your needs.

   Although this page is long, it barely scratches the surface and you
   will be able to find many alternative views on the web (but please
   remember that some things have changed over the years, for example the
   autohinter is no longer the default). The aim here is to give you
   enough information to understand the changes you are making.

   User Notes: [5]http://wiki.linuxfromscratch.org/blfs/wiki/Fontconfig

The Xft Font Protocol

   The Xft font protocol provides antialiased font rendering through
   freetype, and fonts are controlled from the client side using
   fontconfig (except for [6]rxvt-unicode-9.22 which can use fonts listed
   in ~/.Xresources, and [7]AbiWord-3.0.4 which only uses the specified
   font). The default search path is /usr/share/fonts and
   ~/.local/share/fonts although for the moment the old and deprecated
   location ~/.fonts still works. Fontconfig searches directories in its
   path recursively and maintains a cache of the font characteristics in
   each directory. If the cache appears to be out of date, it is ignored,
   and information is fetched from the fonts themselves (that can take a
   few seconds if you installed a lot of fonts).

   If you've installed Xorg in any prefix other than /usr, any X fonts
   were not installed in a location known to Fontconfig. Symlinks were
   created from the OTF and TTF X font directories to
   /usr/share/fonts/X11-{OTF,TTF}. This allows Fontconfig to use the
   OpenType and TrueType fonts provided by X, although many people will
   prefer to use more modern fonts.

   Fontconfig uses names to define fonts. Applications generally use
   generic font names such as "Monospace", "Sans" and "Serif". Fontconfig
   resolves these names to a font that has all characters that cover the
   orthography of the language indicated by the locale settings.

Useful Commands

   The following commands may be helpful when working with fontconfig:

   fc-list | less : show a list of all available fonts (/path/to/filename:
   Font Name:style). If you installed a font more than 30 seconds ago but
   it does not show, then it or one of its directories is not readable by
   your user.

   fc-match 'Font Name' : will tell you which font will be used if the
   named font is requested. Typically you would use this to see what
   happens if a font you have not installed is requested, but you can also
   use it if the system is giving you a different font from what you
   expected (perhaps because fontconfig does not agree that the font
   supports your language).

   fc-match -a 'Type' | less : will provide a list of all fonts which can
   be used for that type (Monospace, Sans, Serif). Note that in-extremis
   fontconfig will take a glyph from any available font, even if it is not
   of the specified type, and unless it knows about the font's type it
   will assume it is Sans.

   If you wish to know which font will be used for a string of text (i.e.
   one or more glyphs, preceded by a space), paste the following command
   and replace the xyz by the text you care about:

   FC_DEBUG=4 pango-view --font=monospace -t xyz | grep family : this
   requires [8]Pango-1.44.7 and [9]ImageMagick-7.0.9-23 - it will invoke
   [10]display to show the text in a tiny window, and after closing that
   the last line of the output will show which font was chosen. This is
   particularly useful for CJK languages, and you can also pass a
   language, e.g. PANGO_LANGUAGE=en;ja (English, then assume Japanese) or
   just zh-cn (or other variants - 'zh' on its own is not valid).

The various files

   The main files are in /etc/fonts/conf.d/, which was intended to be a
   directory populated by symlinks to some of the files in
   /usr/share/fontconfig/conf.avail/. But many people, and some packages,
   create the files directly. Each file name must be in the form of two
   digits, a dash, somename.conf and they are read in sequence.

   By convention, the numbers are assigned as follows:
     * 00-09 extra font directories
     * 10-19 system rendering defaults (antialising etc)
     * 20-29 font rendering options
     * 30-39 family substitution
     * 40-49 map family to generic type
     * 50-59 load alternate config files
     * 60-69 generic aliases, map generic to family
     * 70-79 adjust which fonts are available
     * 80-89 match target scan (modify scanned patterns)
     * 90-99 font synthesis

   You can also have a personal fonts.conf in $XDG_CONFIG_HOME which is
   ~/.config/fontconfig/.

The rules to choose a font

   If the requested font is installed, and provided it contains the
   codepoints required for the current language (in the source, see the
   .orth files in the fc-lang/ directory), it will be used.

   But if the document or page requested a font which is not installed
   (or, occasionally, does not contain all the required codepoints) the
   following rules come into play: First, 30-metric-aliases.conf is used
   to map aliases for some fonts with the same metrics (same size, etc).
   After that, an unknown font will be searched for in 45-latin.conf - if
   it is found it will be mapped as Serif or Monospace or Sans, otherwise
   it will be assumed to be Sans. Then 50-latin.conf provides ordered
   lists of the fallbacks - [11]Dejavu fonts will be used if you installed
   them. Cyrillic and Greek appear to be treated in the same way. There
   are similar files with a 65- prefix for Persian and other non-latin
   writing systems. All of these files prefer commercial fonts if they are
   present, although modern libre fonts are often at least their equals.

   Since fontconfig-2.12.5 there is also generic family matching for some
   emoji and math fonts, see {45,60}-generic.conf.

   In the rare cases where a font does not contain all the expected
   codepoints, see 'Trial the First:' at [12]I stared into the fontconfig
   for the long details.

Hinting and Anti-aliasing

   It is possible to change how, or if, fonts are hinted. The following
   example file contains the default settings, but with comments. The
   settings are very much down to the user's preferences and to the choice
   of fonts, so a change which improves some pages may worsen others. The
   preferred location for this file is: ~/.config/fontconfig/fonts.conf

   To try out different settings, you may need to exit from Xorg and then
   rerun startx so that all applications use the new settings. And if you
   use Gnome or KDE their desktops can override these changes. To explore
   the possibilities, create a file for your user:
mkdir -pv ~/.config/fontconfig &&
cat > ~/.config/fontconfig/fonts.conf << "EOF"
<?xml version='1.0'?>
<!DOCTYPE fontconfig SYSTEM 'fonts.dtd'>
<fontconfig>

  <match target="font" >
    <!-- autohint was the old automatic hinter when hinting was patent
    protected, so turn it off to ensure any hinting information in the font
    itself is used, this is the default -->
    <edit mode="assign" name="autohint">  <bool>false</bool></edit>

    <!-- hinting is enabled by default -->
    <edit mode="assign" name="hinting">   <bool>true</bool></edit>

    <!-- for the lcdfilter see http://www.spasche.net/files/lcdfiltering/ -->
    <edit mode="assign" name="lcdfilter"> <const>lcddefault</const></edit>

    <!-- options for hintstyle:
    hintfull: is supposed to give a crisp font that aligns well to the
    character-cell grid but at the cost of its proper shape.

    hintmedium: poorly documented, maybe a synonym for hintfull.
    hintslight is the default: - supposed to be more fuzzy but retains shape.

    hintnone: seems to turn hinting off.
    The variations are marginal and results vary with different fonts -->
    <edit mode="assign" name="hintstyle"> <const>hintslight</const></edit>

    <!-- antialiasing is on by default and really helps for faint characters
    and also for 'xft:' fonts used in rxvt-unicode -->
    <edit mode="assign" name="antialias"> <bool>true</bool></edit>

    <!-- subpixels are usually rgb, see
    http://www.lagom.nl/lcd-test/subpixel.php -->
    <edit mode="assign" name="rgba">      <const>rgb</const></edit>

    <!-- thanks to the Arch wiki for the lcd and subpixel links -->
  </match>

</fontconfig>
EOF

   You will now need to edit the file in your preferred editor.

   For more examples see the blfs-support thread which started at
   [13]/2016-September/078422, particularly [14]2016-September/078425, and
   the original poster's preferred solution at [15]2016-November/078658.
   There are other examples in [16]Fontconfig in the Arch wiki and
   [17]Fontconfig in the Gentoo wiki.

Disabling Bitmap Fonts

   In previous versions of BLFS, the ugly old Xorg bitmap fonts were
   installed. Now, many people will not need to install any of them. But
   if for some reason you have installed one or more bitmap fonts, you can
   prevent them being used by fontconfig by creating the following file as
   the root user :
cat > /etc/fonts/conf.d/70-no-bitmaps.conf << "EOF"
<?xml version='1.0'?>
<!DOCTYPE fontconfig SYSTEM 'fonts.dtd'>
<fontconfig>
<!-- Reject bitmap fonts -->
 <selectfont>
  <rejectfont>
   <pattern>
     <patelt name="scalable"><bool>false</bool></patelt>
   </pattern>
  </rejectfont>
 </selectfont>
</fontconfig>
EOF

Adding extra font directories

   Normally, system fonts and user fonts are installed in directories
   beneath the locations specified in [18]The Xft Font Protocol and there
   is no obvious reason to put them elsewhere. However, a full BLFS
   install of [19]texlive-20190410 puts many fonts in
   /opt/texlive/2019/texmf-dist/fonts/ in the opentype/ and truetype/
   subdirectories. Although pulling in all of these files may appear
   useful (it allows you to use them in non TeX programs), there are
   several problems with such an approach:
    1. There are hundreds of files, which makes selecting the font hard.
    2. Some of the files do odd things, such as displaying semaphore flags
       instead of ASCII letters, or mapping cyrillic codepoints to
       character forms appropriate to Old Church Slavonic instead of the
       expected current shapes: fine if that is what you need, but painful
       for normal use.
    3. Several fonts have multiple sizes and impenetrable short names,
       which both make selecting the correct font even harder.
    4. When a font is added to CTAN, it is accompanied by TeX packages to
       use it in the old engines (xelatex does not normally need this),
       and then the version is often frozen whilst the font is separately
       maintained. Some of these fonts such as [20]Dejavu fonts are
       probably already installed on your BLFS system in a newer version,
       and if you have multiple versions of a font it is unclear which one
       will be used by fontconfig.

   However, it is sometimes useful to look at these fonts in non-TeX
   applications, if only to see whether you wish to install a current
   version. If you have installed all of texlive, the following example
   will make one of the Arkandis Open Type fonts available to other
   applications, and all three of the ParaType TrueType fonts. Adjust or
   repeat the lines as desired, to either make all the opentype/ or
   truetypefonts available, or to select different font directories. As
   the root user:
cat > /etc/fonts/conf.d/09-texlive.conf << "EOF"
<?xml version='1.0'?>
<!DOCTYPE fontconfig SYSTEM 'fonts.dtd'>
<fontconfig>
  <dir>/opt/texlive/2019/texmf-dist/fonts/opentype/arkandis/berenisadf</dir>
  <dir>/opt/texlive/2019/texmf-dist/fonts/truetype/paratype</dir>
</fontconfig>
EOF

   If you do this, remember to change all instances of the year in that
   file when you upgrade texlive to a later release.

Preferring certain fonts

   There are many reasons why people may wish to have pages which specify
   a particular font use a different font, or prefer specific fonts in
   Monospace or Sans or Serif. As you will expect, there a number of
   different ways of achieving this.

Fontconfig user docs

   Fontconfig installs user documentation that includes an example 'User
   configuration file' which among other things prefers [21]WenQuanYi
   ZenHei (a Sans font) if a Serif font is requested for Chinese (this
   part might be anachronistic unless you have non-free Chinese fonts,
   because in 65-nonlatin.conf this font is already among the preferred
   fonts when Serif is specified for Chinese) and to prefer the modern
   [22]VL Gothic font if a Sans font is specified on a Japanese page
   (otherwise a couple of other fonts would be preferred if they have been
   installed).

   If you have installed the current version, the user documentation is
   available in html, PDF and text versions at
   /usr/share/doc/fontconfig-2.13.1/ : change the version if you installed
   a different one.

Prefer a specific font

   As an example, if for some reason you wished to use the [23]Nimbus
   Roman No9 L font wherever Times New Roman is referenced (it is
   metrically similar, and preferred for Times Roman, but the Serif font
   from [24]Liberation fonts will be preferred for the Times New Roman
   font if installed), as an individual user you could install the font
   and then create the following file:
mkdir -pv ~/.config/fontconfig/conf.d &&
cat >  ~/.config/fontconfig/conf.d/35-prefer-nimbus-for-timesnew.conf << "EOF"
<?xml version='1.0'?>
<!DOCTYPE fontconfig SYSTEM 'fonts.dtd'>
<fontconfig>
<!-- prefer Nimbus Roman No9 L for Times New Roman as well as for Times,
 without this Tinos and Liberation Serif take precedence for Times New Roman
 before fontconfig falls back to whatever matches Times -->
    <alias binding="same">
        <family>Times New Roman</family>
        <accept>
            <family>Nimbus Roman No9 L</family>
        </accept>
    </alias>
</fontconfig>
EOF

   This is something you would normally do in an individual user's
   settings, but the file in this case has been prefixed '35-' so that it
   could, if desired, be used system-wide in /etc/fonts/conf.d/.

Prefer chosen CJK fonts

   The following example of a local configuration (i.e. one that applies
   for all users of the machine) does several things:
    1. If a Serif font is specified, it will prefer the [25]UMing
       variants, so that in the zh-cn, zh-hk and zh-tw languages things
       should look good (also zh-sg which actually uses the same settings
       as zh-cn) without affecting Japanese.
    2. It prefers the Japanese [26]IPAex fonts if they have been installed
       (although [27]VL Gothic will take precedence for (Japanese) Sans if
       it has also been installed.
    3. Because [28]WenQuanYi ZenHei covers Korean Hangul glyphs and is
       also preferred for Serif in 65-nonlatin.conf, if installed it will
       be used by default for Korean Serif. To get a proper Serif font,
       the UnBatang font is specified here - change that line if you
       installed a different Serif font from the choice of [29]Korean
       fonts.
    4. The Monospace fonts are forced to the preferred Sans fonts. If the
       text is in Korean then [30]WenQuanYi ZenHei will be used.

   In a non-CJK locale, the result is that suitable fonts will be used for
   all variants of Chinese, Japanese and Hangul Korean. All other
   languages should already work if a font is present. As the root user:
cat > /etc/fonts/local.conf << "EOF"
<?xml version='1.0'?>
<!DOCTYPE fontconfig SYSTEM 'fonts.dtd'>
<fontconfig>
    <alias>
        <family>serif</family>
        <prefer>
            <family>AR PL UMing</family>
            <family>IPAexMincho</family>
            <!-- WenQuanYi is preferred as Serif in 65-nonlatin.conf,
            override that so a real Korean font can be used for Serif -->
            <family>UnBatang</family>
        </prefer>
    </alias>
    <alias>
         <family>sans-serif</family>
         <prefer>
             <family>WenQuanYi Zen Hei</family>
             <family>VL Gothic</family>
             <family>IPAexGothic</family>
         </prefer>
    </alias>
    <alias>
         <family>monospace</family>
         <prefer>
             <family>VL Gothic</family>
             <family>IPAexGothic</family>
             <family>WenQuanYi Zen Hei</family>
         </prefer>
    </alias>
</fontconfig>
EOF

Editing Old-style conf files

   Some fonts, particularly Chinese fonts, ship with conf files which can
   be installed in /etc/fonts/conf.d. However, if you do that and then use
   a terminal to run any command which uses fontconfig you may see error
   messages such as :

   Fontconfig warning: "/etc/fonts/conf.d/69-odofonts.conf", line 14:
   Having multiple <family> in <alias> isn't supported and may not work as
   expected.

   In practice, these old rules do not work. For non-CJK users, fontconfig
   will usually do a good job without these rules. Their origin dates back
   to when CJK users needed handcrafted bitmaps to be legible at small
   sizes, and those looked ugly next to antialiased Latin glyphs - they
   preferred to use the same CJK font for the Latin glyphs. There is a
   side-effect of doing this : the (Serif) font is often also used for
   Sans, and in such a situation the (English) text in Gtk menus will use
   this font - compared to system fonts, as well as being serif it is both
   faint and rather small. That can make it uncomfortable to read.

   Nevertheless, these old conf files can be fixed if you wish to use
   them. The following example is the first part of 64-arphic-uming.conf
   from [31]UMing - there are a lot more similar items which also need
   changing :

      <match target="pattern">
          <test qual="any" name="lang" compare="contains">
              <string>zh-cn</string>
              <string>zh-sg</string>
          </test>
          <test qual="any" name="family">
              <string>serif</string>
          </test>
          <edit name="family" mode="prepend" binding="strong">
              <string>AR PL UMing CN</string>
          </edit>
       </match>

   The process to correct this is straightforward but tedious - for every
   item which produces an error message, using your editor (as the root
   user) edit the installed file to repeat the whole block as many times
   as there are multiple variables, then reduce each example to have only
   one of them. You may wish to work on one error at a time, save the file
   after each fix, and from a separate term run a command such as fc-list
   2>&1 | less to see that the fix worked. For the block above, the fixed
   version will be :

      <match target="pattern">
          <test qual="any" name="lang" compare="contains">
              <string>zh-cn</string>
          </test>
          <test qual="any" name="family">
              <string>serif</string>
          </test>
          <edit name="family" mode="prepend" binding="strong">
              <string>AR PL UMing CN</string>
          </edit>
       </match>
      <match target="pattern">
          <test qual="any" name="lang" compare="contains">
              <string>zh-sg</string>
          </test>
          <test qual="any" name="family">
              <string>serif</string>
          </test>
          <edit name="family" mode="prepend" binding="strong">
              <string>AR PL UMing CN</string>
          </edit>
       </match>

See Also

I stared into the fontconfig ...

   The blog entries by [32]Eevee are particularly useful if fontconfig
   does not think your chosen font supports your language, and for
   preferring some non-MS Japanese fonts when an ugly MS font is already
   installed.

Fontconfig in the Arch wiki

   Arch has a lot of information in its wiki at [33]font_configuration.

Fontconfig in the Gentoo wiki

   Gentoo has some information in its wiki at [34]Fontconfig although a
   lot of the details (what to enable, and Infinality) are specific to
   Gentoo.

   Last updated on 2018-08-09 14:49:29 -0700

     * [35]Prev
       Xorg-7 Testing and Configuration
     * [36]Next
       TTF and OTF fonts
     * [37]Up
     * [38]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/xorg-config.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/TTF-and-OTF-fonts.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/installing.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. http://wiki.linuxfromscratch.org/blfs/wiki/Fontconfig
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/xsoft/rxvt-unicode.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/xsoft/AbiWord.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/pango.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/imagemagick.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/imagemagick.html#display
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/TTF-and-OTF-fonts.html#dejavu-fonts
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/tuning-fontconfig.html#I-stared-into-the-fontconfig
  13. http://lists.linuxfromscratch.org/pipermail/blfs-support/2016-September/078422.html
  14. http://lists.linuxfromscratch.org/pipermail/blfs-support/2016-September/078425.html
  15. http://lists.linuxfromscratch.org/pipermail/blfs-support/2016-November/078658.html
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/tuning-fontconfig.html#arch-fontconfig
  17. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/tuning-fontconfig.html#gentoo-fontconfig
  18. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/tuning-fontconfig.html#xft-font-protocol
  19. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/pst/texlive.html
  20. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/TTF-and-OTF-fonts.html#dejavu-fonts
  21. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/TTF-and-OTF-fonts.html#wenquanyi-zenhei
  22. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/TTF-and-OTF-fonts.html#VLGothic
  23. https://www.fontsquirrel.com/fonts/nimbus-roman-no9-l
  24. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/TTF-and-OTF-fonts.html#liberation-fonts
  25. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/TTF-and-OTF-fonts.html#UMing
  26. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/TTF-and-OTF-fonts.html#IPAex
  27. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/TTF-and-OTF-fonts.html#VLGothic
  28. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/TTF-and-OTF-fonts.html#wenquanyi-zenhei
  29. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/TTF-and-OTF-fonts.html#Korean-fonts
  30. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/TTF-and-OTF-fonts.html#wenquanyi-zenhei
  31. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/TTF-and-OTF-fonts.html#UMing
  32. https://eev.ee/blog/2015/05/20/i-stared-into-the-fontconfig-and-the-fontconfig-stared-back-at-me/
  33. https://wiki.archlinux.org/index.php/font_configuration
  34. https://wiki.gentoo.org/wiki/Fontconfig
  35. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/xorg-config.html
  36. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/TTF-and-OTF-fonts.html
  37. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/installing.html
  38. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
