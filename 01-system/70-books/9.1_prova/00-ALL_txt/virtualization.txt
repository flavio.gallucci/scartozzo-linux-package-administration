Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Part II. Post LFS Configuration and Extra Software

     * [1]Prev
       zsh-5.8
     * [2]Next
       qemu-4.2.0
     * [3]Up
     * [4]Home

Virtualization

   Virtualization allows running a complete operating system, or virtual
   machine (VM), within another operating environment as a task. There are
   several commercial and open source environments that either emulate
   another processor or utilize the hardware virtualization features of
   the host processor.

Table of Contents

     * [5]qemu-4.2.0

     * [6]Prev
       zsh-5.8
     * [7]Next
       qemu-4.2.0
     * [8]Up
     * [9]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/zsh.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/qemu.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/postlfs.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/qemu.html
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/zsh.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/qemu.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/postlfs.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
