Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 44. Video Utilities

     * [1]Prev
       MPlayer-1.4
     * [2]Next
       VLC-3.0.8
     * [3]Up
     * [4]Home

Transcode-1.1.7

Introduction to Transcode

   Transcode was a fast, versatile and command-line based audio/video
   everything to everything converter primarily focussed on producing AVI
   video files with MP3 audio, but also including a program to read all
   the video and audio streams from a DVD.

   Although outdated and no longer maintained, it can still be used to
   extract items from a DVD using only the required and recommended
   dependencies, or to recode to AVI files.

   Subjects to the comments below, this package is known to build and
   minimally work using an LFS-8.3 platform.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://sources.archlinux.org/other/community/transcode/transcod
       e-1.1.7.tar.bz2
     * Download (FTP):
       [6]ftp://ftp.mirrorservice.org/sites/distfiles.gentoo.org/distfiles
       /transcode-1.1.7.tar.bz2
     * Download MD5 sum: 9bb25a796a8591fb764de46ee87ce505
     * Download size: 2.1 MB
     * Estimated disk space required: 66 MB
     * Estimated build time: 0.6 SBU

Additional Downloads

     * Required patch:
       [7]http://www.linuxfromscratch.org/patches/blfs/9.1/transcode-1.1.7
       -ffmpeg4-1.patch

Transcode Dependencies

Required

   [8]FFmpeg-4.2.2 (configured with --enable-avresample)

Recommended

   [9]alsa-lib-1.2.1.2, [10]LAME-3.100, [11]libdvdread-6.0.2,
   [12]libmpeg2-0.5.1, and [13]Xorg Libraries

Optional

   [14]FAAC-1.29.9.2, [15]FreeType-2.10.1, [16]ImageMagick-6.9.10-93
   libraries, [17]liba52-0.7.4, [18]libdv-1.0.0, [19]libjpeg-turbo-2.0.4,
   [20]libogg-1.3.4, [21]libquicktime-1.2.4, [22]libtheora-1.1.1,
   [23]libvorbis-1.3.6, [24]libxml2-2.9.10, [25]LZO-2.10, [26]SDL-1.2.15,
   [27]v4l-utils-1.18.0, [28]x264-20200218, [29]XviD-1.3.7 [30]MJPEG
   Tools, and [31]PVM3,

Optional (at runtime)

   [32]lsdvd

   User Notes: [33]http://wiki.linuxfromscratch.org/blfs/wiki/transcode

Installation of Transcode

   [Note]

Note

   The details of how the FFmpeg libraries are used has changed since this
   version of Transcode was released. The patch allows the package to be
   compiled, but some or all of the internal calls to FFmpeg may fail at
   run time (they report an error and processing continues, but without
   any output).

   For many packages, that would be a critical error. In this case, the
   main reason to install Transcode is for the tccat program, which works.
   Some of the transcode options work - for the others, use ffmpeg
   directly on the command line.
   [Note]

Note

   Support for most of the dependency packages requires using options
   passed to the configure script. View the INSTALL file and the output
   from ./configure --help for complete information about enabling
   dependency packages.

   Install Transcode by running the following commands:
sed -i 's|doc/transcode|&-$(PACKAGE_VERSION)|' \
       $(find . -name Makefile.in -exec grep -l 'docsdir =' {} \;) &&

patch -Np1 -i ../transcode-1.1.7-ffmpeg4-1.patch                   &&
./configure --prefix=/usr \
            --enable-alsa \
            --enable-libmpeg2 &&
make

   This package does not come with a test suite.

   Now, as the root user:
make install

Command Explanations

   sed -i ...: Fixes install location for documentation.

Contents

   Installed Programs: avifix, aviindex, avimerge, avisplit, avisync,
   tccat, tcdecode, tcdemux, tcextract, tcmodinfo, tcmp3cut, tcprobe,
   tcscan, tcxmlcheck, tcxpm2rgb, tcyait, and transcode
   Installed Libraries: None
   Installed Directories: /usr/lib/transcode and
   /usr/share/doc/transcode-1.1.7

Short Descriptions

   avifix

   fixes the header of an AVI file.

   aviindex

   writes a text file describing the index of an AVI file.

   avimerge

   merges AVI files of the same format. Do not try to merge AVI files of
   different formats, it will most likely result in errors (and format
   means same bitrates, too!).

   avisplit

   splits AVI files into multiple files.

   avisync

   can shift audio in AVI files for better synchronizing of the audio and
   video data signal.

   tccat

   concatenates input files using the input plugins of Transcode. This is
   useful for extracting VOB (Video OBject) files, either for a whole DVD
   or for selected Titles or Chapters, which can then be played or
   recoded.

   tcdecode

   is used to decode input files to raw video and PCM audio streams.

   tcdemux

   demultiplexes (separates) audio/video input that contains multiple
   streams, e.g., VOB files.

   tcextract

   grabs single streams from a file containing multiple streams.

   tcmodinfo

   loads a supplied Transcode filter module and prints its parameters.

   tcmp3cut

   is a tool which can cut MP3 streams at milliseconds positions.

   tcprobe

   prints information about the input file format.

   tcscan

   performs several measurements on the given input data.

   tcxmlcheck

   checks information in a SMIL input file.

   transcode

   is the encoder's user interface that handles the plugins and other
   programs, being the glue between the modules. There are several well
   documented usage examples on both the homepage and the documentation
   included in the package.

   Last updated on 2020-02-20 12:41:28 -0800

     * [34]Prev
       MPlayer-1.4
     * [35]Next
       VLC-3.0.8
     * [36]Up
     * [37]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/mplayer.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/vlc.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/videoutils.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://sources.archlinux.org/other/community/transcode/transcode-1.1.7.tar.bz2
   6. ftp://ftp.mirrorservice.org/sites/distfiles.gentoo.org/distfiles/transcode-1.1.7.tar.bz2
   7. http://www.linuxfromscratch.org/patches/blfs/9.1/transcode-1.1.7-ffmpeg4-1.patch
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/ffmpeg.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/alsa-lib.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lame.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libdvdread.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libmpeg2.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/x7lib.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/faac.html
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/freetype2.html
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/imagemagick6.html
  17. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/liba52.html
  18. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libdv.html
  19. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/libjpeg.html
  20. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libogg.html
  21. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libquicktime.html
  22. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libtheora.html
  23. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libvorbis.html
  24. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/libxml2.html
  25. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/lzo.html
  26. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/sdl.html
  27. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/v4l-utils.html
  28. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/x264.html
  29. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/xvid.html
  30. http://sourceforge.net/projects/mjpeg/
  31. http://www.netlib.org/pvm3/
  32. https://sourceforge.net/projects/lsdvd/files/
  33. http://wiki.linuxfromscratch.org/blfs/wiki/transcode
  34. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/mplayer.html
  35. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/vlc.html
  36. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/videoutils.html
  37. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
