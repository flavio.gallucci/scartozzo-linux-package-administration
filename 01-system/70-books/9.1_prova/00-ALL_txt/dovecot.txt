Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 21. Mail Server Software

     * [1]Prev
       Mail Server Software
     * [2]Next
       Exim-4.93
     * [3]Up
     * [4]Home

Dovecot-2.3.9.3

Introduction to Dovecot

   Dovecot is an Internet Message Access Protocol (IMAP) and Post Office
   Protocol (POP) server, written primarily with security in mind. Dovecot
   aims to be lightweight, fast and easy to set up as well as highly
   configurable and easily extensible with plugins.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://www.dovecot.org/releases/2.3/dovecot-2.3.9.3.tar.gz
     * Download MD5 sum: fbbf4e94ccfd94d910c1ff14c9330f57
     * Download size: 6.9 MB
     * Estimated disk space required: 181 MB
     * Estimated build time: 3.9 SBU

Dovecot Dependencies

Required

   [6]libtirpc-1.2.5

Optional

   [7]CLucene-2.3.3.4, [8]ICU-65.1, [9]libcap-2.31 with PAM,
   [10]Linux-PAM-1.3.1, [11]Lua-5.3.5, [12]MariaDB-10.4.12 or [13]MySQL,
   [14]OpenLDAP-2.4.49, [15]PostgreSQL-12.2, [16]SQLite-3.31.1,
   [17]Valgrind-3.15.0, [18]xfsprogs-5.4.0, [19]Cassandra, [20]lz4,
   [21]stemmer and [22]libsodium

   User Notes: [23]http://wiki.linuxfromscratch.org/blfs/wiki/dovecot

Installation of Dovecot

   There should be dedicated users and groups for unprivileged Dovecot
   processes and for processing users' logins. Issue the following
   commands as the root user:
groupadd -g 42 dovecot &&
useradd -c "Dovecot unprivileged user" -d /dev/null -u 42 \
        -g dovecot -s /bin/false dovecot &&
groupadd -g 43 dovenull &&
useradd -c "Dovecot login user" -d /dev/null -u 43 \
        -g dovenull -s /bin/false dovenull

   First, fix an issue causing a segfault in the test suite by applying a
   sed:
sed -e "s;#include <unistd.h>;&\n#include <crypt.h>;" \
    -i src/auth/mycrypt.c

   Install Dovecot by running the following commands:
CFLAGS+=" -I/usr/include/tirpc" \
LDFLAGS+=" -ltirpc" \
./configure --prefix=/usr                          \
            --sysconfdir=/etc                      \
            --localstatedir=/var                   \
            --docdir=/usr/share/doc/dovecot-2.3.9.3 \
            --disable-static &&
make

   To test the results, issue make -k check.

   Now, as the root user:
make install

Command Explanations

   CFLAGS+=...LDFLAGS+=...: build with libtirpc instead of the recently
   removed RPC code provided by GlibC.

   --disable-static: This switch prevents installation of static versions
   of the libraries.

   --with-ldap: This switch enables OpenLDAP authentication support.

   --with-pgsql: This switch enables PostgreSQL authentication support.

   --with-mysql: This switch enables MySQL authentication support.

   --with-sqlite: This switch enables SQLite authentication support.

   --with-lucene: This switch enables CLucene full text search support.

Configuring Dovecot

Config Files

   /etc/dovecot/dovecot.conf, /etc/dovecot/conf.d/*, and
   /etc/dovecot/local.conf

Configuration Information

   Copy an example configuration, which you can use as a starting point:
cp -rv /usr/share/doc/dovecot-2.3.9.3/example-config/* /etc/dovecot

   The following configuration is a simple proof of concept with IMAP
   service using local users for authentication and mailbox location.
   Reading files from the conf.d directory is commented out since the
   included example configuration requires OpenSSL and Linux PAM.
sed -i '/^\!include / s/^/#/' /etc/dovecot/dovecot.conf &&
chmod -v 1777 /var/mail &&
cat > /etc/dovecot/local.conf << "EOF"
protocols = imap
ssl = no
# The next line is only needed if you have no IPv6 network interfaces
listen = *
mail_location = mbox:~/Mail:INBOX=/var/mail/%u
userdb {
  driver = passwd
}
passdb {
  driver = shadow
}
EOF

   You will definitely want to read the official documentation at
   [24]http://wiki2.dovecot.org/ if you plan to use Dovecot in production
   environment.

Boot Script

   If you want the Dovecot server to start automatically when the system
   is booted, install the /etc/rc.d/init.d/dovecot init script included in
   the [25]blfs-bootscripts-20191204 package.
make install-dovecot

Contents

   Installed Programs: doveadm, doveconf, dovecot, and dsync (symbolic
   link)
   Installed Libraries: various internal plugins in /usr/lib/dovecot
   Installed Directories: /etc/dovecot,
   /usr/{include,lib,libexec,share}/dovecot and
   /usr/share/doc/dovecot-2.3.9.3

Short Descriptions

   doveadm

   is the Dovecot administration tool.

   doveconf

   is Dovecot's configuration dumping utility.

   dovecot

   is the IMAP and POP server.

   dsync

   is Dovecot's mailbox synchronization utility.

   Last updated on 2020-02-27 12:58:21 -0800

     * [26]Prev
       Mail Server Software
     * [27]Next
       Exim-4.93
     * [28]Up
     * [29]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/mail.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/exim.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/mail.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://www.dovecot.org/releases/2.3/dovecot-2.3.9.3.tar.gz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/basicnet/libtirpc.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/clucene.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/icu.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/libcap.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/linux-pam.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/lua.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/mariadb.html
  13. http://www.mysql.com/
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/openldap.html
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/postgresql.html
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/sqlite.html
  17. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/valgrind.html
  18. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/xfsprogs.html
  19. https://cassandra.apache.org/
  20. https://github.com/Cyan4973/lz4
  21. https://github.com/shibukawa/snowball_py
  22. https://libsodium.gitbook.io/doc/
  23. http://wiki.linuxfromscratch.org/blfs/wiki/dovecot
  24. http://wiki2.dovecot.org/
  25. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/introduction/bootscripts.html
  26. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/mail.html
  27. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/exim.html
  28. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/mail.html
  29. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
