Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 13. Programming

     * [1]Prev
       Running a Subversion Server
     * [2]Next
       Tcl-8.6.10
     * [3]Up
     * [4]Home

SWIG-4.0.1

Introduction to SWIG

   SWIG (Simplified Wrapper and Interface Generator) is a compiler that
   integrates C and C++ with languages including Perl, Python, Tcl, Ruby,
   PHP, Java, C#, D, Go, Lua, Octave, R, Scheme, and Ocaml. SWIG can also
   export its parse tree into Lisp s-expressions and XML.

   SWIG reads annotated C/C++ header files and creates wrapper code (glue
   code) in order to make the corresponding C/C++ libraries available to
   the listed languages, or to extend C/C++ programs with a scripting
   language.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://downloads.sourceforge.net/swig/swig-4.0.1.tar.gz
     * Download MD5 sum: 54cc40b3804816f7d38ab510b6f13b04
     * Download size: 7.7 MB
     * Estimated disk space required: 181 MB (1.2 GB with tests)
     * Estimated build time: 0.2 SBU (add 9.4 SBU for tests; both using
       parallelism=4)

SWIG Dependencies

Required

   [6]PCRE-8.44

Optional

   [7]Boost-1.72.0 for tests, and any of the languages mentioned in the
   introduction, as run-time dependencies

   User Notes: [8]http://wiki.linuxfromscratch.org/blfs/wiki/swig

Installation of SWIG

   Install SWIG by running the following commands:
./configure --prefix=/usr \
            --without-maximum-compile-warnings &&
make

   To test the results, issue: make -k check TCL_INCLUDE=. The unsetting
   of the variable TCL_INCLUDE is necessary since it is not correctly set
   by configure. The tests are only executed for the languages installed
   on your machine, so the disk space and SBU values given for the tests
   may vary, and should be considered as mere orders of magnitude. If you
   have [9]Python-2.7.17 installed, the Python-3 tests are not run. You
   can run tests for Python-3 by issuing PY3=1 make check-python-examples
   followed by PY3=1 make check-python-test-suite. According to SWIG's
   documentation, the failure of some tests should not be considered
   harmful.

   Now, as the root user:
make install &&
install -v -m755 -d /usr/share/doc/swig-4.0.1 &&
cp -v -R Doc/* /usr/share/doc/swig-4.0.1

Command Explanations

   --without-maximum-compile-warnings: disables compiler ansi conformance
   enforcement, which triggers errors in the Lua headers (starting with
   Lua 5.3).

   --without-<language>: allows disabling the building of tests and
   examples for <language>, but all the languages capabilities of SWIG are
   always built.

Contents

   Installed Programs: swig and ccache-swig
   Installed Library: None
   Installed Directories: /usr/share/doc/swig-4.0.1 and /usr/share/swig

Short Descriptions

   swig

   takes an interface file containing C/C++ declarations and SWIG special
   instructions, and generates the corresponding wrapper code needed to
   build extension modules.

   ccache-swig

   is a compiler cache, which speeds up re-compilation of C/C++/SWIG code.

   Last updated on 2020-02-17 12:12:55 -0800

     * [10]Prev
       Running a Subversion Server
     * [11]Next
       Tcl-8.6.10
     * [12]Up
     * [13]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/svnserver.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/tcl.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/prog.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://downloads.sourceforge.net/swig/swig-4.0.1.tar.gz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/pcre.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/boost.html
   8. http://wiki.linuxfromscratch.org/blfs/wiki/swig
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/python2.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/svnserver.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/tcl.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/prog.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
