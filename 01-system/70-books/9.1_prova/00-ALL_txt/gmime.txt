Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 9. General Libraries

     * [1]Prev
       GLibmm-2.62.0
     * [2]Next
       GMime-3.2.6
     * [3]Up
     * [4]Home

GMime-2.6.23

Introduction to GMime

   The GMime package contains a set of utilities for parsing and creating
   messages using the Multipurpose Internet Mail Extension (MIME) as
   defined by the applicable RFCs. See the [5]GMime web site for the RFCs
   resourced. This is useful as it provides an API which adheres to the
   MIME specification as closely as possible while also providing
   programmers with an extremely easy to use interface to the API
   functions.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [6]http://ftp.gnome.org/pub/gnome/sources/gmime/2.6/gmime-2.6.23.ta
       r.xz
     * Download (FTP):
       [7]ftp://ftp.gnome.org/pub/gnome/sources/gmime/2.6/gmime-2.6.23.tar
       .xz
     * Download MD5 sum: 247072236d84bd0fbbff299d69bdf333
     * Download size: 5.0 MB
     * Estimated disk space required: 28 MB (with tests)
     * Estimated build time: 0.5 SBU (with tests)

GMime Dependencies

Required

   [8]GLib-2.62.4 and [9]libgpg-error-1.37

Recommended

   [10]gobject-introspection-1.62.0 and [11]Vala-0.46.6

Optional

   [12]DocBook-utils-0.6.14, [13]GPGME-1.13.1, [14]GTK-Doc-1.32 and
   [15]Gtk# (requires [16]Mono)

   User Notes: [17]http://wiki.linuxfromscratch.org/blfs/wiki/gmime

Installation of GMime

   Install GMime by running the following commands:
./configure --prefix=/usr --disable-static &&
make

   To test the results, issue: make check.

   Now, as the root user:
make install

Command Explanations

   --disable-static: This switch prevents installation of static versions
   of the libraries.

   --enable-smime: Use this switch if you have installed [18]GPGME-1.13.1
   and wish to enable S/MIME support in GMime.

   --enable-gtk-doc: Use this parameter if GTK-Doc is installed and you
   wish to rebuild and install the API documentation.

Contents

   Installed Programs: None
   Installed Library: libgmime-2.6.so
   Installed Directories: /usr/include/gmime-2.6 and
   /usr/share/gtk-doc/html/gmime-2.6

Short Descriptions

   libgmime-2.6.so

   contains API functions used by programs that need to comply to the MIME
   standards.

   Last updated on 2020-02-16 18:46:23 -0800

     * [19]Prev
       GLibmm-2.62.0
     * [20]Next
       GMime-3.2.6
     * [21]Up
     * [22]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/glibmm.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gmime3.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. http://spruce.sourceforge.net/gmime/
   6. http://ftp.gnome.org/pub/gnome/sources/gmime/2.6/gmime-2.6.23.tar.xz
   7. ftp://ftp.gnome.org/pub/gnome/sources/gmime/2.6/gmime-2.6.23.tar.xz
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/glib2.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libgpg-error.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gobject-introspection.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/vala.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/pst/docbook-utils.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/gpgme.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gtk-doc.html
  15. http://download.mono-project.com/sources/gtk-sharp/
  16. http://www.mono-project.com/Main_Page
  17. http://wiki.linuxfromscratch.org/blfs/wiki/gmime
  18. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/gpgme.html
  19. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/glibmm.html
  20. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gmime3.html
  21. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
  22. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
