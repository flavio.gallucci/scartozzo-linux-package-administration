Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 5. File Systems and Disk Management

     * [1]Prev
       sshfs-3.7.0
     * [2]Next
       Editors
     * [3]Up
     * [4]Home

xfsprogs-5.4.0

Introduction to xfsprogs

   The xfsprogs package contains administration and debugging tools for
   the XFS file system.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://www.kernel.org/pub/linux/utils/fs/xfs/xfsprogs/xfsprogs-
       5.4.0.tar.xz
     * Download MD5 sum: 61232b1cc453780517d9b0c12ff1699b
     * Download size: 1.2 MB
     * Estimated disk space required: 64 MB
     * Estimated build time: 0.3 SBU (Using parallelism=4)

   User Notes: [6]http://wiki.linuxfromscratch.org/blfs/wiki/xfs

Kernel Configuration

   Enable the following options in the kernel configuration and recompile
   the kernel:
File systems --->
  <*/M> XFS filesystem support [CONFIG_XFS_FS]

Installation of xfsprogs

   Install xfsprogs by running the following commands:
make DEBUG=-DNDEBUG     \
     INSTALL_USER=root  \
     INSTALL_GROUP=root \
     LOCAL_CONFIGURE_OPTIONS="--enable-readline"

   This package does not come with a test suite.

   Now, as the root user:
make PKG_DOC_DIR=/usr/share/doc/xfsprogs-5.4.0 install     &&
make PKG_DOC_DIR=/usr/share/doc/xfsprogs-5.4.0 install-dev &&

rm -rfv /usr/lib/libhandle.a                                &&
rm -rfv /lib/libhandle.{a,la,so}                            &&
ln -sfv ../../lib/libhandle.so.1 /usr/lib/libhandle.so      &&
sed -i "s@libdir='/lib@libdir='/usr/lib@" /usr/lib/libhandle.la

Command Explanations

   make DEBUG=-DNDEBUG: Turns off debugging symbols.

   INSTALL_USER=root INSTALL_GROUP=root: This sets the owner and group of
   the installed files.

   LOCAL_CONFIGURE_OPTIONS="...": This passes extra configuration options
   to the configure script. The example --enable-readline parameter
   enables linking the XFS programs with the libreadline.so library, in
   order to allow editing interactive commands.

   OPTIMIZER="...": Adding this parameter to the end of the make command
   overrides the default optimization settings.

Contents

   Installed Programs: fsck.xfs, mkfs.xfs, xfs_admin, xfs_bmap, xfs_copy,
   xfs_db, xfs_estimate, xfs_freeze, xfs_fsr, xfs_growfs, xfs_info,
   xfs_io, xfs_logprint, xfs_mdrestore, xfs_metadump, xfs_mkfile,
   xfs_ncheck, xfs_quota, xfs_repair, xfs_rtcp, and xfs_spaceman
   Installed Libraries: libhandle.so
   Installed Directories: /usr/include/xfs and
   /usr/share/doc/xfsprogs-5.4.0

Short Descriptions

   fsck.xfs

   simply exits with a zero status, since XFS partitions are checked at
   mount time.

   mkfs.xfs

   constructs an XFS file system.

   xfs_admin

   changes the parameters of an XFS file system.

   xfs_bmap

   prints block mapping for an XFS file.

   xfs_copy

   copies the contents of an XFS file system to one or more targets in
   parallel.

   xfs_estimate

   for each directory argument, estimates the space that directory would
   take if it were copied to an XFS filesystem (does not cross mount
   points).

   xfs_db

   is used to debug an XFS file system.

   xfs_freeze

   suspends access to an XFS file system.

   xfs_fsr

   applicable only to XFS filesystems, improves the organization of
   mounted filesystems, the reorganization algorithm operates on one file
   at a time, compacting or othewise improving the layout of the file
   extents (contiguous blocks of file data).

   xfs_growfs

   expands an XFS file system.

   xfs_info

   is equivalent to invoking xfs_growfs, but specifying that no change to
   the file system is to be made.

   xfs_io

   is a debugging tool like xfs_db, but is aimed at examining the regular
   file I/O path rather than the raw XFS volume itself.

   xfs_logprint

   prints the log of an XFS file system.

   xfs_mdrestore

   restores an XFS metadump image to a filesystem image.

   xfs_metadump

   copies XFS filesystem metadata to a file.

   xfs_mkfile

   creates an XFS file, padded with zeroes by default.

   xfs_ncheck

   generates pathnames from inode numbers for an XFS file system.

   xfs_quota

   is a utility for reporting and editing various aspects of filesystem
   quota.

   xfs_repair

   repairs corrupt or damaged XFS file systems.

   xfs_rtcp

   copies a file to the real-time partition on an XFS file system.

   xfs_spaceman

   reports and controls free space usage in an XFS file system.

   libhandle.so

   contains XFS-specific functions that provide a way to perform certain
   filesystem operations without using a file descriptor to access
   filesystem objects.

   Last updated on 2020-02-18 14:50:03 -0800

     * [7]Prev
       sshfs-3.7.0
     * [8]Next
       Editors
     * [9]Up
     * [10]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/sshfs.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/editors.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/filesystems.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://www.kernel.org/pub/linux/utils/fs/xfs/xfsprogs/xfsprogs-5.4.0.tar.xz
   6. http://wiki.linuxfromscratch.org/blfs/wiki/xfs
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/sshfs.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/editors.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/filesystems.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
