Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 9. General Libraries

     * [1]Prev
       GMime-3.2.6
     * [2]Next
       Grantlee-5.2.0
     * [3]Up
     * [4]Home

gobject-introspection-1.62.0

Introduction to GObject Introspection

   The GObject Introspection is used to describe the program APIs and
   collect them in a uniform, machine readable format.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]http://ftp.gnome.org/pub/gnome/sources/gobject-introspection/1.6
       2/gobject-introspection-1.62.0.tar.xz
     * Download (FTP):
       [6]ftp://ftp.gnome.org/pub/gnome/sources/gobject-introspection/1.62
       /gobject-introspection-1.62.0.tar.xz
     * Download MD5 sum: 37278eab3704e42234b6080b8cf241f1
     * Download size: 950 KB
     * Estimated disk space required: 40 MB (with tests)
     * Estimated build time: 0.2 SBU (Using parallelism=4; with tests)

Required

   [7]GLib-2.62.4

Optional

   [8]Cairo-1.17.2+f93fc72c03e (required for the tests), [9]Gjs-1.58.5 (to
   satisfy one test), [10]GTK-Doc-1.32, [11]Mako-1.1.1, and [12]Markdown
   (to satisfy one test)

   User Notes:
   [13]http://wiki.linuxfromscratch.org/blfs/wiki/gobject-introspection

Installation of GObject Introspection

   Install GObject Introspection by running the following commands:
mkdir build &&
cd    build &&

meson --prefix=/usr .. &&
ninja

   To test the results, issue: ninja test -k0. One test (test_docwriter)
   fails if the optional Markdown module is not installed.

   Now, as the root user:
ninja install

Command Explanations

   -Dgtk_doc=true: Build and install the documentation.

   -Dcairo=true: Use cairo for tests.

   -Ddoctool=true: Install g-ir-doc-tool and run related tests.

Contents

   Installed Program: g-ir-annotation-tool, g-ir-compiler, g-ir-doc-tool,
   g-ir-inspect, g-ir-generate, and g-ir-scanner
   Installed Libraries: libgirepository-1.0.so and
   _giscanner.cpython-37m-x86_64-linux-gnu.so
   Installed Directories: /usr/include/gobject-introspection-1.0,
   /usr/lib/girepository-1.0, /usr/lib/gobject-introspection,
   /usr/share/gir-1.0, and /usr/share/gobject-introspection-1.0

Short Descriptions

   g-ir-annotation-tool

   creates or extracts annotation data from GI typelibs.

   g-ir-compiler

   converts one or more GIR files into one or more typelib.

   g-ir-doc-tool

   generates Mallard files that can be viewed with yelp or rendered to
   HTML with yelp-build from [14]yelp-tools.

   g-ir-inspect

   is a utility that gives information about a GI typelib.

   g-ir-scanner

   is a tool which generates GIR XML files by parsing headers and
   introspecting GObject based libraries.

   g-ir-generate

   is a GIR generator that uses the repository API.

   libgirepository-1.0.so

   provides an API to access the typelib metadata.

   Last updated on 2020-02-15 20:23:35 -0800

     * [15]Prev
       GMime-3.2.6
     * [16]Next
       Grantlee-5.2.0
     * [17]Up
     * [18]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gmime3.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/grantlee.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. http://ftp.gnome.org/pub/gnome/sources/gobject-introspection/1.62/gobject-introspection-1.62.0.tar.xz
   6. ftp://ftp.gnome.org/pub/gnome/sources/gobject-introspection/1.62/gobject-introspection-1.62.0.tar.xz
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/glib2.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/cairo.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/gnome/gjs.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gtk-doc.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/python-modules.html#Mako
  12. https://pypi.org/project/Markdown/
  13. http://wiki.linuxfromscratch.org/blfs/wiki/gobject-introspection
  14. http://ftp.acc.umu.se/pub/gnome/sources/yelp-tools
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gmime3.html
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/grantlee.html
  17. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
  18. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
