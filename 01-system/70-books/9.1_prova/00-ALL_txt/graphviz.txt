Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 11. General Utilities

     * [1]Prev
       desktop-file-utils-0.24
     * [2]Next
       GTK-Doc-1.32
     * [3]Up
     * [4]Home

Graphviz-2.42.3

Introduction to Graphviz

   The Graphviz package contains graph visualization software. Graph
   visualization is a way of representing structural information as
   diagrams of abstract graphs and networks. Graphviz has several main
   graph layout programs. It also has web and interactive graphical
   interfaces, auxiliary tools, libraries, and language bindings.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://www2.graphviz.org/Packages/stable/portable_source/graphv
       iz-2.42.3.tar.gz
     * Download MD5 sum: 9f61dc85517957793c6bb24f0611eac1
     * Download size: 25 MB
     * Estimated disk space required: 214 MB
     * Estimated build time: 1.0 SBU (using parallelism=4)

Graphviz Dependencies

   [Note]

Note

   Graphviz basic usage does not need any libraries out of what is found
   in the LFS book. Its “core” rendering engine allows to generate several
   graphic formats, such as Postscript, SVG, VML, .fig, and Tk. Those
   formats can be converted to almost any other, using for example tools
   from [6]ImageMagick-7.0.9-23. The dependencies below add the ability to
   generate graph images in bitmap format, to display the graph image on
   screen, to edit a graph by seeing directly the result image, or to view
   large graphs. Since Graphviz is a dependency of several other packages
   in this book, it is suggested to first build it without any
   dependencies, then to rebuild it when you have built enough packages to
   suit your needs.

Optional, for various bitmap outputs

   [7]Pango-1.44.7, with [8]Cairo-1.17.2+f93fc72c03e, [9]Xorg Libraries,
   [10]Fontconfig-2.13.1, and [11]libpng-1.6.37, to generate images in
   bitmap SVG, postscript, PNG, and PDF formats, or displaying the image
   on screen

   Adding [12]GTK+-2.24.32 with [13]libjpeg-turbo-2.0.4 allows to add
   JPEG, BMP, TIF, and ICO formats, and to display the image in a GTK+
   window

   [14]GD Library may be used instead of Pango. It adds the ability to
   generate images in GIF, VRML, and GD formats, but Pango provides better
   outputs for the other formats, and is needed for displaying images

   Other formats may be added with [15]libwebp-1.1.0 (WebP support is
   considered experimental), [16]DevIL, [17]libLASi, [18]glitz, and
   [19]libming (Adobe Flash)

Optional, to load graphic images that may be displayed inside the nodes of a
graph

   libgs.so from [20]ghostscript-9.50, [21]librsvg-2.46.4, and
   [22]Poppler-0.85.0

Optional, to build more tools

   [23]Freeglut-3.2.1 (with [24]libglade-2.6.4, [25]GtkGLExt, and
   [26]libGTS, for building the smyrna large graph viewer, which is
   considered experimental), [27]Qt-5.14.1, for building the gvedit graph
   editor. Another graph editor, dotty needs only [28]Xorg Libraries

Optional (To Build Language Bindings)

   [29]SWIG-4.0.1 (SWIG must be installed or no bindings will be built),
   [30]GCC-9.2.0 (for the go language), [31]Guile-3.0.0,
   [32]OpenJDK-12.0.2, [33]Lua-5.3.5, [34]PHP-7.4.2, [35]Python-2.7.17,
   [36]Ruby-2.7.0, [37]Tcl-8.6.10, [38]Tk-8.6.10, [39]Io, [40]Mono,
   [41]OCaml, and [42]R

Optional (building tools)

   [43]Criterion (framework for tests), [44]Electric Fence,

   User Notes: [45]http://wiki.linuxfromscratch.org/blfs/wiki/graphviz

Installation of Graphviz

   Install Graphviz by running the following commands:
sed -i '/LIBPOSTFIX="64"/s/64//' configure.ac &&

autoreconf                &&
./configure --prefix=/usr &&
make

   This package does not come with a test suite that provides meaningful
   results.

   Now, as the root user:
make install

   If desired, create a symbolic link in the system documents directory to
   the documentation installed in /usr/share/graphviz/doc using the
   following command as the root user:
ln -v -s /usr/share/graphviz/doc /usr/share/doc/graphviz-2.42.3

Command Explanations

   sed ... configure.ac: This command is needed to avoid installing files
   in /usr/lib64.

   --with-javaincludedir="$JAVA_HOME/include -I$JAVA_HOME/include/linux":
   If you have built [46]OpenJDK-12.0.2 in /opt, and you want to build the
   JAVA bindings, it is necessary to specify the location of the JAVA
   header files to configure. The configure switch is designed for only
   one directory, but two directories need to be included. This is
   possible nevertheless by using the -I switch inside the variable.

   --with-webp: Even if [47]libwebp-1.1.0 is installed, it is not included
   in the build without this option.

   --with-smyrna: Even if the needed dependencies are installed, the
   interactive graph viewer smyrna is not built without this option.

Configuring Graphviz

Config Files

   /usr/lib/graphviz/config

Configuration Information

   There are no specific configuration requirements for Graphviz. You may
   consider installing the additional plugins and tools available from the
   download page at [48]http://www.graphviz.org/Download_source.php for
   additional capabilities. If additional plugins are installed, you can
   run dot -c (as the root user) to update the config file in
   /usr/lib/graphviz.

Contents

   Installed Programs: acyclic, bcomps, ccomps, circo, cluster, diffimg,
   dijkstra, dot, dot2gxl, dot_builtins, dotty, edgepaint, fdp, gc,
   gml2gv, graphml2gv, gv2gml, gv2gxl, gvcolor, gvedit, gvgen, gvmap,
   gvmap.sh, gvpack, gvpr, gxl2dot, gxl2gv, lefty, lneato, mm2gv, neato,
   nop, osage, patchwork, prune, sccmap, sfdp, tred, twopi, unflatten, and
   vimdot
   Installed Libraries: libcdt.so, libcgraph.so, libgvc.so, libgvpr.so,
   libpathplan.so, libxdot.so, and several plugins in /usr/lib/graphviz.
   There are also several in subdirectories of
   /usr/lib/{lua,perl5,php,python2.7,tcl8.6}. Unfortunately, some
   libraries are duplicated.
   Installed Directories: /usr/include/graphviz, /usr/lib/graphviz,
   /usr/lib/tcl8.6/graphviz, /usr/share/doc/graphviz-2.42.3, and
   /usr/share/graphviz

Short Descriptions

   acyclic

   is a filter that takes a directed graph as input and outputs a copy of
   the graph with sufficient edges reversed to make the graph acyclic.

   bcomps

   decomposes graphs into their biconnected components, printing the
   components to standard output.

   ccomps

   decomposes graphs into their connected components, printing the
   components to standard output.

   circo

   draws graphs using a circular layout.

   cluster

   takes as input a graph in DOT format, finds node clusters and augments
   the graph with this information.

   diffimg

   (needs [49]GD Library) generates an image where each pixel is the
   difference between the corresponding pixel in each of the two source
   images.

   dijkstra

   reads a stream of graphs and for each computes the distance of every
   node from sourcenode.

   dot

   draws directed graphs. It works well on DAGs and other graphs that can
   be drawn as hierarchies. It reads attributed graph files and writes
   drawings. By default, the output format dot is the input file with
   layout coordinates appended.

   dot2gxl

   converts between graphs represented in GXL and in the DOT language.
   Unless a conversion type is specified using a flag, gxl2dot will deduce
   the type of conversion from the suffix of the input file, a .dot suffix
   causing a conversion from DOT to GXL, and a .gxl suffix causing a
   conversion from GXL to DOT.

   dotty

   is a graph editor for the X Window System. It may be run as a
   standalone editor, or as a front end for applications that use graphs.
   It can control multiple windows viewing different graphs.

   edgepaint

   edge coloring to disambiguate crossing edges.

   fdp

   draws undirected graphs using a “spring” model. It relies on a
   force-directed approach in the spirit of Fruchterman and Reingold.

   gc

   is a graph analogue to wc in that it prints to standard output the
   number of nodes, edges, connected components or clusters contained in
   the input files. It also prints a total count for all graphs if more
   than one graph is given.

   gml2gv

   converts a graph specified in the GML format to a graph in the GV
   (formerly DOT) format.

   graphml2gv

   converts a graph specified in the GRAPHML format to a graph in the GV
   (formerly DOT) format.

   gv2gml

   converts a graph specified in the GV format to a graph in the GML
   format.

   gv2gxl

   converts a graph specified in the GV format to a graph in the GXL
   format.

   gvcolor

   is a filter that sets node colors from initial seed values. Colors flow
   along edges from tail to head, and are averaged (as HSB vectors) at
   nodes.

   gvedit

   provides a simple graph editor and viewer. It allows many graphs to be
   viewed at the same time. The text of each graph is displayed in its own
   text window.

   gvgen

   generates a variety of simple, regularly-structured abstract graphs.

   gvmap

   takes as input a graph in DOT format, finds node clusters and produces
   a rendering of the graph as a geographic-style map, with clusters
   highlighted, in xdot format.

   gvmap.sh

   is a pipeline for running gvmap.

   gvpack

   reads in a stream of graphs, combines the graphs into a single layout,
   and produces a single graph serving as the union of the input graphs.

   gvpr

   is a graph stream editor inspired by awk. It copies input graphs to its
   output, possibly transforming their structure and attributes, creating
   new graphs, or printing arbitrary information.

   gxl2dot

   converts between graphs represented in GXL and in the DOT language.
   Unless a conversion type is specified using a flag, gxl2dot will deduce
   the type of conversion from the suffix of the input file, a .dot suffix
   causing a conversion from DOT to GXL, and a .gxl suffix causing a
   conversion from GXL to DOT.

   gxl2gv

   converts between graphs represented in GXL and in the GV language.

   lefty

   is a two-view graphics editor for technical pictures.

   lneato

   is a graph editor for the X Window System. It may be run as a
   standalone editor, or as a front end for applications that use graphs.
   It can control multiple windows viewing different graphs.

   mm2gv

   converts a sparse matrix of the Matrix Market format to a graph in the
   GV (formerly DOT) format.

   neato

   draws undirected graphs using “spring” models. Input files must be
   formatted in the dot attributed graph language. By default, the output
   of neato is the input graph with layout coordinates appended.

   nop

   reads a stream of graphs and prints each in pretty-printed (canonical)
   format on stdout. If no files are given, it reads from stdin.

   osage

   draws clustered graphs. As input, it takes any graph in the DOT format.

   patchwork

   draws clustered graphs using a squarified treemap layout. As input, it
   takes any graph in the DOT format.

   prune

   reads directed graphs in the same format used by dot and removes
   subgraphs rooted at nodes specified on the command line via options.

   sccmap

   decomposes digraphs into strongly connected components and an auxiliary
   map of the relationship between components. In this map, each component
   is collapsed into a node. The resulting graphs are printed to stdout.

   sfdp

   draws undirected graphs using the “spring” model, but it uses a
   multi-scale approach to produce layouts of large graphs in a reasonably
   short time.

   tred

   computes the transitive reduction of directed graphs, and prints the
   resulting graphs to standard output. This removes edges implied by
   transitivity. Nodes and subgraphs are not otherwise affected.

   twopi

   draws graphs using a radial layout. Basically, one node is chosen as
   the center and put at the origin. The remaining nodes are placed on a
   sequence of concentric circles centered about the origin, each a fixed
   radial distance from the previous circle.

   unflatten

   is a preprocessor to dot that is used to improve the aspect ratio of
   graphs having many leaves or disconnected nodes. The usual layout for
   such a graph is generally very wide or tall.

   vimdot

   is a simple script which launches the gvim or vim editor along with a
   GUI window showing the dot output of the edited file.

   libcdt.so

   manages run-time dictionaries using standard container data types:
   unordered set/multiset, ordered set/multiset, list, stack, and queue.

   libcgraph.so

   supports graph programming by maintaining graphs in memory and reading
   and writing graph files. Graphs are composed of nodes, edges, and
   nested subgraphs.

   libgvc.so

   provides a context for applications wishing to manipulate and render
   graphs. It provides a command line parsing, common rendering code, and
   a plugin mechanism for renderers.

   libpathplan.so

   contains functions to find the shortest path between two points in a
   simple polygon.

   libxdot.so

   provides support for parsing and deparsing graphical operations
   specificed by the xdot language.

   Last updated on 2020-02-16 15:15:05 -0800

     * [50]Prev
       desktop-file-utils-0.24
     * [51]Next
       GTK-Doc-1.32
     * [52]Up
     * [53]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/desktop-file-utils.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gtk-doc.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genutils.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://www2.graphviz.org/Packages/stable/portable_source/graphviz-2.42.3.tar.gz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/imagemagick.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/pango.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/cairo.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/x7lib.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/fontconfig.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libpng.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/gtk2.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libjpeg.html
  14. http://www.libgd.org/
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libwebp.html
  16. http://openil.sourceforge.net/projects.php
  17. http://sourceforge.net/projects/lasi/
  18. http://www.freedesktop.org/wiki/Software/glitz
  19. http://www.libming.org/
  20. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/pst/gs.html
  21. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/librsvg.html
  22. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/poppler.html
  23. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/freeglut.html
  24. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/libglade.html
  25. https://projects.gnome.org/gtkglext/
  26. http://gts.sourceforge.net/
  27. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/qt5.html
  28. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/x7lib.html
  29. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/swig.html
  30. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gcc.html
  31. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/guile.html
  32. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/openjdk.html
  33. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lua.html
  34. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/php.html
  35. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/python2.html
  36. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/ruby.html
  37. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/tcl.html
  38. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/tk.html
  39. http://iolanguage.org/
  40. http://www.mono-project.com/Main_Page
  41. http://ocaml.org/
  42. http://www.r-project.org/
  43. https://github.com/Snaipe/Criterion
  44. http://linux.softpedia.com/get/Programming/Debuggers/Electric-Fence-3305.shtml/
  45. http://wiki.linuxfromscratch.org/blfs/wiki/graphviz
  46. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/openjdk.html
  47. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libwebp.html
  48. http://www.graphviz.org/Download_source.php
  49. http://www.libgd.org/
  50. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/desktop-file-utils.html
  51. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gtk-doc.html
  52. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genutils.html
  53. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
