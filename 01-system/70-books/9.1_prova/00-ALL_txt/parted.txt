Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 5. File Systems and Disk Management

     * [1]Prev
       gptfdisk-1.0.5
     * [2]Next
       reiserfsprogs-3.6.27
     * [3]Up
     * [4]Home

parted-3.3

Introduction to parted

   The Parted package is a disk partitioning and partition resizing tool.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://ftp.gnu.org/gnu/parted/parted-3.3.tar.xz
     * Download (FTP): [6]ftp://ftp.gnu.org/gnu/parted/parted-3.3.tar.xz
     * Download MD5 sum: 090655d05f3c471aa8e15a27536889ec
     * Download size: 1.7 MB
     * Estimated disk space required: 34 MB (additional 3 MB for the tests
       and additional 1 MB for optional PDF and Postscript documentation)
     * Estimated build time: 0.3 SBU (additional 1.7 SBU for the tests)

Parted Dependencies

Recommended

   [7]LVM2-2.03.08 (device-mapper, required if building udisks)

Optional

   [8]dosfstools-4.1, [9]Pth-2.0.7, [10]texlive-20190410 (or
   [11]install-tl-unx), and [12]Digest::CRC (for tests)

   User Notes: [13]http://wiki.linuxfromscratch.org/blfs/wiki/parted

Installation of parted

   Install Parted by running the following commands:
./configure --prefix=/usr --disable-static &&
make &&

make -C doc html                                       &&
makeinfo --html      -o doc/html       doc/parted.texi &&
makeinfo --plaintext -o doc/parted.txt doc/parted.texi

   If you have [14]texlive-20190410 installed and wish to create PDF and
   Postcript documentation issue the following commands:
texi2pdf             -o doc/parted.pdf doc/parted.texi &&
texi2dvi             -o doc/parted.dvi doc/parted.texi &&
dvips                -o doc/parted.ps  doc/parted.dvi

   If you wish to run the test suite, first remove a couple of tests that
   are known to fail in a BLFS environment:
sed -i '/t0251-gpt-unicode.sh/d' tests/Makefile &&
sed -i '/t6002-dm-busy.sh/d' tests/Makefile

   To test the results, issue, as the root user:
make check

   [Note]

Note

   Many tests are skipped if not run as the root user.

   Now, as the root user:
make install &&
install -v -m755 -d /usr/share/doc/parted-3.3/html &&
install -v -m644    doc/html/* \
                    /usr/share/doc/parted-3.3/html &&
install -v -m644    doc/{FAT,API,parted.{txt,html}} \
                    /usr/share/doc/parted-3.3

   Install the optional PDF and Postscript documentation by issuing the
   following command as the root user:
install -v -m644 doc/FAT doc/API doc/parted.{pdf,ps,dvi} \
                    /usr/share/doc/parted-3.3

Command Explanations

   --disable-static: This switch prevents installation of static versions
   of the libraries.

   --disable-device-mapper: This option disables device mapper support.
   Add this parameter if you have not installed LVM2.

Contents

   Installed Programs: parted and partprobe
   Installed Libraries: libparted.so and libparted-fs-resize.so
   Installed Directories: /usr/include/parted and
   /usr/share/doc/parted-3.3

Short Descriptions

   parted

   is a partition manipulation program.

   partprobe

   informs the OS of partition table changes.

   libparted.so

   contains the Parted API functions.

   Last updated on 2020-02-18 14:50:03 -0800

     * [15]Prev
       gptfdisk-1.0.5
     * [16]Next
       reiserfsprogs-3.6.27
     * [17]Up
     * [18]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gptfdisk.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/reiserfs.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/filesystems.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://ftp.gnu.org/gnu/parted/parted-3.3.tar.xz
   6. ftp://ftp.gnu.org/gnu/parted/parted-3.3.tar.xz
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lvm2.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/dosfstools.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/pth.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/pst/texlive.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/pst/tl-installer.html
  12. https://metacpan.org/pod/Digest::CRC
  13. http://wiki.linuxfromscratch.org/blfs/wiki/parted
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/pst/texlive.html
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gptfdisk.html
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/reiserfs.html
  17. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/filesystems.html
  18. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
