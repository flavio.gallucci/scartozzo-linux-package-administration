Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 9. General Libraries

     * [1]Prev
       keyutils-1.6.1
     * [2]Next
       libarchive-3.4.2
     * [3]Up
     * [4]Home

libaio-0.3.112

Introduction to libaio

   The libaio package is an asynchronous I/O facility ("async I/O", or
   "aio") that has a richer API and capability set than the simple POSIX
   async I/O facility. This library, libaio, provides the Linux-native API
   for async I/O. The POSIX async I/O facility requires this library in
   order to provide kernel-accelerated async I/O capabilities, as do
   applications which require the Linux-native async I/O API.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]http://ftp.de.debian.org/debian/pool/main/liba/libaio/libaio_0.3
       .112.orig.tar.xz
     * Download MD5 sum: 66e3b7dd255581a73542ee851f8cd737
     * Download size: 40 KB
     * Estimated disk space required: 1.0 MB
     * Estimated build time: less than 0.1 SBU

   User Notes: [6]http://wiki.linuxfromscratch.org/blfs/wiki/libaio

Installation of libaio

   First, disable the installation of the static library:
sed -i '/install.*libaio.a/s/^/#/' src/Makefile

   Build libaio by running the following command:
make

   To test the results, issue: make partcheck.

   Now, install the package as the root user:
make install

Contents

   Installed Programs: None
   Installed Library: libaio.so
   Installed Directories: None

Short Descriptions

   libaio.so

   is the libaio library.

   Last updated on 2020-02-16 18:46:23 -0800

     * [7]Prev
       keyutils-1.6.1
     * [8]Next
       libarchive-3.4.2
     * [9]Up
     * [10]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/keyutils.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libarchive.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. http://ftp.de.debian.org/debian/pool/main/liba/libaio/libaio_0.3.112.orig.tar.xz
   6. http://wiki.linuxfromscratch.org/blfs/wiki/libaio
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/keyutils.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libarchive.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
