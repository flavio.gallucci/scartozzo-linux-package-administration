Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 25. X Libraries

     * [1]Prev
       FLTK-1.3.5
     * [2]Next
       gdk-pixbuf-2.40.0
     * [3]Up
     * [4]Home

Freeglut-3.2.1

Introduction to Freeglut

   Freeglut is intended to be a 100% compatible, completely opensourced
   clone of the GLUT library. GLUT is a window system independent toolkit
   for writing OpenGL programs, implementing a simple windowing API, which
   makes learning about and exploring OpenGL programming very easy.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://downloads.sourceforge.net/freeglut/freeglut-3.2.1.tar.gz
     * Download MD5 sum: cd5c670c1086358598a6d4a9d166949d
     * Download size: 432 KB
     * Estimated disk space required: 4.6 MB
     * Estimated build time: less than 0.1 SBU

Freeglut Dependencies

Required

   [6]CMake-3.16.4 and [7]Mesa-19.3.4

Recommended

   [8]GLU-9.0.1

   User Notes: [9]http://wiki.linuxfromscratch.org/blfs/wiki/freeglut

Installation of Freeglut

   Install Freeglut by running the following commands:
mkdir build &&
cd    build &&

cmake -DCMAKE_INSTALL_PREFIX=/usr       \
      -DCMAKE_BUILD_TYPE=Release        \
      -DFREEGLUT_BUILD_DEMOS=OFF        \
      -DFREEGLUT_BUILD_STATIC_LIBS=OFF  \
      .. &&

make

   This package does not come with a test suite.

   Now, as the root user:
make install

Command Explanations

   -DFREEGLUT_BUILD_DEMOS=OFF: Disable building optional demo programs.
   Note that if you choose to build them, their installation must be done
   manually. The demo programs are limited and installation is not
   recommended.

   -DFREEGLUT_BUILD_STATIC_LIBS=OFF: Do not build the static library.

Contents

   Installed Programs: None
   Installed Library: libglut.so
   Installed Directories: None

Short Descriptions

   libglut.so

   contains functions that implement the OpenGL Utility Toolkit.

   Last updated on 2020-02-16 15:15:05 -0800

     * [10]Prev
       FLTK-1.3.5
     * [11]Next
       gdk-pixbuf-2.40.0
     * [12]Up
     * [13]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/fltk.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gdk-pixbuf.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lib.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://downloads.sourceforge.net/freeglut/freeglut-3.2.1.tar.gz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/cmake.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/mesa.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/glu.html
   9. http://wiki.linuxfromscratch.org/blfs/wiki/freeglut
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/fltk.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gdk-pixbuf.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lib.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
