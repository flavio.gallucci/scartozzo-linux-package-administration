Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 42. Multimedia Libraries and Drivers

     * [1]Prev
       fdk-aac-2.0.1
     * [2]Next
       frei0r-plugins-1.7.0
     * [3]Up
     * [4]Home

FLAC-1.3.3

Introduction to FLAC

   FLAC is an audio CODEC similar to MP3, but lossless, meaning that audio
   is compressed without losing any information.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://downloads.xiph.org/releases/flac/flac-1.3.3.tar.xz
     * Download MD5 sum: 26703ed2858c1fc9ffc05136d13daa69
     * Download size: 1.0 MB
     * Estimated disk space required: 21 MB (additional 95 MB to run the
       test suite)
     * Estimated build time: 0.1 SBU (additional 0.7 SBU to run the test
       suite)

FLAC Dependencies

Optional

   [6]libogg-1.3.4, [7]NASM-2.14.02, [8]DocBook-utils-0.6.14,
   [9]Doxygen-1.8.17 and [10]Valgrind-3.15.0

   User Notes: [11]http://wiki.linuxfromscratch.org/blfs/wiki/flac

Installation of FLAC

   Install FLAC by running the following commands:
./configure --prefix=/usr \
            --disable-thorough-tests &&
make

   To test the results, issue: make check. Note that if you passed the
   --enable-exhaustive-tests and --enable-valgrind-testing parameters to
   configure and then run the test suite, it will take a very long time
   (up to 300 SBUs) and use about 375 MB of disk space.

   Now, as the root user:
make install

Command Explanations

   --disable-thorough-tests: This parameter is used so that the test suite
   will complete in a reasonable amount of time. Remove it if you desire
   more extensive tests.

Contents

   Installed Programs: flac and metaflac
   Installed Libraries: libFLAC.so and libFLAC++.so
   Installed Directories: /usr/include/FLAC, /usr/include/FLAC++ and
   /usr/share/doc/flac-1.3.3

Short Descriptions

   flac

   is a command-line utility for encoding, decoding and converting FLAC
   files.

   metaflac

   is a program for listing, adding, removing, or editing metadata in one
   or more FLAC files.

   libFLAC{,++}.so

   these libraries provide native FLAC and Ogg FLAC C/C++ APIs for
   programs utilizing FLAC.

   Last updated on 2020-02-17 18:27:03 -0800

     * [12]Prev
       fdk-aac-2.0.1
     * [13]Next
       frei0r-plugins-1.7.0
     * [14]Up
     * [15]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/fdk-aac.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/frei0r.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libdriv.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://downloads.xiph.org/releases/flac/flac-1.3.3.tar.xz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libogg.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/nasm.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/pst/docbook-utils.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/doxygen.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/valgrind.html
  11. http://wiki.linuxfromscratch.org/blfs/wiki/flac
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/fdk-aac.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/frei0r.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libdriv.html
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
