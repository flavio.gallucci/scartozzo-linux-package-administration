Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 41. Other X-based Programs

     * [1]Prev
       Pidgin-2.13.0
     * [2]Next
       rxvt-unicode-9.22
     * [3]Up
     * [4]Home

Rox-Filer-2.11

Introduction to Rox-Filer

   rox-filer is a fast, lightweight, gtk2 file manager.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://downloads.sourceforge.net/rox/rox-filer-2.11.tar.bz2
     * Download MD5 sum: 0eebf05a67f7932367750ebf9faf215d
     * Download size: 1.8 MB
     * Estimated disk space required: 19 MB
     * Estimated build time: 0.3 SBU

rox-filer Dependencies

Required

   [6]libglade-2.6.4 and [7]shared-mime-info-1.15

   User Notes: [8]http://wiki.linuxfromscratch.org/blfs/wiki/rox-filer

Kernel Configuration

   If you want rox-filer to be able to update the contents of a directory
   when changes are made to the files by other applications (eg, if a
   script is running) you will need to enable dnotify support in your
   kernel. In make menuconfig:
File systems --->
  [*] Dnotify support   [CONFIG_DNOTIFY]

   Save the new .config and then compile the kernel.

Installation of Rox-Filer

   Compile rox-filer with the following commands:
cd ROX-Filer                                                        &&
sed -i 's:g_strdup(getenv("APP_DIR")):"/usr/share/rox":' src/main.c &&

mkdir build                        &&
pushd build                        &&
  ../src/configure LIBS="-lm -ldl" &&
  make                             &&
popd

   Now install it as the root user:
mkdir -p /usr/share/rox                              &&
cp -av Help Messages Options.xml ROX images style.css .DirIcon /usr/share/rox &&

cp -av ../rox.1 /usr/share/man/man1                  &&
cp -v  ROX-Filer /usr/bin/rox                        &&
chown -Rv root:root /usr/bin/rox /usr/share/rox      &&

cd /usr/share/rox/ROX/MIME                           &&
ln -sv text-x-{diff,patch}.png                       &&
ln -sv application-x-font-{afm,type1}.png            &&
ln -sv application-xml{,-dtd}.png                    &&
ln -sv application-xml{,-external-parsed-entity}.png &&
ln -sv application-{,rdf+}xml.png                    &&
ln -sv application-x{ml,-xbel}.png                   &&
ln -sv application-{x-shell,java}script.png          &&
ln -sv application-x-{bzip,xz}-compressed-tar.png    &&
ln -sv application-x-{bzip,lzma}-compressed-tar.png  &&
ln -sv application-x-{bzip-compressed-tar,lzo}.png   &&
ln -sv application-x-{bzip,xz}.png                   &&
ln -sv application-x-{gzip,lzma}.png                 &&
ln -sv application-{msword,rtf}.png

Command Explanations

   sed -i 's:g_strdup(getenv("APP_DIR")):"/usr/share/rox":' src/main.c:
   This command hard codes /usr/share/rox as the directory for rox-filer's
   private files. Without this sed rox needs the environment variable
   ${APP_DIR} to be set.

   ln -sv application-...: These commands duplicate the icons for some
   common mime types. Without these links rox-filer would just display the
   default "unknown binary blob" icon.

Configuring RoxFiler

Configuration Information

   Most of the configuration of rox-filer is achieved by right clicking on
   a rox-filer window and choosing "Options" from the menu. It stores its
   settings in ~/.config/rox.sourceforge.net.

   A rox-filer feature is that if there is an executable file called
   AppRun in a directory rox-filer will first run AppRun before it opens
   the folder.

   As an example of how this may be used, if you have ssh access to
   another computer (perhaps another computer on your local network) with
   ssh configured for passwordless logins and you have [9]sshfs-3.7.0
   installed you can use AppRun to mount the remote computer in a local
   folder using sshfs. For this example AppRun script to work the folder
   must have the same name as the hostname of the remote computer:
cat > /path/to/hostname/AppRun << "HERE_DOC"
#!/bin/bash

MOUNT_PATH="${0%/*}"
HOST=${MOUNT_PATH##*/}
export MOUNT_PATH HOST
sshfs -o nonempty ${HOST}:/ ${MOUNT_PATH}
rox -x ${MOUNT_PATH}
HERE_DOC

chmod 755 /path/to/hostname/AppRun

   That works fine for mounting, but to unmount it the command fusermount
   -u ${MOUNTPOINT} is ran. You could set that as your default umount
   command in your rox preferences, but you would then be unable to
   unmount any normal mountpoints (that need umount). A script is needed
   that will unmount a Fuse mountpoint with fusermount -u ${MOUNTPOINT}
   and everything else with umount. As the root user:
cat > /usr/bin/myumount << "HERE_DOC" &&
#!/bin/bash
sync
if mount | grep "${@}" | grep -q fuse
then fusermount -u "${@}"
else umount "${@}"
fi
HERE_DOC

chmod 755 /usr/bin/myumount

   Now, to make Rox use this simple script, open a Rox window, right click
   on it and choose Options from the menu. In the left hand list choose
   "Action windows" and then on the right hand side, where it says
   "Unmount command" change umount to myumount.

   If you use a desktop environment like Gnome or KDE you may like to
   create a rox.desktop file so that rox-filer appears in the panel's
   menus. As the root user:
ln -s ../rox/.DirIcon /usr/share/pixmaps/rox.png &&
mkdir -p /usr/share/applications &&

cat > /usr/share/applications/rox.desktop << "HERE_DOC"
[Desktop Entry]
Encoding=UTF-8
Type=Application
Name=Rox
Comment=The Rox File Manager
Icon=rox
Exec=rox
Categories=GTK;Utility;Application;System;Core;
StartupNotify=true
Terminal=false
HERE_DOC

Contents

   Installed Programs: rox
   Installed Libraries: None
   Installed Directories: /usr/share/rox

Short Descriptions

   rox

   is the rox file manager.

   Last updated on

     * [10]Prev
       Pidgin-2.13.0
     * [11]Next
       rxvt-unicode-9.22
     * [12]Up
     * [13]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/pidgin.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/rxvt-unicode.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/other.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://downloads.sourceforge.net/rox/rox-filer-2.11.tar.bz2
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/libglade.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/shared-mime-info.html
   8. http://wiki.linuxfromscratch.org/blfs/wiki/rox-filer
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/sshfs.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/pidgin.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/rxvt-unicode.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/other.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
