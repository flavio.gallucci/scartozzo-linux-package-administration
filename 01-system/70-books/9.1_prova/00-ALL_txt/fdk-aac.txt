Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 42. Multimedia Libraries and Drivers

     * [1]Prev
       FAAD2-2.8.8
     * [2]Next
       FLAC-1.3.3
     * [3]Up
     * [4]Home

fdk-aac-2.0.1

Introduction to fdk-aac

   fdk-aac package provides the Fraunhofer FDK AAC library, which is
   purported to be a high quality Advanced Audio Coding implementation.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://downloads.sourceforge.net/opencore-amr/fdk-aac-2.0.1.tar
       .gz
     * Download MD5 sum: e8b0b38e837df455b8a1ba75417ff0ad
     * Download size: 2.7 MB
     * Estimated disk space required: 51 MB
     * Estimated build time: 0.6 SBU

   User Notes: [6]http://wiki.linuxfromscratch.org/blfs/wiki/fdk-aac

Installation of fdk-aac

   Install fdk-aac by running the following commands:
./configure --prefix=/usr --disable-static &&
make

   This package does not come with a test suite.

   Now, as the root user:
make install

Command Explanations

   --disable-static: This switch prevents installation of static versions
   of the libraries.

Contents

   Installed Programs: None
   Installed Library: libfdk-aac.so
   Installed Directory: /usr/include/fdk-aac

Short Descriptions

   libfdk-aac.so

   provides the functions used to encode audio in AAC format.

   Last updated on 2020-02-17 18:27:03 -0800

     * [7]Prev
       FAAD2-2.8.8
     * [8]Next
       FLAC-1.3.3
     * [9]Up
     * [10]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/faad2.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/flac.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libdriv.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://downloads.sourceforge.net/opencore-amr/fdk-aac-2.0.1.tar.gz
   6. http://wiki.linuxfromscratch.org/blfs/wiki/fdk-aac
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/faad2.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/flac.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libdriv.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
