Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 12. System Utilities

     * [1]Prev
       usbutils-012
     * [2]Next
       Zip-3.0
     * [3]Up
     * [4]Home

Which-2.21 and Alternatives

   The presence or absence of the which program in the main LFS book is
   probably one of the most contentious issues on the mailing lists. It
   has resulted in at least one flame war in the past. To hopefully put an
   end to this once and for all, presented here are two options for
   equipping your system with which. The question of which “which” is for
   you to decide.

Introduction to GNU Which

   The first option is to install the actual GNU which package.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP): [5]https://ftp.gnu.org/gnu/which/which-2.21.tar.gz
     * Download (FTP): [6]ftp://ftp.gnu.org/gnu/which/which-2.21.tar.gz
     * Download MD5 sum: 097ff1a324ae02e0a3b0369f07a7544a
     * Download size: 148 KB
     * Estimated disk space required: 1 MB
     * Estimated build time: less than 0.1 SBU

   User Notes: [7]http://wiki.linuxfromscratch.org/blfs/wiki/which

Installation of Which

   Install which by running the following commands:
./configure --prefix=/usr &&
make

   This package does not come with a test suite.

   Now, as the root user:
make install

Contents

   Installed Program: which
   Installed Libraries: None
   Installed Directories: None

Short Descriptions

   which

   shows the full path of (shell) commands installed in your PATH.

The 'which' Script

   The second option (for those who don't want to install the package) is
   to create a simple script (execute as the root user):
cat > /usr/bin/which << "EOF"
#!/bin/bash
type -pa "$@" | head -n 1 ; exit ${PIPESTATUS[0]}
EOF
chmod -v 755 /usr/bin/which
chown -v root:root /usr/bin/which

   This should work OK and is probably the easiest solution for most
   cases, but is not the most comprehensive implementation.

   Last updated on 2020-02-15 20:23:35 -0800

     * [8]Prev
       usbutils-012
     * [9]Next
       Zip-3.0
     * [10]Up
     * [11]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/usbutils.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/zip.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/sysutils.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://ftp.gnu.org/gnu/which/which-2.21.tar.gz
   6. ftp://ftp.gnu.org/gnu/which/which-2.21.tar.gz
   7. http://wiki.linuxfromscratch.org/blfs/wiki/which
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/usbutils.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/zip.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/sysutils.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
