Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 9. General Libraries

     * [1]Prev
       mtdev-1.1.6
     * [2]Next
       npth-1.6
     * [3]Up
     * [4]Home

Node.js-12.16.1

Introduction to Node.js

   Node.js is a JavaScript runtime built on Chrome's V8 JavaScript engine.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://nodejs.org/dist/v12.16.1/node-v12.16.1.tar.xz
     * Download MD5 sum: 549582c075072c689c245ba12ecac54a
     * Download size: 23 MB
     * Estimated disk space required: 658 MB (add 31 MB for tests)
     * Estimated build time: 7.8 SBU (using parallelism=4; add 2.2 SBU for
       tests)

Node.js Dependencies

Required

   [6]Python-2.7.17 and [7]Which-2.21

Recommended

   [8]c-ares-1.15.0, [9]ICU-65.1, [10]libuv-1.34.2, and [11]nghttp2-1.40.0

Optional

   [12]http-parser, [13]npm (an internal copy of npm will be installed if
   not present)

   User Notes: [14]http://wiki.linuxfromscratch.org/blfs/wiki/nodejs

Installation of Node.js

   Build Node.js by running the following commands:
./configure --prefix=/usr                  \
            --shared-cares                 \
            --shared-libuv                 \
            --shared-nghttp2               \
            --shared-openssl               \
            --shared-zlib                  \
            --with-intl=system-icu         &&
make

   To test the results, issue: make check. One test, test-dns, is known to
   fail.

   Now, as the root user:
make install &&
ln -sf node /usr/share/doc/node-12.16.1

Command Explanations

   --with-intl=system-icu: use the system version of icu. Other values are
   full-icu (to build a local, full icu library) and small-icu (to to
   build a local, minimal icu library).

   --shared-{cares,libuv,nghttp2,openssl,zlib}: use the system installed
   libraries instead of local copies.

   --without-npm: do not build npm (use if you'd like to build a separate
   npm later).

   --shared-http-parser: use the system installed library instead of a
   local copy.

Contents

   Installed Programs: node, npm, and npx
   Installed Library: None
   Installed Directories: /usr/include/node, /usr/lib/node_modules/npm,
   and /usr/share/systemtap/tapset

Short Descriptions

   node

   is the server-side JavaScript runtime.

   npm

   is the Node.js package manager.

   /usr/lib/node_modules/npm/

   is the installation root for Node.js executables and libraries.

   Last updated on 2020-02-20 18:09:20 -0800

     * [15]Prev
       mtdev-1.1.6
     * [16]Next
       npth-1.6
     * [17]Up
     * [18]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/mtdev.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/npth.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://nodejs.org/dist/v12.16.1/node-v12.16.1.tar.xz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/python2.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/which.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/basicnet/c-ares.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/icu.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libuv.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/basicnet/nghttp2.html
  12. https://github.com/nodejs/http-parser
  13. https://www.npmjs.com/
  14. http://wiki.linuxfromscratch.org/blfs/wiki/nodejs
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/mtdev.html
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/npth.html
  17. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
  18. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
