Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 42. Multimedia Libraries and Drivers

     * [1]Prev
       libmusicbrainz-2.1.5
     * [2]Next
       libogg-1.3.4
     * [3]Up
     * [4]Home

libmusicbrainz-5.1.0

Introduction to libmusicbrainz

   The libmusicbrainz package contains a library which allows you to
   access the data held on the MusicBrainz server.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://github.com/metabrainz/libmusicbrainz/releases/download/r
       elease-5.1.0/libmusicbrainz-5.1.0.tar.gz
     * Download MD5 sum: 4cc5556aa40ff7ab8f8cb83965535bc3
     * Download size: 76 KB
     * Estimated disk space required: 6.6 MB (additional 4.4 MB for the
       API documentaion)
     * Estimated build time: 0.1 SBU

libmusicbrainz Dependencies

Required

   [6]CMake-3.16.4, [7]libxml2-2.9.10 and [8]neon-0.30.2

Optional

   [9]Doxygen-1.8.17

   User Notes:
   [10]http://wiki.linuxfromscratch.org/blfs/wiki/libmusicbrainz5

Installation of libmusicbrainz

   Install libmusicbrainz by running the following commands:
mkdir build &&
cd    build &&

cmake -DCMAKE_INSTALL_PREFIX=/usr .. &&
make

   If you have installed [11]Doxygen-1.8.17, optionally build the API
   documentation:
doxygen ../Doxyfile

   This package does not come with a test suite.

   Now, as the root user:
make install

   If you have built the API documentation, install, as the root user:
rm -rf /usr/share/doc/libmusicbrainz-5.1.0 &&
cp -vr docs/ /usr/share/doc/libmusicbrainz-5.1.0

Contents

   Installed Programs: None
   Installed Library: libmusicbrainz5.so
   Installed Directory: /usr/include/libmusicbrainz5 and
   /usr/share/doc/libmusicbrainz-5.1.0

Short Descriptions

   libmusicbrainz5.so

   contains API functions for accessing the MusicBrainz database.

   Last updated on 2020-02-18 14:50:03 -0800

     * [12]Prev
       libmusicbrainz-2.1.5
     * [13]Next
       libogg-1.3.4
     * [14]Up
     * [15]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libmusicbrainz.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libogg.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libdriv.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://github.com/metabrainz/libmusicbrainz/releases/download/release-5.1.0/libmusicbrainz-5.1.0.tar.gz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/cmake.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/libxml2.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/basicnet/neon.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/doxygen.html
  10. http://wiki.linuxfromscratch.org/blfs/wiki/libmusicbrainz5
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/doxygen.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libmusicbrainz.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libogg.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libdriv.html
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
