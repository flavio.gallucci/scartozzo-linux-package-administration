Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 6. Editors

     * [1]Prev
       Kate-19.12.2
     * [2]Next
       Nano-4.8
     * [3]Up
     * [4]Home

Mousepad-0.4.2

Introduction to Mousepad

   Mousepad is a simple GTK+ 2 text editor for the Xfce desktop
   environment.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]http://archive.xfce.org/src/apps/mousepad/0.4/mousepad-0.4.2.tar
       .bz2
     * Download MD5 sum: 98d908842d4a93c35756a67d681c08fe
     * Download size: 680 KB
     * Estimated disk space required: 12 MB
     * Estimated build time: 0.1 SBU

Mousepad Dependencies

Required

   [6]gtksourceview-3.24.11 (optionally, it can be built with
   [7]gtksourceview-2) and [8]Xfconf-4.14.1

Optional

   [9]DConf-0.34.0 (runtime) and [10]dbus-glib-0.110

   User Notes: [11]http://wiki.linuxfromscratch.org/blfs/wiki/mousepad

Installation of Mousepad

   Install Mousepad by running the following commands:
./configure --prefix=/usr --enable-keyfile-settings &&
make

   This package does not come with a test suite.

   Now, as the root user:
make install

Command Explanations

   --enable-keyfile-settings: Use the GSettings keyfile backend rather
   than the default [12]DConf-0.34.0.

Contents

   Installed Program: mousepad
   Installed Libraries: None
   Installed Directories: None

Short Descriptions

   mousepad

   is a simple GTK+ 2 text editor.

   Last updated on 2020-02-20 06:05:58 -0800

     * [13]Prev
       Kate-19.12.2
     * [14]Next
       Nano-4.8
     * [15]Up
     * [16]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/kate5.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/nano.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/editors.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. http://archive.xfce.org/src/apps/mousepad/0.4/mousepad-0.4.2.tar.bz2
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/gtksourceview.html
   7. http://ftp.gnome.org/pub/gnome/sources/gtksourceview/2.10/
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/xfce/xfconf.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/gnome/dconf.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/dbus-glib.html
  11. http://wiki.linuxfromscratch.org/blfs/wiki/mousepad
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/gnome/dconf.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/kate5.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/nano.html
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/editors.html
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
