Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 12. System Utilities

     * [1]Prev
       Pax-20161104
     * [2]Next
       pm-utils-1.4.1
     * [3]Up
     * [4]Home

pciutils-3.6.4

Introduction to PCI Utils

   The PCI Utils package contains a set of programs for listing PCI
   devices, inspecting their status and setting their configuration
   registers.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://www.kernel.org/pub/software/utils/pciutils/pciutils-3.6.
       4.tar.xz
     * Download MD5 sum: 4343b37e19f319ce8f3d59c30031790e
     * Download size: 356 KB
     * Estimated disk space required: 3.6 MB
     * Estimated build time: less than 0.1 SBU

pciutils Dependencies

Recommended

   [6]cURL-7.68.0, [7]Wget-1.20.3, or [8]Lynx-2.8.9rel.1 (for the
   update-pciids script to function correctly).

   User Notes: [9]http://wiki.linuxfromscratch.org/blfs/wiki/pciutils

Installation of PCI Utils

   Install PCI Utils by running the following commands:
make PREFIX=/usr                \
     SHAREDIR=/usr/share/hwdata \
     SHARED=yes

   This package does not come with a test suite.

   Now, as the root user:
make PREFIX=/usr                \
     SHAREDIR=/usr/share/hwdata \
     SHARED=yes                 \
     install install-lib        &&

chmod -v 755 /usr/lib/libpci.so

Command Explanations

   SHARED=yes: This parameter enables building of the shared library
   instead of the static one.

   ZLIB=no: This option prevents compression of the pci.ids file.

Configuring PCI Utils

   The pci.ids data file is constantly being updated. To get a current
   version of this file, run update-pciids as the root user. This program
   requires the [10]Which-2.21 script or program to find [11]cURL-7.68.0,
   [12]Lynx-2.8.9rel.1 or [13]Wget-1.20.3 which are used to download the
   most current file, and then replace the existing file in
   /usr/share/hwdata.

   You should update the /usr/share/hwdata/pci.ids file periodically. If
   you've installed [14]Fcron-3.2.1 and completed the section on periodic
   jobs, execute the following commands, as the root user, to create a
   weekly cron job:
cat > /etc/cron.weekly/update-pciids.sh << "EOF" &&
#!/bin/bash
/usr/sbin/update-pciids
EOF
chmod 754 /etc/cron.weekly/update-pciids.sh

Contents

   Installed Programs: lspci, setpci, and update-pciids
   Installed Library: libpci.so
   Installed Directory: /usr/include/pci and /usr/share/hwdata

Short Descriptions

   lspci

   is a utility for displaying information about all PCI buses in the
   system and all devices connected to them.

   setpci

   is a utility for querying and configuring PCI devices.

   update-pciids

   fetches the current version of the PCI ID list.

   libpci.so

   is library that allows applications to access the PCI subsystem.

   Last updated on 2020-02-16 15:50:16 -0800

     * [15]Prev
       Pax-20161104
     * [16]Next
       pm-utils-1.4.1
     * [17]Up
     * [18]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/pax.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/pm-utils.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/sysutils.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://www.kernel.org/pub/software/utils/pciutils/pciutils-3.6.4.tar.xz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/basicnet/curl.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/basicnet/wget.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/basicnet/lynx.html
   9. http://wiki.linuxfromscratch.org/blfs/wiki/pciutils
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/which.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/basicnet/curl.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/basicnet/lynx.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/basicnet/wget.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/fcron.html
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/pax.html
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/pm-utils.html
  17. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/sysutils.html
  18. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
