Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 9. General Libraries

     * [1]Prev
       libunistring-0.9.10
     * [2]Next
       libuv-1.34.2
     * [3]Up
     * [4]Home

libusb-1.0.23

Introduction to libusb

   The libusb package contains a library used by some applications for USB
   device access.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://github.com//libusb/libusb/releases/download/v1.0.23/libu
       sb-1.0.23.tar.bz2
     * Download MD5 sum: 1e29700f6a134766d32b36b8d1d61a95
     * Download size: 592 KB
     * Estimated disk space required: 6.4 MB
     * Estimated build time: less than 0.1 SBU

libusb Dependencies

Optional

   [6]Doxygen-1.8.17

   User Notes: [7]http://wiki.linuxfromscratch.org/blfs/wiki/libusb

Installation of libusb

   [Note]

Note

   This package does not support parallel build.

   Install libusb by running the following commands:
sed -i "s/^PROJECT_LOGO/#&/" doc/doxygen.cfg.in &&

./configure --prefix=/usr --disable-static &&
make -j1

   If Doxygen is installed and you wish to build the API documentation,
   issue the following command:
make -C doc docs

   This package does not come with a test suite.

   Now, as the root user:
make install

   If you built the API documentation, install it using the following
   commands as the root user:
install -v -d -m755 /usr/share/doc/libusb-1.0.23/apidocs &&
install -v -m644    doc/html/* \
                    /usr/share/doc/libusb-1.0.23/apidocs

Command Explanations

   sed -i ...: This sed prevents a warning when building the
   documentation.

Configuring Libusb

   To access raw USB devices (those not treated as a disk by the
   mass-storage driver), appropriate support must be available in the
   kernel. Check your kernel configuration:
Device Drivers --->
  [*] USB support --->                   [CONFIG_USB_SUPPORT]
    <*/M> Support for Host-side USB      [CONFIG_USB]
    (Select any USB hardware device drivers you may need on the same page)

   For more details on setting up USB devices, see [8]the section called
   “USB Device Issues”.

Contents

   Installed Programs: None
   Installed Library: libusb-1.0.so
   Installed Directories: /usr/include/libusb-1.0 and
   /usr/share/doc/libusb-1.0.23

Short Descriptions

   libusb-1.0.so

   contains API functions used for accessing USB hardware.

   Last updated on 2020-02-16 18:46:23 -0800

     * [9]Prev
       libunistring-0.9.10
     * [10]Next
       libuv-1.34.2
     * [11]Up
     * [12]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libunistring.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libuv.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://github.com//libusb/libusb/releases/download/v1.0.23/libusb-1.0.23.tar.bz2
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/doxygen.html
   7. http://wiki.linuxfromscratch.org/blfs/wiki/libusb
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/devices.html#usb-device-issues
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libunistring.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libuv.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
