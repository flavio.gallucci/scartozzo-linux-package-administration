Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 3. After LFS Configuration Issues

     * [1]Prev
       After LFS Configuration Issues
     * [2]Next
       About Console Fonts
     * [3]Up
     * [4]Home

Creating a Custom Boot Device

Decent Rescue Boot Device Needs

   This section is really about creating a rescue device. As the name
   rescue implies, the host system has a problem, often lost partition
   information or corrupted file systems, that prevents it from booting
   and/or operating normally. For this reason, you must not depend on
   resources from the host being "rescued". To presume that any given
   partition or hard drive will be available is a risky presumption.

   In a modern system, there are many devices that can be used as a rescue
   device: floppy, cdrom, usb drive, or even a network card. Which one you
   use depends on your hardware and your BIOS. In the past, a rescue
   device was thought to be a floppy disk. Today, many systems do not even
   have a floppy drive.

   Building a complete rescue device is a challenging task. In many ways,
   it is equivalent to building an entire LFS system. In addition, it
   would be a repetition of information already available. For these
   reasons, the procedures for a rescue device image are not presented
   here.

Creating a Rescue Floppy

   The software of today's systems has grown large. Linux 2.6 no longer
   supports booting directly from a floppy. In spite of this, there are
   solutions available using older versions of Linux. One of the best is
   Tom's Root/Boot Disk available at [5]http://www.toms.net/rb/. This will
   provide a minimal Linux system on a single floppy disk and provides the
   ability to customize the contents of your disk if necessary.

Creating a Bootable CD-ROM

   There are several sources that can be used for a rescue CD-ROM. Just
   about any commercial distribution's installation CD-ROMs or DVDs will
   work. These include RedHat, Ubuntu, and SuSE. One very popular option
   is Knoppix.

   Also, the LFS Community has developed its own LiveCD available at
   [6]http://www.linuxfromscratch.org/livecd/. This LiveCD, is no longer
   capable of building an entire LFS/BLFS system, but is still a good
   rescue CD-ROM. If you download the ISO image, use [7]xorriso to copy
   the image to a CD-ROM.

   The instructions for using GRUB2 to make a custom rescue CD-ROM are
   also available in [8]LFS Chapter 8.

Creating a Bootable USB Drive

   A USB Pen drive, sometimes called a Thumb drive, is recognized by Linux
   as a SCSI device. Using one of these devices as a rescue device has the
   advantage that it is usually large enough to hold more than a minimal
   boot image. You can save critical data to the drive as well as use it
   to diagnose and recover a damaged system. Booting such a drive requires
   BIOS support, but building the system consists of formatting the drive,
   adding GRUB as well as the Linux kernel and supporting files.

   User Notes:
   [9]http://wiki.linuxfromscratch.org/blfs/wiki/CreatingaCustomBootDevice

   Last updated on 2017-04-23 10:21:19 -0700

     * [10]Prev
       After LFS Configuration Issues
     * [11]Next
       About Console Fonts
     * [12]Up
     * [13]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/config.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/console-fonts.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/config.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. http://www.toms.net/rb/
   6. http://www.linuxfromscratch.org/livecd/
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/multimedia/libisoburn.html#xorriso
   8. http://www.linuxfromscratch.org/lfs/view/stable/chapter08/grub.html
   9. http://wiki.linuxfromscratch.org/blfs/wiki/CreatingaCustomBootDevice
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/config.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/console-fonts.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/config.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
