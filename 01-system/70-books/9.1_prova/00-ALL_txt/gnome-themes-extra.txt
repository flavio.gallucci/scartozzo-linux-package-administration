Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 28. Icons

     * [1]Prev
       gnome-icon-theme-symbolic-3.12.0
     * [2]Next
       hicolor-icon-theme-0.17
     * [3]Up
     * [4]Home

gnome-themes-extra-3.28

Introduction to GNOME Themes Extra

   The GNOME Themes Extra package, formerly known as GNOME Themes
   Standard, contains various components of the default GNOME theme.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]http://ftp.gnome.org/pub/gnome/sources/gnome-themes-extra/3.28/g
       nome-themes-extra-3.28.tar.xz
     * Download (FTP):
       [6]ftp://ftp.gnome.org/pub/gnome/sources/gnome-themes-extra/3.28/gn
       ome-themes-extra-3.28.tar.xz
     * Download MD5 sum: f9f2c6c521948da427f702372e16f826
     * Download size: 2.8 MB
     * Estimated disk space required: 40 MB
     * Estimated build time: 0.3 SBU

GNOME Themes Extra Dependencies

Required

   [7]GTK+-2.24.32 or [8]GTK+-3.24.13 with [9]librsvg-2.46.4 or both

   User Notes:
   [10]http://wiki.linuxfromscratch.org/blfs/wiki/gnome-themes-extra

Installation of GNOME Themes Extra

   Install GNOME Themes Extra by running the following commands:
./configure --prefix=/usr &&
make

   This package does not come with a test suite.

   Now, as the root user:
make install

Command Explanations

   --disable-gtk2-engine: This switch disables the GTK+-2 theming engine.

   --disable-gtk3-engine: This switch disables the GTK+-3 theming engine.

Contents

   Installed Programs: None
   Installed Library: libadwaita.so
   Installed Directories: /usr/lib/gtk-2.0/2.10.0/engines,
   /usr/share/icons/HighContrast, /usr/share/themes/Adwaita,
   /usr/share/themes/Adwaita-dark, and /usr/share/themes/HighContrast

Short Descriptions

   libadwaita.so

   is the Adwaita GTK+-2 engine theme.

   Last updated on 2020-02-20 12:41:28 -0800

     * [11]Prev
       gnome-icon-theme-symbolic-3.12.0
     * [12]Next
       hicolor-icon-theme-0.17
     * [13]Up
     * [14]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gnome-icon-theme-symbolic.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/hicolor-icon-theme.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/icons.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. http://ftp.gnome.org/pub/gnome/sources/gnome-themes-extra/3.28/gnome-themes-extra-3.28.tar.xz
   6. ftp://ftp.gnome.org/pub/gnome/sources/gnome-themes-extra/3.28/gnome-themes-extra-3.28.tar.xz
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gtk2.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gtk3.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/librsvg.html
  10. http://wiki.linuxfromscratch.org/blfs/wiki/gnome-themes-extra
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gnome-icon-theme-symbolic.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/hicolor-icon-theme.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/icons.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
