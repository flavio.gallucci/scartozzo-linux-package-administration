Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 15. Networking Programs

     * [1]Prev
       Wget-1.20.3
     * [2]Next
       wpa_supplicant-2.9
     * [3]Up
     * [4]Home

Wireless Tools-29

Introduction to Wireless Tools

   The Wireless Extension (WE) is a generic API in the Linux kernel
   allowing a driver to expose configuration and statistics specific to
   common Wireless LANs to user space. A single set of tools can support
   all the variations of Wireless LANs, regardless of their type as long
   as the driver supports Wireless Extensions. WE parameters may also be
   changed on the fly without restarting the driver (or Linux).

   The Wireless Tools (WT) package is a set of tools allowing manipulation
   of the Wireless Extensions. They use a textual interface to support the
   full Wireless Extension.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://hewlettpackard.github.io/wireless-tools/wireless_tools.2
       9.tar.gz
     * Download MD5 sum: e06c222e186f7cc013fd272d023710cb
     * Download size: 288 KB
     * Estimated disk space required: 2.0 MB
     * Estimated build time: less than 0.1 SBU

Additional Downloads

     * Required patch:
       [6]http://www.linuxfromscratch.org/patches/blfs/9.1/wireless_tools-
       29-fix_iwlist_scanning-1.patch

   User Notes: [7]http://wiki.linuxfromscratch.org/blfs/wiki/WirelessTools

Kernel Configuration

   To use Wireless Tools, the kernel must have the appropriate drivers and
   other support available. The appropriate bus must also be available.
   For many laptops, the PCMCIA bus (CONFIG_PCCARD) needs to be built. In
   some cases, this bus support will also need to be built for embedded
   wireless cards. The appropriate bridge support also needs to be built.
   For many modern laptops, the CardBus host bridge (CONFIG_YENTA) will be
   needed.

   In addition to the bus, the actual driver for the specific wireless
   card must also be available. There are many wireless cards and they
   don't all work with Linux. The first place to look for card support is
   the kernel. The drivers are located in Device Drivers → Network Device
   Support → Wireless LAN (non-hamradio). There are also external drivers
   available for some very common cards. For more information, look at the
   user notes.

   After the correct drivers are loaded, the interface will appear in
   /proc/net/wireless.

Installation of Wireless Tools

   First, apply a patch that fixes a problem when numerous networks are
   available:
patch -Np1 -i ../wireless_tools-29-fix_iwlist_scanning-1.patch

   To install Wireless Tools, use the following commands:
make

   This package does not come with a test suite.

   Now, as the root user:
make PREFIX=/usr INSTALL_MAN=/usr/share/man install

Command Explanations

   INSTALL_MAN=/usr/share/man: Install manual pages in /usr/share/man
   instead of /usr/man

Contents

   Installed Programs: ifrename, iwconfig, iwevent, iwgetid, iwlist,
   iwpriv, and iwspy
   Installed Library: libiw.so
   Installed Directories: None

Short Descriptions

   ifrename

   renames network interfaces based on various static criteria.

   iwconfig

   configures a wireless network interface.

   iwevent

   displays wireless events generated by drivers and setting changes.

   iwgetid

   reports ESSID, NWID or AP/Cell Address of wireless networks.

   iwlist

   gets detailed wireless information from a wireless interface.

   iwpriv

   configures optional (private) parameters of a wireless network
   interface.

   iwspy

   gets wireless statistics from specific node.

   libiw.so

   contains functions required by the wireless programs and provides an
   API for other programs.

   Last updated on 2020-02-17 12:03:00 -0800

     * [8]Prev
       Wget-1.20.3
     * [9]Next
       wpa_supplicant-2.9
     * [10]Up
     * [11]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/wget.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/wpa_supplicant.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/netprogs.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://hewlettpackard.github.io/wireless-tools/wireless_tools.29.tar.gz
   6. http://www.linuxfromscratch.org/patches/blfs/9.1/wireless_tools-29-fix_iwlist_scanning-1.patch
   7. http://wiki.linuxfromscratch.org/blfs/wiki/WirelessTools
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/wget.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/wpa_supplicant.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/netprogs.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
