Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 17. Networking Libraries

     * [1]Prev
       glib-networking-2.62.3
     * [2]Next
       libevent-2.1.11
     * [3]Up
     * [4]Home

ldns-1.7.1

Introduction to ldns

   ldns is a fast DNS library with the goal to simplify DNS programming
   and to allow developers to easily create software conforming to current
   RFCs and Internet drafts. This packages also includes the drill tool.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]http://www.nlnetlabs.nl/downloads/ldns/ldns-1.7.1.tar.gz
     * Download MD5 sum: 166262a46995d9972aba417fd091acd5
     * Download size: 1.2 MB
     * Estimated disk space required: 28 MB (with docs)
     * Estimated build time: 0.2 SBU (with docs)

ldns Dependencies

Optional

   [6]make-ca-1.5 and [7]libpcap-1.9.1 (for example programs),
   [8]Python-2.7.17 and [9]SWIG-4.0.1 (for Python bindings), and
   [10]Doxygen-1.8.17 (for html documentation)

   User Notes: [11]http://wiki.linuxfromscratch.org/blfs/wiki/ldns

Installation of ldns

   Install ldns by running the following commands:
./configure --prefix=/usr           \
            --sysconfdir=/etc       \
            --disable-static        \
            --with-drill            &&
make

   If you have [12]Doxygen-1.8.17 installed and want to build html
   documentation, run the following command:
make doc

   This package does not come with a working test suite.

   Now, as the root user:
make install

   If you built html documentation, install it by running the following
   commands as the root user:
install -v -m755 -d /usr/share/doc/ldns-1.7.1 &&
install -v -m644 doc/html/* /usr/share/doc/ldns-1.7.1

Command Explanations

   --disable-static: This switch prevents installation of static versions
   of the libraries.

   --with-drill: This option enables building of the drill tool (used for
   obtaining debug information from DNS(SEC))

   --disable-dane-ta-usage: This option disables DANE-TA (DNS-Based
   Authentication of Named Entities) support. It is only needed if
   OpenSSL-1.1.0 or later is not installed.

   --with-examples: This option enables building of the example programs.

   --with-pyldns: This option enables building of the Python bindings.

Contents

   Installed Programs: drill and ldns-config
   Installed Library: libldns.so and
   /usr/lib/python2.7/site-packages/_ldns.so
   Installed Directories: /usr/include/ldns and /usr/share/doc/ldns-1.7.1

Short Descriptions

   drill

   is a tool like dig from [13]BIND Utilities-9.14.10 designed to get all
   sorts of information out of the DNS.

   ldns-config

   shows compiler and linker flags for ldns usage.

   libldns.so

   provides the ldns API functions to programs.

   Last updated on 2020-02-17 12:12:55 -0800

     * [14]Prev
       glib-networking-2.62.3
     * [15]Next
       libevent-2.1.11
     * [16]Up
     * [17]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/glib-networking.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libevent.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/netlibs.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. http://www.nlnetlabs.nl/downloads/ldns/ldns-1.7.1.tar.gz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/make-ca.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libpcap.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/python2.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/swig.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/doxygen.html
  11. http://wiki.linuxfromscratch.org/blfs/wiki/ldns
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/doxygen.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/bind-utils.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/glib-networking.html
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libevent.html
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/netlibs.html
  17. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
