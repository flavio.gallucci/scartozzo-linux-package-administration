Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 12. System Utilities

     * [1]Prev
       cpio-2.13
     * [2]Next
       dbus-1.12.16
     * [3]Up
     * [4]Home

cups-pk-helper-0.2.6

Introduction to cups-pk-helper

   The cups-pk-helper package contains a PolicyKit helper used to
   configure Cups with fine-grained privileges.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://www.freedesktop.org/software/cups-pk-helper/releases/cup
       s-pk-helper-0.2.6.tar.xz
     * Download MD5 sum: a9045c7db6e502d6496da8fc71d8820e
     * Download size: 160 KB
     * Estimated disk space required: 5.9 MB
     * Estimated build time: less than 0.1 SBU

cups-pk-helper Dependencies

Required

   [6]Cups-2.3.1 and [7]Polkit-0.116

   User Notes:
   [8]http://wiki.linuxfromscratch.org/blfs/wiki/cups-pk-helper

Installation of cups-pk-helper

   Install cups-pk-helper by running the following commands:
./configure --prefix=/usr --sysconfdir=/etc &&
make

   This package does not come with a test suite.

   Now, as the root user:
make install

Contents

   Installed Programs: cups-pk-helper-mechanism (library executable)
   Installed Libraries: None
   Installed Directories: None

   Last updated on 2020-02-19 16:31:22 -0800

     * [9]Prev
       cpio-2.13
     * [10]Next
       dbus-1.12.16
     * [11]Up
     * [12]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/cpio.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/dbus.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/sysutils.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://www.freedesktop.org/software/cups-pk-helper/releases/cups-pk-helper-0.2.6.tar.xz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/pst/cups.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/polkit.html
   8. http://wiki.linuxfromscratch.org/blfs/wiki/cups-pk-helper
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/cpio.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/dbus.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/sysutils.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
