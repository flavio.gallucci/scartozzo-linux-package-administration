Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 17. Networking Libraries

     * [1]Prev
       libsoup-2.68.3
     * [2]Next
       neon-0.30.2
     * [3]Up
     * [4]Home

libtirpc-1.2.5

Introduction to libtirpc

   The libtirpc package contains libraries that support programs that use
   the Remote Procedure Call (RPC) API. It replaces the RPC, but not the
   NIS library entries that used to be in glibc.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://downloads.sourceforge.net/libtirpc/libtirpc-1.2.5.tar.bz
       2
     * Download MD5 sum: 688787ddff7c6a92ef15ae3f5dc4dfa1
     * Download size: 504 KB
     * Estimated disk space required: 8.3 MB
     * Estimated build time: 0.1 SBU

libtirpc Dependencies

Optional

   [6]MIT Kerberos V5-1.18 for the GSSAPI

   User Notes: [7]http://wiki.linuxfromscratch.org/blfs/wiki/libtirpc

Installation of libtirpc

   [Note]

Note

   If updating this package, you will also need to update any existing
   version of [8]rpcbind-1.2.5
./configure --prefix=/usr                                   \
            --sysconfdir=/etc                               \
            --disable-static                                \
            --disable-gssapi                                &&
make

   This package does not come with a test suite.

   Now, as the root user:
make install &&
mv -v /usr/lib/libtirpc.so.* /lib &&
ln -sfv ../../lib/libtirpc.so.3.0.0 /usr/lib/libtirpc.so

Command Explanations

   --disable-static: This switch prevents installation of static versions
   of the libraries.

   --disable-gssapi: This switch is needed if no GSSAPI is installed.
   Remove this switch if you have one installed (for example [9]MIT
   Kerberos V5-1.18) and you wish to use it.

   mv -v /usr/lib/libtirpc.so.* ...: Move shared libraries into /lib so
   they are available before /usr is mounted.

Contents

   Installed Programs: None
   Installed Libraries: libtirpc.so
   Installed Directory: /usr/include/tirpc

Short Descriptions

   libtirpc.so

   provides the Remote Procedure Call (RPC) API functions required by
   other programs.

   Last updated on 2020-02-15 08:54:30 -0800

     * [10]Prev
       libsoup-2.68.3
     * [11]Next
       neon-0.30.2
     * [12]Up
     * [13]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libsoup.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/neon.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/netlibs.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://downloads.sourceforge.net/libtirpc/libtirpc-1.2.5.tar.bz2
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/mitkrb.html
   7. http://wiki.linuxfromscratch.org/blfs/wiki/libtirpc
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/rpcbind.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/mitkrb.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libsoup.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/neon.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/netlibs.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
