Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 9. General Libraries

     * [1]Prev
       libblockdev-2.23
     * [2]Next
       libcroco-0.6.13
     * [3]Up
     * [4]Home

libbytesize-2.2

Introduction to libbytesize

   The libbytesize package is a library facilitates the common operations
   with sizes in bytes.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://github.com/storaged-project/libbytesize/releases/downloa
       d/2.2/libbytesize-2.2.tar.gz
     * Download MD5 sum: b3cde64313e1d0d72828a5c6729d2721
     * Download size: 436 KB
     * Estimated disk space required: 4.2 MB
     * Estimated build time: less than 0.1 SBU (including tests)

libbytesize Dependencies

Required

   [6]pcre2-10.34

Optional

   [7]GTK-Doc-1.32, [8]six-1.14.0 (needed for tests and python bindings),
   [9]pocketlint (python module for one test), and [10]polib (python
   module for one test)

   User Notes: [11]http://wiki.linuxfromscratch.org/blfs/wiki/libbytesize

Installation of libbytesize

   Install libbytesize by running the following commands:
./configure --prefix=/usr &&
make

   If you have the optional python modules installed, the regression tests
   can be run with: make check.

   Now, as the root user:
make install

Contents

   Installed Programs: bscalc
   Installed Library: libbytesize.so
   Installed Directories: /usr/include/bytesize,
   /usr/share/gtk-doc/html/libbytesize, and
   /usr/lib/python3.7/site-packages/bytesize

   Last updated on 2020-02-16 18:46:23 -0800

     * [12]Prev
       libblockdev-2.23
     * [13]Next
       libcroco-0.6.13
     * [14]Up
     * [15]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libblockdev.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libcroco.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://github.com/storaged-project/libbytesize/releases/download/2.2/libbytesize-2.2.tar.gz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/pcre2.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gtk-doc.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/python-modules.html#six
   9. https://github.com/rhinstaller/pocketlint/releases
  10. https://pypi.python.org/pypi/polib
  11. http://wiki.linuxfromscratch.org/blfs/wiki/libbytesize
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libblockdev.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libcroco.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
