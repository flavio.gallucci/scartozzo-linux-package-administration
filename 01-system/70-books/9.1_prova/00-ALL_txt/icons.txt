Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Part VI. X + Window and Display Managers

     * [1]Prev
       Other Window Managers
     * [2]Next
       Introduction
     * [3]Up
     * [4]Home

Icons

Table of Contents

     * [5]Introduction
     * [6]adwaita-icon-theme-3.34.3
     * [7]breeze-icons-5.67.0
     * [8]gnome-icon-theme-3.12.0
     * [9]gnome-icon-theme-extras-3.12.0
     * [10]gnome-icon-theme-symbolic-3.12.0
     * [11]gnome-themes-extra-3.28
     * [12]hicolor-icon-theme-0.17
     * [13]icon-naming-utils-0.8.90
     * [14]lxde-icon-theme-0.5.1
     * [15]oxygen-icons5-5.67.0

     * [16]Prev
       Other Window Managers
     * [17]Next
       Introduction
     * [18]Up
     * [19]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/other-wms.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/icons-introduction.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/x.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/icons-introduction.html
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/adwaita-icon-theme.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/breeze-icons.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gnome-icon-theme.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gnome-icon-theme-extras.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gnome-icon-theme-symbolic.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gnome-themes-extra.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/hicolor-icon-theme.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/icon-naming-utils.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lxde-icon-theme.html
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/oxygen-icons5.html
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/other-wms.html
  17. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/icons-introduction.html
  18. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/x.html
  19. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
