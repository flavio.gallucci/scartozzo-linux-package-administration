Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 27. Window Managers

     * [1]Prev
       Fluxbox-1.3.7
     * [2]Next
       openbox-3.6.1
     * [3]Up
     * [4]Home

IceWM-1.6.4

Introduction to IceWM

   IceWM is a window manager with the goals of speed, simplicity, and not
   getting in the user's way.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://github.com/ice-wm/icewm/archive/1.6.4/icewm-1.6.4.tar.gz
     * Download MD5 sum: e1756fcb05777238654f51faef8cd9d0
     * Download size: 2.1 MB
     * Estimated disk space required: 43 MB
     * Estimated build time: 0.5 SBU (Using parallelixm=4)

IceWM Dependencies

Required

   [6]X Window System and [7]gdk-pixbuf-2.40.0

Optional

   [8]FriBidi-1.0.8 (for languages written right to left),
   [9]librsvg-2.46.4, [10]libsndfile-1.0.28 and [11]alsa-lib-1.2.1.2 (for
   the experimental icesound program)

   User Notes: [12]http://wiki.linuxfromscratch.org/blfs/wiki/icewm

Installation of IceWM

   Install IceWM by running the following commands:
sed -i '/ADD_EXECUTABLE(icesh/s/yarray.cc/& ytimer.cc/' src/CMakeLists.txt &&

mkdir build &&
cd    build &&

cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release  \
      -DCFGDIR=/etc               \
      -DENABLE_LTO=ON             \
      -DCMAKE_EXE_LINKER_FLAGS='-lXrandr -lXinerama' \
      -DDOCDIR=/usr/share/doc/icewm-1.6.4  \
      .. &&
make

   This package does not have a working testsuite.

   Now, as the root user:
cp ../lib/IceWM.jpg lib &&
make install         &&
rm /usr/share/xsessions/icewm.desktop

Command Explanations

   sed ... src/CMakeLists.txt : Add a missing dependency to the build
   instructions.

   -DENABLE_LTO=ON: This option enables Link Time Optimization and is
   required for this package.

   rm /usr/share/xsessions/icewm.desktop: The package ships with both
   icewm.desktop and icewm-xsession.desktop. The former has apparently
   been used in the past, but it will hang if used with a modern
   desktop-manager such as [13]lightdm-1.30.0.

   cp ../lib/IceWM.jpg lib: This instruction fixes an install problem.

Configuring IceWM

Config Files

   ~/.icewm/keys, ~/.icewm/menu, and ~/.icewm/preferences, and
   ~/.icewm/toolbar, and ~/.icewm/winoptions. The default versions are
   installed in /usr/share/icewm/ and will be used if you have not copied
   them to ~/.icewm.

Configuration Information

   If IceWM is the only Window Manager you want to use, you can start it
   with an .xinitrc file in your home folder. Be sure to backup your
   current .xinitrc before proceeding.
echo icewm-session > ~/.xinitrc

   Now create the IceWM configuration files:
mkdir -v ~/.icewm                                       &&
cp -v /usr/share/icewm/keys ~/.icewm/keys               &&
cp -v /usr/share/icewm/menu ~/.icewm/menu               &&
cp -v /usr/share/icewm/preferences ~/.icewm/preferences &&
cp -v /usr/share/icewm/toolbar ~/.icewm/toolbar         &&
cp -v /usr/share/icewm/winoptions ~/.icewm/winoptions

   You can now edit these files to meet your requirements. In particular,
   review the preferences file. You can use Logout -> Restart-IceWM on the
   main menu to load your changed preferences, but changes to the
   background only take effect when IceWM is started.

   At this point you can either modify the traditional menu files to suit
   your requirements, or use the newer icewm-menu-fdo described later.

   The syntax of the menus is explained in the help files, which you can
   access by running help from the menu, but some of the detail is out of
   date and the default selections in the menus (a few old applications on
   the main menu, everything else on the Programs menu) will benefit from
   being updated to meet your needs. The following examples are provided
   to encourage you to think about how you wish to organise your menus.
   Please note the following:
     * If a program listed in the menu has not been installed, it will not
       appear when the menu is displayed. Similarly, if the program exists
       but the specified icon does not, no icon will be displayed in the
       menu.
     * The icons can be either .xpm or .png files, and there is no need to
       specify the extension. If the icon is located in the "library"
       (/usr/share/icewm/icons) there is no need to specifiy the path.
     * Most programs are in sub-menus, and the main menu will always
       append entries for windows, help, settings, logout at the bottom.
     * An icon for firefox was copied to the library directory and given a
       meaningful name. The icon for xine is xine.xpm which was installed
       to a pixmap directory.
     * The default toolbar is not altered.

   If you wish to use this traditional method, there are more examples in
   previous releases of this book (e.g. BLFS-7.8).

   Alternatively, you can create a menu which conforms to the FDO Desktop
   Menu Specifications, where programs can be found because they have a
   .desktop file in the XDG_DATA_HOME or XDG_DATA_DIR directories. Unlike
   most windowmanagers, icewm does not search for programs when the menu
   is invoked, so if you take this route you will need to rerun the
   following command after installing or removing programs:
icewm-menu-fdo >~/.icewm/menu

   If you wish to put icons on your desktop, you will need to install a
   program such as [14]Rox-Filer-2.11 which provides a pinboard. If you do
   that you will no longer be able to access the menu by right-clicking on
   the desktop, you will have to use the IceWM button. To ensure that the
   rox pinboard is running, the following commands will put it in the
   startup file:
cat > ~/.icewm/startup << "EOF"
rox -p Default &
EOF &&
chmod +x ~/.icewm/startup

   [Tip]

Tip

   There are a number of keyboard shortcuts in IceWM:
     * Ctrl + Alt + FN : go to ttyN.
     * Ctrl + Alt + N : go to desktop number N
     * Ctrl + Alt + Space : open a box on the taskbar where you can key in
       the name of an application and run it.

Contents

   Installed Programs: icehelp, icesh, icesound, icewm, icewm-menu-fdo,
   icewm-session, icewm-set-gnomewm, icewmbg, icewmhint, icewmtray
   Installed Libraries: None
   Installed Directories: /usr/share/doc/icewm-1.6.4, /usr/share/icewm and
   ~/.icewm

Short Descriptions

   icehelp

   is used to display the html manual.

   icesh

   is a command-line window manager which can be used in ~/.icewm/startup.

   icesound

   plays audio files on GUI events raised by IceWM.

   icewm

   is the window manager.

   icewm-menu-fdo

   can create a file in a format suitable for an IceWM menu, which lists
   those programs currently installed in a layout conforming to the FDO
   Desktop Menu Specifications.

   icewm-session

   runs icewmbg, icewm, icewmtray, startup, shutdown (i.e. startup and
   shutdown scripts are run if installed).

   icewm-set-gnomewm

   is a script to set the GNOMEwindowmanager to icewm using gconftool.

   icewmbg

   is used to set the background, according to the various
   DesktopBackground settings in the preferences.

   icewmhint

   is used internally.

   icewmtray

   provides the tray.

   Last updated on 2020-02-16 15:50:16 -0800

     * [15]Prev
       Fluxbox-1.3.7
     * [16]Next
       openbox-3.6.1
     * [17]Up
     * [18]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/fluxbox.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/openbox.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/wm.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://github.com/ice-wm/icewm/archive/1.6.4/icewm-1.6.4.tar.gz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/installing.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gdk-pixbuf.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/fribidi.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/librsvg.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/multimedia/libsndfile.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/multimedia/alsa-lib.html
  12. http://wiki.linuxfromscratch.org/blfs/wiki/icewm
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lightdm.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/xsoft/rox-filer.html
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/fluxbox.html
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/openbox.html
  17. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/wm.html
  18. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
