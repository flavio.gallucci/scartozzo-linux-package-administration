Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 13. Programming

     * [1]Prev
       Subversion-1.13.0
     * [2]Next
       SWIG-4.0.1
     * [3]Up
     * [4]Home

Running a Subversion Server

Running a Subversion Server

   This section will describe how to set up, administer and secure a
   Subversion server.

Subversion Server Dependencies

Required

   [5]Subversion-1.13.0 and [6]OpenSSH-8.2p1

Setting up a Subversion Server.

   The following instructions will install a Subversion server, which will
   be set up to use OpenSSH as the secure remote access method, with
   svnserve available for anonymous access.

   Configuration of the Subversion server consists of the following steps:

1. Setup Users, Groups, and Permissions

   You'll need to be user root for the initial portion of configuration.
   Create the svn user and group with the following commands:
groupadd -g 56 svn &&
useradd -c "SVN Owner" -d /home/svn -m -g svn -s /bin/false -u 56 svn

   If you plan to have multiple repositories, you should have a group
   dedicated to each repository for ease of administration. Create the
   svntest group for the test repository and add the svn user to that
   group with the following commands:
groupadd -g 57 svntest &&
usermod -G svntest -a svn

   Additionally you should set umask 002 while working with a repository
   so that all new files will be writable by owner and group. This is made
   mandatory by creating a wrapper script for svn and svnserve:
mv /usr/bin/svn /usr/bin/svn.orig &&
mv /usr/bin/svnserve /usr/bin/svnserve.orig &&
cat >> /usr/bin/svn << "EOF"
#!/bin/sh
umask 002
/usr/bin/svn.orig "$@"
EOF
cat >> /usr/bin/svnserve << "EOF"
#!/bin/sh
umask 002
/usr/bin/svnserve.orig "$@"
EOF
chmod 0755 /usr/bin/svn{,serve}

   [Note]

Note

   If you use Apache for working with the repository over HTTP, even for
   anonymous access, you should wrap /usr/sbin/httpd in a similar script.

2. Create a Subversion repository.

   There are several ways to set up a subversion repository. It is
   recommended to have a look at the [7]SVN Book corresponding chapter. A
   basic repository can be set up with the instructions below.

   Create a new Subversion repository with the following commands (as the
   root user):
install -v -m 0755 -d /srv/svn &&
install -v -m 0755 -o svn -g svn -d /srv/svn/repositories &&
svnadmin create /srv/svn/repositories/svntest

   Now that the repository is created, it should be populated with
   something useful. You'll need to have a predefined directory layout set
   up exactly as you want your repository to look. For example, here is a
   sample BLFS layout setup with a root of svntest/. You'll need to setup
   a directory tree similar to the following:
svntest/            # The name of the repository
   trunk/           # Contains the existing source tree
      BOOK/
      bootscripts/
      edguide/
      patches/
      scripts/
   branches/        # Needed for additional branches
   tags/            # Needed for tagging release points

   Once you've created your directory layout as shown above, you are ready
   to do the initial import:
svn import -m "Initial import." \
    </path/to/source/tree>      \
    file:///srv/svn/repositories/svntest

   Now change owner and group information on the repository, and add an
   unprivileged user to the svn and svntest groups:
chown -R svn:svntest /srv/svn/repositories/svntest    &&
chmod -R g+w         /srv/svn/repositories/svntest    &&
chmod g+s            /srv/svn/repositories/svntest/db &&
usermod -G svn,svntest -a <username>

   svntest is the group assigned to the svntest repository. As mentioned
   earlier, this eases administration of multiple repositories when using
   OpenSSH for authentication. Going forward, you'll need to add your
   unprivileged user, and any additional users that you wish to have write
   access to the repository, to the svn and svntest groups.

   In addition, you'll notice that the new repository's db directory is
   set-groupID. If the reasoning is not immediately obvious, when using
   any external authentication method (such as ssh), the sticky bit is set
   so that all new files will be owned by the user, but group of svntest.
   Anyone in the svntest group can create files, but still give the entire
   group write access to those files. This avoids locking out other users
   from the repository.

   Now, return to an unprivileged user account, and take a look at the new
   repository using svnlook:
svnlook tree /srv/svn/repositories/svntest/

   [Note]

Note

   You may need to log out and back in again to refresh your group
   memberships. 'su <username>' should work as well.

3. Configure the Server

   As mentioned previously, these instructions will configure the server
   to use only ssh for write access to the repository and to provide
   anonymous access using svnserve. There are several other ways to
   provide access to the repository. These additional configurations are
   best explained at [8]http://svnbook.red-bean.com/.

   Access configuration needs to be done for each repository. Create the
   svnserve.conf file for the svntest repository using the following
   commands:
cp /srv/svn/repositories/svntest/conf/svnserve.conf \
   /srv/svn/repositories/svntest/conf/svnserve.conf.default &&

cat > /srv/svn/repositories/svntest/conf/svnserve.conf << "EOF"
[general]
anon-access = read
auth-access = write
EOF

   There is not a lot to the configuration file at all. You'll notice that
   only the general section is required. Take a look at the
   svnserve.conf.default file for information on using svnserve's built-in
   authentication method.

4. Starting the Server

   To start the server at boot time, install the svn bootscript included
   in the [9]blfs-bootscripts-20191204 package.
make install-svn

   Last updated on 2016-06-08 20:56:23 -0700

     * [10]Prev
       Subversion-1.13.0
     * [11]Next
       SWIG-4.0.1
     * [12]Up
     * [13]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/subversion.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/swig.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/prog.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/subversion.html
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/openssh.html
   7. http://svnbook.red-bean.com/nightly/en/svn.reposadmin.html
   8. http://svnbook.red-bean.com/
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/introduction/bootscripts.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/subversion.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/swig.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/prog.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
