Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Part II. Post LFS Configuration and Extra Software

     * [1]Prev
       volume_key-0.3.12
     * [2]Next
       About initramfs
     * [3]Up
     * [4]Home

File Systems and Disk Management

   Journaling file systems reduce the time needed to recover a file system
   that was not unmounted properly. While this can be extremely important
   in reducing downtime for servers, it has also become popular for
   desktop environments. This chapter contains other journaling file
   systems you can use instead of the default LFS extended file system
   (ext2/3/4). It also provides introductory material on managing disk
   arrays.

Table of Contents

     * [5]About initramfs
     * [6]btrfs-progs-5.4.1
     * [7]dosfstools-4.1
     * [8]Fuse-3.9.0
     * [9]jfsutils-1.1.15
     * [10]LVM2-2.03.08
     * [11]About Logical Volume Management (LVM)
     * [12]About RAID
     * [13]mdadm-4.1
     * [14]ntfs-3g-2017.3.23
     * [15]gptfdisk-1.0.5
     * [16]parted-3.3
     * [17]reiserfsprogs-3.6.27
     * [18]smartmontools-7.1
     * [19]sshfs-3.7.0
     * [20]xfsprogs-5.4.0

     * [21]Prev
       volume_key-0.3.12
     * [22]Next
       About initramfs
     * [23]Up
     * [24]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/volume_key.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/initramfs.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/postlfs.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/initramfs.html
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/btrfs-progs.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/dosfstools.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/fuse.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/jfsutils.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lvm2.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/aboutlvm.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/raid.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/mdadm.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/ntfs-3g.html
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gptfdisk.html
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/parted.html
  17. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/reiserfs.html
  18. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/smartmontools.html
  19. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/sshfs.html
  20. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/xfsprogs.html
  21. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/volume_key.html
  22. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/initramfs.html
  23. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/postlfs.html
  24. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
