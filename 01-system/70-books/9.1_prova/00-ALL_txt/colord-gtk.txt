Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 25. X Libraries

     * [1]Prev
       clutter-gtk-1.8.4
     * [2]Next
       FLTK-1.3.5
     * [3]Up
     * [4]Home

colord-gtk-0.2.0

Introduction to Colord GTK

   The Colord GTK package contains GTK+ bindings for Colord.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://www.freedesktop.org/software/colord/releases/colord-gtk-
       0.2.0.tar.xz
     * Download MD5 sum: 66d048803c8b89e5e63da4b461484933
     * Download size: 20 KB
     * Estimated disk space required: 41 MB
     * Estimated build time: less than 0.1 SBU

Colord GTK Dependencies

Required

   [6]colord-1.4.4 and [7]GTK+-3.24.13

Recommended

   [8]gobject-introspection-1.62.0 and [9]Vala-0.46.6

Optional

   [10]DocBook-utils-0.6.14, [11]GTK+-2.24.32 and [12]GTK-Doc-1.32

   User Notes: [13]http://wiki.linuxfromscratch.org/blfs/wiki/colord-gtk

Installation of Colord GTK

   [Warning]

Warning

   If building the documentation make -j1 must be used.

   Install Colord GTK by running the following commands:
mkdir build &&
cd    build &&

meson --prefix=/usr  \
      -Dgtk2=true    \
      -Dvapi=true    \
      -Ddocs=false   \
      -Dman=false .. &&
ninja

   To test the results, issue: ninja test. The tests need to be run from
   an X session, and may require a color profile for your primary display.

   Now, as the root user:
ninja install

Command Explanations

   -Dvapi=true: This switch enables building of the Vala bindings. Remove
   if you don't have [14]Vala-0.46.6 installed.

   -Dgtk2=true: This switch enables building the GTK+-2 bindings for
   colord.

   -Ddocs=false: This switch disables building GTK-DOC based
   documentation. Even if GTK-DOC is installed, you will need the
   namespaced versions of the Docbook XSL stylesheets.

   -Dman=false: This switch disables generating the manual pages for this
   package. Remove this switch if you have namespaced versions of the
   Docbook XSL stylesheets installed.

Contents

   Installed Programs: cd-convert
   Installed Libraries: libcolord-gtk.so and libcolord-gtk2.so
   Installed Directories: /usr/include/colord-1/colord-gtk and
   /usr/share/gtk-doc/html/colord-gtk

Short Descriptions

   cd-convert

   is a Color Manager Testing Tool.

   libcolord-gtk.so

   contains the Colord GTK+ bindings.

   libcolord-gtk2.so

   contains the Colord GTK+-2 bindings.

   Last updated on 2020-02-18 14:50:03 -0800

     * [15]Prev
       clutter-gtk-1.8.4
     * [16]Next
       FLTK-1.3.5
     * [17]Up
     * [18]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/clutter-gtk.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/fltk.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lib.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://www.freedesktop.org/software/colord/releases/colord-gtk-0.2.0.tar.xz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/colord.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gtk3.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/gobject-introspection.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/vala.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/pst/docbook-utils.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gtk2.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/gtk-doc.html
  13. http://wiki.linuxfromscratch.org/blfs/wiki/colord-gtk
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/vala.html
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/clutter-gtk.html
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/fltk.html
  17. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lib.html
  18. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
