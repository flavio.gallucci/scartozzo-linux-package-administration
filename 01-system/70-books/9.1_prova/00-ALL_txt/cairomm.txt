Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 25. X Libraries

     * [1]Prev
       Cairo-1.17.2+f93fc72c03e
     * [2]Next
       Cogl-1.22.4
     * [3]Up
     * [4]Home

Cairomm-1.12.2

Introduction to Cairomm

   The Cairomm package provides a C++ interface to Cairo.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://www.cairographics.org/releases/cairomm-1.12.2.tar.gz
     * Download MD5 sum: 9d2282ea34cf9aaa89208bb4bb911909
     * Download size: 1.3 MB
     * Estimated disk space required: 11 MB
     * Estimated build time: 0.1 SBU

Cairomm Dependencies

Required

   [6]Cairo-1.17.2+f93fc72c03e and [7]libsigc++-2.10.2

Optional

   [8]Boost-1.72.0 and [9]Doxygen-1.8.17

   User Notes: [10]http://wiki.linuxfromscratch.org/blfs/wiki/cairomm

Installation of Cairomm

   First, fix the documentation directory name:
sed -e '/^libdocdir =/ s/$(book_name)/cairomm-1.12.2/' \
    -i docs/Makefile.in

   Install Cairomm by running the following commands:
./configure --prefix=/usr &&
make

   This package does not come with a test suite.

   Now, as the root user:
make install

Contents

   Installed Programs: None
   Installed Library: libcairomm-1.0.so
   Installed Directories: /usr/{include,lib}/cairomm-1.0 and
   /usr/share/{devhelp/books/cairomm-1.0,doc/cairomm-1.12.2}

Short Descriptions

   libcairomm-1.0.so

   contains the Cairo API classes.

   Last updated on 2020-02-17 18:27:03 -0800

     * [11]Prev
       Cairo-1.17.2+f93fc72c03e
     * [12]Next
       Cogl-1.22.4
     * [13]Up
     * [14]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/cairo.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/cogl.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lib.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://www.cairographics.org/releases/cairomm-1.12.2.tar.gz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/cairo.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/libsigc.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/boost.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/doxygen.html
  10. http://wiki.linuxfromscratch.org/blfs/wiki/cairomm
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/cairo.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/cogl.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lib.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
