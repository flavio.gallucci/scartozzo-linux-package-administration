Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 9. General Libraries

     * [1]Prev
       libgcrypt-1.8.5
     * [2]Next
       libgrss-0.7.0
     * [3]Up
     * [4]Home

libgpg-error-1.37

Introduction to libgpg-error

   The libgpg-error package contains a library that defines common error
   values for all GnuPG components.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://www.gnupg.org/ftp/gcrypt/libgpg-error/libgpg-error-1.37.
       tar.bz2
     * Download (FTP):
       [6]ftp://ftp.gnupg.org/gcrypt/libgpg-error/libgpg-error-1.37.tar.bz
       2
     * Download MD5 sum: 729f22d917494fdc4b54fce5aa6547c7
     * Download size: 916 KB
     * Estimated disk space required: 9.9 MB (with tests)
     * Estimated build time: 0.1 SBU (with tests)

   User Notes: [7]http://wiki.linuxfromscratch.org/blfs/wiki/libgpg-error

Installation of libgpg-error

   Install libgpg-error by running the following commands:
./configure --prefix=/usr &&
make

   To test the results, issue: make check.

   Now, as the root user:
make install &&
install -v -m644 -D README /usr/share/doc/libgpg-error-1.37/README

Contents

   Installed Programs: gpg-error, gpg-error-config, gpgrt-config, and
   yat2m
   Installed Library: libgpg-error.so
   Installed Directories: /usr/share/common-lisp/source/gpg-error,
   /usr/share/libgpg-error, and /usr/share/doc/libgpg-error-1.37

Short Descriptions

   gpg-error

   is used to determine libgpg-error error codes.

   gpg-error-config

   is a utility used to configure and build applications based on the
   libgpg-error library. It can be used to query the C compiler and linker
   flags which are required to correctly compile and link the application
   against the libgpg-error library.

   gpgrt-config

   is the same script as gpg-error-config.

   yat2m

   extracts man pages from a Texinfo source.

   libgpg-error.so

   contains the libgpg-error API functions.

   Last updated on 2020-02-16 15:15:05 -0800

     * [8]Prev
       libgcrypt-1.8.5
     * [9]Next
       libgrss-0.7.0
     * [10]Up
     * [11]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libgcrypt.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libgrss.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://www.gnupg.org/ftp/gcrypt/libgpg-error/libgpg-error-1.37.tar.bz2
   6. ftp://ftp.gnupg.org/gcrypt/libgpg-error/libgpg-error-1.37.tar.bz2
   7. http://wiki.linuxfromscratch.org/blfs/wiki/libgpg-error
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libgcrypt.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libgrss.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
