Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 25. X Libraries

     * [1]Prev
       Cairomm-1.12.2
     * [2]Next
       Clutter-1.26.2
     * [3]Up
     * [4]Home

Cogl-1.22.4

Introduction to Cogl

   Cogl is a modern 3D graphics API with associated utility APIs designed
   to expose the features of 3D graphics hardware using a direct state
   access API design, as opposed to the state-machine style of OpenGL.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]http://ftp.gnome.org/pub/gnome/sources/cogl/1.22/cogl-1.22.4.tar
       .xz
     * Download (FTP):
       [6]ftp://ftp.gnome.org/pub/gnome/sources/cogl/1.22/cogl-1.22.4.tar.
       xz
     * Download MD5 sum: 31755015a865c4af51ac84f6e53af8ab
     * Download size: 1.6 MB
     * Estimated disk space required: 55 MB
     * Estimated build time: 0.3 SBU (Using parallelism=4; add 0.5 SBU for
       tests)

Cogl Dependencies

Required

   [7]Cairo-1.17.2+f93fc72c03e, [8]gdk-pixbuf-2.40.0, [9]GLU-9.0.1,
   [10]Mesa-19.3.4, [11]Pango-1.44.7, and [12]Wayland-1.18.0

Recommended

   [13]gobject-introspection-1.62.0

Optional

   [14]gst-plugins-base-1.16.2, [15]GTK-Doc-1.32, [16]SDL-1.2.15, and
   [17]SDL2-2.0.10

   User Notes: [18]http://wiki.linuxfromscratch.org/blfs/wiki/cogl

Installation of Cogl

   Install Cogl by running the following commands:
   [Note]

Note

   This package may occasionally fail when building with multiple
   processors. See [19]Using Multiple Processors for more information.
./configure --prefix=/usr --enable-gles1 --enable-gles2         \
    --enable-{kms,wayland,xlib}-egl-platform                    \
    --enable-wayland-egl-server                                 &&
make

   To test the results, issue: make check. The tests should be run from an
   X terminal on the hardware accelerated Xorg Server. A few tests will
   use all CPUs in parallel, regardless of parallellism settings.

   Now, as the root user:
make install

Command Explanations

   --enable-gles1: This switch enables support for OpenGL ES 1.1.

   --enable-gles2: This switch enables support for OpenGL ES 2.0.

   --enable-{kms,wayland,xlib}-egl-platform: These switches enable support
   for KMS, Wayland and Xlib EGL platforms. They are required for GNOME
   Wayland support.

   --enable-wayland-egl-server: This switch enables Cogl's Wayland Server
   API which is required for GNOME Wayland support.

   --enable-cogl-gst: This switch enables gstreamer support.

   --enable-gtk-doc: Use this parameter if GTK-Doc is installed and you
   wish to rebuild and install the API documentation.

Contents

   Installed Programs: None
   Installed Libraries: libcogl-gles2.so, libcogl-pango.so,
   libcogl-path.so, and libcogl.so, and optional libraries libcogl-gst.so
   and /usr/lib/gstreamer-1.0/libgstcogl.so
   Installed Directories: /usr/include/cogl and /usr/share/cogl

Short Descriptions

   libcogl-gles2.so

   is the OpenGL ES 2.0 integration library for Cogl.

   libcogl-pango.so

   is the Pango integration library for Cogl.

   libcogl.so

   is an object oriented GL/GLES Abstraction/Utility Layer library.

   Last updated on 2020-02-17 18:27:03 -0800

     * [20]Prev
       Cairomm-1.12.2
     * [21]Next
       Clutter-1.26.2
     * [22]Up
     * [23]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/cairomm.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/clutter.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lib.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. http://ftp.gnome.org/pub/gnome/sources/cogl/1.22/cogl-1.22.4.tar.xz
   6. ftp://ftp.gnome.org/pub/gnome/sources/cogl/1.22/cogl-1.22.4.tar.xz
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/cairo.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gdk-pixbuf.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/glu.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/mesa.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/pango.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/wayland.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/gobject-introspection.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/multimedia/gst10-plugins-base.html
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/gtk-doc.html
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/multimedia/sdl.html
  17. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/multimedia/sdl2.html
  18. http://wiki.linuxfromscratch.org/blfs/wiki/cogl
  19. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/introduction/notes-on-building.html#parallel-builds
  20. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/cairomm.html
  21. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/clutter.html
  22. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lib.html
  23. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
