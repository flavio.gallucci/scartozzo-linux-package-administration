Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 9. General Libraries

     * [1]Prev
       General Libraries
     * [2]Next
       Apr-Util-1.6.1
     * [3]Up
     * [4]Home

Apr-1.7.0

Introduction to Apr

   The Apache Portable Runtime (APR) is a supporting library for the
   Apache web server. It provides a set of application programming
   interfaces (APIs) that map to the underlying Operating System (OS).
   Where the OS doesn't support a particular function, APR will provide an
   emulation. Thus programmers can use the APR to make a program portable
   across different platforms.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://archive.apache.org/dist/apr/apr-1.7.0.tar.bz2
     * Download (FTP):
       [6]ftp://ftp.mirrorservice.org/sites/ftp.apache.org/apr/apr-1.7.0.t
       ar.bz2
     * Download MD5 sum: 7a14a83d664e87599ea25ff4432e48a7
     * Download size: 852 KB
     * Estimated disk space required: 11 MB (additional 4 MB for the
       tests)
     * Estimated build time: 0.2 SBU (add 1.7 SBU for tests)

   User Notes: [7]http://wiki.linuxfromscratch.org/blfs/wiki/apr

Installation of Apr

   Install Apr by running the following commands:
./configure --prefix=/usr    \
            --disable-static \
            --with-installbuilddir=/usr/share/apr-1/build &&
make

   To test the results, issue: make test.

   Now, as the root user:
make install

Command Explanations

   --disable-static: This switch prevents installation of static versions
   of the libraries.

Contents

   Installed Program: apr-1-config
   Installed Library: libapr-1.so
   Installed Directories: /usr/include/apr-1 and /usr/share/apr-1

Short Descriptions

   apr-1-config

   is a shell script used to retrieve information about the apr library in
   the system. It is typically used to compile and link against the
   library.

   libapr-1.so

   is the Apache Portable Runtime library.

   Last updated on 2020-02-15 08:54:30 -0800

     * [8]Prev
       General Libraries
     * [9]Next
       Apr-Util-1.6.1
     * [10]Up
     * [11]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/apr-util.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://archive.apache.org/dist/apr/apr-1.7.0.tar.bz2
   6. ftp://ftp.mirrorservice.org/sites/ftp.apache.org/apr/apr-1.7.0.tar.bz2
   7. http://wiki.linuxfromscratch.org/blfs/wiki/apr
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/apr-util.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
