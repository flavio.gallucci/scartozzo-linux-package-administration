Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 42. Multimedia Libraries and Drivers

     * [1]Prev
       libtheora-1.1.1
     * [2]Next
       libvpx-1.8.2
     * [3]Up
     * [4]Home

libvorbis-1.3.6

Introduction to libvorbis

   The libvorbis package contains a general purpose audio and music
   encoding format. This is useful for creating (encoding) and playing
   (decoding) sound in an open (patent free) format.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://downloads.xiph.org/releases/vorbis/libvorbis-1.3.6.tar.x
       z
     * Download MD5 sum: b7d1692f275c73e7833ed1cc2697cd65
     * Download size: 1.1 MB
     * Estimated disk space required: 12 MB
     * Estimated build time: 0.1 SBU

libvorbis Dependencies

Required

   [6]libogg-1.3.4

Optional

   [7]Doxygen-1.8.17 and [8]texlive-20190410 (or [9]install-tl-unx)
   (specifically, pdflatex and htlatex) to build the PDF documentation

   User Notes: [10]http://wiki.linuxfromscratch.org/blfs/wiki/Libvorbis

Installation of libvorbis

   Install libvorbis by running the following commands:
./configure --prefix=/usr --disable-static &&
make

   To test the results, issue: make check.

   Now, as the root user:
make install &&
install -v -m644 doc/Vorbis* /usr/share/doc/libvorbis-1.3.6

Command Explanations

   --disable-static: This switch prevents installation of static versions
   of the libraries.

   --enable-docs: This switch enables building the documentation in
   formats other than the supplied html.

Contents

   Installed Programs: None
   Installed Libraries: libvorbis.so, libvorbisenc.so and libvorbisfile.so
   Installed Directories: /usr/include/vorbis and
   /usr/share/doc/libvorbis-1.3.6

Short Descriptions

   libvorbis.so

   provides the functions used to read and write sound files.

   Last updated on 2020-02-17 18:27:03 -0800

     * [11]Prev
       libtheora-1.1.1
     * [12]Next
       libvpx-1.8.2
     * [13]Up
     * [14]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libtheora.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libvpx.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libdriv.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://downloads.xiph.org/releases/vorbis/libvorbis-1.3.6.tar.xz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libogg.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/doxygen.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/pst/texlive.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/pst/tl-installer.html
  10. http://wiki.linuxfromscratch.org/blfs/wiki/Libvorbis
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libtheora.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libvpx.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libdriv.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
