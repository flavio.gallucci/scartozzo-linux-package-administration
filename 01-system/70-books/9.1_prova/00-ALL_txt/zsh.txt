Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 7. Shells

     * [1]Prev
       Tcsh-6.22.02
     * [2]Next
       Virtualization
     * [3]Up
     * [4]Home

zsh-5.8

Introduction to zsh

   The zsh package contains a command interpreter (shell) usable as an
   interactive login shell and as a shell script command processor. Of the
   standard shells, zsh most closely resembles ksh but includes many
   enhancements.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP): [5]http://www.zsh.org/pub/zsh-5.8.tar.xz
     * Download MD5 sum: e02a5428620b3dd268800c7843b3dd4d
     * Download size: 3.0 MB
     * Estimated disk space required: 72 MB (includes documentation and
       tests)
     * Estimated build time: 1.3 SBU (Using parallelism=4; includes
       documentation and tests)

Additional Downloads

     * Optional Documentation:
       [6]http://www.zsh.org/pub/zsh-5.8-doc.tar.xz
     * Documentation MD5 sum: ef8514401a81bb1a4c4b655ebda8a1a2
     * Documentation download size: 3.0 MB

   [Note]

Note

   When there is a new zsh release, the old files shown above are moved to
   a new server directory: [7]http://www.zsh.org/pub/old/.

zsh Dependencies

Optional

   [8]libcap-2.31 with PAM, [9]PCRE-8.44, and [10]Valgrind-3.15.0,

   User Notes: [11]http://wiki.linuxfromscratch.org/blfs/wiki/zsh

Installation of zsh

   If you downloaded the optional documentation, unpack it with the
   following command:
tar --strip-components=1 -xvf ../zsh-5.8-doc.tar.xz

   Install zsh by running the following commands:
./configure --prefix=/usr         \
            --bindir=/bin         \
            --sysconfdir=/etc/zsh \
            --enable-etcdir=/etc/zsh                  &&
make                                                  &&

makeinfo  Doc/zsh.texi --plaintext -o Doc/zsh.txt     &&
makeinfo  Doc/zsh.texi --html      -o Doc/html        &&
makeinfo  Doc/zsh.texi --html --no-split --no-headers -o Doc/zsh.html

   If you have [12]texlive-20190410 installed, you can build PDF format of
   the documentation by issuing the following command:
texi2pdf  Doc/zsh.texi -o Doc/zsh.pdf

   To test the results, issue: make check.

   Now, as the root user:
make install                              &&
make infodir=/usr/share/info install.info &&

install -v -m755 -d                 /usr/share/doc/zsh-5.8/html &&
install -v -m644 Doc/html/*         /usr/share/doc/zsh-5.8/html &&
install -v -m644 Doc/zsh.{html,txt} /usr/share/doc/zsh-5.8

   If you downloaded the optional documentation, install it by issuing the
   following commands as the root user:
make htmldir=/usr/share/doc/zsh-5.8/html install.html &&
install -v -m644 Doc/zsh.dvi /usr/share/doc/zsh-5.8

   If you built the PDF format of the documentation, install it by issuing
   the following command as the root user:
install -v -m644 Doc/zsh.pdf /usr/share/doc/zsh-5.8

Command Explanations

   --sysconfdir=/etc/zsh and --enable-etcdir=/etc/zsh: These parameters
   are used so that all the zsh configuration files are consolidated into
   the /etc/zsh directory. Omit these parameters if you wish to retain
   historical compatibility by having all the files located in the /etc
   directory.

   --bindir=/bin: This parameter places the zsh binaries into the root
   filesystem.

   --enable-cap: This option enables POSIX capabilities.

   --disable-gdbm: This option disables the use of the GDBM library.

   --enable-pcre: This option allows zsh to use the PCRE regular
   expression library in shell builtins.

Multiple partitions

   Linking zsh dynamically against pcre and/or gdbm produces runtime
   dependencies on libpcre.so and/or libgdbm.so respectively, which both
   reside in /usr hierarchy. If /usr is a separate mount point and zsh
   needs to be available in boot time, then its supporting libraries
   should be in /lib too. You can move the libraries as follows:
mv -v /usr/lib/libpcre.so.* /lib &&
ln -v -sf ../../lib/libpcre.so.0 /usr/lib/libpcre.so

mv -v /usr/lib/libgdbm.so.* /lib &&
ln -v -sf ../../lib/libgdbm.so.3 /usr/lib/libgdbm.so

   Alternatively you can statically link zsh against pcre and gdbm if you
   modify the config.modules file (you need first to run configure to
   generate it).

Configuring zsh

Config Files

   There are a whole host of configuration files for zsh including
   /etc/zsh/zshenv, /etc/zsh/zprofile, /etc/zsh/zshrc, /etc/zsh/zlogin and
   /etc/zsh/zlogout. You can find more information on these in the zsh(1)
   and related manual pages.

   The first time zsh is executed, you will be prompted by messages asking
   several questions. The answers will be used to create a ~/.zshrc file.
   If you wish to run these questions again, run zsh
   /usr/share/zsh/5.8/functions/zsh-newuser-install -f.

   There are several built-in advanced prompts. In the zsh shell, start
   advanced prompt support with autoload -U promptinit, then promptinit.
   Available prompt names are listed with prompt -l. Select a particular
   one with prompt <prompt-name>. Display all available prompts with
   prompt -p. Except for the list and display commands above, you can
   insert the other ones in ~/.zshrc to be automatically executed at shell
   start, with the prompt you chose.

Configuration Information

   Update /etc/shells to include the zsh shell program names (as the root
   user):
cat >> /etc/shells << "EOF"
/bin/zsh
EOF

Contents

   Installed Programs: zsh and zsh-5.8 (hardlinked to each other)
   Installed Libraries: Numerous plugin helper modules under
   /usr/lib/zsh/5.8/
   Installed Directories: /usr/{lib,share}/zsh and /usr/share/doc/zsh-5.8

Short Description

   zsh

   is a shell which has command-line editing, built-in spelling
   correction, programmable command completion, shell functions (with
   autoloading), a history mechanism, and a host of other features.

   Last updated on 2020-02-17 18:27:03 -0800

     * [13]Prev
       Tcsh-6.22.02
     * [14]Next
       Virtualization
     * [15]Up
     * [16]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/tcsh.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/virtualization.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/shells.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. http://www.zsh.org/pub/zsh-5.8.tar.xz
   6. http://www.zsh.org/pub/zsh-5.8-doc.tar.xz
   7. http://www.zsh.org/pub/old/
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libcap.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/pcre.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/valgrind.html
  11. http://wiki.linuxfromscratch.org/blfs/wiki/zsh
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/pst/texlive.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/tcsh.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/virtualization.html
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/shells.html
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
