Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 9. General Libraries

     * [1]Prev
       Boost-1.72.0
     * [2]Next
       CLucene-2.3.3.4
     * [3]Up
     * [4]Home

brotli-1.0.7

Introduction to Brotli

   Brotli provides a general-purpose lossless compression algorithm that
   compresses data using a combination of a modern variant of the LZ77
   algorithm, Huffman coding and 2nd order context modeling. Its libraries
   are particularly used for WOFF2 fonts on webpages.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://github.com/google/brotli/archive/v1.0.7/brotli-v1.0.7.ta
       r.gz
     * Download MD5 sum: 7b6edd4f2128f22794d0ca28c53898a5
     * Download size: 23 MB
     * Estimated disk space required: 43 MB (add 5 MB if installing both
       sets of python bindings, add 9 MB for the main test and 5MB for
       testing the bindings)
     * Estimated build time: 0.2 SBU (add 0.3 SBU for the python bindings,
       and 1.2 SBU if testing them)

Brotli Dependencies

Required

   [6]CMake-3.16.4

Optional

   [7]Lua-5.3.5 (to create Lua bindings) and [8]Python-2.7.17 (to create
   python2 bindings)

   User Notes: [9]http://wiki.linuxfromscratch.org/blfs/wiki/brotli

Installation of Brotli

   Install brotli by running the following commands:
mkdir out &&
cd    out &&

cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release  \
      ..  &&
make

   To test the results, issue: make test.

   If desired, either or both sets of python bindings can be built and
   installed without any conflicts. If you need the Python2 bindings, add
   or substitute python2 for python3 in the following instructions:
pushd ..               &&
python3 setup.py build &&
popd

   If you wish to test the bindings, go back to the top-level directory
   and issue: python3 setup.py test.

   Now, as the root user:
make install &&
cd ..

   If you have built either or both sets of python bindings, install them
   as the root user (as before, use the appropriate version(s) of python:
python3 setup.py install --optimize=1

Contents

   Installed Programs: brotli
   Installed Libraries: libbrotlicommon{-static.a,.so},
   libbrotlidec{,-static,.so} and libbrotlienc{,-static,.so}
   Installed Directory: /usr/include/brotli

Short Descriptions

   brotli

   can compress or decompress files, or test the integrity of compressed
   files.

   libbrotlicommon{-static.a,.so}

   is the Brotli common dictionary library.

   libbrotlidec{-static.a,.so}

   is the Brotli decoder library.

   libbrotlienc{-static.a,.so}

   is the Brotli common encoder library.

   Last updated on 2020-02-16 18:46:23 -0800

     * [10]Prev
       Boost-1.72.0
     * [11]Next
       CLucene-2.3.3.4
     * [12]Up
     * [13]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/boost.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/clucene.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://github.com/google/brotli/archive/v1.0.7/brotli-v1.0.7.tar.gz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/cmake.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lua.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/python2.html
   9. http://wiki.linuxfromscratch.org/blfs/wiki/brotli
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/boost.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/clucene.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
