Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 42. Multimedia Libraries and Drivers

     * [1]Prev
       libsamplerate-0.1.9
     * [2]Next
       libtheora-1.1.1
     * [3]Up
     * [4]Home

libsndfile-1.0.28

Introduction to libsndfile

   Libsndfile is a library of C routines for reading and writing files
   containing sampled audio data.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]http://www.mega-nerd.com/libsndfile/files/libsndfile-1.0.28.tar.
       gz
     * Download MD5 sum: 646b5f98ce89ac60cdb060fcd398247c
     * Download size: 1.1 MB
     * Estimated disk space required: 34 MB (with tests)
     * Estimated build time: 0.6 SBU (with tests)

libsndfile Dependencies

Recommended

   [6]FLAC-1.3.3, [7]libogg-1.3.4, and [8]libvorbis-1.3.6

Optional

   [9]alsa-lib-1.2.1.2 and [10]SQLite-3.31.1

   User Notes: [11]http://wiki.linuxfromscratch.org/blfs/wiki/libsndfile

Installation of libsndfile

   Install libsndfile by running the following commands:
./configure --prefix=/usr    \
            --disable-static \
            --docdir=/usr/share/doc/libsndfile-1.0.28 &&
make

   To test the results, issue: make check.

   Now, as the root user:
make install

Command Explanations

   --disable-static: This switch prevents installation of static versions
   of the libraries.

Contents

   Installed Programs: sndfile-cmp, sndfile-concat, sndfile-convert,
   sndfile-deinterleave, sndfile-info, sndfile-interleave,
   sndfile-metadata-get, sndfile-metadata-set, sndfile-play,
   sndfile-regtest and sndfile-salvage
   Installed Library: libsndfile.so
   Installed Directory: /usr/share/doc/libsndfile-1.0.28

Short Descriptions

   sndfile-cmp

   compares two audio files.

   sndfile-concat

   concatenates two or more audio files.

   sndfile-convert

   converts a sound files from one format to another.

   sndfile-deinterleave

   splits a multi-channel into multiple single channel files.

   sndfile-info

   displays information about a sound file.

   sndfile-interleave

   converts multiple single channel files into a multi-channel file.

   sndfile-metadata-get

   retrieves metadata from a sound file.

   sndfile-metadata-set

   sets metadata in a sound file.

   sndfile-play

   plays a sound file.

   libsndfile.so

   contains the libsndfile API functions.

   Last updated on 2020-02-17 18:27:03 -0800

     * [12]Prev
       libsamplerate-0.1.9
     * [13]Next
       libtheora-1.1.1
     * [14]Up
     * [15]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libsamplerate.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libtheora.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libdriv.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. http://www.mega-nerd.com/libsndfile/files/libsndfile-1.0.28.tar.gz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/flac.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libogg.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libvorbis.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/alsa-lib.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/server/sqlite.html
  11. http://wiki.linuxfromscratch.org/blfs/wiki/libsndfile
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libsamplerate.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libtheora.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libdriv.html
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
