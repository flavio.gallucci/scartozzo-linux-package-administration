Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 21. Mail Server Software

     * [1]Prev
       Dovecot-2.3.9.3
     * [2]Next
       Postfix-3.4.8
     * [3]Up
     * [4]Home

Exim-4.93

Introduction to Exim

   The Exim package contains a Mail Transport Agent written by the
   University of Cambridge, released under the GNU Public License.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://ftp.exim.org/pub/exim/exim4/exim-4.93.tar.xz
     * Download (FTP):
       [6]ftp://ftp.exim.org/pub/exim/exim4/exim-4.93.tar.xz
     * Download MD5 sum: 60aa3b38844a8ccece06670e8ff7d657
     * Download size: 1.8 MB
     * Estimated disk space required: 16 MB
     * Estimated build time: 0.2 SBU

Additional Downloads

     * Additional formats of the documentation (text-based docs are
       shipped with the sources) can be downloaded by following the links
       shown at [7]http://exim.org/docs.html.

Exim Dependencies

Required

   [8]libnsl-1.2.0 and [9]PCRE-8.44

Optional

   [10]TDB (alternative to GDBM, built in LFS), [11]Cyrus SASL-2.1.27,
   [12]libidn-1.35, [13]Linux-PAM-1.3.1, [14]MariaDB-10.4.12 or [15]MySQL,
   [16]OpenLDAP-2.4.49, [17]GnuTLS-3.6.12, [18]PostgreSQL-12.2,
   [19]SQLite-3.31.1, [20]X Window System, [21]Heimdal GSSAPI, and
   [22]OpenDMARC

   User Notes: [23]http://wiki.linuxfromscratch.org/blfs/wiki/exim

Installation of Exim

   Before building Exim, as the root user you should create the group and
   user exim which will run the exim daemon:
groupadd -g 31 exim &&
useradd -d /dev/null -c "Exim Daemon" -g exim -s /bin/false -u 31 exim

   Install Exim with the following commands:
sed -e 's,^BIN_DIR.*$,BIN_DIRECTORY=/usr/sbin,'    \
    -e 's,^CONF.*$,CONFIGURE_FILE=/etc/exim.conf,' \
    -e 's,^EXIM_USER.*$,EXIM_USER=exim,'           \
    -e '/# SUPPORT_TLS=yes/s,^#,,'                   \
    -e '/# USE_OPENSSL/s,^#,,'                       \
    -e 's,^EXIM_MONITOR,#EXIM_MONITOR,' src/EDITME > Local/Makefile &&

printf "USE_GDBM = yes\nDBMLIB = -lgdbm\n" >> Local/Makefile &&
make

   This package does not come with a test suite.

   Now, as the root user:
make install                                    &&
install -v -m644 doc/exim.8 /usr/share/man/man8 &&

install -v -d -m755    /usr/share/doc/exim-4.93 &&
install -v -m644 doc/* /usr/share/doc/exim-4.93 &&

ln -sfv exim /usr/sbin/sendmail                 &&
install -v -d -m750 -o exim -g exim /var/spool/exim

Command Explanations

   sed -e ... > Local/Makefile: Most of Exim's configuration options are
   defined in Local/Makefile, which is created from the src/EDITME file.
   This command specifies the minimum set of options. Descriptions for the
   options are listed below.

   printf ... > Local/Makefile: Setting those variables allows to use GDBM
   instead of the default Berkeley DB. Remove this command if you have
   installed [24]Berkeley DB-5.3.28.

   BIN_DIRECTORY=/usr/sbin: This installs all of Exim's binaries and
   scripts in /usr/sbin.

   CONFIGURE_FILE=/etc/exim.conf: This installs Exim's main configuration
   file in /etc.

   EXIM_USER=exim: This tells Exim that after the daemon no longer needs
   root privileges, the process hands off the daemon to the exim user.

   SUPPORT_TLS=yes: This allows to support STARTTLS connections. If you
   use this option, you need to select whether OpenSSL or GnuTLS is used
   (see src/EDITME).

   USE_OPENSSL_PC=openssl: This tells the build system to use OpenSSL, and
   to find the needed libraries with pkg-config.

   #EXIM_MONITOR: This defers building the Exim monitor program, as it
   requires X Window System support, by commenting out the EXIM_MONITOR
   line in the Makefile. If you wish to build the monitor program, omit
   this sed command and issue the following command before building the
   package (modify Local/eximon.conf, if necessary): cp
   exim_monitor/EDITME Local/eximon.conf.

   ln -sfv exim /usr/sbin/sendmail: Creates a link to sendmail for
   applications which need it. Exim will accept most Sendmail command-line
   options.

   install -v -m750 -o exim -g exim /var/spool/exim: Since /var/spool is
   owned by root and this version of exim drops root privileges early, to
   run as user exim, it cannot create the /var/spool/exim directory. As a
   work around, it is created manually.

Adding Additional Functionality

   To utilize some or all of the dependency packages, you'll need to
   modify Local/Makefile to include the appropriate directives and
   parameters to link additional libraries before you build Exim.
   Local/Makefile is heavily commented with instructions on how to do
   this. Listed below is additional information to help you link these
   dependency packages or add additional functionality.

   If you wish to build and install the .info documentation, refer to
   [25]http://exim.org/exim-html-4.93/doc/html/spec_html/ch04.html#SECTins
   infdoc.

   If you wish to build in Exim's interfaces for calling virus and spam
   scanning software directly from access control lists, uncomment the
   WITH_CONTENT_SCAN=yes parameter and review the information found at
   [26]http://exim.org/exim-html-4.93/doc/html/spec_html/ch45.html.

   To use a backend database other than Berkeley DB, see the instructions
   at
   [27]http://exim.org/exim-html-4.93/doc/html/spec_html/ch04.html#SECTdb.

   For SSL functionality, see the instructions at
   [28]http://exim.org/exim-html-4.93/doc/html/spec_html/ch04.html#SECTinc
   tlsssl and
   [29]http://exim.org/exim-html-4.93/doc/html/spec_html/ch42.html.

   For tcpwrappers functionality, see the instructions at
   [30]http://exim.org/exim-html-4.93/doc/html/spec_html/ch04.html#SECID27
   .

   For information about adding authentication mechanisms to the build,
   see chapters 33—41 of
   [31]http://exim.org/exim-html-4.93/doc/html/spec_html/index.html.

   For information about linking Linux-PAM, refer to the instructions
   [32]http://exim.org/exim-html-4.93/doc/html/spec_html/ch11.html#SECTexp
   cond.

   For information about linking database engine libraries used for Exim
   name lookups, see the instructions at
   [33]http://exim.org/exim-html-4.93/doc/html/spec_html/ch09.html.

   If you wish to add Readline support to Exim when invoked in “test
   expansion” (-be) mode, see the information in the -be section of
   [34]http://exim.org/exim-html-4.93/doc/html/spec_html/ch05.html#id25259
   74.

   You may wish to modify the default configuration and send log files to
   syslog instead of the default /var/spool/exim/log directory. See the
   information at
   [35]http://exim.org/exim-html-4.93/doc/html/spec_html/ch52.html.

   A wealth of information can be also found at the [36]Exim Wiki.

Configuring Exim

Config Files

   /etc/exim.conf and /etc/aliases

Configuration Information

   Review the file /etc/exim.conf, and modify any settings to suit your
   needs. Note that the default configuration assumes that the /var/mail
   directory is world writable, but has the sticky bit set. If you want to
   use the default configuration, issue as the root user:
chmod -v a+wt /var/mail

   A default (nothing but comments) /etc/aliases file is installed during
   the package installation if this file did not exist on your system.
   Create the necessary aliases and start the Exim daemon using the
   following commands:
cat >> /etc/aliases << "EOF"
postmaster: root
MAILER-DAEMON: root
EOF
exim -v -bi &&
/usr/sbin/exim -bd -q15m

   [Note]

Note

   To protect an existing /etc/aliases file, the command above appends
   these aliases to it. This file should be checked and duplicate aliases
   removed, if present.

   The /usr/sbin/exim -bd -q15m command starts the Exim daemon with a 15
   minute interval in processing the mail queue. Adjust this parameter to
   suit your desires.

Boot Script

   To automatically start exim at boot, install the /etc/rc.d/init.d/exim
   init script included in the [37]blfs-bootscripts-20191204 package.
make install-exim

   The bootscript also starts the Exim daemon and dispatches a queue
   runner process every 15 minutes. Modify the -q<time interval> parameter
   in /etc/rc.d/init.d/exim, if necessary for your installation.

Contents

   Installed Programs: exicyclog, exigrep, exim, exim-4.93-5,
   exim_checkaccess, exim_dbmbuild, exim_dumpdb, exim_fixdb, exim_lock,
   exim_tidydb, eximstats, exinext, exipick, exiqgrep, exiqsumm, exiwhat,
   and optionally, eximon, eximon.bin, and sendmail (symlink)
   Installed Libraries: None
   Installed Directories: /usr/share/doc/exim-4.93 and /var/spool/exim

Short Descriptions

   exicyclog

   cycles Exim log files.

   exigrep

   searches Exim log files.

   exim

   is a symlink to the exim-4.93-5 MTA daemon.

   exim-4.93-5

   is the Exim mail transport agent daemon.

   exim_checkaccess

   states whether a given recipient address from a given host is
   acceptable or not.

   exim_dbmbuild

   creates and rebuilds Exim databases.

   exim_dumpdb

   writes the contents of Exim databases to the standard output.

   exim_fixdb

   modifies data in Exim databases.

   exim_lock

   locks a mailbox file.

   exim_tidydb

   removes old records from Exim databases.

   eximstats

   generates mail statistics from Exim log files.

   exinext

   queries remote host retry times.

   exipick

   selects messages based on various criteria.

   exiqgrep

   is a utility for selective queue listing.

   exiqsumm

   produces a summary of the messages in the mail queue.

   exiwhat

   queries running Exim processes.

   eximon

   is a start-up shell script for eximon.bin used to set the required
   environment variables before running the program.

   eximon.bin

   is a monitor program which displays current information in an X window,
   and also contains a menu interface to Exim's command line
   administration options.

   Last updated on 2020-02-22 08:39:05 -0800

     * [38]Prev
       Dovecot-2.3.9.3
     * [39]Next
       Postfix-3.4.8
     * [40]Up
     * [41]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/dovecot.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/postfix.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/mail.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://ftp.exim.org/pub/exim/exim4/exim-4.93.tar.xz
   6. ftp://ftp.exim.org/pub/exim/exim4/exim-4.93.tar.xz
   7. http://exim.org/docs.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/basicnet/libnsl.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/pcre.html
  10. http://sourceforge.net/projects/tdb
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/cyrus-sasl.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/libidn.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/linux-pam.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/mariadb.html
  15. http://www.mysql.com/
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/openldap.html
  17. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/gnutls.html
  18. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/postgresql.html
  19. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/sqlite.html
  20. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/installing.html
  21. http://www.h5l.org/
  22. http://www.trusteddomain.org/opendmarc/
  23. http://wiki.linuxfromscratch.org/blfs/wiki/exim
  24. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/db.html
  25. http://exim.org/exim-html-4.93/doc/html/spec_html/ch04.html#SECTinsinfdoc
  26. http://exim.org/exim-html-4.93/doc/html/spec_html/ch45.html
  27. http://exim.org/exim-html-4.93/doc/html/spec_html/ch04.html#SECTdb
  28. http://exim.org/exim-html-4.93/doc/html/spec_html/ch04.html#SECTinctlsssl
  29. http://exim.org/exim-html-4.93/doc/html/spec_html/ch42.html
  30. http://exim.org/exim-html-4.93/doc/html/spec_html/ch04.html#SECID27
  31. http://exim.org/exim-html-4.93/doc/html/spec_html/index.html
  32. http://exim.org/exim-html-4.93/doc/html/spec_html/ch11.html#SECTexpcond
  33. http://exim.org/exim-html-4.93/doc/html/spec_html/ch09.html
  34. http://exim.org/exim-html-4.93/doc/html/spec_html/ch05.html#id2525974
  35. http://exim.org/exim-html-4.93/doc/html/spec_html/ch52.html
  36. https://github.com/Exim/exim/wiki
  37. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/introduction/bootscripts.html
  38. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/dovecot.html
  39. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/postfix.html
  40. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/mail.html
  41. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
