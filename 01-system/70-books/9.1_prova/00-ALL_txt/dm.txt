Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Part VI. X + Window and Display Managers

     * [1]Prev
       WebKitGTK+-2.26.4
     * [2]Next
       Introduction
     * [3]Up
     * [4]Home

Display Managers

Table of Contents

     * [5]Introduction
     * [6]lightdm-1.30.0
     * [7]lxdm-0.5.3

     * [8]Prev
       WebKitGTK+-2.26.4
     * [9]Next
       Introduction
     * [10]Up
     * [11]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/webkitgtk.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/dm-introduction.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/x.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/dm-introduction.html
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lightdm.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lxdm.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/webkitgtk.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/dm-introduction.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/x.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
