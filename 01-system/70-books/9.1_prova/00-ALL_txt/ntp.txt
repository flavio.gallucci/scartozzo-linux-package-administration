Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 15. Networking Programs

     * [1]Prev
       Configuring for Network Filesystems
     * [2]Next
       rpcbind-1.2.5
     * [3]Up
     * [4]Home

ntp-4.2.8p13

Introduction to ntp

   The ntp package contains a client and server to keep the time
   synchronized between various computers over a network. This package is
   the official reference implementation of the NTP protocol.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://www.eecis.udel.edu/~ntp/ntp_spool/ntp4/ntp-4.2/ntp-4.2.8
       p13.tar.gz
     * Download MD5 sum: ea040ab9b4ca656b5229b89d6b822f13
     * Download size: 6.6 MB
     * Estimated disk space required: 117 MB (with tests)
     * Estimated build time: 0.8 SBU (with tests)

ntp Dependencies

Required

   [6]IO-Socket-SSL-2.067

Optional

   [7]libcap-2.31 with PAM, [8]libevent-2.1.11, [9]libedit, and
   [10]libopts from AutoGen

   User Notes: [11]http://wiki.linuxfromscratch.org/blfs/wiki/ntp

Installation of ntp

   There should be a dedicated user and group to take control of the ntpd
   daemon after it is started. Issue the following commands as the root
   user:
groupadd -g 87 ntp &&
useradd -c "Network Time Protocol" -d /var/lib/ntp -u 87 \
        -g ntp -s /bin/false ntp

   The update-leap command needs to be fixed in order to run properly:
sed -e 's/"(\\S+)"/"?([^\\s"]+)"?/' \
    -i scripts/update-leap/update-leap.in

   Install ntp by running the following commands:
./configure CFLAGS="-O2 -g -fPIC" \
            --prefix=/usr         \
            --bindir=/usr/sbin    \
            --sysconfdir=/etc     \
            --enable-linuxcaps    \
            --with-lineeditlibs=readline \
            --docdir=/usr/share/doc/ntp-4.2.8p13 &&
make

   To test the results, issue: make check.

   Now, as the root user:
make install &&
install -v -o ntp -g ntp -d /var/lib/ntp

Command Explanations

   CFLAGS="-O2 -g -fPIC": This environment variable is necessary to
   generate Position Independent Code needed for use in the package
   libraries.

   --bindir=/usr/sbin: This parameter places the administrative programs
   in /usr/sbin.

   --enable-linuxcaps: ntpd is run as user ntp, so use Linux capabilities
   for non-root clock control.

   --with-lineeditlibs=readline: This switch enables Readline support for
   ntpdc and ntpq programs. If omitted, libedit will be used if installed,
   otherwise no readline capabilites will be compiled.

Configuring ntp

Config Files

   /etc/ntp.conf

Configuration Information

   The following configuration file first defines various ntp servers with
   open access from different continents. Second, it creates a drift file
   where ntpd stores the frequency offset and a pid file to store the ntpd
   process ID. Third, it defines the location for the leap-second
   definition file /etc/ntp.leapseconds, that the update-leap script
   checks and updates, when necessary. This script can be run as a cron
   job and the ntp developers recommend a frequency of about three weeks
   for the updates. Since the documentation included with the package is
   sparse, visit the ntp website at [12]http://www.ntp.org/ and
   [13]http://www.pool.ntp.org/ for more information.
cat > /etc/ntp.conf << "EOF"
# Asia
server 0.asia.pool.ntp.org

# Australia
server 0.oceania.pool.ntp.org

# Europe
server 0.europe.pool.ntp.org

# North America
server 0.north-america.pool.ntp.org

# South America
server 2.south-america.pool.ntp.org

driftfile /var/lib/ntp/ntp.drift
pidfile   /var/run/ntpd.pid

leapfile  /var/lib/ntp/ntp.leapseconds
EOF

   You may wish to add a “Security session”. For explanations, see
   [14]https://www.eecis.udel.edu/~mills/ntp/html/accopt.html#restrict.
cat >> /etc/ntp.conf << "EOF"
# Security session
restrict    default limited kod nomodify notrap nopeer noquery
restrict -6 default limited kod nomodify notrap nopeer noquery

restrict 127.0.0.1
restrict ::1
EOF

Synchronizing the Time

   There are two options. Option one is to run ntpd continuously and allow
   it to synchronize the time in a gradual manner. The other option is to
   run ntpd periodically (using cron) and update the time each time ntpd
   is scheduled.

   If you choose Option one, then install the /etc/rc.d/init.d/ntp init
   script included in the [15]blfs-bootscripts-20191204 package.
make install-ntpd

   If you prefer to run ntpd periodically, add the following command to
   root's crontab:
ntpd -q

   Execute the following command if you would like to set the hardware
   clock to the current system time at shutdown and reboot:
ln -v -sf ../init.d/setclock /etc/rc.d/rc0.d/K46setclock &&
ln -v -sf ../init.d/setclock /etc/rc.d/rc6.d/K46setclock

   The other way around is already set up by LFS.

Contents

   Installed Programs: calc_tickadj, ntp-keygen, ntp-wait, ntpd, ntpdate,
   ntpdc, ntpq, ntptime, ntptrace, sntp, tickadj and update-leap
   Installed Libraries: None
   Installed Directories: /usr/share/ntp, /usr/share/doc/ntp-4.2.8 and
   /var/lib/ntp

Short Descriptions

   calc_tickadj

   calculates optimal value for tick given ntp drift file.

   ntp-keygen

   generates cryptographic data files used by the NTPv4 authentication and
   identification schemes.

   ntp-wait

   is useful at boot time, to delay the boot sequence until ntpd has set
   the time.

   ntpd

   is a ntp daemon that runs in the background and keeps the date and time
   synchronized based on response from configured ntp servers. It also
   functions as a ntp server.

   ntpdate

   is a client program that sets the date and time based on the response
   from an ntp server. This command is deprecated.

   ntpdc

   is used to query the ntp daemon about its current state and to request
   changes in that state.

   ntpq

   is a utility program used to monitor ntpd operations and determine
   performance.

   ntptime

   reads and displays time-related kernel variables.

   ntptrace

   traces a chain of ntp servers back to the primary source.

   sntp

   is a Simple Network Time Protocol (SNTP) client.

   tickadj

   reads, and optionally modifies, several timekeeping-related variables
   in older kernels that do not have support for precision timekeeping.

   update-leap

   is a script to verify and, if necessary, update the leap-second
   definition file.

   Last updated on 2020-02-25 07:28:56 -0800

     * [16]Prev
       Configuring for Network Filesystems
     * [17]Next
       rpcbind-1.2.5
     * [18]Up
     * [19]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/netfs.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/rpcbind.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/netprogs.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://www.eecis.udel.edu/~ntp/ntp_spool/ntp4/ntp-4.2/ntp-4.2.8p13.tar.gz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/perl-modules.html#perl-io-socket-ssl
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/libcap.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libevent.html
   9. http://www.thrysoee.dk/editline/
  10. https://www.gnu.org/software/autogen/
  11. http://wiki.linuxfromscratch.org/blfs/wiki/ntp
  12. http://www.ntp.org/
  13. http://www.pool.ntp.org/
  14. https://www.eecis.udel.edu/~mills/ntp/html/accopt.html#restrict
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/introduction/bootscripts.html
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/netfs.html
  17. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/rpcbind.html
  18. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/netprogs.html
  19. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
