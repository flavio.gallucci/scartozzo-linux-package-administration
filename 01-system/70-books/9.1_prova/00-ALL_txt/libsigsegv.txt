Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 9. General Libraries

     * [1]Prev
       libsigc++-2.10.2
     * [2]Next
       libssh2-1.9.0
     * [3]Up
     * [4]Home

libsigsegv-2.12

Introduction to libsigsegv

   libsigsegv is a library for handling page faults in user mode. A page
   fault occurs when a program tries to access to a region of memory that
   is currently not available. Catching and handling a page fault is a
   useful technique for implementing pageable virtual memory,
   memory-mapped access to persistent databases, generational garbage
   collectors, stack overflow handlers, and distributed shared memory.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://ftp.gnu.org/gnu/libsigsegv/libsigsegv-2.12.tar.gz
     * Download (FTP):
       [6]ftp://ftp.gnu.org/gnu/libsigsegv/libsigsegv-2.12.tar.gz
     * Download MD5 sum: 58a6db48f79f5c735a9dce3a37c52779
     * Download size: 444 KB
     * Estimated disk space required: 3.5 MB (with checks)
     * Estimated build time: less than 0.1 SBU (with checks)

   User Notes: [7]http://wiki.linuxfromscratch.org/blfs/wiki/libsigsegv

Installation of libsigsegv

   Install libsigsegv by running the following commands:
./configure --prefix=/usr   \
            --enable-shared \
            --disable-static &&
make

   To test the results, issue: make check.

   Now, as the root user:
make install

Command Explanations

   --enable-shared: This switch ensures that shared libraries are
   compiled.

   --disable-static: This switch prevents installation of static versions
   of the libraries.

Contents

   Installed Programs: None
   Installed Library: libsigsegv.so
   Installed Directories: None

Short Descriptions

   libsigsegv.so

   is a library for handling page faults in user mode.

   Last updated on 2020-02-16 18:46:23 -0800

     * [8]Prev
       libsigc++-2.10.2
     * [9]Next
       libssh2-1.9.0
     * [10]Up
     * [11]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libsigc.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libssh2.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://ftp.gnu.org/gnu/libsigsegv/libsigsegv-2.12.tar.gz
   6. ftp://ftp.gnu.org/gnu/libsigsegv/libsigsegv-2.12.tar.gz
   7. http://wiki.linuxfromscratch.org/blfs/wiki/libsigsegv
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libsigc.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libssh2.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
