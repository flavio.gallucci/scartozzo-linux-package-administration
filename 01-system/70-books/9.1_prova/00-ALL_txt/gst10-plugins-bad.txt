Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 42. Multimedia Libraries and Drivers

     * [1]Prev
       gst-plugins-good-1.16.2
     * [2]Next
       gst-plugins-ugly-1.16.2
     * [3]Up
     * [4]Home

gst-plugins-bad-1.16.2

Introduction to GStreamer Bad Plug-ins

   The GStreamer Bad Plug-ins package contains a set of plug-ins that
   aren't up to par compared to the rest. They might be close to being
   good quality, but they're missing something - be it a good code review,
   some documentation, a set of tests, a real live maintainer, or some
   actual wide use.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://gstreamer.freedesktop.org/src/gst-plugins-bad/gst-plugin
       s-bad-1.16.2.tar.xz
     * Download MD5 sum: ccc7404230afddec723bbdb63c89feec
     * Download size: 4.7 MB
     * Estimated disk space required: 110 MB (with tests)
     * Estimated build time: 0.9 SBU (Using parallelism=4; with tests)

GStreamer Bad Plug-ins Dependencies

Required

   [6]gst-plugins-base-1.16.2

Recommended

   [7]libdvdread-6.0.2, [8]libdvdnav-6.0.1, and [9]SoundTouch-2.1.0

Optional

   [10]BlueZ-5.53, [11]Clutter-1.26.2, [12]cURL-7.68.0, [13]FAAC-1.29.9.2,
   [14]FAAD2-2.8.8, [15]fdk-aac-2.0.1, [16]GnuTLS-3.6.12,
   [17]GTK-Doc-1.32, [18]GTK+-2.24.32 or [19]GTK+-3.24.13, [20]Little
   CMS-2.9, [21]libass-0.14.0, [22]libexif-0.6.21, [23]libgcrypt-1.8.5,
   [24]libgudev-233, [25]libmpeg2-0.5.1, [26]libssh2-1.9.0,
   [27]libusb-1.0.23, [28]libvdpau-1.3, [29]libwebp-1.1.0,
   [30]neon-0.30.2, [31]Nettle-3.5.1, [32]opencv-4.2.0 (with additional
   modules), [33]OpenJPEG-2.3.1, [34]Opus-1.3.1, [35]Qt-5.14.1 (for
   examples), [36]SDL-1.2.15, [37]Valgrind-3.15.0, [38]Wayland-1.18.0
   ([39]GTK+-3.24.13 must have been compiled with wayland support),
   [40]x265-3.3, [41]Xorg Libraries, [42]bs2b, [43]Chromaprint, [44]daala,
   [45]Flite, [46]Game Music Emu, [47]GSM, [48]LADSPA, [49]libmimic,
   [50]libmms, [51]libofa, [52]MJPEG Tools, [53]OpenAL, [54]Orc, [55]VO
   AAC, [56]VO AMRWB, and [57]ZBAR

   User Notes:
   [58]http://wiki.linuxfromscratch.org/blfs/wiki/gst10-plugins-bad

Installation of GStreamer Bad Plug-ins

   [Note]

Note

   If you do not have an Objective-C compiler installed, the build system
   for this package will emit a warning about a failed sanity check. This
   is harmless, and it is safe to continue.
   [Note]

Note

   If you need a plugin for a given dependency, that dependency needs to
   be installed before this package.

   Install GStreamer Bad Plug-ins by running the following commands:
mkdir build &&
cd    build &&

meson  --prefix=/usr       \
       -Dbuildtype=release \
       -Dpackage-origin=http://www.linuxfromscratch.org/blfs/view/svn/ \
       -Dpackage-name="GStreamer 1.16.2 BLFS" &&
ninja

   To test the results, issue: ninja test. Several tests need a terminal
   emulator in a graphical session. One test, elements_dash_mpd, is known
   to fail.

   Now, as the root user:
ninja install

Contents

   Installed Programs: None
   Installed Libraries: libgstadaptivedemux-1.0.so, libgstbadaudio-1.0.so,
   libgstbadvideo-1.0.so, libgstbasecamerabinsrc-1.0.so,
   libgstcodecparsers-1.0.so, libgstinsertbin-1.0.so, libgstisoff-1.0.so,
   libgstmpegts-1.0.so, libgstphotography-1.0.so, libgstplayer-1.0.so,
   libgsturidownloader-1.0.so, libgstwayland-1.0.so, libgstwebrtc-1.0.so,
   and several plugins under /usr/lib/gstreamer-1.0
   Installed Directories:
   /usr/include/gstreamer-1.0/gst/{audio,basecamerabinsrc,codecparsers},
   /usr/include/gstreamer-1.0/gst/{insertbin,interfaces,is,mpegts},
   /usr/include/gstreamer-1.0/gst/{player,uridownloader,video,webrtc},
   /usr/lib/gstreamer-1.0/include, and
   /usr/share/gtk-doc/html/gst-plugins-bad-{libs,plugins}-1.0

   Last updated on 2020-02-17 18:27:03 -0800

     * [59]Prev
       gst-plugins-good-1.16.2
     * [60]Next
       gst-plugins-ugly-1.16.2
     * [61]Up
     * [62]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gst10-plugins-good.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gst10-plugins-ugly.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libdriv.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://gstreamer.freedesktop.org/src/gst-plugins-bad/gst-plugins-bad-1.16.2.tar.xz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gst10-plugins-base.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libdvdread.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libdvdnav.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/soundtouch.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/bluez.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/clutter.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/basicnet/curl.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/faac.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/faad2.html
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/fdk-aac.html
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/gnutls.html
  17. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/gtk-doc.html
  18. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/gtk2.html
  19. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/gtk3.html
  20. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/lcms2.html
  21. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libass.html
  22. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/libexif.html
  23. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/libgcrypt.html
  24. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/libgudev.html
  25. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libmpeg2.html
  26. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/libssh2.html
  27. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/libusb.html
  28. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/x7driver.html#libvdpau
  29. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/libwebp.html
  30. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/basicnet/neon.html
  31. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/nettle.html
  32. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/opencv.html
  33. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/openjpeg2.html
  34. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/opus.html
  35. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/qt5.html
  36. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/sdl.html
  37. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/valgrind.html
  38. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/wayland.html
  39. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/gtk3.html
  40. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/x265.html
  41. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/x7lib.html
  42. http://bs2b.sourceforge.net/
  43. https://acoustid.org/chromaprint
  44. https://www.xiph.org/daala/
  45. http://www.speech.cs.cmu.edu/flite/
  46. https://bitbucket.org/mpyne/game-music-emu/
  47. http://www.quut.com/gsm/
  48. https://www.ladspa.org/
  49. http://sourceforge.net/projects/farsight/files/libmimic/
  50. http://sourceforge.net/projects/libmms/
  51. http://code.google.com/p/musicip-libofa/
  52. http://mjpeg.sourceforge.net/
  53. http://kcat.strangesoft.net/openal.html
  54. http://gstreamer.freedesktop.org/src/orc/
  55. http://sourceforge.net/projects/opencore-amr/files/vo-aacenc/
  56. http://sourceforge.net/projects/opencore-amr/files/vo-amrwbenc/
  57. http://zbar.sourceforge.net/
  58. http://wiki.linuxfromscratch.org/blfs/wiki/gst10-plugins-bad
  59. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gst10-plugins-good.html
  60. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gst10-plugins-ugly.html
  61. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libdriv.html
  62. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
