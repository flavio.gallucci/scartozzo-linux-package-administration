Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 6. Editors

     * [1]Prev
       Editors
     * [2]Next
       Ed-1.15
     * [3]Up
     * [4]Home

Bluefish-2.2.11

Introduction to Bluefish

   Bluefish is a GTK+ text editor targeted towards programmers and web
   designers, with many options to write websites, scripts and programming
   code. Bluefish supports many programming and markup languages, and it
   focuses on editing dynamic and interactive websites.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]http://www.bennewitz.com/bluefish/stable/source/bluefish-2.2.11.
       tar.bz2
     * Download MD5 sum: 8c08aebcb7eaeccfebcaf467983733eb
     * Download size: 4.0 MB
     * Estimated disk space required: 55 MB
     * Estimated build time: 0.4 SBU

Bluefish Dependencies

Required

   [6]GTK+-2.24.32 or [7]GTK+-3.24.13 (If both are installed, configure
   defaults to using GTK+ 3)

Recommended

   [8]desktop-file-utils-0.24 (for updating the desktop database)

Optional

   [9]enchant-2.2.7 (for spell checking), [10]Gucharmap-12.0.1,
   [11]PCRE-8.44 and [12]Jing

   User Notes: [13]http://wiki.linuxfromscratch.org/blfs/wiki/bluefish

Installation of Bluefish

   Install Bluefish by running the following commands:
./configure --prefix=/usr --docdir=/usr/share/doc/bluefish-2.2.11 &&
make

   This package does not come with a test suite.

   Now, as the root user:
make install

   [Note]

Note

   This package installs icon files into the /usr/share/icons/hicolor
   hierarchy and desktop files into the /usr/share/applications hierarchy.
   You can improve system performance and memory usage by updating
   /usr/share/icons/hicolor/icon-theme.cache and
   /usr/share/applications/mimeinfo.cache. To perform the update you must
   have [14]desktop-file-utils-0.24 (for the desktop cache) and issue the
   following commands as the root user:
gtk-update-icon-cache -t -f --include-image-data /usr/share/icons/hicolor &&
update-desktop-database

Contents

   Installed Program: bluefish
   Installed Libraries: several under /usr/lib/bluefish/
   Installed Directories: /usr/lib/bluefish, /usr/share/bluefish,
   /usr/share/doc/bluefish-2.2.11, and /usr/share/xml/bluefish

Short Descriptions

   bluefish

   is a GTK+ text editor for markup and programming.

   Last updated on 2020-02-20 06:05:58 -0800

     * [15]Prev
       Editors
     * [16]Next
       Ed-1.15
     * [17]Up
     * [18]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/editors.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/ed.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/editors.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. http://www.bennewitz.com/bluefish/stable/source/bluefish-2.2.11.tar.bz2
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/gtk2.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/gtk3.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/desktop-file-utils.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/enchant.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/gnome/gucharmap.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/pcre.html
  12. https://github.com/relaxng/jing-trang
  13. http://wiki.linuxfromscratch.org/blfs/wiki/bluefish
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/desktop-file-utils.html
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/editors.html
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/ed.html
  17. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/editors.html
  18. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
