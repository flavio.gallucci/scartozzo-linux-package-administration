Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Part II. Post LFS Configuration and Extra Software

     * [1]Prev
       Post LFS Configuration and Extra Software
     * [2]Next
       Creating a Custom Boot Device
     * [3]Up
     * [4]Home

After LFS Configuration Issues

   The intention of LFS is to provide a basic system which you can build
   upon. There are several things about tidying up the system which many
   people wonder about once they have done the base install. We hope to
   cover these issues in this chapter.

   Most people coming from non-Unix like backgrounds to Linux find the
   concept of text-only configuration files slightly strange. In Linux,
   just about all configuration is done via the manipulation of text
   files. The majority of these files can be found in the /etc hierarchy.
   There are often graphical configuration programs available for
   different subsystems but most are simply pretty front ends to the
   process of editing a text file. The advantage of text-only
   configuration is that you can edit parameters using your favorite text
   editor, whether that be vim, emacs, or any other editor.

   The first task is making a recovery boot device in [5]Creating a Custom
   Boot Device because it's the most critical need. Hardware issues
   relevant to firmware and other devices is addressed next. The system is
   then configured to ease addition of new users, because this can affect
   the choices you make in the two subsequent topics—[6]The Bash Shell
   Startup Files and [7]The vimrc Files.

   The remaining topics, [8]Customizing your Logon with /etc/issue,
   [9]Random number generation, and [10]Autofs-5.1.6 are then addressed,
   in that order. They don't have much interaction with the other topics
   in this chapter.

Table of Contents

     * [11]Creating a Custom Boot Device
     * [12]About Console Fonts
     * [13]About Firmware
     * [14]About Devices
     * [15]Configuring for Adding Users
     * [16]About System Users and Groups
     * [17]The Bash Shell Startup Files
     * [18]The /etc/vimrc and ~/.vimrc Files
     * [19]Customizing your Logon with /etc/issue
     * [20]Random Number Generation

     * [21]Prev
       Post LFS Configuration and Extra Software
     * [22]Next
       Creating a Custom Boot Device
     * [23]Up
     * [24]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/postlfs.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/bootdisk.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/postlfs.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/bootdisk.html
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/profile.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/vimrc.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/logon.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/random.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/autofs.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/bootdisk.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/console-fonts.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/firmware.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/devices.html
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/skel.html
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/users.html
  17. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/profile.html
  18. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/vimrc.html
  19. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/logon.html
  20. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/random.html
  21. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/postlfs.html
  22. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/bootdisk.html
  23. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/postlfs.html
  24. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
