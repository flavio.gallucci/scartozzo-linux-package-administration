Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 5. File Systems and Disk Management

     * [1]Prev
       About initramfs
     * [2]Next
       dosfstools-4.1
     * [3]Up
     * [4]Home

btrfs-progs-5.4.1

Introduction to btrfs-progs

   The btrfs-progs package contains administration and debugging tools for
   the B-tree file system (btrfs).

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://www.kernel.org/pub/linux/kernel/people/kdave/btrfs-progs
       /btrfs-progs-v5.4.1.tar.xz
     * Download MD5 sum: 4fb3ac57dc4e0afcf723cdf34640179b
     * Download size: 2.0 MB
     * Estimated disk space required: 62 MB (add 71 MB for tests)
     * Estimated build time: 0.2 SBU (add 15 SBU for tests)

Btrfs-progs Dependencies

Required

   [6]LZO-2.10

Recommended

   [7]asciidoc-8.6.9 and [8]xmlto-0.0.28 (both required to generate man
   pages)

Optional

   [9]LVM2-2.03.08 (dmsetup is used in tests), [10]Python-2.7.17 (python
   bindings), and [11]reiserfsprogs-3.6.27 (for tests)

   User Notes: [12]http://wiki.linuxfromscratch.org/blfs/wiki/xfs

Kernel Configuration

   Enable the following option in the kernel configuration and recompile
   the kernel:
File systems --->
  <*/M> Btrfs filesystem support [CONFIG_BTRFS_FS]

   [Note]

Note

   CONFIG_BTRFS_FS_POSIX_ACL and CONFIG_REISERFS_FS_XATTR are required for
   some tests. Other Btrfs options in the kernel are optional.

Installation of btrfs-progs

   Install btrfs-progs by running the following commands:
./configure --prefix=/usr  \
            --bindir=/bin  \
            --libdir=/lib  \
            --disable-zstd &&
make

   [Note]

Note

   Some tests require grep built with perl regular expressions. To obtain
   this, rebuild grep with the LFS Chapter 6 instructions after installing
   [13]PCRE-8.44.

   Before running tests, build a support program and disable several that
   fail:
make fssum &&

sed -i '/found/s/^/: #/' tests/convert-tests.sh &&

mv tests/convert-tests/010-reiserfs-basic/test.sh{,.broken}                 &&
mv tests/convert-tests/011-reiserfs-delete-all-rollback/test.sh{,.broken}   &&
mv tests/convert-tests/012-reiserfs-large-hole-extent/test.sh{,.broken}     &&
mv tests/convert-tests/013-reiserfs-common-inode-flags/test.sh{,.broken}    &&
mv tests/convert-tests/014-reiserfs-tail-handling/test.sh{,.broken}         &&
mv tests/misc-tests/025-zstd-compression/test.sh{,.broken}

   To test the results, issue (as the root user):
pushd tests
   ./fsck-tests.sh
   ./mkfs-tests.sh
   ./cli-tests.sh
   ./convert-tests.sh
   ./misc-tests.sh
   ./fuzz-tests.sh
popd

   Install the package as the root user:
make install &&

ln -sfv ../../lib/$(readlink /lib/libbtrfs.so) /usr/lib/libbtrfs.so &&
ln -sfv ../../lib/$(readlink /lib/libbtrfsutil.so) /usr/lib/libbtrfsutil.so &&
rm -fv /lib/libbtrfs.{a,so} /lib/libbtrfsutil.{a,so} &&
mv -v /bin/{mkfs,fsck}.btrfs /sbin

Command Explanations

   --disable-documentation: This option is needed if the recommended
   dependencies are not installed.

   mv tests/{cli,convert,misc,fuzz}-tests/ ...: Disables tests that fail
   and prevent tests from completing.

   ln -s ... /usr/lib/libbtrfs.so: Creates a symbolic link in the
   directory where it is expected.

   rm /lib/libbtrfs.{a,so}: Removes unneeded library entries.

Contents

   Installed Programs: btrfs, btrfs-convert, btrfs-find-root, btrfs-image,
   btrfs-map-logical, btrfs-select-super, btrfsck (link to btrfs),
   btrfstune, fsck.btrfs, and mkfs.btrfs
   Installed Libraries: libbtrfs.so and libbtrfsutil.so
   Installed Directories: /usr/include/btrfs

Short Descriptions

   btrfs

   is the main interface into btrfs filesystem operations.

   btrfs-convert

   converts from an ext2/3/4 filesystem to btrfs.

   btrfs-find-root

   is a filter to find btrfs root.

   btrfs-map-logical

   maps btrfs logical extent to physical extent.

   btrfs-select-super

   overwrites the primary superblock with a backup copy.

   btrfstune

   tunes various filesystem parameters.

   fsck.btrfs

   does nothing, but is present for consistency with fstab.

   mkfs.btrfs

   creates a btrfs file system.

   Last updated on 2020-02-18 14:50:03 -0800

     * [14]Prev
       About initramfs
     * [15]Next
       dosfstools-4.1
     * [16]Up
     * [17]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/initramfs.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/dosfstools.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/filesystems.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://www.kernel.org/pub/linux/kernel/people/kdave/btrfs-progs/btrfs-progs-v5.4.1.tar.xz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/lzo.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/asciidoc.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/pst/xmlto.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lvm2.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/python2.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/reiserfs.html
  12. http://wiki.linuxfromscratch.org/blfs/wiki/xfs
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/pcre.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/initramfs.html
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/dosfstools.html
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/filesystems.html
  17. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
