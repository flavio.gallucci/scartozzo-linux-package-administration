Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 39. Office Programs

     * [1]Prev
       Gnumeric-1.12.46
     * [2]Next
       Graphical Web Browsers
     * [3]Up
     * [4]Home

LibreOffice-6.4.0

Introduction to LibreOffice

   LibreOffice is a full-featured office suite. It is largely compatible
   with Microsoft Office and is descended from OpenOffice.org.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Core Download:
       [5]http://download.documentfoundation.org/libreoffice/src/6.4.0/lib
       reoffice-6.4.0.3.tar.xz
     * Core Download MD5 sum: 93ffa098e65dde29516ffe7212092f29
     * Core Download size: 222 MB
     * Estimated disk space required: 9.0 GB including the additional
       files if downloaded during the build (820 MB installed). Installing
       the whole set of languages for 6.4.0.3 required up to 25 GB (3.2 GB
       installed)
     * Estimated build time: 54 SBU with parallelism=4 including typical
       download time

Additional Downloads

   [Note]

Note

   Around 80 smallish tarballs and zip files will be downloaded during the
   build. If you have not downloaded one or more of the following, they
   too will also be downloaded during the build.
     * Dictionaries:
       [6]http://download.documentfoundation.org/libreoffice/src/6.4.0/lib
       reoffice-dictionaries-6.4.0.3.tar.xz
     * Dictionaries MD5 sum: 5c021d899e8987f796d93e6d69cd5a1f
     * Dictionaries size: 46 MB
     * Help Files:
       [7]http://download.documentfoundation.org/libreoffice/src/6.4.0/lib
       reoffice-help-6.4.0.3.tar.xz
     * Help Files MD5 sum: c7cf3fdfd70e097fcc49c58887053cea
     * Help Files size: 85 MB
     * Translations:
       [8]http://download.documentfoundation.org/libreoffice/src/6.4.0/lib
       reoffice-translations-6.4.0.3.tar.xz
     * Translations MD5 sum: 9aaecb32945a04ebc8a89835536105f9
     * Translations size: 164 MB

LibreOffice Dependencies

Required

   [9]Archive-Zip-1.67, [10]UnZip-6.0, [11]Wget-1.20.3, [12]Which-2.21,
   and [13]Zip-3.0

Recommended

   [Note]

Note

   Most of these packages are recommended because if they're not
   installed, the build process will compile and install its own (often
   older) version, with the side effect of increasing build and installed
   disk space and build time.

   [14]apache-ant-1.10.7, [15]Apr-1.7.0, [16]Boost-1.72.0,
   [17]CLucene-2.3.3.4, [18]Cups-2.3.1, [19]cURL-7.68.0,
   [20]dbus-glib-0.110, [21]libjpeg-turbo-2.0.4, [22]GLM-0.9.9.7,
   [23]GLU-9.0.1, [24]GPGME-1.13.1, [25]Graphite2-1.3.13,
   [26]gst-plugins-base-1.16.2, [27]GTK+-3.24.13, [28]HarfBuzz-2.6.4,
   [29]ICU-65.1, [30]libatomic_ops-7.6.10, [31]Little CMS-2.9,
   [32]librsvg-2.46.4, [33]libxml2-2.9.10 and [34]libxslt-1.1.34,
   [35]lxml-4.5.0, [36]Mesa-19.3.4, [37]neon-0.30.2, [38]NSS-3.50,
   [39]OpenLDAP-2.4.49 (if connecting to an LDAP server),
   [40]Poppler-0.85.0, [41]PostgreSQL-12.2, [42]Redland-1.0.17,
   [43]Serf-1.3.9, and [44]unixODBC-2.3.7

Optional

   [45]Avahi-0.7, [46]BlueZ-5.53, [47]DConf-0.34.0,
   [48]desktop-file-utils-0.24, [49]Doxygen-1.8.17 (not relevant if using
   --disable-odk), [50]evolution-data-server-3.34.4, [51]GDB-9.1,
   [52]GnuTLS-3.6.12, [53]KDE Frameworks-5.67.0, [54]libpaper-1.1.24+nmu5,
   [55]MariaDB-10.4.12 or [56]MySQL, [57]MIT Kerberos V5-1.18,
   [58]NASM-2.14.02, [59]SANE-1.0.27, [60]telepathy-glib-0.24.1,
   [61]Valgrind-3.15.0, [62]VLC-3.0.8, [63]Zenity-3.32.0, [64]Apache
   Commons Codec, [65]Apache Commons HttpClient, [66]Apache Commons Lang,
   [67]Apache Commons Logging, [68]BeanShell, [69]CoinMP, [70]Cppunit,
   [71]Firebird, [72]glyphy, [73]Gnome VFS, [74]hamcrest, [75]Hunspell,
   [76]Hyphen, [77]iwyu, [78]libabw, [79]libcdr, [80]libcmis,
   [81]libebook, [82]libexttextcat, [83]libfreehand, [84]liblangtag,
   [85]libmspub, [86]libmwaw, [87]libodfgen, [88]libpagemaker,
   [89]librevenge (WordPerfect Document importer), [90]libvisio,
   [91]libwpd, [92]libwpg, [93]libwps, [94]lp_solve, [95]mdds, [96]MyThes,
   [97]OpenCOLLADA, [98]Orcus, and [99]VIGRA

   There are many optional dependencies not listed here. They can be found
   in “download.lst” (source directory).

   User Notes: [100]http://wiki.linuxfromscratch.org/blfs/wiki/libreoffice

Installation of LibreOffice

   [Important]

Important

   Unlike the other packages, we assume that you have not yet unpacked the
   package. This is because the --no-overwrite-dir switch is needed in
   case you unpack as the root user.
tar -xf libreoffice-6.4.0.3.tar.xz --no-overwrite-dir &&
cd libreoffice-6.4.0.3

   If you have downloaded the dictionaries, help and translations
   tarballs, create symlinks to them from the source directory so they
   won't get downloaded again:
install -dm755 external/tarballs &&
ln -sv ../../../libreoffice-dictionaries-6.4.0.3.tar.xz external/tarballs/ &&
ln -sv ../../../libreoffice-help-6.4.0.3.tar.xz         external/tarballs/ &&
ln -sv ../../../libreoffice-translations-6.4.0.3.tar.xz external/tarballs/

   [Note]

Note

   During the build process, some packages will be downloaded (including
   the ones listed as recommended and optional dependencies) if they are
   not present on the system. Because of this, build time may vary from
   the published time more than usual.

   Due to the large size of the package, you may prefer to install it in
   /opt, instead of /usr. Depending on your choice, replace <PREFIX> by
   /usr or by /opt/libreoffice-6.4.0.3:
export LO_PREFIX=<PREFIX>

   Locales "fr" and "en-GB", you will find below, are just examples; you
   should change them to suit your needs - you might want to read the
   "Command Explanations", further below, before proceeding.

   Prepare LibreOffice for compilation by running the following commands:
sed -e "/gzip -f/d"   \
    -e "s|.1.gz|.1|g" \
    -i bin/distro-install-desktop-integration &&

sed -e "/distro-install-file-lists/d" -i Makefile.in &&

sed -e '/JAVA_SOURCE_VER/s/6/7/' \
    -e '/JAVA_TARGET_VER/s/6/7/' \
    -i configure.ac &&

sed -e 's/globalParams = new GlobalParams()/globalParams.reset(new GlobalParams(
))/' \
    -i sdext/source/pdfimport/xpdfwrapper/wrapper_gpl.cxx &&

sed -e 's/printPath( GfxPath/printPath(const GfxPath/' \
    -e 's/  GfxSubpath/ const GfxSubpath/' \
    -i sdext/source/pdfimport/xpdfwrapper/pdfioutdev_gpl.* &&

./autogen.sh --prefix=$LO_PREFIX         \
             --sysconfdir=/etc           \
             --with-vendor=BLFS          \
             --with-lang='fr en-GB'      \
             --with-help                 \
             --with-myspell-dicts        \
             --without-junit             \
             --without-system-dicts      \
             --disable-dconf             \
             --disable-odk               \
             --enable-release-build=yes  \
             --enable-python=system      \
             --with-jdk-home=/opt/jdk    \
             --with-system-apr           \
             --with-system-boost         \
             --with-system-clucene       \
             --with-system-curl          \
             --with-system-epoxy         \
             --with-system-expat         \
             --with-system-glm           \
             --with-system-gpgmepp       \
             --with-system-graphite      \
             --with-system-harfbuzz      \
             --with-system-icu           \
             --with-system-jpeg          \
             --with-system-lcms2         \
             --with-system-libatomic_ops \
             --with-system-libpng        \
             --with-system-libxml        \
             --with-system-neon          \
             --with-system-nss           \
             --with-system-odbc          \
             --with-system-openldap      \
             --with-system-openssl       \
             --with-system-poppler       \
             --with-system-postgresql    \
             --with-system-redland       \
             --with-system-serf          \
             --with-system-zlib

   The instructions below will only build the package without running any
   unit tests. If you prefer to run the unit tests, replace make
   build-nocheck with make build or (as a regular user only) just make,
   but do not be surprised if a unit test fails right at the end of the
   build and breaks it.

   Build the package:
make build-nocheck

   Now, as the root user:
make distro-pack-install

   If installed in /opt/libreoffice-6.4.0.3 some additional steps are
   necessary. Issue the following commands, as root user:
if [ "$LO_PREFIX" != "/usr" ]; then

  # This symlink is necessary for the desktop menu entries
  ln -svf $LO_PREFIX/lib/libreoffice/program/soffice /usr/bin/libreoffice &&

  # Set up a generic location independent of version number
  ln -sfv libreoffice-6.4.0.3 /opt/libreoffice

  # Icons
  mkdir -vp /usr/share/pixmaps
  for i in $LO_PREFIX/share/icons/hicolor/32x32/apps/*; do
    ln -svf $i /usr/share/pixmaps
  done &&

  # Desktop menu entries
  for i in $LO_PREFIX/lib/libreoffice/share/xdg/*; do
    ln -svf $i /usr/share/applications/libreoffice-$(basename $i)
  done &&

  # Man pages
  for i in $LO_PREFIX/share/man/man1/*; do
    ln -svf $i /usr/share/man/man1/
  done

  unset i
fi

   If you have installed [101]desktop-file-utils-0.24, and you wish to
   update the MIME database, issue, as the root user:
update-desktop-database

   The suite and main modules can be started from the menu. From a
   terminal, the suite can be started with libreoffice command and the
   modules with libreoffice --<module> command, respectively, where
   <module> is one of base, calc, draw, impress, math or writer. Modules
   cannot be started using their shell starting script names (see
   "Installed Programs", below), if LO_PREFIX is other than /usr, unless
   $LO_PREFIX/bin directory is appended to the PATH.

Command Explanations

   sed -e ...: The first sed prevents compression of the manual pages, the
   second one prevents the install from failing, the third allows to build
   with OpenJDK-12, and the third and fourth allow poppler-0.85.0 to be
   used.

   --with-vendor=BLFS: This switch sets BLFS as the vendor which is
   mentioned when you click "About" from the Help menu on the toolbar.

   --with-lang='fr en-GB': This switch sets what languages to support. To
   list several languages, separate them with a space. For all languages,
   use --with-lang=ALL.
   [Note]

Note

   For a list of the available languages, look in solenv/inc/langlist.mk.

   --with-help: Without this switch, the help files are not built.

   --with-jdk-home=/opt/jdk: This parameter will silence a warning that
   the configure script attempted to find JAVA_HOME automatically (even
   though that is passed in the environment). Omit this if you disabled
   java.

   --with-myspell-dicts: This switch adds myspell dictionaries to the
   LibreOffice installation set.

   --with-system-boost: This switch enables using system boost. From time
   to time, recent versions of boost break the build of libreoffice. In
   this case, it is possible to use the bundled copy of boost, by removing
   this flag.

   --disable-dconf: This switch disables compiling LibreOffice with the
   GNOME DConf configuration system support.

   --without-junit: This switch disables the tests for the deprecated
   HSQLDB driver which is written in Java. If you wish to build this
   driver replace this switch with the set of switches described below
   after '--without-java'.

   --without-system-dicts: This switch disables use of dictionaries from
   system paths, so the ones installed by this package are used.

   --disable-odk: This switch disables installing the office development
   kit. Remove if you want to develop a LibreOffice based application.

   --enable-release-build=yes: This switch enables a Release Build.
   LibreOffice can be built as a Release Build or as a Developer Build,
   because their default installation paths and user profile paths are
   different. Developer Build displays the words "Dev" and "Beta" in
   several places (e.g, menu and splash screen).

   --enable-python=system: This switch tells LibreOffice to use installed
   Python 3 instead of the bundled one.

   --with-system-*: These switches prevent LibreOffice from trying to
   compile its own versions of these dependencies. If you've not installed
   some of the dependencies, remove the corresponding switches.

   make distro-pack-install: this does a normal install, but if you add a
   DESTDIR environment variable it will also install a lot of (text)
   gid_Module_* files in the DESTDIR, to help distributions which want to
   break the package into parts.

   --with-parallelism=<count>: This switch tells LibreOffice to use
   <count> CPU cores to compile in parallel. (Do not include literal angle
   brackets.) The default is to use all available cores on the system.

   --disable-cups: Use this switch if you don't need printing support.

   --disable-dbus: Use this switch if you've not installed D-Bus-1.8.0 or
   later. It also disables Bluetooth support and font install via
   PackageKit.

   --disable-firebird-sdbc: By default, the ability to connect to a
   firebird database is enabled. Add this switch if you do not need that
   feature.

   --disable-gstreamer-1-0: Use this switch if you have not installed
   [102]gst-plugins-base-1.16.2.

   --disable-postgresql-sdbc: This switch disables compiling LibreOffice
   with the ability to connect to a PostgreSQL database. Use it if you
   have not installed PostgreSQL and you do not want LibreOffice to
   compile its bundled copy.

   --enable-gtk3-kde5: This switch allows the Visual Class Library, which
   is responsible for widgets, to be built with KF5 file dialogs if
   GTK+-3, Qt5 and Plasma are all available.

   --enable-lto: This switch will enable Link Time Optimization, which
   results in slightly smaller libraries (about 40 MB). This is said to
   make LibreOffice programs load faster (and possibly run faster, e.g.
   when recalculating a spreadsheet). On an 8-core machine with 16GB of
   memory, the difference in compile times was minimal, but an extra 2GB
   was used for the work files. On machines with fewer processors or a
   lack of memory, the compile might be much slower.

   --without-java: This switch disables Java support in LibreOffice. Java
   is needed for the deprecated HSQLDB driver, which allows reading
   databases created by other programs or in earlier versions of
   libreoffice base.

   --with-junit=/opt/ant/lib/junit-4.12.jar and
   --with-hamcrest=/opt/ant/lib/hamcrest-core-1.3.jar: Those options are
   needed if you want to run the tests.

   --without-fonts: LibreOffice includes a number of third-party TrueType
   fonts. If you have already installed some or all of the same fonts, you
   may prefer to use the system versions.

   --enable-kde5: Builds with KDE/Plasma integration. If [103]Qt-5.14.1
   and/or [104]KDE Frameworks-5.67.0 are not installed in /usr, the
   include and library directories must be specified in QT5INC, QT5LIB,
   KF5INC, and KF5LIB, respectively.

   --with-system-icu: This command would use the system version of
   [105]ICU-65.1 which is normally considered to be a good thing to do.
   However, one of the very minor packages pulled in by LibreOffice,
   libfreehand-0.1.2, does not compile with ICU-65.1 because of a missing
   semi-colon at the end of one line in one of its files. Do not use this
   option unless you have separately installed a patched or newer version
   of [106]libfreehand.

Contents

   Installed Programs: libreoffice, lobase, localc, lodraw, loffice,
   lofromtemplate, loimpress, lomath, loweb, lowriter, soffice and unopkg;
   several programs under $LO_PREFIX/lib/libreoffice/program
   Installed Libraries: several libraries under
   $LO_PREFIX/lib/libreoffice/program
   Installed Directory: $LO_PREFIX/lib/libreoffice

Short Descriptions

   libreoffice (or soffice)

   is the main libreofice suite (symlink to
   $LO_PREFIX/lib/libreoffice/program/soffice).

   lobase

   is the database manager module shell starting script.

   localc

   is the spreadsheet module shell starting script.

   lodraw

   is the vector graphics editor and diagramming tool module shell
   starting script.

   loimpress

   is the (PowerPoint) presentations editor and displayer module shell
   starting script.

   lomath

   is the mathematical formula editor module shell starting script.

   loweb

   is the HTML editor module shell starting script.

   lowriter

   is the word processor module shell starting script.

   unopkg

   is a tool to manage LibreOffice extensions from the command line.

   Last updated on 2020-02-25 07:28:56 -0800

     * [107]Prev
       Gnumeric-1.12.46
     * [108]Next
       Graphical Web Browsers
     * [109]Up
     * [110]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gnumeric.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/graphweb.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/office.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. http://download.documentfoundation.org/libreoffice/src/6.4.0/libreoffice-6.4.0.3.tar.xz
   6. http://download.documentfoundation.org/libreoffice/src/6.4.0/libreoffice-dictionaries-6.4.0.3.tar.xz
   7. http://download.documentfoundation.org/libreoffice/src/6.4.0/libreoffice-help-6.4.0.3.tar.xz
   8. http://download.documentfoundation.org/libreoffice/src/6.4.0/libreoffice-translations-6.4.0.3.tar.xz
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/perl-modules.html#perl-archive-zip
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/unzip.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/basicnet/wget.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/which.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/zip.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/apache-ant.html
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/apr.html
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/boost.html
  17. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/clucene.html
  18. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/pst/cups.html
  19. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/basicnet/curl.html
  20. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/dbus-glib.html
  21. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/libjpeg.html
  22. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/glm.html
  23. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/glu.html
  24. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/gpgme.html
  25. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/graphite2.html
  26. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/multimedia/gst10-plugins-base.html
  27. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/gtk3.html
  28. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/harfbuzz.html
  29. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/icu.html
  30. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/libatomic_ops.html
  31. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/lcms2.html
  32. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/librsvg.html
  33. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/libxml2.html
  34. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/libxslt.html
  35. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/python-modules.html#lxml
  36. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/mesa.html
  37. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/basicnet/neon.html
  38. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/nss.html
  39. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/server/openldap.html
  40. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/poppler.html
  41. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/server/postgresql.html
  42. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/redland.html
  43. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/basicnet/serf.html
  44. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/unixodbc.html
  45. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/basicnet/avahi.html
  46. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/bluez.html
  47. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/gnome/dconf.html
  48. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/desktop-file-utils.html
  49. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/doxygen.html
  50. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/gnome/evolution-data-server.html
  51. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/gdb.html
  52. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/gnutls.html
  53. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/kde/krameworks5.html
  54. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/libpaper.html
  55. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/server/mariadb.html
  56. http://www.mysql.com/
  57. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/mitkrb.html
  58. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/nasm.html
  59. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/pst/sane.html
  60. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/telepathy-glib.html
  61. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/valgrind.html
  62. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/multimedia/vlc.html
  63. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/gnome/zenity.html
  64. https://commons.apache.org/proper/commons-codec/
  65. https://hc.apache.org/httpclient-3.x/
  66. https://commons.apache.org/proper/commons-lang/
  67. https://commons.apache.org/proper/commons-logging/
  68. http://www.beanshell.org/
  69. https://projects.coin-or.org/CoinMP/
  70. http://sourceforge.net/projects/cppunit/
  71. http://www.firebirdsql.org/
  72. https://github.com/behdad/glyphy
  73. http://ftp.gnome.org/pub/gnome/sources/gnome-vfs/
  74. https://github.com/hamcrest/JavaHamcrest/releases
  75. https://hunspell.github.io/
  76. http://sourceforge.net/projects/hunspell/files/Hyphen/
  77. http://include-what-you-use.org/
  78. https://wiki.documentfoundation.org/DLP/Libraries/libabw
  79. https://wiki.documentfoundation.org/DLP/Libraries/libcdr
  80. http://sourceforge.net/projects/libcmis/
  81. http://www.sourceforge.net/projects/libebook/
  82. https://wiki.documentfoundation.org/Libexttextcat
  83. https://wiki.documentfoundation.org/DLP/Libraries/libfreehand
  84. http://tagoh.bitbucket.org/liblangtag/
  85. https://wiki.documentfoundation.org/DLP/Libraries/libmspub
  86. http://sourceforge.net/projects/libmwaw/
  87. http://sourceforge.net/projects/libwpd/files/libodfgen/
  88. https://wiki.documentfoundation.org/DLP/Libraries/libpagemaker
  89. http://sourceforge.net/projects/libwpd/files/librevenge/
  90. https://wiki.documentfoundation.org/DLP/Libraries/libvisio
  91. http://libwpd.sourceforge.net/
  92. http://libwpg.sourceforge.net/
  93. http://libwps.sourceforge.net/
  94. http://lpsolve.sourceforge.net/
  95. https://gitlab.com/mdds/mdds
  96. http://sourceforge.net/projects/hunspell/files/MyThes/
  97. https://github.com/KhronosGroup/OpenCOLLADA/
  98. https://gitlab.com/orcus/orcus
  99. https://ukoethe.github.io/vigra/
 100. http://wiki.linuxfromscratch.org/blfs/wiki/libreoffice
 101. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/desktop-file-utils.html
 102. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/multimedia/gst10-plugins-base.html
 103. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/qt5.html
 104. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/kde/krameworks5.html
 105. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/icu.html
 106. https://wiki.documentfoundation.org/DLP/Libraries/libfreehand
 107. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gnumeric.html
 108. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/graphweb.html
 109. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/office.html
 110. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
