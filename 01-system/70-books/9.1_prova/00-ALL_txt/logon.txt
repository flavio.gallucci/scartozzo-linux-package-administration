Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 3. After LFS Configuration Issues

     * [1]Prev
       The /etc/vimrc and ~/.vimrc Files
     * [2]Next
       Random Number Generation
     * [3]Up
     * [4]Home

Customizing your Logon with /etc/issue

   When you first boot up your new LFS system, the logon screen will be
   nice and plain (as it should be in a bare-bones system). Many people
   however, will want their system to display some information in the
   logon message. This can be accomplished using the file /etc/issue.

   The /etc/issue file is a plain text file which will also accept certain
   escape sequences (see below) in order to insert information about the
   system. There is also the file issue.net which can be used when logging
   on remotely. ssh however, will only use it if you set the option in the
   configuration file and will not interpret the escape sequences shown
   below.

   One of the most common things which people want to do is clear the
   screen at each logon. The easiest way of doing that is to put a "clear"
   escape sequence into /etc/issue. A simple way of doing this is to issue
   the command clear > /etc/issue. This will insert the relevant escape
   code into the start of the /etc/issue file. Note that if you do this,
   when you edit the file, you should leave the characters (normally
   '^[[H^[[2J') on the first line alone.
   [Note]

Note

   Terminal escape sequences are special codes recognized by the terminal.
   The ^[ represents an ASCII ESC character. The sequence ESC [ H puts the
   cursor in the upper left hand corner of the screen and ESC 2 J erases
   the screen. For more information on terminal escape sequences see
   [5]http://rtfm.etla.org/xterm/ctlseq.html

   The following sequences are recognized by agetty (the program which
   usually parses /etc/issue). This information is from man agetty where
   you can find extra information about the logon process.

   The issue file can contain certain character sequences to display
   various information. All issue sequences consist of a backslash (\)
   immediately followed by one of the letters explained below (so \d in
   /etc/issue would insert the current date).
b   Insert the baudrate of the current line.
d   Insert the current date.
s   Insert the system name, the name of the operating system.
l   Insert the name of the current tty line.
m   Insert the architecture identifier of the machine, e.g., i686.
n   Insert the nodename of the machine, also known as the hostname.
o   Insert the domainname of the machine.
r   Insert the release number of the kernel, e.g., 2.6.11.12.
t   Insert the current time.
u   Insert the number of current users logged in.
U   Insert the string "1 user" or "<n> users" where <n> is the
    number of current users logged in.
v   Insert the version of the OS, e.g., the build-date etc.

   Last updated on 2007-04-04 12:42:53 -0700

     * [6]Prev
       The /etc/vimrc and ~/.vimrc Files
     * [7]Next
       Random Number Generation
     * [8]Up
     * [9]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/vimrc.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/random.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/config.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. http://rtfm.etla.org/xterm/ctlseq.html
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/vimrc.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/random.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/config.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
