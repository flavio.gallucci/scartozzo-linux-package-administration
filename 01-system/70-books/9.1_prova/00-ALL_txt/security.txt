Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Part II. Post LFS Configuration and Extra Software

     * [1]Prev
       Random Number Generation
     * [2]Next
       Vulnerabilities
     * [3]Up
     * [4]Home

Security

   Security takes many forms in a computing environment. After some
   initial discussion, this chapter gives examples of three different
   types of security: access, prevention and detection.

   Access for users is usually handled by login or an application designed
   to handle the login function. In this chapter, we show how to enhance
   login by setting policies with PAM modules. Access via networks can
   also be secured by policies set by iptables, commonly referred to as a
   firewall. The Network Security Services (NSS) and Netscape Portable
   Runtime (NSPR) libraries can be installed and shared among the many
   applications requiring them. For applications that don't offer the best
   security, you can use the Stunnel package to wrap an application daemon
   inside an SSL tunnel.

   Prevention of breaches, like a trojan, are assisted by applications
   like GnuPG, specifically the ability to confirm signed packages, which
   recognizes modifications of the tarball after the packager creates it.

   Finally, we touch on detection with a package that stores "signatures"
   of critical files (defined by the administrator) and then regenerates
   those "signatures" and compares for files that have been changed.

Table of Contents

     * [5]Vulnerabilities
     * [6]make-ca-1.5
     * [7]CrackLib-2.9.7
     * [8]cryptsetup-2.0.6
     * [9]Cyrus SASL-2.1.27
     * [10]GnuPG-2.2.19
     * [11]GnuTLS-3.6.12
     * [12]GPGME-1.13.1
     * [13]Haveged-1.9.2
     * [14]iptables-1.8.4
     * [15]Setting Up a Network Firewall
     * [16]libcap-2.31 with PAM
     * [17]Linux-PAM-1.3.1
     * [18]liboauth-1.0.3
     * [19]libpwquality-1.4.2
     * [20]MIT Kerberos V5-1.18
     * [21]Nettle-3.5.1
     * [22]NSS-3.50
     * [23]OpenSSH-8.2p1
     * [24]p11-kit-0.23.20
     * [25]Polkit-0.116
     * [26]Shadow-4.8.1
     * [27]ssh-askpass-8.2p1
     * [28]stunnel-5.56
     * [29]Sudo-1.8.31
     * [30]Tripwire-2.4.3.7
     * [31]volume_key-0.3.12

     * [32]Prev
       Random Number Generation
     * [33]Next
       Vulnerabilities
     * [34]Up
     * [35]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/random.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/vulnerabilities.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/postlfs.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/vulnerabilities.html
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/make-ca.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/cracklib.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/cryptsetup.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/cyrus-sasl.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gnupg.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gnutls.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gpgme.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/haveged.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/iptables.html
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/firewall.html
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libcap.html
  17. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/linux-pam.html
  18. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/liboauth.html
  19. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libpwquality.html
  20. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/mitkrb.html
  21. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/nettle.html
  22. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/nss.html
  23. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/openssh.html
  24. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/p11-kit.html
  25. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/polkit.html
  26. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/shadow.html
  27. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/ssh-askpass.html
  28. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/stunnel.html
  29. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/sudo.html
  30. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/tripwire.html
  31. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/volume_key.html
  32. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/random.html
  33. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/vulnerabilities.html
  34. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/postlfs.html
  35. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
