Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 4. Security

     * [1]Prev
       iptables-1.8.4
     * [2]Next
       libcap-2.31 with PAM
     * [3]Up
     * [4]Home

Setting Up a Network Firewall

   Before you read this part of the chapter, you should have already
   installed iptables as described in the previous section.

Introduction to Firewall Creation

   The general purpose of a firewall is to protect a computer or a network
   against malicious access.

   In a perfect world, every daemon or service on every machine is
   perfectly configured and immune to flaws such as buffer overflows or
   other problems regarding its security. Furthermore, you trust every
   user accessing your services. In this world, you do not need to have a
   firewall.

   In the real world however, daemons may be misconfigured and exploits
   against essential services are freely available. You may wish to choose
   which services are accessible by certain machines or you may wish to
   limit which machines or applications are allowed external access.
   Alternatively, you may simply not trust some of your applications or
   users. You are probably connected to the Internet. In this world, a
   firewall is essential.

   Don't assume however, that having a firewall makes careful
   configuration redundant, or that it makes any negligent
   misconfiguration harmless. It doesn't prevent anyone from exploiting a
   service you intentionally offer but haven't recently updated or patched
   after an exploit went public. Despite having a firewall, you need to
   keep applications and daemons on your system properly configured and up
   to date. A firewall is not a cure all, but should be an essential part
   of your overall security strategy.

Meaning of the Word "Firewall"

   The word firewall can have several different meanings.

[5]Personal Firewall

   This is a hardware device or software program commercially sold (or
   offered via freeware) by companies such as Symantec which claims that
   it secures a home or desktop computer connected to the Internet. This
   type of firewall is highly relevant for users who do not know how their
   computers might be accessed via the Internet or how to disable that
   access, especially if they are always online and connected via
   broadband links.

[6]Masquerading Router

   This is a system placed between the Internet and an intranet. To
   minimize the risk of compromising the firewall itself, it should
   generally have only one role—that of protecting the intranet. Although
   not completely risk free, the tasks of doing the routing and IP
   masquerading (rewriting IP headers of the packets it routes from
   clients with private IP addresses onto the Internet so that they seem
   to come from the firewall itself) are commonly considered relatively
   secure.

[7]BusyBox

   This is often an old computer you may have retired and nearly
   forgotten, performing masquerading or routing functions, but offering
   non-firewall services such as a web-cache or mail. This may be used for
   home networks, but is not to be considered as secure as a firewall only
   machine because the combination of server and router/firewall on one
   machine raises the complexity of the setup.

Firewall with a Demilitarized Zone [Not Further Described Here]

   This box performs masquerading or routing, but grants public access to
   some branch of your network which, because of public IPs and a
   physically separated structure, is essentially a separate network with
   direct Internet access. The servers on this network are those which
   must be easily accessible from both the Internet and intranet. The
   firewall protects both networks. This type of firewall has a minimum of
   three network interfaces.

Packetfilter

   This type of firewall does routing or masquerading, but does not
   maintain a state table of ongoing communication streams. It is fast,
   but quite limited in its ability to block undesired packets without
   blocking desired packets.

Now You Can Start to Build your Firewall

   [Caution]

Caution

   This introduction on how to setup a firewall is not a complete guide to
   securing systems. Firewalling is a complex issue that requires careful
   configuration. The scripts quoted here are simply intended to give
   examples of how a firewall works. They are not intended to fit into any
   particular configuration and may not provide complete protection from
   an attack.

   Customization of these scripts for your specific situation will be
   necessary for an optimal configuration, but you should make a serious
   study of the iptables documentation and creating firewalls in general
   before hacking away. Have a look at the list of [8]links for further
   reading at the end of this section for more details. There you will
   find a list of URLs that contain quite comprehensive information about
   building your own firewall.

   The firewall configuration script installed in the iptables section
   differs from the standard configuration script. It only has two of the
   standard targets: start and status. The other targets are clear and
   lock. For instance if you issue:
/etc/rc.d/init.d/iptables start

   the firewall will be restarted just as it is upon system startup. The
   status target will present a list of all currently implemented rules.
   The clear target turns off all firewall rules and the lock target will
   block all packets in and out of the computer with the exception of the
   loopback interface.

   The main startup firewall is located in the file /etc/rc.d/rc.iptables.
   The sections below provide three different approaches that can be used
   for a system.
   [Note]

Note

   You should always run your firewall rules from a script. This ensures
   consistency and a record of what was done. It also allows retention of
   comments that are essential for understanding the rules long after they
   were written.

Personal Firewall

   A Personal Firewall is designed to let you access all the services
   offered on the Internet, but keep your box secure and your data
   private.

   Below is a slightly modified version of Rusty Russell's recommendation
   from the [9]Linux 2.4 Packet Filtering HOWTO. It is still applicable to
   the Linux 2.6 kernels.
cat > /etc/rc.d/rc.iptables << "EOF"
#!/bin/sh

# Begin rc.iptables

# Insert connection-tracking modules
# (not needed if built into the kernel)
modprobe nf_conntrack
modprobe xt_LOG

# Enable broadcast echo Protection
echo 1 > /proc/sys/net/ipv4/icmp_echo_ignore_broadcasts

# Disable Source Routed Packets
echo 0 > /proc/sys/net/ipv4/conf/all/accept_source_route
echo 0 > /proc/sys/net/ipv4/conf/default/accept_source_route

# Enable TCP SYN Cookie Protection
echo 1 > /proc/sys/net/ipv4/tcp_syncookies

# Disable ICMP Redirect Acceptance
echo 0 > /proc/sys/net/ipv4/conf/default/accept_redirects

# Do not send Redirect Messages
echo 0 > /proc/sys/net/ipv4/conf/all/send_redirects
echo 0 > /proc/sys/net/ipv4/conf/default/send_redirects

# Drop Spoofed Packets coming in on an interface, where responses
# would result in the reply going out a different interface.
echo 1 > /proc/sys/net/ipv4/conf/all/rp_filter
echo 1 > /proc/sys/net/ipv4/conf/default/rp_filter

# Log packets with impossible addresses.
echo 1 > /proc/sys/net/ipv4/conf/all/log_martians
echo 1 > /proc/sys/net/ipv4/conf/default/log_martians

# be verbose on dynamic ip-addresses  (not needed in case of static IP)
echo 2 > /proc/sys/net/ipv4/ip_dynaddr

# disable Explicit Congestion Notification
# too many routers are still ignorant
echo 0 > /proc/sys/net/ipv4/tcp_ecn

# Set a known state
iptables -P INPUT   DROP
iptables -P FORWARD DROP
iptables -P OUTPUT  DROP

# These lines are here in case rules are already in place and the
# script is ever rerun on the fly. We want to remove all rules and
# pre-existing user defined chains before we implement new rules.
iptables -F
iptables -X
iptables -Z

iptables -t nat -F

# Allow local-only connections
iptables -A INPUT  -i lo -j ACCEPT

# Free output on any interface to any ip for any service
# (equal to -P ACCEPT)
iptables -A OUTPUT -j ACCEPT

# Permit answers on already established connections
# and permit new connections related to established ones
# (e.g. port mode ftp)
iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT

# Log everything else. What's Windows' latest exploitable vulnerability?
iptables -A INPUT -j LOG --log-prefix "FIREWALL:INPUT "

# End $rc_base/rc.iptables
EOF
chmod 700 /etc/rc.d/rc.iptables

   This script is quite simple, it drops all traffic coming into your
   computer that wasn't initiated from your computer, but as long as you
   are simply surfing the Internet you are unlikely to exceed its limits.

   If you frequently encounter certain delays at accessing FTP servers,
   take a look at [10]BusyBox example number 4.

   Even if you have daemons or services running on your system, these will
   be inaccessible everywhere but from your computer itself. If you want
   to allow access to services on your machine, such as ssh or ping, take
   a look at [11]BusyBox.

Masquerading Router

   A true Firewall has two interfaces, one connected to an intranet, in
   this example eth0, and one connected to the Internet, here ppp0. To
   provide the maximum security for the firewall itself, make sure that
   there are no unnecessary servers running on it such as X11 et al. As a
   general principle, the firewall itself should not access any untrusted
   service (think of a remote server giving answers that makes a daemon on
   your system crash, or even worse, that implements a worm via a
   buffer-overflow).
cat > /etc/rc.d/rc.iptables << "EOF"
#!/bin/sh

# Begin rc.iptables

echo
echo "You're using the example configuration for a setup of a firewall"
echo "from Beyond Linux From Scratch."
echo "This example is far from being complete, it is only meant"
echo "to be a reference."
echo "Firewall security is a complex issue, that exceeds the scope"
echo "of the configuration rules below."
echo "You can find additional information"
echo "about firewalls in Chapter 4 of the BLFS book."
echo "http://www.linuxfromscratch.org/blfs"
echo

# Insert iptables modules (not needed if built into the kernel).

modprobe nf_conntrack
modprobe nf_conntrack_ftp
modprobe xt_conntrack
modprobe xt_LOG
modprobe xt_state

# Enable broadcast echo Protection
echo 1 > /proc/sys/net/ipv4/icmp_echo_ignore_broadcasts

# Disable Source Routed Packets
echo 0 > /proc/sys/net/ipv4/conf/all/accept_source_route

# Enable TCP SYN Cookie Protection
echo 1 > /proc/sys/net/ipv4/tcp_syncookies

# Disable ICMP Redirect Acceptance
echo 0 > /proc/sys/net/ipv4/conf/all/accept_redirects

# Don't send Redirect Messages
echo 0 > /proc/sys/net/ipv4/conf/default/send_redirects

# Drop Spoofed Packets coming in on an interface where responses
# would result in the reply going out a different interface.
echo 1 > /proc/sys/net/ipv4/conf/default/rp_filter

# Log packets with impossible addresses.
echo 1 > /proc/sys/net/ipv4/conf/all/log_martians

# Be verbose on dynamic ip-addresses  (not needed in case of static IP)
echo 2 > /proc/sys/net/ipv4/ip_dynaddr

# Disable Explicit Congestion Notification
# Too many routers are still ignorant
echo 0 > /proc/sys/net/ipv4/tcp_ecn

# Set a known state
iptables -P INPUT   DROP
iptables -P FORWARD DROP
iptables -P OUTPUT  DROP

# These lines are here in case rules are already in place and the
# script is ever rerun on the fly. We want to remove all rules and
# pre-existing user defined chains before we implement new rules.
iptables -F
iptables -X
iptables -Z

iptables -t nat -F

# Allow local connections
iptables -A INPUT  -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# Allow forwarding if the initiated on the intranet
iptables -A FORWARD -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
iptables -A FORWARD ! -i ppp+ -m conntrack --ctstate NEW       -j ACCEPT

# Do masquerading
# (not needed if intranet is not using private ip-addresses)
iptables -t nat -A POSTROUTING -o ppp+ -j MASQUERADE

# Log everything for debugging
# (last of all rules, but before policy rules)
iptables -A INPUT   -j LOG --log-prefix "FIREWALL:INPUT "
iptables -A FORWARD -j LOG --log-prefix "FIREWALL:FORWARD "
iptables -A OUTPUT  -j LOG --log-prefix "FIREWALL:OUTPUT "

# Enable IP Forwarding
echo 1 > /proc/sys/net/ipv4/ip_forward
EOF
chmod 700 /etc/rc.d/rc.iptables

   With this script your intranet should be reasonably secure against
   external attacks. No one should be able to setup a new connection to
   any internal service and, if it's masqueraded, makes your intranet
   invisible to the Internet. Furthermore, your firewall should be
   relatively safe because there are no services running that a cracker
   could attack.
   [Note]

Note

   If the interface you're connecting to the Internet doesn't connect via
   PPP, you will need to change <ppp+> to the name of the interface (e.g.,
   eth1) which you are using.

BusyBox

   This scenario isn't too different from the [12]Masquerading Router, but
   additionally offers some services to your intranet. Examples of this
   can be when you want to administer your firewall from another host on
   your intranet or use it as a proxy or a name server.
   [Note]

Note

   Outlining a true concept of how to protect a server that offers
   services on the Internet goes far beyond the scope of this document.
   See the references at the end of this section for more information.

   Be cautious. Every service you have enabled makes your setup more
   complex and your firewall less secure. You are exposed to the risks of
   misconfigured services or running a service with an exploitable bug. A
   firewall should generally not run any extra services. See the
   introduction to the [13]Masquerading Router for some more details.

   If you want to add services such as internal Samba or name servers that
   do not need to access the Internet themselves, the additional
   statements are quite simple and should still be acceptable from a
   security standpoint. Just add the following lines into the script
   before the logging rules.
iptables -A INPUT  -i ! ppp+  -j ACCEPT
iptables -A OUTPUT -o ! ppp+  -j ACCEPT

   If daemons, such as squid, have to access the Internet themselves, you
   could open OUTPUT generally and restrict INPUT.
iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
iptables -A OUTPUT -j ACCEPT

   However, it is generally not advisable to leave OUTPUT unrestricted.
   You lose any control over trojans who would like to "call home", and a
   bit of redundancy in case you've (mis-)configured a service so that it
   broadcasts its existence to the world.

   To accomplish this, you should restrict INPUT and OUTPUT on all ports
   except those that it's absolutely necessary to have open. Which ports
   you have to open depends on your needs: mostly you will find them by
   looking for failed accesses in your log files.

   Have a Look at the Following Examples:
     * Squid is caching the web:
iptables -A OUTPUT -p tcp --dport 80 -j ACCEPT
iptables -A INPUT  -p tcp --sport 80 -m conntrack --ctstate ESTABLISHED \
  -j ACCEPT

     * Your caching name server (e.g., named) does its lookups via UDP:
iptables -A OUTPUT -p udp --dport 53 -j ACCEPT

     * You want to be able to ping your computer to ensure it's still
       alive:
iptables -A INPUT  -p icmp -m icmp --icmp-type echo-request -j ACCEPT
iptables -A OUTPUT -p icmp -m icmp --icmp-type echo-reply   -j ACCEPT

     * If you are frequently accessing FTP servers or enjoy chatting, you
       might notice certain delays because some implementations of these
       daemons have the feature of querying an identd on your system to
       obtain usernames. Although there's really little harm in this,
       having an identd running is not recommended because many security
       experts feel the service gives out too much additional information.
       To avoid these delays you could reject the requests with a
       'tcp-reset':
iptables -A INPUT  -p tcp --dport 113 -j REJECT --reject-with tcp-reset

     * To log and drop invalid packets (packets that came in after
       netfilter's timeout or some types of network scans) insert these
       rules at the top of the chain:
iptables -I INPUT 0 -p tcp -m conntrack --ctstate INVALID \
  -j LOG --log-prefix "FIREWALL:INVALID "
iptables -I INPUT 1 -p tcp -m conntrack --ctstate INVALID -j DROP

     * Anything coming from the outside should not have a private address,
       this is a common attack called IP-spoofing:
iptables -A INPUT -i ppp+ -s 10.0.0.0/8     -j DROP
iptables -A INPUT -i ppp+ -s 172.16.0.0/12  -j DROP
iptables -A INPUT -i ppp+ -s 192.168.0.0/16 -j DROP

       There are other addresses that you may also want to drop:
       0.0.0.0/8, 127.0.0.0/8, 224.0.0.0/3 (multicast and experimental),
       169.254.0.0/16 (Link Local Networks), and 192.0.2.0/24 (IANA
       defined test network).
     * If your firewall is a DHCP client, you need to allow those packets:
iptables -A INPUT  -i ppp0 -p udp -s 0.0.0.0 --sport 67 \
   -d 255.255.255.255 --dport 68 -j ACCEPT

     * To simplify debugging and be fair to anyone who'd like to access a
       service you have disabled, purposely or by mistake, you could
       REJECT those packets that are dropped.
       Obviously this must be done directly after logging as the very last
       lines before the packets are dropped by policy:
iptables -A INPUT -j REJECT

   These are only examples to show you some of the capabilities of the
   firewall code in Linux. Have a look at the man page of iptables. There
   you will find much more information. The port numbers needed for this
   can be found in /etc/services, in case you didn't find them by trial
   and error in your log file.

Conclusion

   Finally, there is one fact you must not forget: The effort spent
   attacking a system corresponds to the value the cracker expects to gain
   from it. If you are responsible for valuable information, you need to
   spend the time to protect it properly.

Extra Information

Where to Start with Further Reading on Firewalls

   [14]www.netfilter.org - Homepage of the netfilter/iptables project
   [15]Netfilter related FAQ
   [16]Netfilter related HOWTO's
   [17]en.tldp.org/LDP/nag2/x-087-2-firewall.html
   [18]en.tldp.org/HOWTO/Security-HOWTO.html
   [19]en.tldp.org/HOWTO/Firewall-HOWTO.html
   [20]www.linuxsecurity.com/docs/
   [21]www.little-idiot.de/firewall (German & outdated, but very comprehen
   sive)
   [22]linux.oreillynet.com/pub/a/linux/2000/03/10/netadmin/ddos.html
   [23]staff.washington.edu/dittrich/misc/ddos
   [24]www.e-infomax.com/ipmasq
   [25]www.circlemud.org/~jelson/writings/security/index.htm
   [26]www.securityfocus.com
   [27]www.cert.org - tech_tips
   [28]security.ittoolbox.com
   [29]www.insecure.org/reading.html

   Last updated on 2020-02-26 08:20:10 -0800

     * [30]Prev
       iptables-1.8.4
     * [31]Next
       libcap-2.31 with PAM
     * [32]Up
     * [33]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/iptables.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libcap.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/security.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/firewall.html#fw-persFw
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/firewall.html#fw-masqRouter
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/firewall.html#fw-busybox
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/firewall.html#fw-library
   9. http://www.netfilter.org/documentation/HOWTO/packet-filtering-HOWTO.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/firewall.html#fw-BB-4
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/firewall.html#fw-busybox
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/firewall.html#fw-masqRouter
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/firewall.html#fw-masqRouter
  14. http://www.netfilter.org/
  15. http://www.netfilter.org/documentation/FAQ/netfilter-faq.html
  16. http://www.netfilter.org/documentation/index.html#HOWTO
  17. http://en.tldp.org/LDP/nag2/x-087-2-firewall.html
  18. http://en.tldp.org/HOWTO/Security-HOWTO.html
  19. http://en.tldp.org/HOWTO/Firewall-HOWTO.html
  20. http://www.linuxsecurity.com/docs/
  21. http://www.little-idiot.de/firewall
  22. http://linux.oreillynet.com/pub/a/linux/2000/03/10/netadmin/ddos.html
  23. http://staff.washington.edu/dittrich/misc/ddos
  24. http://www.e-infomax.com/ipmasq
  25. http://www.circlemud.org/~jelson/writings/security/index.htm
  26. http://www.securityfocus.com/
  27. http://www.cert.org/tech_tips/
  28. http://security.ittoolbox.com/
  29. http://www.insecure.org/reading.html
  30. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/iptables.html
  31. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libcap.html
  32. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/security.html
  33. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
