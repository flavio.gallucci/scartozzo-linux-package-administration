Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 24. X Window System Environment

     * [1]Prev
       util-macros-1.19.2
     * [2]Next
       libXau-1.0.9
     * [3]Up
     * [4]Home

xorgproto-2019.2

Introduction to xorgproto

   The xorgproto package provides the header files required to build the X
   Window system, and to allow other applications to build against the
   installed X Window system.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://xorg.freedesktop.org/archive/individual/proto/xorgproto-
       2019.2.tar.bz2
     * Download MD5 sum: a02dcaff48b4141b949ac99dfc344d86
     * Download size: 828 KB
     * Estimated disk space required: 8.2 MB
     * Estimated build time: less than 0.1 SBU

xorgproto Dependencies

Required

   [6]util-macros-1.19.2

Optional

   [7]fop-2.4, [8]libxslt-1.1.34, [9]xmlto-0.0.28 and [10]asciidoc-8.6.9
   (to build additional documentation)
   [Note]

Note

   There is a reciprocal dependency with [11]fop-2.4. If you wish to build
   the documentation, you'll need to re-install the Protocol Headers after
   the installation is complete and [12]fop-2.4 has been installed.

   User Notes:
   [13]http://wiki.linuxfromscratch.org/blfs/wiki/Xorg7ProtocolHeaders

Installation of xorgproto

   Install xorgproto by running the following commands:
mkdir build &&
cd    build &&

meson --prefix=$XORG_PREFIX .. &&
ninja

   This package does not come with a test suite.

   Now, as the root user:
ninja install &&

install -vdm 755 $XORG_PREFIX/share/doc/xorgproto-2019.2 &&
install -vm 644 ../[^m]*.txt ../PM_spec $XORG_PREFIX/share/doc/xorgproto-2019.2

Command Explanations

   install -vm 644 ../[^m]*.txt ../PM_spec ...: The meson build system
   does not install the text files into /usr/share/doc. The [^m] part
   prevents copying the meson_options.txt file.

Contents

   Installed Programs: None
   Installed Libraries: None
   Installed Directories: $XORG_PREFIX/include/GL,
   $XORG_PREFIX/include/X11, and $XORG_PREFIX/share/doc/xorgproto-2019.2

   Last updated on 2018-02-18 22:04:14 +0100

     * [14]Prev
       util-macros-1.19.2
     * [15]Next
       libXau-1.0.9
     * [16]Up
     * [17]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/util-macros.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libXau.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/installing.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://xorg.freedesktop.org/archive/individual/proto/xorgproto-2019.2.tar.bz2
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/util-macros.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/pst/fop.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/libxslt.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/pst/xmlto.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/asciidoc.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/pst/fop.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/pst/fop.html
  13. http://wiki.linuxfromscratch.org/blfs/wiki/Xorg7ProtocolHeaders
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/util-macros.html
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libXau.html
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/installing.html
  17. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
