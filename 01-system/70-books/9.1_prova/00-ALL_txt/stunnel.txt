Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 4. Security

     * [1]Prev
       ssh-askpass-8.2p1
     * [2]Next
       Sudo-1.8.31
     * [3]Up
     * [4]Home

stunnel-5.56

Introduction to stunnel

   The stunnel package contains a program that allows you to encrypt
   arbitrary TCP connections inside SSL (Secure Sockets Layer) so you can
   easily communicate with clients over secure channels. stunnel can be
   used to add SSL functionality to commonly used Inetd daemons such as
   POP-2, POP-3, and IMAP servers, along with standalone daemons such as
   NNTP, SMTP, and HTTP. stunnel can also be used to tunnel PPP over
   network sockets without changes to the server package source code.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (FTP):
       [5]ftp://ftp.stunnel.org/stunnel/archive/5.x/stunnel-5.56.tar.gz
     * Download MD5 sum: 01b0ca9e071f582ff803a85d5ed72166
     * Download size: 960 KB
     * Estimated disk space required: 7.5 MB
     * Estimated build time: 0.1 SBU

stunnel Dependencies

Optional

   [6]netcat (required for tests), [7]tcpwrappers and [8]TOR

   User Notes: [9]http://wiki.linuxfromscratch.org/blfs/wiki/stunnel

Installation of stunnel

   The stunnel daemon will be run in a chroot jail by an unprivileged
   user. Create the new user and group using the following commands as the
   root user:
groupadd -g 51 stunnel &&
useradd -c "stunnel Daemon" -d /var/lib/stunnel \
        -g stunnel -s /bin/false -u 51 stunnel

   [Note]

Note

   A signed SSL Certificate and a Private Key is necessary to run the
   stunnel daemon. After the package is installed, there are instructions
   to generate them. However, if you own or have already created a signed
   SSL Certificate you wish to use, copy it to /etc/stunnel/stunnel.pem
   before starting the build (ensure only root has read and write access).
   The .pem file must be formatted as shown below:
-----BEGIN PRIVATE KEY-----
<many encrypted lines of private key>
-----END PRIVATE KEY-----
-----BEGIN CERTIFICATE-----
<many encrypted lines of certificate>
-----END CERTIFICATE-----
-----BEGIN DH PARAMETERS-----
<encrypted lines of dh parms>
-----END DH PARAMETERS-----

   Install stunnel by running the following commands:
   [Note]

Note

   For some systems with binutils versions prior to 2.25, configure may
   fail. If necessary, fix it either with:
sed -i '/LDFLAGS.*static_flag/ s/^/#/' configure

   or, if [10]LLVM-9.0.1 with Clang is installed, you can replace
   ./configure ... with CC=clang ./configure ... in the first command
   below.
./configure --prefix=/usr        \
            --sysconfdir=/etc    \
            --localstatedir=/var \
            --disable-systemd    &&
make

   If you have installed the optional netcat application, the regression
   tests can be run with make check.

   Now, as the root user:
make docdir=/usr/share/doc/stunnel-5.56 install

   If you do not already have a signed SSL Certificate and Private Key,
   create the stunnel.pem file in the /etc/stunnel directory using the
   command below. You will be prompted to enter the necessary information.
   Ensure you reply to the
Common Name (FQDN of your server) [localhost]:

   prompt with the name or IP address you will be using to access the
   service(s).

   To generate a certificate, as the root user, issue:
make cert

Command Explanations

   --disable-systemd: This switch disables systemd socket activation
   support which is not available in BLFS.

   make docdir=... install: This command installs the package and changes
   the documentation installation directory to standard naming
   conventions.

Configuring stunnel

Config Files

   /etc/stunnel/stunnel.conf

Configuration Information

   As the root user, create the directory used for the .pid file created
   when the stunnel daemon starts:
install -v -m750 -o stunnel -g stunnel -d /var/lib/stunnel/run &&
chown stunnel:stunnel /var/lib/stunnel

   Next, create a basic /etc/stunnel/stunnel.conf configuration file using
   the following commands as the root user:
cat >/etc/stunnel/stunnel.conf << "EOF"
; File: /etc/stunnel/stunnel.conf

; Note: The pid and output locations are relative to the chroot location.

pid    = /run/stunnel.pid
chroot = /var/lib/stunnel
client = no
setuid = stunnel
setgid = stunnel
cert   = /etc/stunnel/stunnel.pem

;debug = 7
;output = stunnel.log

;[https]
;accept  = 443
;connect = 80
;; "TIMEOUTclose = 0" is a workaround for a design flaw in Microsoft SSL
;; Microsoft implementations do not use SSL close-notify alert and thus
;; they are vulnerable to truncation attacks
;TIMEOUTclose = 0

EOF

   Finally, add the service(s) you wish to encrypt to the configuration
   file. The format is as follows:
[<service>]
accept  = <hostname:portnumber>
connect = <hostname:portnumber>

   If you use stunnel to encrypt a daemon started from [x]inetd, you may
   need to disable that daemon in the /etc/[x]inetd.conf file and enable a
   corresponding <service>_stunnel service. You may have to add an
   appropriate entry in /etc/services as well.

   For a full explanation of the commands and syntax used in the
   configuration file, issue man stunnel.

Boot Script

   To automatically start the stunnel daemon when the system is booted,
   install the /etc/rc.d/init.d/stunnel bootscript from the
   [11]blfs-bootscripts-20191204 package.
make install-stunnel

Contents

   Installed Programs: stunnel and stunnel3
   Installed Library: libstunnel.so
   Installed Directories: /{etc,usr/lib,var/lib}/stunnel and
   /usr/share/doc/stunnel-5.56

Short Descriptions

   stunnel

   is a program designed to work as an SSL encryption wrapper between
   remote clients and local ({x}inetd-startable) or remote servers.

   stunnel3

   is a Perl wrapper script to use stunnel 3.x syntax with stunnel >=4.05.

   libstunnel.so

   contains the API functions required by stunnel.

   Last updated on 2020-02-20 12:41:28 -0800

     * [12]Prev
       ssh-askpass-8.2p1
     * [13]Next
       Sudo-1.8.31
     * [14]Up
     * [15]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/ssh-askpass.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/sudo.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/security.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. ftp://ftp.stunnel.org/stunnel/archive/5.x/stunnel-5.56.tar.gz
   6. http://netcat.sourceforge.net/
   7. ftp://ftp.porcupine.org/pub/security/
   8. https://dist.torproject.org/
   9. http://wiki.linuxfromscratch.org/blfs/wiki/stunnel
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/llvm.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/introduction/bootscripts.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/ssh-askpass.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/sudo.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/security.html
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
