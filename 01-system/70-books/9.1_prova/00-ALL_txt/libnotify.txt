Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 25. X Libraries

     * [1]Prev
       libglade-2.6.4
     * [2]Next
       libxklavier-5.4
     * [3]Up
     * [4]Home

libnotify-0.7.8

Introduction to libnotify

   The libnotify library is used to send desktop notifications to a
   notification daemon, as defined in the Desktop Notifications spec.
   These notifications can be used to inform the user about an event or
   display some form of information without getting in the user's way.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]http://ftp.gnome.org/pub/gnome/sources/libnotify/0.7/libnotify-0
       .7.8.tar.xz
     * Download (FTP):
       [6]ftp://ftp.gnome.org/pub/gnome/sources/libnotify/0.7/libnotify-0.
       7.8.tar.xz
     * Download MD5 sum: babb4b07b5f21bef42a386d3d7019599
     * Download size: 108 KB
     * Estimated disk space required: 3.2 MB
     * Estimated build time: 0.1 SBU

libnotify Dependencies

Required

   [7]GTK+-3.24.13

Optional (Required if building GNOME)

   [8]gobject-introspection-1.62.0

Optional

   [9]GTK-Doc-1.32

Required (runtime)

   [10]notification-daemon-3.20.0 or [11]xfce4-notifyd-0.4.4
   [Note]

Note

   GNOME Shell and KDE KWin provide their own notification daemons.

   User Notes: [12]http://wiki.linuxfromscratch.org/blfs/wiki/libnotify

Installation of libnotify

   Install libnotify by running the following commands:
mkdir build &&
cd    build &&

meson --prefix=/usr -Dgtk_doc=false .. &&
ninja

   This package does not come with a test suite.

   Now, as the root user:
ninja install

Command Explanations

   -Dgtk_doc=false: Remove this parameter if GTK-Doc is installed and you
   wish to rebuild and install the API documentation.

Contents

   Installed Program: notify-send
   Installed Library: libnotify.so
   Installed Directories: /usr/include/libnotify and
   /usr/share/gtk-doc/html/libnotify

Short Descriptions

   notify-send

   is a command used to send notifications.

   libnotify.so

   contains the libnotify API functions.

   Last updated on 2020-02-17 12:12:55 -0800

     * [13]Prev
       libglade-2.6.4
     * [14]Next
       libxklavier-5.4
     * [15]Up
     * [16]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libglade.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libxklavier.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lib.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. http://ftp.gnome.org/pub/gnome/sources/libnotify/0.7/libnotify-0.7.8.tar.xz
   6. ftp://ftp.gnome.org/pub/gnome/sources/libnotify/0.7/libnotify-0.7.8.tar.xz
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gtk3.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/gobject-introspection.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/gtk-doc.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/gnome/notification-daemon.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/xfce/xfce4-notifyd.html
  12. http://wiki.linuxfromscratch.org/blfs/wiki/libnotify
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libglade.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libxklavier.html
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lib.html
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
