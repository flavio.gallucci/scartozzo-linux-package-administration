Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 9. General Libraries

     * [1]Prev
       GLib-2.62.4
     * [2]Next
       GMime-2.6.23
     * [3]Up
     * [4]Home

GLibmm-2.62.0

Introduction to GLibmm

   The GLibmm package is a set of C++ bindings for GLib.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]http://ftp.gnome.org/pub/gnome/sources/glibmm/2.62/glibmm-2.62.0
       .tar.xz
     * Download (FTP):
       [6]ftp://ftp.gnome.org/pub/gnome/sources/glibmm/2.62/glibmm-2.62.0.
       tar.xz
     * Download MD5 sum: 7da228e3f0c6a10024b9a7708c53691e
     * Download size: 7.1 MB
     * Estimated disk space required: 210 MB (with tests)
     * Estimated build time: 1.3 SBU (Using parallelism=4; with tests)

GLibmm Dependencies

Required

   [7]GLib-2.62.4 and [8]libsigc++-2.10.2

Optional

   [9]Doxygen-1.8.17, [10]glib-networking-2.62.3 (for tests),
   [11]GnuTLS-3.6.12 (for tests), and [12]libxslt-1.1.34

   User Notes: [13]http://wiki.linuxfromscratch.org/blfs/wiki/glibmm

Installation of GLibmm

   First, fix the documents directory name:
sed -e '/^libdocdir =/ s/$(book_name)/glibmm-2.62.0/' \
    -i docs/Makefile.in

   Install GLibmm by running the following commands:
./configure --prefix=/usr &&
make

   To test the results, issue: make check.

   Now, as the root user:
make install

Contents

   Installed Programs: None
   Installed Libraries: libgiomm-2.4.so, libglibmm-2.4.so and
   libglibmm_generate_extra_defs-2.4.so
   Installed Directories: /usr/{include,lib}/g{io,lib}mm-2.4 and
   /usr/share/{devhelp/books/glibmm-2.4,doc/glibmm-2.62.0}

Short Descriptions

   libgiomm-2.4.so

   contains the GIO API classes.

   libglibmm-2.4.so

   contains the GLib API classes.

   Last updated on 2020-02-16 18:46:23 -0800

     * [14]Prev
       GLib-2.62.4
     * [15]Next
       GMime-2.6.23
     * [16]Up
     * [17]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/glib2.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gmime.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. http://ftp.gnome.org/pub/gnome/sources/glibmm/2.62/glibmm-2.62.0.tar.xz
   6. ftp://ftp.gnome.org/pub/gnome/sources/glibmm/2.62/glibmm-2.62.0.tar.xz
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/glib2.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libsigc.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/doxygen.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/basicnet/glib-networking.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/gnutls.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libxslt.html
  13. http://wiki.linuxfromscratch.org/blfs/wiki/glibmm
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/glib2.html
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gmime.html
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
  17. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
