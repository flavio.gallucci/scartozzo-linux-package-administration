Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 24. X Window System Environment

     * [1]Prev
       xcursor-themes-1.0.6
     * [2]Next
       XKeyboardConfig-2.29
     * [3]Up
     * [4]Home

Xorg Fonts

Introduction to Xorg Fonts

   The Xorg font packages provide some scalable fonts and supporting
   packages for Xorg applications. Many people will want to install other
   TTF or OTF fonts in addition to, or instead of, these. Some are listed
   at [5]the section called “TTF and OTF fonts”.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP): [6]https://www.x.org/pub/individual/font/
     * Download (FTP): [7]ftp://ftp.x.org/pub/individual/font/
     * Download size: 3.0 MB
     * Estimated disk space required: 8.8 MB
     * Estimated build time: 0.1 SBU

Xorg Fonts Dependencies

Required

   [8]xcursor-themes-1.0.6

   User Notes: [9]http://wiki.linuxfromscratch.org/blfs/wiki/Xorg7Fonts

Downloading Xorg Fonts

   First, create a list of files to be downloaded. This file will also be
   used to verify the integrity of the downloads when complete:
cat > font-7.md5 << "EOF"
3d6adb76fdd072db8c8fae41b40855e8  font-util-1.3.2.tar.bz2
bbae4f247b88ccde0e85ed6a403da22a  encodings-1.0.5.tar.bz2
6d25f64796fef34b53b439c2e9efa562  font-alias-1.0.3.tar.bz2
fcf24554c348df3c689b91596d7f9971  font-adobe-utopia-type1-1.0.4.tar.bz2
e8ca58ea0d3726b94fe9f2c17344be60  font-bh-ttf-1.0.3.tar.bz2
53ed9a42388b7ebb689bdfc374f96a22  font-bh-type1-1.0.3.tar.bz2
bfb2593d2102585f45daa960f43cb3c4  font-ibm-type1-1.0.3.tar.bz2
6306c808f7d7e7d660dfb3859f9091d2  font-misc-ethiopic-1.0.3.tar.bz2
3eeb3fb44690b477d510bbd8f86cf5aa  font-xfree86-type1-1.0.4.tar.bz2
EOF

   To download the needed files using wget, use the following commands:
mkdir font &&
cd font &&
grep -v '^#' ../font-7.md5 | awk '{print $2}' | wget -i- -c \
    -B https://www.x.org/pub/individual/font/ &&
md5sum -c ../font-7.md5

Installation of Xorg Fonts

   [Note]

Note

   When installing multiple packages in a script, the installation needs
   to be done as the root user. There are three general options that can
   be used to do this:
    1. Run the entire script as the root user (not recommended).
    2. Use the sudo command from the [10]Sudo-1.8.31 package.
    3. Use su -c "command arguments" (quotes required) which will ask for
       the root password for every iteration of the loop.

   One way to handle this situation is to create a short bash function
   that automatically selects the appropriate method. Once the command is
   set in the environment, it does not need to be set again.
as_root()
{
  if   [ $EUID = 0 ];        then $*
  elif [ -x /usr/bin/sudo ]; then sudo $*
  else                            su -c \\"$*\\"
  fi
}

export -f as_root

   First, start a subshell that will exit on error:
bash -e

   Install all of the packages by running the following commands:
for package in $(grep -v '^#' ../font-7.md5 | awk '{print $2}')
do
  packagedir=${package%.tar.bz2}
  tar -xf $package
  pushd $packagedir
    ./configure $XORG_CONFIG
    make
    as_root make install
  popd
  as_root rm -rf $packagedir
done

   Finally, exit the shell that was started earlier:
exit

   When all of the fonts have been installed, the system must be
   configured so that Fontconfig can find the TrueType fonts since they
   are outside of the default search path of /usr/share/fonts. Make
   symlinks to the Xorg TrueType font directories by running the following
   commands as the root user:
install -v -d -m755 /usr/share/fonts                               &&
ln -svfn $XORG_PREFIX/share/fonts/X11/OTF /usr/share/fonts/X11-OTF &&
ln -svfn $XORG_PREFIX/share/fonts/X11/TTF /usr/share/fonts/X11-TTF

Contents

   Installed Programs: bdftruncate and ucs2any
   Installed Libraries: None
   Installed Directories: $XORG_PREFIX/share/fonts

Short Descriptions

   bdftruncate

   generates a truncated BDF font from an ISO 10646-1 encoded BDF font.

   ucs2any

   generates BDF fonts in any encoding from an ISO 10646-1 encoded BDF
   font.

   Last updated on 2020-02-15 20:23:35 -0800

     * [11]Prev
       xcursor-themes-1.0.6
     * [12]Next
       XKeyboardConfig-2.29
     * [13]Up
     * [14]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/xcursor-themes.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/xkeyboard-config.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/installing.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/TTF-and-OTF-fonts.html
   6. https://www.x.org/pub/individual/font/
   7. ftp://ftp.x.org/pub/individual/font/
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/xcursor-themes.html
   9. http://wiki.linuxfromscratch.org/blfs/wiki/Xorg7Fonts
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/sudo.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/xcursor-themes.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/xkeyboard-config.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/installing.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
