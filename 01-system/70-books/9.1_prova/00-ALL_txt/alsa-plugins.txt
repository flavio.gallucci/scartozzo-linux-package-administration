Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 42. Multimedia Libraries and Drivers

     * [1]Prev
       alsa-lib-1.2.1.2
     * [2]Next
       alsa-utils-1.2.1
     * [3]Up
     * [4]Home

alsa-plugins-1.2.1

Introduction to ALSA Plugins

   The ALSA Plugins package contains plugins for various audio libraries
   and sound servers.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://www.alsa-project.org/files/pub/plugins/alsa-plugins-1.2.
       1.tar.bz2
     * Download (FTP):
       [6]ftp://ftp.alsa-project.org/pub/plugins/alsa-plugins-1.2.1.tar.bz
       2
     * Download MD5 sum: 5b11cd3ec92e5f9190ec378565b529e8
     * Download size: 400 KB
     * Estimated disk space required: 4.8 MB
     * Estimated build time: less than 0.1 SBU

ALSA Plugins Dependencies

Required

   [7]alsa-lib-1.2.1.2

Optional

   [8]FFmpeg-4.2.2, [9]libsamplerate-0.1.9, [10]PulseAudio-13.0,
   [11]Speex-1.2.0, [12]JACK, and [13]maemo

   User Notes: [14]http://wiki.linuxfromscratch.org/blfs/wiki/alsa-plugins

Installation of ALSA Plugins

   Install ALSA Plugins by running the following commands:
./configure --sysconfdir=/etc &&
make

   This package does not come with a test suite.

   Now, as the root user:
make install

Contents

   Installed Programs: None
   Installed Libraries: Numerous libasound_module_<module>.so modules
   including ctl_oss, ctl_pulse, pcm_a52, pcm_jack, pcm_oss, pcm_pulse,
   pcm_upmix, pcm_usb_stream, pcm_vdownmix, rate_lavcrate*,
   rate_samplerate*, and rate_speexrate*
   Installed Directories: None

Short Descriptions

   libasound_module_pcm_oss.so

   Allows native ALSA applications to run on OSS.

   libasound_module_pcm_upmix.so

   Allows upmixing sound to 4 or 6 channels.

   libasound_module_pcm_vdownmix.so

   Allows downmixing sound from 4-6 channels to 2 channel stereo output.

   libasound_module_pcm_jack.so

   Allows native ALSA applications to work with jackd.

   libasound_module_pcm_pulse.so

   Allows native ALSA applications to access a PulseAudio sound daemon.

   libasound_module_pcm_a52.so

   Converts S16 linear sound format to A52 compressed format and sends it
   to an SPDIF output.

   libasound_module_rate_samplerate.so

   Provides an external rate converter through libsamplerate.

   Last updated on 2020-02-17 18:27:03 -0800

     * [15]Prev
       alsa-lib-1.2.1.2
     * [16]Next
       alsa-utils-1.2.1
     * [17]Up
     * [18]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/alsa-lib.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/alsa-utils.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libdriv.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://www.alsa-project.org/files/pub/plugins/alsa-plugins-1.2.1.tar.bz2
   6. ftp://ftp.alsa-project.org/pub/plugins/alsa-plugins-1.2.1.tar.bz2
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/alsa-lib.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/ffmpeg.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libsamplerate.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/pulseaudio.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/speex.html
  12. http://jackaudio.org/
  13. http://maemo.org/
  14. http://wiki.linuxfromscratch.org/blfs/wiki/alsa-plugins
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/alsa-lib.html
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/alsa-utils.html
  17. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libdriv.html
  18. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
