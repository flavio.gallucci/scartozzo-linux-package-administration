Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 11. General Utilities

     * [1]Prev
       ISO Codes-4.4
     * [2]Next
       pinentry-1.1.0
     * [3]Up
     * [4]Home

lsof-4.91

Introduction to lsof

   The lsof package is useful to LiSt Open Files for a given running
   application or process.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (FTP):
       [5]https://www.mirrorservice.org/sites/lsof.itap.purdue.edu/pub/too
       ls/unix/lsof/lsof_4.91.tar.gz
     * Download MD5 sum: 10e1353aa4bf2fd5bbed65db9ef6fd47
     * Download size: 1.1 MB
     * Estimated disk space required: 9.6 MB
     * Estimated build time: less than 0.1 SBU

lsof Dependencies

Required

   [6]libtirpc-1.2.5

   User Notes: [7]http://wiki.linuxfromscratch.org/blfs/wiki/lsof

Installation of lsof

   The contents of the lsof tarball include another tarball with the
   source code, which needs, in turn, to be unpacked. Install lsof by
   running the following commands:
tar -xf lsof_4.91_src.tar  &&
cd lsof_4.91_src           &&
./Configure -n linux       &&
make CFGL="-L./lib -ltirpc"

   This package does not come with a working test suite.

   Now, as the root user:
install -v -m0755 -o root -g root lsof /usr/bin &&
install -v lsof.8 /usr/share/man/man8

Command Explanations

   ./Configure -n linux: Avoid AFS, customization, and inventory checks,
   and use the linux dialect.

   make CFGL="-L./lib -ltirpc": Add the libtirpc libraries location to the
   make command.

Contents

   Installed Program: lsof
   Installed Libraries: None
   Installed Directories: None

Short Descriptions

   lsof

   lists open files for running processes.

   Last updated on 2020-02-17 12:03:00 -0800

     * [8]Prev
       ISO Codes-4.4
     * [9]Next
       pinentry-1.1.0
     * [10]Up
     * [11]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/iso-codes.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/pinentry.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genutils.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://www.mirrorservice.org/sites/lsof.itap.purdue.edu/pub/tools/unix/lsof/lsof_4.91.tar.gz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/basicnet/libtirpc.html
   7. http://wiki.linuxfromscratch.org/blfs/wiki/lsof
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/iso-codes.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/pinentry.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genutils.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
