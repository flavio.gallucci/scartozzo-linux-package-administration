Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 13. Programming

     * [1]Prev
       SWIG-4.0.1
     * [2]Next
       Tk-8.6.10
     * [3]Up
     * [4]Home

Tcl-8.6.10

Introduction to Tcl

   The Tcl package contains the Tool Command Language, a robust
   general-purpose scripting language.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://downloads.sourceforge.net/tcl/tcl8.6.10-src.tar.gz
     * Download MD5 sum: 97c55573f8520bcab74e21bfd8d0aadc
     * Download size: 9.7 MB
     * Estimated disk space required: 67 MB (including html documentation)
     * Estimated build time: 0.7 SBU (add 2.7 SBU for tests)

Additional Downloads

   Optional Documentation
     * Download (HTTP):
       [6]https://downloads.sourceforge.net/tcl/tcl8.6.10-html.tar.gz
     * Download MD5 sum: a012711241ba3a5bd4a04e833001d489
     * Download size: 1.2 MB

   User Notes: [7]http://wiki.linuxfromscratch.org/blfs/wiki/tcl

Installation of Tcl

   This package is also installed in LFS during the bootstrap phase. As it
   is not installed during Chapter 6 of LFS, installation instructions are
   included here in BLFS.

   If you downloaded the optional documentation, unpack the tarball by
   issuing the following command:
tar -xf ../tcl8.6.10-html.tar.gz --strip-components=1

   Install Tcl by running the following commands:
export SRCDIR=`pwd` &&

cd unix &&

./configure --prefix=/usr           \
            --mandir=/usr/share/man \
            $([ $(uname -m) = x86_64 ] && echo --enable-64bit) &&
make &&

sed -e "s#$SRCDIR/unix#/usr/lib#" \
    -e "s#$SRCDIR#/usr/include#"  \
    -i tclConfig.sh               &&

sed -e "s#$SRCDIR/unix/pkgs/tdbc1.1.1#/usr/lib/tdbc1.1.1#" \
    -e "s#$SRCDIR/pkgs/tdbc1.1.1/generic#/usr/include#"    \
    -e "s#$SRCDIR/pkgs/tdbc1.1.1/library#/usr/lib/tcl8.6#" \
    -e "s#$SRCDIR/pkgs/tdbc1.1.1#/usr/include#"            \
    -i pkgs/tdbc1.1.1/tdbcConfig.sh                        &&

sed -e "s#$SRCDIR/unix/pkgs/itcl4.2.0#/usr/lib/itcl4.2.0#" \
    -e "s#$SRCDIR/pkgs/itcl4.2.0/generic#/usr/include#"    \
    -e "s#$SRCDIR/pkgs/itcl4.2.0#/usr/include#"            \
    -i pkgs/itcl4.2.0/itclConfig.sh                        &&

unset SRCDIR

   To test the results, issue: make test.

   Now, as the root user:
make install &&
make install-private-headers &&
ln -v -sf tclsh8.6 /usr/bin/tclsh &&
chmod -v 755 /usr/lib/libtcl8.6.so

   If you downloaded the optional documentation, install it by issuing the
   following commands as the root user:
mkdir -v -p /usr/share/doc/tcl-8.6.10 &&
cp -v -r  ../html/* /usr/share/doc/tcl-8.6.10

Command Explanations

   $([ $(uname -m) = x86_64 ] && echo --enable-64bit): This switch is used
   to enable 64 bit support in Tcl on 64 bit operating systems.

   make install-private-headers: This command is used to install the Tcl
   library interface headers used by other packages if they link to the
   Tcl library.

   ln -v -sf tclsh8.6 /usr/bin/tclsh: This command is used to create a
   compatibility symbolic link to the tclsh8.6 file as many packages
   expect a file named tclsh.

   sed -e ...: The Tcl package expects that its source tree is preserved
   so that packages depending on it for their compilation can utilize it.
   These sed remove the references to the build directory and replace them
   with saner system-wide locations.

Contents

   Installed Programs: sqlite3_analyzer, tclsh, and tclsh8.6
   Installed Libraries: libtcl8.6.so and libtclstub8.6.a
   Installed Directories: /usr/lib/itcl4.2.0, /usr/lib/sqlite3.30.1.2
   /usr/lib/tcl8, /usr/lib/tcl8.6, /usr/lib/tdbc1.1.1,
   /usr/lib/tdbcmysql1.1.1, /usr/lib/tdbcodbc1.1.1,
   /usr/lib/tdbcpostgres1.1.1, /usr/lib/thread2.8.5,
   /usr/share/doc/tcl-8.6.10, and /usr/share/man/mann

Short Descriptions

   sqlite3_analyzer

   is a program to analyze sqlite3 database files and output a report that
   gives information on size and storage efficiency for the database and
   it's tables and indexes.

   tclsh

   is a symlink to the tclsh8.6 program.

   tclsh8.6

   is a simple shell containing the Tcl interpreter.

   libtcl8.6.so

   contains the API functions required by Tcl.

   Last updated on 2020-02-15 08:54:30 -0800

     * [8]Prev
       SWIG-4.0.1
     * [9]Next
       Tk-8.6.10
     * [10]Up
     * [11]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/swig.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/tk.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/prog.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://downloads.sourceforge.net/tcl/tcl8.6.10-src.tar.gz
   6. https://downloads.sourceforge.net/tcl/tcl8.6.10-html.tar.gz
   7. http://wiki.linuxfromscratch.org/blfs/wiki/tcl
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/swig.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/tk.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/prog.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
