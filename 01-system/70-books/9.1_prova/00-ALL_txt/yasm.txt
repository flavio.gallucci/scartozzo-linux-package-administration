Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 13. Programming

     * [1]Prev
       Valgrind-3.15.0
     * [2]Next
       Other Programming Tools
     * [3]Up
     * [4]Home

yasm-1.3.0

Introduction to yasm

   Yasm is a complete rewrite of the [5]NASM-2.14.02 assembler. It
   supports the x86 and AMD64 instruction sets, accepts NASM and GAS
   assembler syntaxes and outputs binary, ELF32 and ELF64 object formats.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [6]http://www.tortall.net/projects/yasm/releases/yasm-1.3.0.tar.gz
     * Download MD5 sum: fc9e586751ff789b34b1f21d572d96af
     * Download size: 1.5 MB
     * Estimated disk space required: 27 MB (additional 12 MB for the
       tests)
     * Estimated build time: 0.1 SBU (additional 0.1 SBU for the tests)

yasm Dependencies

Optional

   [7]Python-2.7.17 and [8]Cython

   User Notes: [9]http://wiki.linuxfromscratch.org/blfs/wiki/yasm

Installation of yasm

   Install yasm by running the following commands:
sed -i 's#) ytasm.*#)#' Makefile.in &&

./configure --prefix=/usr &&
make

   To test the results, issue: make check.

   Now, as the root user:
make install

Command Explanations

   sed -i 's#) ytasm.*#)#' Makefile.in: This sed prevents it compiling 2
   programs (vsyasm and ytasm) that are only of use on Microsoft Windows.

Contents

   Installed Program: yasm
   Installed Library: libyasm.a
   Installed Directory: /usr/include/libyasm

Short Descriptions

   yasm

   is a portable, retargetable assembler that supports the x86 and AMD64
   instruction sets, accepts NASM and GAS assembler syntaxes and outputs
   binaries in ELF32 and ELF64 object formats.

   libyasm.a

   provides all of the core functionality of yasm, for manipulating
   machine instructions and object file constructs.

   Last updated on 2020-02-16 15:15:05 -0800

     * [10]Prev
       Valgrind-3.15.0
     * [11]Next
       Other Programming Tools
     * [12]Up
     * [13]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/valgrind.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/other-tools.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/prog.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/nasm.html
   6. http://www.tortall.net/projects/yasm/releases/yasm-1.3.0.tar.gz
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/python2.html
   8. http://cython.org/
   9. http://wiki.linuxfromscratch.org/blfs/wiki/yasm
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/valgrind.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/other-tools.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/prog.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
