Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 28. Icons

     * [1]Prev
       gnome-icon-theme-extras-3.12.0
     * [2]Next
       gnome-themes-extra-3.28
     * [3]Up
     * [4]Home

gnome-icon-theme-symbolic-3.12.0

Introduction to GNOME Icon Theme Symbolic

   The GNOME Icon Theme Symbolic package contains symbolic icons for the
   default GNOME icon theme.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]http://ftp.gnome.org/pub/gnome/sources/gnome-icon-theme-symbolic
       /3.12/gnome-icon-theme-symbolic-3.12.0.tar.xz
     * Download (FTP):
       [6]ftp://ftp.gnome.org/pub/gnome/sources/gnome-icon-theme-symbolic/
       3.12/gnome-icon-theme-symbolic-3.12.0.tar.xz
     * Download MD5 sum: 3c9c0e6b9fa04b3cbbb84da825a26fd9
     * Download size: 228 KB
     * Estimated disk space required: 6.8 MB
     * Estimated build time: less than 0.1 SBU

GNOME Icon Theme Symbolic Dependencies

Required

   [7]gnome-icon-theme-3.12.0

Optional

   [8]git-2.25.0 and [9]Inkscape-0.92.4

   User Notes:
   [10]http://wiki.linuxfromscratch.org/blfs/wiki/gnome-icon-theme-symboli
   c

Installation of GNOME Icon Theme Symbolic

   Install GNOME Icon Theme Symbolic by running the following commands:
./configure --prefix=/usr &&
make

   This package does not come with a test suite.

   Now, as the root user:
make install

Contents

   Installed Programs: None
   Installed Libraries: None
   Installed Directories: /usr/share/icons/gnome/scalable

   Last updated on 2020-02-20 12:41:28 -0800

     * [11]Prev
       gnome-icon-theme-extras-3.12.0
     * [12]Next
       gnome-themes-extra-3.28
     * [13]Up
     * [14]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gnome-icon-theme-extras.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gnome-themes-extra.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/icons.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. http://ftp.gnome.org/pub/gnome/sources/gnome-icon-theme-symbolic/3.12/gnome-icon-theme-symbolic-3.12.0.tar.xz
   6. ftp://ftp.gnome.org/pub/gnome/sources/gnome-icon-theme-symbolic/3.12/gnome-icon-theme-symbolic-3.12.0.tar.xz
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gnome-icon-theme.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/git.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/xsoft/inkscape.html
  10. http://wiki.linuxfromscratch.org/blfs/wiki/gnome-icon-theme-symbolic
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gnome-icon-theme-extras.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gnome-themes-extra.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/icons.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
