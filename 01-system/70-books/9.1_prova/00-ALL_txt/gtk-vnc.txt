Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 25. X Libraries

     * [1]Prev
       Gtkmm-3.24.2
     * [2]Next
       gtksourceview-3.24.11
     * [3]Up
     * [4]Home

gtk-vnc-1.0.0

Introduction to Gtk VNC

   The Gtk VNC package contains a VNC viewer widget for GTK+. It is built
   using coroutines allowing it to be completely asynchronous while
   remaining single threaded.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]http://ftp.gnome.org/pub/gnome/sources/gtk-vnc/1.0/gtk-vnc-1.0.0
       .tar.xz
     * Download (FTP):
       [6]ftp://ftp.gnome.org/pub/gnome/sources/gtk-vnc/1.0/gtk-vnc-1.0.0.
       tar.xz
     * Download MD5 sum: ff2e531525f6200be613643b9ee87fbf
     * Download size: 212 KB
     * Estimated disk space required: 8.6 MB
     * Estimated build time: 0.2 SBU

Gtk VNC Dependencies

Required

   [7]GnuTLS-3.6.12, [8]GTK+-3.24.13 and [9]libgcrypt-1.8.5

Recommended

   [10]gobject-introspection-1.62.0, [11]Python-2.7.17, and
   [12]Vala-0.46.6

Optional

   [13]Cyrus SASL-2.1.27 and [14]PulseAudio-13.0

   User Notes: [15]http://wiki.linuxfromscratch.org/blfs/wiki/gtk-vnc

Installation of Gtk VNC

   Install Gtk VNC by running the following commands:
mkdir build &&
cd    build &&

meson --prefix=/usr .. &&
ninja

   This package does not come with a testsuite.

   Now, as the root user:
ninja install

Command Explanations

   -Dwith-vala=false: This switch disables building of the Vala bindings.
   Add this if you decide to build gtk-vnc without vala installed.

Contents

   Installed Program: gvnccapture
   Installed Libraries: libgtk-vnc-2.0.so, libgvnc-1.0.so and
   libgvncpulse-1.0.so
   Installed Directories: /usr/include/gtk-vnc-2.0, /usr/include/gvnc-1.0
   and /usr/include/gvncpulse-1.0

Short Descriptions

   gvnccapture

   is used to capture image from VNC server.

   libgtk-vnc-2.0.so

   contains the GTK+ 3 bindings for Gtk VNC.

   libgvnc-1.0.so

   contains the GObject bindings for Gtk VNC.

   libgvncpulse-1.0.so

   is the PulseAudio bridge for Gtk VNC.

   Last updated on 2020-02-18 14:50:03 -0800

     * [16]Prev
       Gtkmm-3.24.2
     * [17]Next
       gtksourceview-3.24.11
     * [18]Up
     * [19]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gtkmm3.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gtksourceview.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lib.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. http://ftp.gnome.org/pub/gnome/sources/gtk-vnc/1.0/gtk-vnc-1.0.0.tar.xz
   6. ftp://ftp.gnome.org/pub/gnome/sources/gtk-vnc/1.0/gtk-vnc-1.0.0.tar.xz
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/gnutls.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gtk3.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/libgcrypt.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/gobject-introspection.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/python2.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/vala.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/cyrus-sasl.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/multimedia/pulseaudio.html
  15. http://wiki.linuxfromscratch.org/blfs/wiki/gtk-vnc
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gtkmm3.html
  17. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gtksourceview.html
  18. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lib.html
  19. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
