Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 42. Multimedia Libraries and Drivers

     * [1]Prev
       Speex-1.2.0
     * [2]Next
       v4l-utils-1.18.0
     * [3]Up
     * [4]Home

Taglib-1.11.1

Introduction to Taglib

   Taglib is a library used for reading, writing and manipulating audio
   file tags and is used by applications such as Amarok and VLC.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://taglib.github.io/releases/taglib-1.11.1.tar.gz
     * Download MD5 sum: cee7be0ccfc892fa433d6c837df9522a
     * Download size: 1.2 MB
     * Estimated disk space required: 9.4 MB
     * Estimated build time: 0.4 SBU

Taglib Dependencies

Required

   [6]CMake-3.16.4

   User Notes: [7]http://wiki.linuxfromscratch.org/blfs/wiki/taglib

Installation of Taglib

   Install Taglib by running the following commands:
mkdir build &&
cd    build &&

cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release  \
      -DBUILD_SHARED_LIBS=ON \
      .. &&
make

   This package does not come with a test suite.

   Now, as the root user:
make install

Contents

   Installed Programs: taglib-config
   Installed Libraries: libtag.so and libtag_c.so
   Installed Directories: /usr/include/taglib

Short Descriptions

   taglib-config

   is a tool used to print information about the taglib installation.

   Last updated on 2020-02-18 14:50:03 -0800

     * [8]Prev
       Speex-1.2.0
     * [9]Next
       v4l-utils-1.18.0
     * [10]Up
     * [11]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/speex.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/v4l-utils.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libdriv.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://taglib.github.io/releases/taglib-1.11.1.tar.gz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/cmake.html
   7. http://wiki.linuxfromscratch.org/blfs/wiki/taglib
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/speex.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/v4l-utils.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libdriv.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
