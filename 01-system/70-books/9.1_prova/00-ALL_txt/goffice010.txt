Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 25. X Libraries

     * [1]Prev
       GLU-9.0.1
     * [2]Next
       GTK+-2.24.32
     * [3]Up
     * [4]Home

GOffice-0.10.46

Introduction to GOffice

   The GOffice package contains a library of GLib/GTK document centric
   objects and utilities. This is useful for performing common operations
   for document centric applications that are conceptually simple, but
   complex to implement fully. Some of the operations provided by the
   GOffice library include support for plugins, load/save routines for
   application documents and undo/redo functions.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]http://ftp.gnome.org/pub/gnome/sources/goffice/0.10/goffice-0.10
       .46.tar.xz
     * Download (FTP):
       [6]ftp://ftp.gnome.org/pub/gnome/sources/goffice/0.10/goffice-0.10.
       46.tar.xz
     * Download MD5 sum: ff049170664a6b0dd904f843ee7eb6e9
     * Download size: 2.3 MB
     * Estimated disk space required: 80 MB (with tests)
     * Estimated build time: 0.4 SBU (Using parallelism=4; with tests)

GOffice Dependencies

Required

   [7]GTK+-3.24.13, [8]libgsf-1.14.46, [9]librsvg-2.46.4,
   [10]libxslt-1.1.34, and [11]Which-2.21

Optional

   [12]gobject-introspection-1.62.0, [13]ghostscript-9.50,
   [14]gsettings-desktop-schemas-3.34.0, [15]GTK-Doc-1.32, [16]Lasem, and
   [17]libspectre

   User Notes: [18]http://wiki.linuxfromscratch.org/blfs/wiki/goffice010

Installation of GOffice

   Install GOffice by running the following commands:
./configure --prefix=/usr &&
make

   If you wish to run the tests, issue: make check.

   Now, as the root user:
make install

Command Explanations

   --enable-gtk-doc: Use this parameter if GTK-Doc is installed and you
   wish to rebuild and install the API documentation.

Contents

   Installed Programs: None
   Installed Libraries: libgoffice-0.10.so
   Installed Directories: /usr/include/libgoffice-0.10,
   /usr/{lib,share}/goffice, and /usr/share/gtk-doc/html/goffice-0.10

Short Descriptions

   libgoffice-0.10.so

   contains API functions to provide support for document centric objects
   and utilities.

   Last updated on 2020-02-18 13:55:17 -0800

     * [19]Prev
       GLU-9.0.1
     * [20]Next
       GTK+-2.24.32
     * [21]Up
     * [22]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/glu.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gtk2.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lib.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. http://ftp.gnome.org/pub/gnome/sources/goffice/0.10/goffice-0.10.46.tar.xz
   6. ftp://ftp.gnome.org/pub/gnome/sources/goffice/0.10/goffice-0.10.46.tar.xz
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gtk3.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/libgsf.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/librsvg.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/libxslt.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/which.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/gobject-introspection.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/pst/gs.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/gnome/gsettings-desktop-schemas.html
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/gtk-doc.html
  16. http://ftp.gnome.org/pub/gnome/sources/lasem/
  17. http://www.freedesktop.org/wiki/Software/libspectre
  18. http://wiki.linuxfromscratch.org/blfs/wiki/goffice010
  19. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/glu.html
  20. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gtk2.html
  21. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lib.html
  22. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
