Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 9. General Libraries

     * [1]Prev
       libwacom-0.29
     * [2]Next
       LZO-2.10
     * [3]Up
     * [4]Home

libyaml-0.2.2

Introduction to libyaml

   The libyaml package contains a C library for parsing and emitting YAML
   (YAML Ain't Markup Language) code.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://github.com/yaml/libyaml/archive/0.2.2/libyaml-dist-0.2.2
       .tar.gz
     * Download MD5 sum: 2ad4119a57f94739cc39a1b482c81264
     * Download size: 80 KB
     * Estimated disk space required: 6.4 MB (with tests)
     * Estimated build time: 0.1 SBU (with tests)

libyaml Dependencies

Optional

   [6]Doxygen-1.8.17

   User Notes: [7]http://wiki.linuxfromscratch.org/blfs/wiki/libyaml

Installation of libyaml

   Install libyaml by running the following commands:
./bootstrap                                &&
./configure --prefix=/usr --disable-static &&
make

   To test the results, issue: make check.

   Now, as the root user:
make install

Command Explanations

   --disable-static: This switch prevents installation of static versions
   of the libraries.

Contents

   Installed Programs: None
   Installed Libraries: libyaml.so
   Installed Directories: None

Short Descriptions

   libyaml.so

   contains API functions for parsing and emitting YAML code.

   Last updated on 2020-02-15 08:54:30 -0800

     * [8]Prev
       libwacom-0.29
     * [9]Next
       LZO-2.10
     * [10]Up
     * [11]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libwacom.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lzo.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://github.com/yaml/libyaml/archive/0.2.2/libyaml-dist-0.2.2.tar.gz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/doxygen.html
   7. http://wiki.linuxfromscratch.org/blfs/wiki/libyaml
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libwacom.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lzo.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
