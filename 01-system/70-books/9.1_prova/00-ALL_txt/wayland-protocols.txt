Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 9. General Libraries

     * [1]Prev
       Wayland-1.18.0
     * [2]Next
       wv-1.2.9
     * [3]Up
     * [4]Home

Wayland-Protocols-1.18

Introduction to Wayland-Protocols

   The Wayland-Protocols package contains additional Wayland protocols
   that add functionality outside of protocols already in the Wayland
   core.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://wayland.freedesktop.org/releases/wayland-protocols-1.18.
       tar.xz
     * Download MD5 sum: af38f22d8e233c2f2e00ddc8dcc94694
     * Download size: 108 KB
     * Estimated disk space required: 1.2 MB (with tests)
     * Estimated build time: less than 0.1 SBU (with tests)

Wayland-protocols Dependencies

Required

   [6]Wayland-1.18.0

   User Notes:
   [7]http://wiki.linuxfromscratch.org/blfs/wiki/wayland-protocols

Installation of Wayland-protocols

   Install Wayland-protocols by running the following commands:
./configure --prefix=/usr &&
make

   To test the results, issue: make check.

   Now, as the root user:
make install

Contents

   Installed Programs: None
   Installed Libraries: None
   Installed Directories: /usr/share/wayland-protocols

   Last updated on 2020-02-15 20:23:35 -0800

     * [8]Prev
       Wayland-1.18.0
     * [9]Next
       wv-1.2.9
     * [10]Up
     * [11]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/wayland.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/wv.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://wayland.freedesktop.org/releases/wayland-protocols-1.18.tar.xz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/wayland.html
   7. http://wiki.linuxfromscratch.org/blfs/wiki/wayland-protocols
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/wayland.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/wv.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
