Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 9. General Libraries

     * [1]Prev
       JSON-GLib-1.4.4
     * [2]Next
       libaio-0.3.112
     * [3]Up
     * [4]Home

keyutils-1.6.1

Introduction to keyutils

   Keyutils is a set of utilities for managing the key retention facility
   in the kernel, which can be used by filesystems, block devices and more
   to gain and retain the authorization and encryption keys required to
   perform secure operations.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]http://people.redhat.com/~dhowells/keyutils/keyutils-1.6.1.tar.b
       z2
     * Download MD5 sum: 919af7f33576816b423d537f8a8692e8
     * Download size: 96 KB
     * Estimated disk space required: 1.9 MB (with tests)
     * Estimated build time: less than 0.1 SBU (add 0.6 SBU for tests)

keyutils Dependencies

Required

   [6]MIT Kerberos V5-1.18

   User Notes: [7]http://wiki.linuxfromscratch.org/blfs/wiki/keyutils

Installation of keyutils

   Install keyutils by running the following commands:
make

   To test the results, issue, as the root user:
sed -i '/find/s:/usr/bin/::' tests/Makefile &&
make -k test

   Note that several tests will fail if certain uncommon kernel options
   were not used when the kernel was built. These include CONFIG_BIG_KEYS,
   CONFIG_KEY_DH_OPERATIONS, and CONFIG_CRYPTO_DH.

   Now, as the root user:
make NO_ARLIB=1 install

Command Explanations

   NO_ARLIB=1: This make flag disables installing the static library.

Configuring keyutils

Config Files

   /etc/request-key.conf and /etc/request-key.d/*

Contents

   Installed Programs: keyctl, key.dns_resolver, and request-key
   Installed Library: libkeyutils.so
   Installed Directory: /etc/request-key.d and /usr/share/keyutils

Short Descriptions

   keyctl

   is to control the key management facility in various ways using a
   variety of subcommands.

   key.dns_resolver

   is invoked by request-key on behalf of the kernel when kernel services
   (such as NFS, CIFS and AFS) need to perform a hostname lookup and the
   kernel does not have the key cached. It is not ordinarily intended to
   be called directly.

   request-key

   is invoked by the kernel when the kernel is asked for a key that it
   doesn't have immediately available. The kernel creates a temporary key
   and then calls out to this program to instantiate it. It is not
   intended to be called directly.

   libkeyutils.so

   contains the keyutils library API instantiation.

   Last updated on 2020-02-17 13:54:09 -0800

     * [8]Prev
       JSON-GLib-1.4.4
     * [9]Next
       libaio-0.3.112
     * [10]Up
     * [11]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/json-glib.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libaio.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. http://people.redhat.com/~dhowells/keyutils/keyutils-1.6.1.tar.bz2
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/mitkrb.html
   7. http://wiki.linuxfromscratch.org/blfs/wiki/keyutils
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/json-glib.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libaio.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
