Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 9. General Libraries

     * [1]Prev
       gobject-introspection-1.62.0
     * [2]Next
       Gsl-2.6
     * [3]Up
     * [4]Home

Grantlee-5.2.0

Introduction to grantlee

   Grantlee is a set of free software libraries written using the Qt
   framework. Currently two libraries are shipped with Grantlee: Grantlee
   Templates and Grantlee TextDocument. The goal of Grantlee Templates is
   to make it easier for application developers to separate the structure
   of documents from the data they contain, opening the door for theming.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]http://downloads.grantlee.org/grantlee-5.2.0.tar.gz
     * Download MD5 sum: 6239b3703674f88b2236d30d0ed67eea
     * Download size: 1.1 MB
     * Estimated disk space required: 22 MB
     * Estimated build time: 0.6 SBU (Using parallelism=4)

Grantlee Dependencies

Required

   [6]CMake-3.16.4 and [7]Qt-5.14.1

   User Notes: [8]http://wiki.linuxfromscratch.org/blfs/wiki/grantlee

Installation of Grantlee

   Install Grantlee by running the following commands:
mkdir build &&
cd    build &&

cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release  \
      .. &&
make

   This package does not come with a test suite.

   Now, as the root user:
make install

Contents

   Installed Programs: none
   Installed Libraries: libgrantlee_core.so and libgrantlee_gui.so
   Installed Directories: /usr/lib/cmake/grantlee, /usr/lib/grantlee/0.4,
   and /usr/include/grantlee

   Last updated on 2020-02-19 16:31:22 -0800

     * [9]Prev
       gobject-introspection-1.62.0
     * [10]Next
       Gsl-2.6
     * [11]Up
     * [12]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gobject-introspection.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gsl.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. http://downloads.grantlee.org/grantlee-5.2.0.tar.gz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/cmake.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/qt5.html
   8. http://wiki.linuxfromscratch.org/blfs/wiki/grantlee
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gobject-introspection.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gsl.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
