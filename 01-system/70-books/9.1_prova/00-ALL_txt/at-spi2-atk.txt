Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 25. X Libraries

     * [1]Prev
       at-spi2-core-2.34.0
     * [2]Next
       Cairo-1.17.2+f93fc72c03e
     * [3]Up
     * [4]Home

at-spi2-atk-2.34.1

Introduction to At-Spi2 Atk

   The At-Spi2 Atk package contains a library that bridges ATK to At-Spi2
   D-Bus service.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]http://ftp.gnome.org/pub/gnome/sources/at-spi2-atk/2.34/at-spi2-
       atk-2.34.1.tar.xz
     * Download (FTP):
       [6]ftp://ftp.gnome.org/pub/gnome/sources/at-spi2-atk/2.34/at-spi2-a
       tk-2.34.1.tar.xz
     * Download MD5 sum: e0f99641c5a403041c4214be04722e15
     * Download size: 96 KB
     * Estimated disk space required: 9.4 MB (with tests)
     * Estimated build time: less than 0.1 SBU (with tests)

At-Spi2 Atk Dependencies

Required

   [7]at-spi2-core-2.34.0 and [8]ATK-2.34.1

   User Notes: [9]http://wiki.linuxfromscratch.org/blfs/wiki/at-spi2-atk

Installation of At-Spi2 Atk

   Install At-Spi2 Atk by running the following commands:
mkdir build &&
cd build &&

meson --prefix=/usr .. &&
ninja

   To test the results, issue: ninja test. An already active graphical
   session with bus address is necessary to run the tests.

   Now, as the root user:
ninja install

   [Note]

Note

   If you installed the package to your system using a “DESTDIR” method,
   /usr/share/glib-2.0/schemas/gschemas.compiled was not updated/created.
   Create (or update) the file using the following command as the root
   user:
glib-compile-schemas /usr/share/glib-2.0/schemas

Contents

   Installed Programs: None
   Installed Libraries: libatk-bridge-2.0.so and
   /usr/lib/gtk-2.0/modules/libatk-bridge.so
   Installed Directory: /usr/include/at-spi2-atk and
   /usr/share/gnome-settings-daemon-3.0

Short Descriptions

   libatk-bridge.so

   is the Accessibility Toolkit GTK+ module.

   libatk-bridge-2.0.so

   Contains the common functions used by GTK+ Accessibility Toolkit
   Bridge.

   Last updated on 2020-02-16 15:15:05 -0800

     * [10]Prev
       at-spi2-core-2.34.0
     * [11]Next
       Cairo-1.17.2+f93fc72c03e
     * [12]Up
     * [13]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/at-spi2-core.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/cairo.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lib.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. http://ftp.gnome.org/pub/gnome/sources/at-spi2-atk/2.34/at-spi2-atk-2.34.1.tar.xz
   6. ftp://ftp.gnome.org/pub/gnome/sources/at-spi2-atk/2.34/at-spi2-atk-2.34.1.tar.xz
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/at-spi2-core.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/atk.html
   9. http://wiki.linuxfromscratch.org/blfs/wiki/at-spi2-atk
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/at-spi2-core.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/cairo.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lib.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
