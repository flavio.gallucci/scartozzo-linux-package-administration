Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 24. X Window System Environment

     * [1]Prev
       xcb-util-0.4.0
     * [2]Next
       xcb-util-keysyms-0.4.0
     * [3]Up
     * [4]Home

xcb-util-image-0.4.0

Introduction to xcb-util-image

   The xcb-util-image package provides additional extensions to the XCB
   library.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://xcb.freedesktop.org/dist/xcb-util-image-0.4.0.tar.bz2
     * Download MD5 sum: 08fe8ffecc8d4e37c0ade7906b3f4c87
     * Download size: 324 KB
     * Estimated disk space required: 3.2 MB
     * Estimated build time: less than 0.1 SBU

xcb-util-image Dependencies

Required

   [6]xcb-util-0.4.0

Optional

   [7]Doxygen-1.8.17 (for documentation)

   User Notes:
   [8]http://wiki.linuxfromscratch.org/blfs/wiki/xcb-util-image

Installation of xcb-util-image

   Install xcb-util-image by running the following commands:
./configure $XORG_CONFIG &&
make

   To test the results, issue: LD_LIBRARY_PATH=$XORG_PREFIX/lib make
   check.

   Now, as the root user:
make install

Contents

   Installed Programs: None
   Installed Library: libxcb-image.so
   Installed Directories: None

Short Descriptions

   libxcb-image.so

   Is a port of Xlib's XImage and XShmImage functions.

   Last updated on 2020-02-15 20:23:35 -0800

     * [9]Prev
       xcb-util-0.4.0
     * [10]Next
       xcb-util-keysyms-0.4.0
     * [11]Up
     * [12]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/xcb-util.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/xcb-util-keysyms.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/installing.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://xcb.freedesktop.org/dist/xcb-util-image-0.4.0.tar.bz2
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/xcb-util.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/doxygen.html
   8. http://wiki.linuxfromscratch.org/blfs/wiki/xcb-util-image
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/xcb-util.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/xcb-util-keysyms.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/installing.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
