Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 18. Text Web Browsers

     * [1]Prev
       Links-2.20.2
     * [2]Next
       Mail/News Clients
     * [3]Up
     * [4]Home

Lynx-2.8.9rel.1

Introduction to Lynx

   Lynx is a text based web browser.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://invisible-mirror.net/archives/lynx/tarballs/lynx2.8.9rel
       .1.tar.bz2
     * Download MD5 sum: 44316f1b8a857b59099927edc26bef79
     * Download size: 2.5 MB
     * Estimated disk space required: 31 MB
     * Estimated build time: 0.3 SBU

Lynx Dependencies

Optional

   [6]GnuTLS-3.6.12 (experimental, to replace openssl), [7]Zip-3.0,
   [8]UnZip-6.0, an [9]MTA (that provides a sendmail command), and
   [10]Sharutils-4.15.2 (for a uudecode program)

   User Notes: [11]http://wiki.linuxfromscratch.org/blfs/wiki/Lynx

Installation of Lynx

   Install Lynx by running the following commands:
./configure --prefix=/usr          \
            --sysconfdir=/etc/lynx \
            --datadir=/usr/share/doc/lynx-2.8.9rel.1 \
            --with-zlib            \
            --with-bzlib           \
            --with-ssl             \
            --with-screen=ncursesw \
            --enable-locale-charset &&
make

   This package does not come with a test suite.

   Now, as the root user:
make install-full &&
chgrp -v -R root /usr/share/doc/lynx-2.8.9rel.1/lynx_doc

Command Explanations

   --sysconfdir=/etc/lynx: This parameter is used so that the
   configuration files are located in /etc/lynx instead of /usr/etc.

   --datadir=/usr/share/doc/lynx-2.8.9rel.1: This parameter is used so
   that the documentation files are installed into
   /usr/share/doc/lynx-2.8.9rel.1 instead of /usr/share/lynx_{doc,help}.

   --with-zlib: This enables support for linking libz into Lynx.

   --with-bzlib: This enables support for linking libbz2 into Lynx.

   --with-ssl: This enables support for linking SSL into Lynx.

   --with-screen=ncursesw: This switch enables the use of advanced
   wide-character support present in the system NCurses library. This is
   needed for proper display of characters and line wrapping in multibyte
   locales.

   --enable-locale-charset: This switch allows Lynx to deduce the proper
   character encoding for terminal output from the current locale. A
   configuration step is still needed (see below), but unlike the
   situation without this switch, the configuration step becomes the same
   for all users (without the switch one must specify the display
   character set explicitly). This is important for environments such as a
   LiveCD, where the amount of system-specific configuration steps has to
   be reduced to the minimum.

   --enable-nls: This switch allows Lynx to print translated messages
   (such as questions about cookies and SSL certificates).

   --with-gnutls: This enables experimental support for linking GnuTLS
   into Lynx. Remove the --with-ssl switch if you want to use gnutls.

   make install-full: In addition to the standard installation, this
   target installs the documentation and help files.

   chgrp -v -R root /usr/share/doc/lynx-2.8.9rel.1/lynx_doc: This command
   corrects the improper group ownership of installed documentation files.

Configuring Lynx

Config Files

   /etc/lynx/lynx.cfg

Configuration Information

   The proper way to get the display character set is to examine the
   current locale. However, Lynx does not do this by default. As the root
   user, change this setting:
sed -e '/#LOCALE/     a LOCALE_CHARSET:TRUE'     \
    -i /etc/lynx/lynx.cfg

   The built-in editor in Lynx [12]Breaks Multibyte Characters. This issue
   manifests itself in multibyte locales, e.g., as the Backspace key not
   erasing non-ASCII characters properly, and as incorrect data being sent
   to the network when one edits the contents of text areas. The only
   solution to this problem is to configure Lynx to use an external editor
   (bound to the “Ctrl+X e” key combination by default). Still as the root
   user:
sed -e '/#DEFAULT_ED/ a DEFAULT_EDITOR:vi'       \
    -i /etc/lynx/lynx.cfg

   Lynx handles the following values of the DEFAULT_EDITOR option
   specially by adding cursor-positioning arguments: “emacs”, “jed”,
   “jmacs”, “joe”, “jove”, “jpico”, “jstar”, “nano”, “pico”, “rjoe”, “vi”
   (but not “vim”: in order to position the cursor in [13]Vim-8.2.0190,
   set this option to “vi”).

   By default, Lynx doesn't save cookies between sessions. Again as the
   root user, change this setting:
sed -e '/#PERSIST/    a PERSISTENT_COOKIES:TRUE' \
    -i /etc/lynx/lynx.cfg

   Many other system-wide settings such as proxies can also be set in the
   /etc/lynx/lynx.cfg file.

Contents

   Installed Program: lynx
   Installed Libraries: None
   Installed Directories: /etc/lynx and /usr/share/doc/lynx-2.8.9rel.1

Short Descriptions

   lynx

   is a general purpose, text-based, distributed information browser for
   the World Wide Web.

   Last updated on 2020-02-16 15:15:05 -0800

     * [14]Prev
       Links-2.20.2
     * [15]Next
       Mail/News Clients
     * [16]Up
     * [17]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/links.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/mailnews.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/textweb.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://invisible-mirror.net/archives/lynx/tarballs/lynx2.8.9rel.1.tar.bz2
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/gnutls.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/zip.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/unzip.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/server/mail.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/sharutils.html
  11. http://wiki.linuxfromscratch.org/blfs/wiki/Lynx
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/introduction/locale-issues.html#locale-wrong-multibyte-characters
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/vim.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/links.html
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/mailnews.html
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/textweb.html
  17. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
