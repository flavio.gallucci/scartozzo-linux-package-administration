Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 26. Display Managers

     * [1]Prev
       Display Managers
     * [2]Next
       lightdm-1.30.0
     * [3]Up
     * [4]Home

Introduction

   Display Managers are graphical programs used for starting the graphical
   display (currently, the X server) and providing a login capability for
   a Window Manager or Desktop Environment.

   There are many Display Managers available. Some of the more well known
   include: GDM, KDM (deprecated), LightDM, LXDM, Slim, and SDDM.

   Among the Desktop Environments available for Linux you find:
   Enlightenment, GNOME, KDE, LXDE, LXQt, and XFce.

   Choosing a Display Manager or Desktop Environment is highly subjective.
   The choice depends on the look and feel of the packages, the resources
   (memory and disk space) required, and the utilities included.

   In this chapter, the installation instructions of some Display Managers
   are presented. Later in the book, you will find other ones, which are
   provided as part of some Desktop Environments.

   Last updated on 2018-01-05 14:14:37 -0800

     * [5]Prev
       Display Managers
     * [6]Next
       lightdm-1.30.0
     * [7]Up
     * [8]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/dm.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lightdm.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/dm.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/dm.html
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lightdm.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/dm.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
