Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 28. Icons

     * [1]Prev
       hicolor-icon-theme-0.17
     * [2]Next
       lxde-icon-theme-0.5.1
     * [3]Up
     * [4]Home

icon-naming-utils-0.8.90

Introduction to icon-naming-utils

   The icon-naming-utils package contains a Perl script used for
   maintaining backwards compatibility with current desktop icon themes,
   while migrating to the names specified in the [5]Icon Naming
   Specification.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [6]http://tango.freedesktop.org/releases/icon-naming-utils-0.8.90.t
       ar.bz2
     * Download MD5 sum: dd8108b56130b9eedc4042df634efa66
     * Download size: 57 KB
     * Estimated disk space required: 440 KB
     * Estimated build time: less than 0.1 SBU

icon-naming-utils Dependencies

Required

   [7]XML-Simple-2.25

   User Notes:
   [8]http://wiki.linuxfromscratch.org/blfs/wiki/icon-naming-utils

Installation of icon-naming-utils

   Install icon-naming-utils by running the following commands:
./configure --prefix=/usr &&
make

   This package does not come with a test suite.

   Now, as the root user:
make install

Contents

   Installed Programs: icon-name-mapping
   Installed Libraries: None
   Installed Directories: /usr/share/dtds and /usr/share/icon-naming-utils

Short Descriptions

   icon-name-mapping

   is a Perl script used for maintaining backwards compatibility with
   current desktop icon themes, while migrating to the names specified in
   the Icon Naming Specification.

   Last updated on 2020-02-17 12:12:55 -0800

     * [9]Prev
       hicolor-icon-theme-0.17
     * [10]Next
       lxde-icon-theme-0.5.1
     * [11]Up
     * [12]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/hicolor-icon-theme.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lxde-icon-theme.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/icons.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. http://standards.freedesktop.org/icon-naming-spec/latest/
   6. http://tango.freedesktop.org/releases/icon-naming-utils-0.8.90.tar.bz2
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/perl-modules.html#perl-xml-simple
   8. http://wiki.linuxfromscratch.org/blfs/wiki/icon-naming-utils
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/hicolor-icon-theme.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lxde-icon-theme.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/icons.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
