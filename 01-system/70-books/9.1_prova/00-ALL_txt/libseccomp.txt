Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 9. General Libraries

     * [1]Prev
       libqmi-1.24.4
     * [2]Next
       libsigc++-2.10.2
     * [3]Up
     * [4]Home

libseccomp-2.4.2

Introduction to libseccomp

   The libseccomp package provides an easy to use and platform independent
   interface to the Linux kernel's syscall filtering mechanism.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://github.com/seccomp/libseccomp/releases/download/v2.4.2/l
       ibseccomp-2.4.2.tar.gz
     * Download MD5 sum: ee87fb68ae293883bde9e042c6bc324f
     * Download size: 588 KB
     * Estimated disk space required: 6.6 MB (additional 5 MB for tests)
     * Estimated build time: less than 0.1 SBU (additional 2.8 SBU for
       tests)

libseccomp Dependencies

Optional

   [6]Which-2.21 (needed for tests), [7]Valgrind-3.15.0, [8]Cython, and
   [9]LCOV

   User Notes: [10]http://wiki.linuxfromscratch.org/blfs/wiki/libseccomp

Installation of libseccomp

   Install libseccomp by running the following commands:
./configure --prefix=/usr --disable-static &&
make

   To test the results, issue: make check.

   Now, as the root user:
make install

Command Explanations

   --disable-static: This switch prevents installation of static versions
   of the libraries.

Contents

   Installed Program: scmp_sys_resolver
   Installed Library: libseccomp.so
   Installed Directories: None

Short Descriptions

   scmp_sys_resolver

   is used to resolve system calls for applications.

   libseccomp.so

   contains API functions for translating syscalls.

   Last updated on 2020-02-16 18:46:23 -0800

     * [11]Prev
       libqmi-1.24.4
     * [12]Next
       libsigc++-2.10.2
     * [13]Up
     * [14]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libqmi.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libsigc.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://github.com/seccomp/libseccomp/releases/download/v2.4.2/libseccomp-2.4.2.tar.gz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/which.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/valgrind.html
   8. http://cython.org/
   9. http://ltp.sourceforge.net/coverage/lcov.php
  10. http://wiki.linuxfromscratch.org/blfs/wiki/libseccomp
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libqmi.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libsigc.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
