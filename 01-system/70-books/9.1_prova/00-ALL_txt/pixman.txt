Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 10. Graphics and Font Libraries

     * [1]Prev
       OpenJPEG-2.3.1
     * [2]Next
       Poppler-0.85.0
     * [3]Up
     * [4]Home

Pixman-0.38.4

Introduction to Pixman

   The Pixman package contains a library that provides low-level pixel
   manipulation features such as image compositing and trapezoid
   rasterization.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://www.cairographics.org/releases/pixman-0.38.4.tar.gz
     * Download MD5 sum: 267a7af290f93f643a1bc74490d9fdd1
     * Download size: 880 KB
     * Estimated disk space required: 55 MB (with tests)
     * Estimated build time: 0.8 SBU (with tests)

Pixman Dependencies

Optional

   [6]GTK+-2.24.32 and [7]libpng-1.6.37 (for tests and demos)

   User Notes: [8]http://wiki.linuxfromscratch.org/blfs/wiki/pixman

Installation of Pixman

   Install Pixman by running the following commands:
mkdir build &&
cd    build &&

meson --prefix=/usr &&
ninja

   To test the results, issue: ninja test.

   Now, as the root user:
ninja install

Contents

   Installed Programs: None
   Installed Library: libpixman-1.so
   Installed Directory: /usr/include/pixman-1

Short Descriptions

   libpixman-1.so

   contains functions that provide low-level pixel manipulation features.

   Last updated on 2020-02-15 20:23:35 -0800

     * [9]Prev
       OpenJPEG-2.3.1
     * [10]Next
       Poppler-0.85.0
     * [11]Up
     * [12]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/openjpeg2.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/poppler.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/graphlib.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://www.cairographics.org/releases/pixman-0.38.4.tar.gz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/gtk2.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libpng.html
   8. http://wiki.linuxfromscratch.org/blfs/wiki/pixman
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/openjpeg2.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/poppler.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/graphlib.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
