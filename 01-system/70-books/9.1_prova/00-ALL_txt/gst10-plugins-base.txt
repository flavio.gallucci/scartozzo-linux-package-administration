Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 42. Multimedia Libraries and Drivers

     * [1]Prev
       gstreamer-1.16.2
     * [2]Next
       gst-plugins-good-1.16.2
     * [3]Up
     * [4]Home

gst-plugins-base-1.16.2

Introduction to GStreamer Base Plug-ins

   The GStreamer Base Plug-ins is a well-groomed and well-maintained
   collection of GStreamer plug-ins and elements, spanning the range of
   possible types of elements one would want to write for GStreamer. You
   will need at least one of Good, Bad, Ugly or Libav plugins for
   GStreamer applications to function properly.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://gstreamer.freedesktop.org/src/gst-plugins-base/gst-plugi
       ns-base-1.16.2.tar.xz
     * Download MD5 sum: 3fdb32823535799a748c1fc14f978e2c
     * Download size: 3.8 MB
     * Estimated disk space required: 93 MB (with tests; without docs)
     * Estimated build time: 0.8 SBU (Using parallelism=4; with tests;
       without docs)

GStreamer Base Plug-ins Dependencies

Required

   [6]gstreamer-1.16.2

Recommended

   [7]alsa-lib-1.2.1.2, [8]CDParanoia-III-10.2 (for building the CDDA
   plugin), [9]gobject-introspection-1.62.0, [10]ISO Codes-4.4,
   [11]libogg-1.3.4, [12]libtheora-1.1.1, [13]libvorbis-1.3.6, and
   [14]Xorg Libraries

Optional

   [15]GTK+-3.24.13 (for examples), [16]GTK-Doc-1.32, [17]Opus-1.3.1,
   [18]Qt-5.14.1 (for examples), [19]SDL-1.2.15, [20]Valgrind-3.15.0,
   [21]libvisual, [22]Orc, and [23]Tremor

   User Notes:
   [24]http://wiki.linuxfromscratch.org/blfs/wiki/gst10-plugins-base

Installation of GStreamer Base Plug-ins

   [Note]

Note

   If you do not have an Objective-C compiler installed, the build system
   for this package will emit a warning about a failed sanity check. This
   is harmless, and it is safe to continue.
   [Note]

Note

   If you need a plugin for a given dependency, that dependency needs to
   be installed before this package.

   Install GStreamer Base Plug-ins by running the following commands:
mkdir build &&
cd    build &&

meson  --prefix=/usr       \
       -Dbuildtype=release \
       -Dgtk_doc=disabled  \
       -Dpackage-origin=http://www.linuxfromscratch.org/blfs/view/svn/ \
       -Dpackage-name="GStreamer 1.16.2 BLFS" &&
ninja

   To test the results, issue: ninja test. The tests require an X terminal
   running, or all of the GL tests will fail. Five tests may produce
   timeouts on some systems depending on their graphics hardware and
   speed.
   [Note]

Note

   When installing, the Makefile does some additional linking. If you do
   not have Xorg in /usr, the LIBRARY_PATH variable needs to be defined
   for the root user. If using sudo to assume root, use the -E option to
   pass your current environment variables for the install process.

   Now, as the root user:
ninja install

Contents

   Installed Programs: gst-device-monitor-1.0, gst-discoverer-1.0, and
   gst-play-1.0
   Installed Libraries: libgstallocators-1.0.so, libgstapp-1.0.so,
   libgstaudio-1.0.so, libgstfft-1.0.so, libgstgl-1.0.so,
   libgstpbutils-1.0.so, libgstriff-1.0.so, libgstrtp-1.0.so,
   libgstrtsp-1.0.so, libgstsdp-1.0.so, libgsttag-1.0.so,
   libgstvideo-1.0.so and several plugins under /usr/lib/gstreamer-1.0
   Installed Directories:
   /usr/include/gstreamer-1.0/gst/{allocators,app,audio,fft,gl,pbutils},
   /usr/include/gstreamer-1.0/gst/{riff,rtp,rtsp,sdp,tag,video},
   /usr/share/gst-plugins-base, and
   /usr/share/gtk-doc/html/gst-plugins-base-{libs,plugins}-1.0

Short Descriptions

   gst-device-monitor-1.0

   is a command line tool that can be used to test GStreamer's device
   monitoring functionality

   gst-discoverer-1.0

   is a tool that can be used to print basic metadata and stream
   information about a media file.

   gst-play-1.0

   is a command line tool that can be used to test basic playback using
   the playbin element.

   Last updated on 2020-02-17 18:27:03 -0800

     * [25]Prev
       gstreamer-1.16.2
     * [26]Next
       gst-plugins-good-1.16.2
     * [27]Up
     * [28]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gstreamer10.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gst10-plugins-good.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libdriv.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://gstreamer.freedesktop.org/src/gst-plugins-base/gst-plugins-base-1.16.2.tar.xz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gstreamer10.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/alsa-lib.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/cdparanoia.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/gobject-introspection.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/iso-codes.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libogg.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libtheora.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libvorbis.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/x7lib.html
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/gtk3.html
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/gtk-doc.html
  17. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/opus.html
  18. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/qt5.html
  19. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/sdl.html
  20. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/valgrind.html
  21. http://libvisual.org/
  22. http://gstreamer.freedesktop.org/src/orc/
  23. http://wiki.xiph.org/Tremor
  24. http://wiki.linuxfromscratch.org/blfs/wiki/gst10-plugins-base
  25. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gstreamer10.html
  26. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gst10-plugins-good.html
  27. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libdriv.html
  28. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
