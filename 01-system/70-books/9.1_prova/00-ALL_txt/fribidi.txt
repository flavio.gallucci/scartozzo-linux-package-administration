Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 10. Graphics and Font Libraries

     * [1]Prev
       Fontconfig-2.13.1
     * [2]Next
       gegl-0.4.22
     * [3]Up
     * [4]Home

FriBidi-1.0.8

Introduction to FriBidi

   The FriBidi package is an implementation of the [5]Unicode
   Bidirectional Algorithm (BIDI). This is useful for supporting Arabic
   and Hebrew alphabets in other packages.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [6]https://github.com/fribidi/fribidi/releases/download/v1.0.8/frib
       idi-1.0.8.tar.bz2
     * Download MD5 sum: 962c7d8ebaa711d4e306161dbe14aa55
     * Download size: 2.0 MB
     * Estimated disk space required: 21 MB
     * Estimated build time: less than 0.1 SBU

FriBidi Dependencies

Optional

   [7]c2man (to build man pages)

   User Notes: [8]http://wiki.linuxfromscratch.org/blfs/wiki/fribidi

Installation of FriBidi

   Install FriBidi by running the following commands:
mkdir build &&
cd    build &&

meson --prefix=/usr .. &&
ninja

   To test the results, issue: ninja test.

   Now, as the root user:
ninja install

Contents

   Installed Program: fribidi
   Installed Library: libfribidi.so
   Installed Directory: /usr/include/fribidi

Short Descriptions

   fribidi

   is a command-line interface to the libfribidi library and can be used
   to convert a logical string to visual output.

   libfribidi.so

   contains functions used to implement the [9]Unicode Bidirectional
   Algorithm.

   Last updated on 2020-02-16 15:15:05 -0800

     * [10]Prev
       Fontconfig-2.13.1
     * [11]Next
       gegl-0.4.22
     * [12]Up
     * [13]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/fontconfig.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gegl.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/graphlib.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. http://www.unicode.org/reports/tr9/
   6. https://github.com/fribidi/fribidi/releases/download/v1.0.8/fribidi-1.0.8.tar.bz2
   7. http://www.ciselant.de/c2man/c2man.html
   8. http://wiki.linuxfromscratch.org/blfs/wiki/fribidi
   9. http://www.unicode.org/reports/tr9/
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/fontconfig.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gegl.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/graphlib.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
