Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 11. General Utilities

     * [1]Prev
       Rep-gtk-0.90.8.3
     * [2]Next
       shared-mime-info-1.15
     * [3]Up
     * [4]Home

Screen-4.8.0

Introduction to Screen

   Screen is a terminal multiplexor that runs several separate processes,
   typically interactive shells, on a single physical character-based
   terminal. Each virtual terminal emulates a DEC VT100 plus several ANSI
   X3.64 and ISO 2022 functions and also provides configurable input and
   output translation, serial port support, configurable logging,
   multi-user support, and many character encodings, including UTF-8.
   Screen sessions can be detached and resumed later on a different
   terminal.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://ftp.gnu.org/gnu/screen/screen-4.8.0.tar.gz
     * Download (FTP): [6]ftp://ftp.gnu.org/gnu/screen/screen-4.8.0.tar.gz
     * Download MD5 sum: d276213d3acd10339cd37848b8c4ab1e
     * Download size: 836 KB
     * Estimated disk space required: 7.4 MB
     * Estimated build time: 0.1 SBU

Screen Dependencies

Optional

   [7]Linux-PAM-1.3.1

   User Notes: [8]http://wiki.linuxfromscratch.org/blfs/wiki/screen

Installation of Screen

   Install Screen by running the following commands:
./configure --prefix=/usr                     \
            --infodir=/usr/share/info         \
            --mandir=/usr/share/man           \
            --with-socket-dir=/run/screen     \
            --with-pty-group=5                \
            --with-sys-screenrc=/etc/screenrc &&

sed -i -e "s%/usr/local/etc/screenrc%/etc/screenrc%" {etc,doc}/* &&
make

   This package does not come with a test suite.

   Now, as the root user:
make install &&
install -m 644 etc/etcscreenrc /etc/screenrc

Command Explanations

   --with-socket-dir=/run/screen: This option places the per-user sockets
   in a standard location.

   --with-sys-screenrc=/etc/screenrc: This option places the global
   screenrc file in /etc.

   --with-pty-group=5: This option sets the gid to the value used by LFS.
   [Note]

Note

   Older versions of LFS use the value 4 for the tty group. If you are
   using LFS version 7.1 or older, change the pty-group option to 4.

   sed -i -e "s%/usr/local/etc/screenrc%/etc/screenrc%" {etc,doc}/*: This
   command corrects the configuration and documentation files to the
   location that is used here for the global screenrc file.

Configuring Screen

Config Files

   /etc/screenrc and ~/.screenrc

Configuration Information

   You may want to look at the example configuration file that was
   installed and customize it for your needs.

Contents

   Installed Program: screen (symlink) and screen-4.8.0
   Installed Libraries: None
   Installed Directory: /usr/share/screen and /run/screen

Short Descriptions

   screen

   is a terminal multiplexor with VT100/ANSI terminal emulation.

   Last updated on 2020-02-15 22:43:28 -0800

     * [9]Prev
       Rep-gtk-0.90.8.3
     * [10]Next
       shared-mime-info-1.15
     * [11]Up
     * [12]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/rep-gtk.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/shared-mime-info.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genutils.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://ftp.gnu.org/gnu/screen/screen-4.8.0.tar.gz
   6. ftp://ftp.gnu.org/gnu/screen/screen-4.8.0.tar.gz
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/linux-pam.html
   8. http://wiki.linuxfromscratch.org/blfs/wiki/screen
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/rep-gtk.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/shared-mime-info.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genutils.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
