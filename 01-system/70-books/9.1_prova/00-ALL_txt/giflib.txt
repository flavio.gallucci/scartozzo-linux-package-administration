Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 10. Graphics and Font Libraries

     * [1]Prev
       gegl-0.4.22
     * [2]Next
       GLM-0.9.9.7
     * [3]Up
     * [4]Home

giflib-5.2.1

Introduction to giflib

   The giflib package contains libraries for reading and writing GIFs as
   well as programs for converting and working with GIF files.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://sourceforge.net/projects/giflib/files/giflib-5.2.1.tar.g
       z
     * Download (HTTP) MD5 sum: 6f03aee4ebe54ac2cc1ab3e4b0a049e5
     * Download (HTTP) size: 436 KB
     * Estimated disk space required: 3.2 MB (with documentation)
     * Estimated build time: less than 0.1 SBU (with documentation)

giflib Dependencies

Required

   [6]xmlto-0.0.28

   User Notes: [7]http://wiki.linuxfromscratch.org/blfs/wiki/giflib

Installation of giflib

   Install giflib by running the following commands:
make

   This package does not come with a test suite.

   Now, as the root user:
make PREFIX=/usr install &&


find doc \( -name Makefile\* -o -name \*.1 \
         -o -name \*.xml \) -exec rm -v {} \; &&

install -v -dm755 /usr/share/doc/giflib-5.2.1 &&
cp -v -R doc/* /usr/share/doc/giflib-5.2.1

Command Explanations

   find doc ... -exec rm -v {} \;: This command removes Makefiles, man and
   xml files from the documentation directory that would otherwise be
   installed by the commands that follow.

Contents

   Installed Programs: gif2rgb, gifbuild, gifclrmp, giffix, giftext, and
   giftool
   Installed Library: libgif.so
   Installed Directory: /usr/share/doc/giflib-5.2.1

Short Descriptions

   gif2rgb

   converts images saved as GIF to 24-bit RGB images.

   gifbuild

   dumps GIF data in a textual format, or undumps it to a GIF.

   gifclrmp

   modifies GIF image colormaps.

   giffix

   clumsily attempts to fix truncated GIF images.

   giftext

   prints (text only) general information about a GIF file.

   giftool

   is a GIF transformation tool.

   libgif.so

   contains API functions required by the giflib programs and any other
   programs needing library functionality to read, write and manipulate
   GIF images.

   Last updated on 2020-02-17 14:39:03 -0800

     * [8]Prev
       gegl-0.4.22
     * [9]Next
       GLM-0.9.9.7
     * [10]Up
     * [11]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gegl.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/glm.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/graphlib.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://sourceforge.net/projects/giflib/files/giflib-5.2.1.tar.gz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/pst/xmlto.html
   7. http://wiki.linuxfromscratch.org/blfs/wiki/giflib
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gegl.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/glm.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/graphlib.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
