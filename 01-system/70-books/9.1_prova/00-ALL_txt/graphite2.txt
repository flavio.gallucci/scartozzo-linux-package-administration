Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 10. Graphics and Font Libraries

     * [1]Prev
       GLM-0.9.9.7
     * [2]Next
       HarfBuzz-2.6.4
     * [3]Up
     * [4]Home

Graphite2-1.3.13

Introduction to Graphite2

   Graphite2 is a rendering engine for graphite fonts. These are TrueType
   fonts with additional tables containing smart rendering information and
   were originally developed to support complex non-Roman writing systems.
   They may contain rules for e.g. ligatures, glyph substitution, kerning,
   justification - this can make them useful even on text written in Roman
   writing systems such as English. Note that firefox by default provides
   an internal copy of the graphite engine and cannot use a system version
   (although it can now be patched to use it), but it too should benefit
   from the availability of graphite fonts.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://github.com/silnrsi/graphite/releases/download/1.3.13/gra
       phite2-1.3.13.tgz
     * Download MD5 sum: 29616d4f9651706036ca25c111508272
     * Download size: 6.4 MB
     * Estimated disk space required: 27 MB (with tests add docs)
     * Estimated build time: 0.2 SBU (with tests and docs)

Graphite2 Dependencies

Required

   [6]CMake-3.16.4

Optional

   [7]FreeType-2.10.1, [8]Python-2.7.17, and [9]silgraphite to build the
   comparerender test and benchmarking tool, and if that is present,
   [10]HarfBuzz-2.6.4 to add more functionality to it (this is a circular
   dependency, you would need to first build graphite2 without harfbuzz).

   To build the documentation: [11]asciidoc-8.6.9, [12]Doxygen-1.8.17,
   [13]texlive-20190410 (or [14]install-tl-unx), and [15]dblatex (for PDF
   docs)

   To execute the test suite you will need [16]FontTools (Python 3
   module), otherwise, the "cmp" tests fail.

Optional (at runtime)

   You will need at least one suitable [17]graphite font for the package
   to be useful.

   User Notes: [18]http://wiki.linuxfromscratch.org/blfs/wiki/graphite2

Installation of Graphite2

   Some tests fail if [19]FontTools (Python 3 module) is not installed.
   These tests can be removed with:
sed -i '/cmptest/d' tests/CMakeLists.txt

   Install Graphite2 by running the following commands:
mkdir build &&
cd    build &&

cmake -DCMAKE_INSTALL_PREFIX=/usr .. &&
make

   If you wish to build the documentation, issue:
make docs

   To test the results, issue: make test.

   Now, as the root user:
make install

   If you built the documentation, install, as the root user:
install -v -d -m755 /usr/share/doc/graphite2-1.3.13 &&

cp      -v -f    doc/{GTF,manual}.html \
                    /usr/share/doc/graphite2-1.3.13 &&
cp      -v -f    doc/{GTF,manual}.pdf \
                    /usr/share/doc/graphite2-1.3.13

Command Explanations

   -DCMAKE_VERBOSE_MAKEFILE=ON: This switch turns on build verbose mode.

Contents

   Installed Programs: gr2fonttest, and optionally comparerender
   Installed Libraries: libgraphite2.so
   Installed Directories: /usr/{include,share}/graphite2 and optionally
   /usr/share/doc/graphite2-1.3.13

Short Descriptions

   comparerender

   is a test and benchmarking tool.

   gr2fonttest

   is a diagnostic console tool for graphite fonts.

   libgraphite2.so

   is a rendering engine for graphite fonts.

   Last updated on 2020-02-15 20:23:35 -0800

     * [20]Prev
       GLM-0.9.9.7
     * [21]Next
       HarfBuzz-2.6.4
     * [22]Up
     * [23]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/glm.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/harfbuzz.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/graphlib.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://github.com/silnrsi/graphite/releases/download/1.3.13/graphite2-1.3.13.tgz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/cmake.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/freetype2.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/python2.html
   9. http://sourceforge.net/projects/silgraphite/files/silgraphite/2.3.1/
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/harfbuzz.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/asciidoc.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/doxygen.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/pst/texlive.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/pst/tl-installer.html
  15. http://dblatex.sourceforge.net/
  16. https://pypi.python.org/pypi/FontTools
  17. http://scripts.sil.org/cms/scripts/page.php?site_id=projects&item_id=graphite_fonts
  18. http://wiki.linuxfromscratch.org/blfs/wiki/graphite2
  19. https://pypi.python.org/pypi/FontTools
  20. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/glm.html
  21. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/harfbuzz.html
  22. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/graphlib.html
  23. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
