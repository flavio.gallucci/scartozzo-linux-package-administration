Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 13. Programming

     * [1]Prev
       SCons-3.1.2
     * [2]Next
       Subversion-1.13.0
     * [3]Up
     * [4]Home

slang-2.3.2

Introduction to slang

   S-Lang (slang) is an interpreted language that may be embedded into an
   application to make the application extensible. It provides facilities
   required by interactive applications such as display/screen management,
   keyboard input and keymaps.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]http://www.jedsoft.org/releases/slang/slang-2.3.2.tar.bz2
     * Download MD5 sum: c2d5a7aa0246627da490be4e399c87cb
     * Download size: 1.5 MB
     * Estimated disk space required: 29 MB (add 4 MB for tests)
     * Estimated build time: 0.4 SBU (add 0.2 SBU for tests)

Slang Dependencies

Optional

   [6]libpng-1.6.37, [7]PCRE-8.44, and [8]Oniguruma

   User Notes: [9]http://wiki.linuxfromscratch.org/blfs/wiki/slang

Installation of Slang

   [Note]

Note

   This package does not support parallel build.

   Install slang by running the following commands:
./configure --prefix=/usr \
            --sysconfdir=/etc \
            --with-readline=gnu &&
make -j1

   To test the results, issue: make check. Note that this will also create
   a static version of the library which will then be installed in the
   next step.

   Now, as the root user:
make install_doc_dir=/usr/share/doc/slang-2.3.2   \
     SLSH_DOC_DIR=/usr/share/doc/slang-2.3.2/slsh \
     install-all &&

chmod -v 755 /usr/lib/libslang.so.2.3.2 \
             /usr/lib/slang/v2/modules/*.so

Command Explanations

   --with-readline=gnu: This parameter sets GNU Readline to be used by the
   parser interface instead of the slang internal version.

   make install_doc_dir=/usr/share/doc/slang-2.3.2
   SLSH_DOC_DIR=/usr/share/doc/slang-2.3.2/slsh install-all: This command
   installs the static library as well as the dynamic shared version and
   related modules. It also changes the documentation installation
   directories to a versioned directory.

Configuring slang

Config Files

   ~/.slshrc and /etc/slsh.rc

Contents

   Installed Program: slsh
   Installed Libraries: libslang.{so,a} and numerous support modules
   Installed Directories: /usr/lib/slang, /usr/share/doc/slang-2.3.2 and
   /usr/share/slsh

Short Descriptions

   slsh

   is a simple program for interpreting slang scripts. It supports dynamic
   loading of slang modules and includes a Readline interface for
   interactive use.

   Last updated on 2020-02-17 12:12:55 -0800

     * [10]Prev
       SCons-3.1.2
     * [11]Next
       Subversion-1.13.0
     * [12]Up
     * [13]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/scons.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/subversion.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/prog.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. http://www.jedsoft.org/releases/slang/slang-2.3.2.tar.bz2
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libpng.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/pcre.html
   8. https://github.com/kkos/oniguruma
   9. http://wiki.linuxfromscratch.org/blfs/wiki/slang
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/scons.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/subversion.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/prog.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
