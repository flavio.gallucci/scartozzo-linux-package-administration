Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 25. X Libraries

     * [1]Prev
       keybinder-0.3.1
     * [2]Next
       Libhandy-0.0.11
     * [3]Up
     * [4]Home

libdazzle-3.34.1

Introduction to libdazzle

   libdazzle is a companion library to GObject and GTK+ that adds APIs for
   special graphical effects.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]http://ftp.gnome.org/pub/gnome/sources/libdazzle/3.34/libdazzle-
       3.34.1.tar.xz
     * Download (FTP):
       [6]ftp://ftp.gnome.org/pub/gnome/sources/libdazzle/3.34/libdazzle-3
       .34.1.tar.xz
     * Download MD5 sum: e796a92dd3f529616ed388c15208359b
     * Download size: 432 KB
     * Estimated disk space required: 34 MB (with tests)
     * Estimated build time: 0.2 SBU (uning parallelism=4; with tests)

libdazzle Dependencies

Required

   [7]GTK+-3.24.13

   User Notes: [8]http://wiki.linuxfromscratch.org/blfs/wiki/libdazzle

Installation of libdazzle

   Install libdazzle by running the following commands:
mkdir build &&
cd    build &&

meson --prefix=/usr .. &&
ninja

   To test the results, issue: ninja test. Two tests may fail due to a
   theme parsing error.

   Now, as the root user:
ninja install

Contents

   Installed Program: dazzle-list-counters
   Installed Library: libdazzle-1.0.so
   Installed Directory: /usr/include/libdazzle-1.0

Short Descriptions

   dazzle-list-counters

   lists counters that are in use by a process.

   libdazzle-1.0.so

   contains API functions for graphical effects

   Last updated on 2020-02-18 14:50:03 -0800

     * [9]Prev
       keybinder-0.3.1
     * [10]Next
       Libhandy-0.0.11
     * [11]Up
     * [12]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/keybinder.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libhandy.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lib.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. http://ftp.gnome.org/pub/gnome/sources/libdazzle/3.34/libdazzle-3.34.1.tar.xz
   6. ftp://ftp.gnome.org/pub/gnome/sources/libdazzle/3.34/libdazzle-3.34.1.tar.xz
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gtk3.html
   8. http://wiki.linuxfromscratch.org/blfs/wiki/libdazzle
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/keybinder.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libhandy.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lib.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
