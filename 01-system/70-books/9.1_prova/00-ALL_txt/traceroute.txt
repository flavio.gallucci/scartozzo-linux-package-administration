Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 16. Networking Utilities

     * [1]Prev
       Nmap-7.80
     * [2]Next
       Whois-5.4.3
     * [3]Up
     * [4]Home

Traceroute-2.1.0

Introduction to Traceroute

   The Traceroute package contains a program which is used to display the
   network route that packets take to reach a specified host. This is a
   standard network troubleshooting tool. If you find yourself unable to
   connect to another system, traceroute can help pinpoint the problem.
   [Note]

Note

   This package overwrites the version of traceroute that was installed in
   the inetutils package in LFS. This version is more powerful and allows
   many more options than the standard version.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://downloads.sourceforge.net/traceroute/traceroute-2.1.0.ta
       r.gz
     * Download MD5 sum: 84d329d67abc3fb83fc8cb12aeaddaba
     * Download size: 72 KB
     * Estimated disk space required: 588 KB
     * Estimated build time: less than 0.1 SBU

   User Notes: [6]http://wiki.linuxfromscratch.org/blfs/wiki/traceroute

Installation of Traceroute

   Install Traceroute by running the following commands:
make

   This package does not come with a test suite.

   Now, as the root user:
make prefix=/usr install                                 &&
mv /usr/bin/traceroute /bin                              &&
ln -sv -f traceroute /bin/traceroute6                    &&
ln -sv -f traceroute.8 /usr/share/man/man8/traceroute6.8 &&
rm -fv /usr/share/man/man1/traceroute.1

   The traceroute.1 file that was installed in LFS by inetutils is no
   longer relevant. This package overwrites that version of traceroute and
   installs the man page in man chapter 8.

Contents

   Installed Program: traceroute and traceroute6 (symlink)
   Installed Libraries: None
   Installed Directories: None

Short Descriptions

   traceroute

   does basically what it says: it traces the route your packets take from
   the host you are working on to another host on a network, showing all
   the intermediate hops (gateways) along the way.

   traceroute6

   is equivalent to traceroute -6.

   Last updated on 2020-02-17 12:12:55 -0800

     * [7]Prev
       Nmap-7.80
     * [8]Next
       Whois-5.4.3
     * [9]Up
     * [10]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/nmap.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/whois.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/netutils.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://downloads.sourceforge.net/traceroute/traceroute-2.1.0.tar.gz
   6. http://wiki.linuxfromscratch.org/blfs/wiki/traceroute
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/nmap.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/whois.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/netutils.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
