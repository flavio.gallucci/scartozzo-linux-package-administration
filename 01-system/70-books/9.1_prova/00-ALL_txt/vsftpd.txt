Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 20. Major Servers

     * [1]Prev
       ProFTPD-1.3.6c
     * [2]Next
       Mail Server Software
     * [3]Up
     * [4]Home

vsftpd-3.0.3

Introduction to vsftpd

   The vsftpd package contains a very secure and very small FTP daemon.
   This is useful for serving files over a network.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://security.appspot.com/downloads/vsftpd-3.0.3.tar.gz
     * Download MD5 sum: da119d084bd3f98664636ea05b5bb398
     * Download size: 196 KB
     * Estimated disk space required: 2 MB
     * Estimated build time: less than 0.1 SBU

vsftpd Dependencies

Required

   [6]libnsl-1.2.0

Optional

   [7]libcap-2.31 with PAM, and [8]Linux-PAM-1.3.1

   User Notes: [9]http://wiki.linuxfromscratch.org/blfs/wiki/vsftpd

Installation of vsftpd

   For security reasons, running vsftpd as an unprivileged user and group
   is encouraged. Also, a user should be created to map anonymous users.
   As the root user, create the needed directories, users, and groups with
   the following commands:
install -v -d -m 0755 /usr/share/vsftpd/empty &&
install -v -d -m 0755 /home/ftp               &&
groupadd -g 47 vsftpd                         &&
groupadd -g 45 ftp                            &&

useradd -c "vsftpd User"  -d /dev/null -g vsftpd -s /bin/false -u 47 vsftpd &&
useradd -c anonymous_user -d /home/ftp -g ftp    -s /bin/false -u 45 ftp

   Build vsftpd as an unprivileged user using the following command:
make

   This package does not come with a test suite.

   Once again, become the root user and install vsftpd with the following
   commands:
install -v -m 755 vsftpd        /usr/sbin/vsftpd    &&
install -v -m 644 vsftpd.8      /usr/share/man/man8 &&
install -v -m 644 vsftpd.conf.5 /usr/share/man/man5 &&
install -v -m 644 vsftpd.conf   /etc

Command Explanations

   install -v -d ...: This creates the directory that anonymous users will
   use (/home/ftp) and the directory the daemon will chroot into
   (/usr/share/vsftpd/empty).
   [Note]

Note

   /home/ftp should not be owned by the user vsftpd, or the user ftp.

   echo "#define VSF_BUILD_TCPWRAPPERS" >>builddefs.h: Use this prior to
   make to add support for tcpwrappers.

   echo "#define VSF_BUILD_SSL" >>builddefs.h: Use this prior to make to
   add support for SSL.

   install -v -m ...: The Makefile uses non-standard installation paths.
   These commands install the files in /usr and /etc.

Configuring vsftpd

Config Files

   /etc/vsftpd.conf

Configuration Information

   vsftpd comes with a basic anonymous-only configuration file that was
   copied to /etc above. While still as root, this file should be modified
   because it is now recommended to run vsftpd in standalone mode. Also,
   you should specify the privilege separation user created above.
   Finally, you should specify the chroot directory. man vsftpd.conf will
   give you all the details.
cat >> /etc/vsftpd.conf << "EOF"
background=YES
listen=YES
nopriv_user=vsftpd
secure_chroot_dir=/usr/share/vsftpd/empty
EOF

   The vsftpd daemon uses seccomp to improve security by default. But it's
   known to cause vsftpd unable to handle ftp LIST command with recent
   kernel versions. Append a line to /etc/vsftpd.conf (as the root user)
   to disable seccomp and workaround this issue:
cat >> /etc/vsftpd.conf << "EOF"
seccomp_sandbox=NO
EOF

   To enable local logins, append the following to the /etc/vsftpd.conf
   file (as the root user):
cat >> /etc/vsftpd.conf << "EOF"
local_enable=YES
EOF

   In addition, if using Linux-PAM and vsftpd with local user logins, you
   will need a Linux-PAM configuration file. As the root user, create the
   /etc/pam.d/vsftpd file, and add the needed configuration changes for
   Linux-PAM session support using the following commands:
cat > /etc/pam.d/vsftpd << "EOF" &&
# Begin /etc/pam.d/vsftpd
auth       required     /lib/security/pam_listfile.so item=user sense=deny \
                                                      file=/etc/ftpusers \
                                                      onerr=succeed
auth       required     pam_shells.so
auth       include      system-auth
account    include      system-account
session    include      system-session
EOF

cat >> /etc/vsftpd.conf << "EOF"
session_support=YES
pam_service_name=vsftpd
EOF

Boot Script

   Install the /etc/rc.d/init.d/vsftpd init script included in the
   [10]blfs-bootscripts-20191204 package.
make install-vsftpd

Contents

   Installed Program: vsftpd
   Installed Libraries: None
   Installed Directories: /usr/share/vsftpd, /home/ftp

Short Descriptions

   vsftpd

   is the FTP daemon.

   Last updated on 2020-02-21 09:11:06 -0800

     * [11]Prev
       ProFTPD-1.3.6c
     * [12]Next
       Mail Server Software
     * [13]Up
     * [14]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/proftpd.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/mail.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/majorservers.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://security.appspot.com/downloads/vsftpd-3.0.3.tar.gz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/basicnet/libnsl.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/libcap.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/linux-pam.html
   9. http://wiki.linuxfromscratch.org/blfs/wiki/vsftpd
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/introduction/bootscripts.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/proftpd.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/mail.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/majorservers.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
