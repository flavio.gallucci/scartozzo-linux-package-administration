Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 13. Programming

     * [1]Prev
       Clisp-2.49
     * [2]Next
       DejaGnu-1.6.2
     * [3]Up
     * [4]Home

CMake-3.16.4

Introduction to CMake

   The CMake package contains a modern toolset used for generating
   Makefiles. It is a successor of the auto-generated configure script and
   aims to be platform- and compiler-independent. A significant user of
   CMake is KDE since version 4.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://cmake.org/files/v3.16/cmake-3.16.4.tar.gz
     * Download MD5 sum: e3dacb6b612a6b06f7054c7fb4e18616
     * Download size: 8.7 MB
     * Estimated disk space required: 443 MB (add 541 MB for tests)
     * Estimated build time: 2.8 SBU (add 3.8 SBU for tests, both using
       parallelism=4)

CMake Dependencies

Required

   [6]libuv-1.34.2

Recommended

   [7]cURL-7.68.0 and [8]libarchive-3.4.2

Optional

   [9]Qt-5.14.1 (for the Qt-based GUI), [10]Subversion-1.13.0 (for
   testing), and [11]Sphinx (for building documents)

   User Notes: [12]http://wiki.linuxfromscratch.org/blfs/wiki/cmake

Installation of CMake

   Install CMake by running the following commands:
sed -i '/"lib64"/s/64//' Modules/GNUInstallDirs.cmake &&

./bootstrap --prefix=/usr        \
            --system-libs        \
            --mandir=/share/man  \
            --no-system-jsoncpp  \
            --no-system-librhash \
            --docdir=/share/doc/cmake-3.16.4 &&
make

   To test the results, issue: bin/ctest -j<N> -O cmake-3.16.4-test.log,
   where <N> is an integer between 1 and the number of system cores. In
   case the environment variable LANG is set to a non-blank value and
   failures occur, try running the tests without having LANG set. The test
   RunCMake.CommandLineTar is known to fail if the [13]zstd package is not
   installed.

   Now, as the root user:
make install

Command Explanations

   sed ... Modules/GNUInstallDirs.cmake: This command disables
   applications using cmake from attempting to install files in
   /usr/lib64/.

   --system-libs: This switch forces the build system to link against
   Zlib, Bzip2, cURL, Expat and libarchive installed on the system.

   --no-system-jsoncpp: This switch removes the JSON-C++ library from the
   list of system libraries. A bundled version of that library is used
   instead.

   --qt-gui: This switch enables building of the Qt-based GUI for CMake.

Contents

   Installed Programs: ccmake, cmake, cmake-gui (optional), cpack, and
   ctest
   Installed Libraries: None
   Installed Directories: /usr/share/cmake-3.16 and
   /usr/share/doc/cmake-3.16.4

Short Descriptions

   ccmake

   is a curses based interactive frontend to cmake.

   cmake

   is the makefile generator.

   cmake-gui

   (optional) is the Qt-based frontent to cmake.

   cpack

   is the CMake packaging program.

   ctest

   is a testing utility for cmake-generated build trees.

   Last updated on 2020-02-15 08:54:30 -0800

     * [14]Prev
       Clisp-2.49
     * [15]Next
       DejaGnu-1.6.2
     * [16]Up
     * [17]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/clisp.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/dejagnu.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/prog.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://cmake.org/files/v3.16/cmake-3.16.4.tar.gz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libuv.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/basicnet/curl.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libarchive.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/qt5.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/subversion.html
  11. https://pypi.python.org/pypi/Sphinx
  12. http://wiki.linuxfromscratch.org/blfs/wiki/cmake
  13. https://github.com/facebook/zstd
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/clisp.html
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/dejagnu.html
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/prog.html
  17. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
