Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 42. Multimedia Libraries and Drivers

     * [1]Prev
       gst-libav-1.16.2
     * [2]Next
       id3lib-3.8.3
     * [3]Up
     * [4]Home

gstreamer-vaapi-1.16.2

Introduction to gstreamer-vaapi

   The gstreamer-vaapi package contains a gstreamer plugin for hardware
   accelerated video decode/encode for the prevailing coding standards
   today (MPEG-2, MPEG-4 ASP/H.263, MPEG-4 AVC/H.264, and VC-1/VMW3).

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://gstreamer.freedesktop.org/src/gstreamer-vaapi/gstreamer-
       vaapi-1.16.2.tar.xz
     * Download MD5 sum: 13f7cb6a64bde24e67f563377487dcce
     * Download size: 1.0 MB
     * Estimated disk space required: 19 MB
     * Estimated build time: 0.1 SBU

gstreamer-vaapi Dependencies

Required

   [6]gstreamer-1.16.2, [7]gst-plugins-base-1.16.2,
   [8]gst-plugins-bad-1.16.2, and [9]libva-2.6.1

   User Notes:
   [10]http://wiki.linuxfromscratch.org/blfs/wiki/gstreamer10-vaapi

Installation of gstreamer-vaapi

   [Note]

Note

   If you do not have an Objective-C compiler installed, the build system
   for this package will emit a warning about a failed sanity check. This
   is harmless, and it is safe to continue.

   Install gstreamer-vaapi by running the following commands:
mkdir build &&
cd    build &&

meson  --prefix=/usr       \
       -Dbuildtype=release \
       -Dpackage-origin=http://www.linuxfromscratch.org/blfs/view/svn/ \
       -Dpackage-name="GStreamer 1.16.2 BLFS" &&
ninja

   This package does not come with a test suite.

   Now, as the root user:
ninja install

Contents

   Installed Programs: None
   Installed Library: libgstvaapi.so
   Installed Directory: None

   Last updated on 2020-02-18 14:50:03 -0800

     * [11]Prev
       gst-libav-1.16.2
     * [12]Next
       id3lib-3.8.3
     * [13]Up
     * [14]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gst10-libav.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/id3lib.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libdriv.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://gstreamer.freedesktop.org/src/gstreamer-vaapi/gstreamer-vaapi-1.16.2.tar.xz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gstreamer10.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gst10-plugins-base.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gst10-plugins-bad.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/x7driver.html#libva
  10. http://wiki.linuxfromscratch.org/blfs/wiki/gstreamer10-vaapi
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gst10-libav.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/id3lib.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libdriv.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
