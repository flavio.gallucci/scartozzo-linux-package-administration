Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 10. Graphics and Font Libraries

     * [1]Prev
       LibTIFF-4.1.0
     * [2]Next
       mypaint-brushes-1.3.0
     * [3]Up
     * [4]Home

libwebp-1.1.0

Introduction to libwebp

   The libwebp package contains a library and support programs to encode
   and decode images in WebP format.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]http://downloads.webmproject.org/releases/webp/libwebp-1.1.0.tar
       .gz
     * Download MD5 sum: 7e047f2cbaf584dff7a8a7e0f8572f18
     * Download size: 3.8 MB
     * Estimated disk space required: 45 MB
     * Estimated build time: 0.3 SBU

libwebp Dependencies

Recommended

   [6]libjpeg-turbo-2.0.4, [7]libpng-1.6.37, [8]LibTIFF-4.1.0, and
   [9]SDL-1.2.15 (for improved 3D Acceleration)

Optional

   [10]Freeglut-3.2.1 and [11]giflib-5.2.1

   User Notes: [12]http://wiki.linuxfromscratch.org/blfs/wiki/libwebp

Installation of libwebp

   Install libwebp by running the following commands:
./configure --prefix=/usr           \
            --enable-libwebpmux     \
            --enable-libwebpdemux   \
            --enable-libwebpdecoder \
            --enable-libwebpextras  \
            --enable-swap-16bit-csp \
            --disable-static        &&
make

   This package does not come with a test suite.

   Now, as the root user:
make install

Command Explanations

   --enable-swap-16bit-csp: This switch enables byte swap for 16 bit
   colorspaces.

   --disable-static: This switch prevents installation of static versions
   of the libraries.

Contents

   Installed Programs: cwebp, dwebp, gif2webp, vwebp, and webpmux
   Installed Library: libwebpdecoder.so, libwebpdemux.so,
   libwebpextras.so, libwebpmux.so, and libwebp.so
   Installed Directory: /usr/include/webp

Short Descriptions

   cwebp

   compresses an image using the WebP format.

   dwebp

   decompresses WebP files into PNG, PAM, PPM or PGM images.

   gif2webp

   converts a GIF image to a WebP image.

   vwebp

   decompress a WebP file and display it in a window.

   webpmux

   creates animated WebP files from non-animated WebP images, extracts
   frames from animated WebP images, and manages XMP/EXIF metadata and ICC
   profile.

   libwebp.so

   contains the API functions for WebP encoding and decoding.

   Last updated on 2020-02-16 15:15:05 -0800

     * [13]Prev
       LibTIFF-4.1.0
     * [14]Next
       mypaint-brushes-1.3.0
     * [15]Up
     * [16]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libtiff.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/mypaint-brushes.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/graphlib.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. http://downloads.webmproject.org/releases/webp/libwebp-1.1.0.tar.gz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libjpeg.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libpng.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libtiff.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/multimedia/sdl.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/freeglut.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/giflib.html
  12. http://wiki.linuxfromscratch.org/blfs/wiki/libwebp
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libtiff.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/mypaint-brushes.html
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/graphlib.html
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
