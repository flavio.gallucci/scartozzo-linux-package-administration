Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 27. Window Managers

     * [1]Prev
       sawfish-1.12.0
     * [2]Next
       Icons
     * [3]Up
     * [4]Home

Other Window Managers

   twm is the Tab Window Manager. This is the default window manager
   installed by the [5]X Window System packages.

   mwm is the Motif® Window Manager. It is an OSF/Motif® clone packaged
   and installed with [6]LessTif.

   Last updated on 2012-05-06 08:26:39 -0700

     * [7]Prev
       sawfish-1.12.0
     * [8]Next
       Icons
     * [9]Up
     * [10]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/sawfish.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/icons.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/wm.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/installing.html
   6. http://sourceforge.net/projects/lesstif/
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/sawfish.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/icons.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/wm.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
