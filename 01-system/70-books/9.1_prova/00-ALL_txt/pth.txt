Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 9. General Libraries

     * [1]Prev
       Popt-1.16
     * [2]Next
       Ptlib-2.10.11
     * [3]Up
     * [4]Home

Pth-2.0.7

Introduction to Pth

   The Pth package contains a very portable POSIX/ANSI-C based library for
   Unix platforms which provides non-preemptive priority-based scheduling
   for multiple threads of execution (multithreading) inside event-driven
   applications. All threads run in the same address space of the server
   application, but each thread has its own individual program-counter,
   run-time stack, signal mask and errno variable.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP): [5]https://ftp.gnu.org/gnu/pth/pth-2.0.7.tar.gz
     * Download (FTP): [6]ftp://ftp.gnu.org/gnu/pth/pth-2.0.7.tar.gz
     * Download MD5 sum: 9cb4a25331a4c4db866a31cbe507c793
     * Download size: 652 KB
     * Estimated disk space required: 5 MB
     * Estimated build time: 0.2 SBU

   User Notes: [7]http://wiki.linuxfromscratch.org/blfs/wiki/pth

Installation of Pth

   [Caution]

Caution

   Don't add the --enable-pthread parameter to the configure command below
   else you will overwrite the pthread library and interface header
   installed by the Glibc package in LFS.

   Install Pth by running the following commands:
sed -i 's#$(LOBJS): Makefile#$(LOBJS): pth_p.h Makefile#' Makefile.in &&
./configure --prefix=/usr           \
            --disable-static        \
            --mandir=/usr/share/man &&
make

   To test the results, issue: make test.

   Now, as the root user:
make install &&
install -v -m755 -d /usr/share/doc/pth-2.0.7 &&
install -v -m644    README PORTING SUPPORT TESTS \
                    /usr/share/doc/pth-2.0.7

Command Explanations

   sed -i 's#$(LOBJS) ...: This sed fixes a race condition in the
   Makefile. It allows you to run make with multiple jobs (e.g., make -j4)
   .

   --disable-static: This switch prevents installation of static versions
   of the libraries.

   --mandir=/usr/share/man: This switch puts the man pages in
   /usr/share/man instead of /usr/man.

Contents

   Installed Program: pth-config
   Installed Library: libpth.so
   Installed Directory: /usr/share/doc/pth-2.0.7

Short Descriptions

   pth-config

   is a utility used to configure and build applications based on the
   pth(3) library. It can be used to query the C compiler and linker flags
   which are required to correctly compile and link the application
   against the pth(3) library.

   libpth.so

   contains the API functions used by the GNU Portable Threads Library.

   Last updated on 2020-02-15 08:54:30 -0800

     * [8]Prev
       Popt-1.16
     * [9]Next
       Ptlib-2.10.11
     * [10]Up
     * [11]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/popt.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/ptlib.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://ftp.gnu.org/gnu/pth/pth-2.0.7.tar.gz
   6. ftp://ftp.gnu.org/gnu/pth/pth-2.0.7.tar.gz
   7. http://wiki.linuxfromscratch.org/blfs/wiki/pth
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/popt.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/ptlib.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/genlib.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
