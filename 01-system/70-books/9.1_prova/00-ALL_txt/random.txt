Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 3. After LFS Configuration Issues

     * [1]Prev
       Customizing your Logon with /etc/issue
     * [2]Next
       Security
     * [3]Up
     * [4]Home

Random Number Generation

   The Linux kernel supplies a random number generator which is accessed
   through /dev/random and /dev/urandom. Programs that utilize the random
   and urandom devices, such as OpenSSH, will benefit from these
   instructions.

   When a Linux system starts up without much operator interaction, the
   entropy pool (data used to compute a random number) may be in a fairly
   predictable state. This creates the real possibility that the number
   generated at startup may always be the same. In order to counteract
   this effect, you should carry the entropy pool information across your
   shut-downs and start-ups.

   Install the /etc/rc.d/init.d/random init script included with the
   [5]blfs-bootscripts-20191204 package.
make install-random

   Last updated on 2016-06-03 20:04:06 -0700

     * [6]Prev
       Customizing your Logon with /etc/issue
     * [7]Next
       Security
     * [8]Up
     * [9]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/logon.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/security.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/config.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/introduction/bootscripts.html
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/logon.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/security.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/config.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
