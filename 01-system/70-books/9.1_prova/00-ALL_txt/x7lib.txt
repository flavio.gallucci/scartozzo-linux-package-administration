Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 24. X Window System Environment

     * [1]Prev
       libxcb-1.13.1
     * [2]Next
       xcb-util-0.4.0
     * [3]Up
     * [4]Home

Xorg Libraries

Introduction to Xorg Libraries

   The Xorg libraries provide library routines that are used within all X
   Window applications.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP): [5]https://www.x.org/pub/individual/lib/
     * Download (FTP): [6]ftp://ftp.x.org/pub/individual/lib/
     * Download size: 13 MB
     * Estimated disk space required: 224 MB (34 MB installed) - if source
       directories not deleted
     * Estimated build time: 2.4 SBU

Xorg Libraries Dependencies

Required

   [7]Fontconfig-2.13.1 and [8]libxcb-1.13.1

Recommended

   [9]elogind-243.4

Optional

   [10]xmlto-0.0.28 with one or more of the following: [11]fop-2.4,
   [12]Links-2.20.2, [13]Lynx-2.8.9rel.1, and [14]W3m (to generate
   additional PDF or text documentation for the libXfont package).

   User Notes:
   [15]http://wiki.linuxfromscratch.org/blfs/wiki/Xorg7Libraries

Downloading Xorg Libraries

   First, create a list of files to be downloaded. This file will also be
   used to verify the integrity of the downloads when complete:
cat > lib-7.md5 << "EOF"
ce2fb8100c6647ee81451ebe388b17ad  xtrans-1.4.0.tar.bz2
55adbfb6d4370ecac5e70598c4e7eed2  libX11-1.6.9.tar.bz2
f5b48bb76ba327cd2a8dc7a383532a95  libXext-1.3.4.tar.bz2
4e1196275aa743d6ebd3d3d5ec1dff9c  libFS-1.0.8.tar.bz2
76d77499ee7120a56566891ca2c0dbcf  libICE-1.0.10.tar.bz2
87c7fad1c1813517979184c8ccd76628  libSM-1.2.3.tar.bz2
eeea9d5af3e6c143d0ea1721d27a5e49  libXScrnSaver-1.2.3.tar.bz2
a9019421d3ee8b4937b6afd9025f018a  libXt-1.2.0.tar.bz2
ac774cff8b493f566088a255dbf91201  libXmu-1.1.3.tar.bz2
6f0ecf8d103d528cfc803aa475137afa  libXpm-3.5.13.tar.bz2
e5e06eb14a608b58746bdd1c0bd7b8e3  libXaw-1.0.13.tar.bz2
07e01e046a0215574f36a3aacb148be0  libXfixes-5.0.3.tar.bz2
3fa0841ea89024719b20cd702a9b54e0  libXcomposite-0.4.5.tar.bz2
802179a76bded0b658f4e9ec5e1830a4  libXrender-0.9.10.tar.bz2
9b9be0e289130fb820aedf67705fc549  libXcursor-1.2.0.tar.bz2
e3f554267a7a04b042dc1f6352bd6d99  libXdamage-1.1.5.tar.bz2
6447db6a689fb530c218f0f8328c3abc  libfontenc-1.1.4.tar.bz2
00516bed7ec1453d56974560379fff2f  libXfont2-2.0.4.tar.bz2
4a433c24627b4ff60a4dd403a0990796  libXft-2.3.3.tar.bz2
62c4af0839072024b4b1c8cbe84216c7  libXi-1.7.10.tar.bz2
0d5f826a197dae74da67af4a9ef35885  libXinerama-1.1.4.tar.bz2
18f3b20d522f45e4dadd34afb5bea048  libXrandr-1.5.2.tar.bz2
5d6d443d1abc8e1f6fc1c57fb27729bb  libXres-1.2.0.tar.bz2
ef8c2c1d16a00bd95b9fdcef63b8a2ca  libXtst-1.2.3.tar.bz2
210b6ef30dda2256d54763136faa37b9  libXv-1.0.11.tar.bz2
3569ff7f3e26864d986d6a21147eaa58  libXvMC-1.0.12.tar.bz2
0ddeafc13b33086357cfa96fae41ee8e  libXxf86dga-1.1.5.tar.bz2
298b8fff82df17304dfdb5fe4066fe3a  libXxf86vm-1.1.4.tar.bz2
d2f1f0ec68ac3932dd7f1d9aa0a7a11c  libdmx-1.1.4.tar.bz2
b34e2cbdd6aa8f9cc3fa613fd401a6d6  libpciaccess-0.16.tar.bz2
dd7e1e946def674e78c0efbc5c7d5b3b  libxkbfile-1.1.0.tar.bz2
42dda8016943dc12aff2c03a036e0937  libxshmfence-1.3.tar.bz2
EOF

   To download the needed files using wget, use the following commands:
mkdir lib &&
cd lib &&
grep -v '^#' ../lib-7.md5 | awk '{print $2}' | wget -i- -c \
    -B https://www.x.org/pub/individual/lib/ &&
md5sum -c ../lib-7.md5

Installation of Xorg Libraries

   [Note]

Note

   When installing multiple packages in a script, the installation needs
   to be done as the root user. There are three general options that can
   be used to do this:
    1. Run the entire script as the root user (not recommended).
    2. Use the sudo command from the [16]Sudo-1.8.31 package.
    3. Use su -c "command arguments" (quotes required) which will ask for
       the root password for every iteration of the loop.

   One way to handle this situation is to create a short bash function
   that automatically selects the appropriate method. Once the command is
   set in the environment, it does not need to be set again.
as_root()
{
  if   [ $EUID = 0 ];        then $*
  elif [ -x /usr/bin/sudo ]; then sudo $*
  else                            su -c \\"$*\\"
  fi
}

export -f as_root

   Some libraries come with a test suite. If you wish to execute them,
   either comment out the rm -rf ... below, so that, after all libraries
   are installed, you can come back to the corresponding directory and run
   make check, or do individual builds, running the tests for each of
   those distributed with working test suites. Alternatively, you can
   uncomment the line #make check ..., and at the end, check the test
   results with:
grep -A9 summary *make_check.log

   BLFS developers have confirmed that libX11, libXt and libxshmfence are
   distributed with working test suites.

   First, start a subshell that will exit on error:
bash -e

   Install all of the packages by running the following commands:
for package in $(grep -v '^#' ../lib-7.md5 | awk '{print $2}')
do
  packagedir=${package%.tar.bz2}
  tar -xf $package
  pushd $packagedir
  case $packagedir in
    libICE* )
      ./configure $XORG_CONFIG ICE_LIBS=-lpthread
    ;;

    libXfont2-[0-9]* )
      ./configure $XORG_CONFIG --disable-devel-docs
    ;;

    libXt-[0-9]* )
      ./configure $XORG_CONFIG \
                  --with-appdefaultdir=/etc/X11/app-defaults
    ;;

    * )
      ./configure $XORG_CONFIG
    ;;
  esac
  make
  #make check 2>&1 | tee ../$packagedir-make_check.log
  as_root make install
  popd
  rm -rf $packagedir
  as_root /sbin/ldconfig
done

   Finally, exit the shell that was started earlier:
exit

Command Explanations

   ./configure $XORG_CONFIG ICE_LIBS=-lpthread: This fixes a change in
   glibc which breaks pulseaudio at runtime.

   --disable-devel-docs: Disable generation of text documentation in the
   libXfont2 package if [17]xmlto-0.0.28 is installed without a text
   browser. Omit this parameter (or the entire case statement) if a text
   browser is installed.

   --with-fop: Use [18]fop-2.4 to generate PDF documentation (only for the
   libXfont package).

Configuration of Xorg Libraries

   If you've chosen to install Xorg into /usr, then no further
   configuration is necessary and you can skip the rest of this section.
   If you've opted for an alternate prefix, you should create two symlinks
   to satisfy the expected environment of several packages. Execute the
   following commands as the root user:
ln -sv $XORG_PREFIX/lib/X11 /usr/lib/X11 &&
ln -sv $XORG_PREFIX/include/X11 /usr/include/X11

Contents

   Installed Programs: cxpm and sxpm
   Installed Libraries: libdmx.so, libfontenc.so, libFS.so, libICE.so,
   libpciaccess.so, libSM.so, libX11.so, libXaw6.so, libXaw7.so,
   libXaw.so, libXcomposite.so, libXcursor.so, libXdamage.so, libXext.so,
   libXfixes.so, libXfont2.so, libXft.so, libXinerama.so, libXi.so,
   libxkbfile.so, libXmu.so, libXmuu.so, libXpm.so, libXrandr.so,
   libXrender.so, libXRes.so, libxshmfence.so, libXss.so, libXt.so,
   libXtst.so, libXvMC.so, libXvMCW.so, libXv.so, libXxf86dga.so and
   libXxf86vm.so
   Installed Directories: $XORG_PREFIX/include/X11/fonts,
   $XORG_PREFIX/include/X11/Xtrans, $XORG_PREFIX/share/doc/libFS,
   $XORG_PREFIX/share/doc/libICE, $XORG_PREFIX/share/doc/libSM,
   $XORG_PREFIX/share/doc/libX11, $XORG_PREFIX/share/doc/libXaw,
   $XORG_PREFIX/share/doc/libXext, $XORG_PREFIX/share/doc/libXi,
   $XORG_PREFIX/share/doc/libXmu, $XORG_PREFIX/share/doc/libXrender,
   $XORG_PREFIX/share/doc/libXt, $XORG_PREFIX/share/doc/libXtst,
   $XORG_PREFIX/share/doc/libXvMC, $XORG_PREFIX/share/doc/xtrans and
   $XORG_PREFIX/share/X11/locale

Short Descriptions

   cxpm

   checks the format of an XPM file.

   sxpm

   shows an XPM file and/or converts XPM 1 or 2 files to XPM 3.

   libdmx.so

   is the X Window System DMX (Distributed Multihead X) extension library.

   libfontenc.so

   is the X11 font encoding library.

   libFS.so

   is the library interface to the X Font Server.

   libICE.so

   is the X Inter Client Exchange Library.

   libpciaccess.so

   is the generic PCI Access library for X.

   libSM.so

   is the X Session Management Library.

   libX11.so

   is the Xlib Library.

   libXaw6.so

   is the X Athena Widgets Library, version 6.

   libXaw7.so

   is the X Athena Widgets Library, version 7.

   libXaw.so

   are symbolic links to the current X Athena Widgets Library, version 7.

   libXcomposite.so

   is the X Composite Library.

   libXcursor.so

   is the X Cursor management library.

   libXdamage.so

   is the X Damage Library.

   libXext.so

   is the Misc X Extension Library.

   libXfixes.so

   provides augmented versions of core protocol requests.

   libXfont2.so

   is the X font library.

   libXft.so

   is the X FreeType interface library.

   libXinerama.so

   is the Xinerama Library.

   libXi.so

   is the X Input Extension Library.

   libxkbfile.so

   is the xkbfile Library.

   libXmu.so

   is the X interface library for miscellaneous utilities not part of the
   Xlib standard.

   libXmuu.so

   is the Mini Xmu Library.

   libXpm.so

   is the X Pixmap Library.

   libXrandr.so

   is the X Resize, Rotate and Reflection extension library.

   libXrender.so

   is the X Render Library.

   libXRes.so

   is the X-Resource extension client library.

   libxshmfence.so

   exposes an event API on top of Linux futexes.

   libXss.so

   is the X11 Screen Saver extension client library.

   libXt.so

   is the X Toolkit Library.

   libXtst.so

   is the Xtst Library.

   libXvMC.so

   is the X-Video Motion Compensation Library.

   libXvMCW.so

   is the XvMC Wrapper including the Nonstandard VLD extension.

   libXv.so

   is the X Window System video extension library.

   libXxf86dga.so

   is the client library for the XFree86-DGA extension.

   libXxf86vm.so

   is the client library for the XFree86-VidMode X extension.

   Last updated on 2020-02-15 20:23:35 -0800

     * [19]Prev
       libxcb-1.13.1
     * [20]Next
       xcb-util-0.4.0
     * [21]Up
     * [22]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libxcb.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/xcb-util.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/installing.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://www.x.org/pub/individual/lib/
   6. ftp://ftp.x.org/pub/individual/lib/
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/fontconfig.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libxcb.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/elogind.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/pst/xmlto.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/pst/fop.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/basicnet/links.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/basicnet/lynx.html
  14. http://w3m.sourceforge.net/
  15. http://wiki.linuxfromscratch.org/blfs/wiki/Xorg7Libraries
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/sudo.html
  17. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/pst/xmlto.html
  18. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/pst/fop.html
  19. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libxcb.html
  20. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/xcb-util.html
  21. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/installing.html
  22. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
