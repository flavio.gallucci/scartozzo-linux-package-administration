Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 25. X Libraries

     * [1]Prev
       libepoxy-1.5.4
     * [2]Next
       libnotify-0.7.8
     * [3]Up
     * [4]Home

libglade-2.6.4

Introduction to libglade

   The libglade package contains libglade libraries. These are useful for
   loading Glade interface files in a program at runtime.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]http://ftp.gnome.org/pub/gnome/sources/libglade/2.6/libglade-2.6
       .4.tar.bz2
     * Download (FTP):
       [6]ftp://ftp.gnome.org/pub/gnome/sources/libglade/2.6/libglade-2.6.
       4.tar.bz2
     * Download MD5 sum: d1776b40f4e166b5e9c107f1c8fe4139
     * Download size: 348 KB
     * Estimated disk space required: 5 MB
     * Estimated build time: 0.1 SBU

libglade Dependencies

Required

   [7]libxml2-2.9.10 and [8]GTK+-2.24.32

Optional

   [9]Python-2.7.17 and [10]GTK-Doc-1.32

   User Notes: [11]http://wiki.linuxfromscratch.org/blfs/wiki/libglade

Installation of libglade

   Install libglade by running the following commands:
sed -i '/DG_DISABLE_DEPRECATED/d' glade/Makefile.in &&
./configure --prefix=/usr --disable-static &&
make

   To test the results, issue: make check. One of the tests, test-convert,
   is known to fail.

   Now, as the root user:
make install

Command Explanations

   sed -i '/DG_DISABLE_DEPRECATED/d': Some of the glib functions that
   libglade uses were declared deprecated in glib-2.30. This sed removes
   the G_DISABLE_DEPRECATED CFLAG.

   --disable-static: This switch prevents installation of static versions
   of the libraries.

   --enable-gtk-doc: Use this parameter if GTK-Doc is installed and you
   wish to rebuild and install the API documentation.

Contents

   Installed Program: libglade-convert (requires python)
   Installed Library: libglade-2.0.so
   Installed Directories:
   /usr/{include/libglade-2.0/glade,share/{gtk-doc/html/libglade,
   xml/libglade}}

Short Descriptions

   libglade-convert

   is used to convert old Glade interface files to Glade-2.0 standards.

   libglade-2.0.so

   contains the functions necessary to load Glade interface files.

   Last updated on 2020-02-16 15:15:05 -0800

     * [12]Prev
       libepoxy-1.5.4
     * [13]Next
       libnotify-0.7.8
     * [14]Up
     * [15]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libepoxy.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libnotify.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lib.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. http://ftp.gnome.org/pub/gnome/sources/libglade/2.6/libglade-2.6.4.tar.bz2
   6. ftp://ftp.gnome.org/pub/gnome/sources/libglade/2.6/libglade-2.6.4.tar.bz2
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/libxml2.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gtk2.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/python2.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/gtk-doc.html
  11. http://wiki.linuxfromscratch.org/blfs/wiki/libglade
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libepoxy.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libnotify.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/lib.html
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
