Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 42. Multimedia Libraries and Drivers

     * [1]Prev
       SDL-1.2.15
     * [2]Next
       sound-theme-freedesktop-0.8
     * [3]Up
     * [4]Home

SDL2-2.0.10

Introduction to SDL2

   The Simple DirectMedia Layer Version 2 (SDL2 for short) is a
   cross-platform library designed to make it easy to write multimedia
   software, such as games and emulators.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]http://www.libsdl.org/release/SDL2-2.0.10.tar.gz
     * Download MD5 sum: 5a2114f2a6f348bdab5bf52b994811db
     * Download size: 5.3 MB
     * Estimated disk space required: 169 MB (with docs)
     * Estimated build time: 0.4 SBU (using parallelism=4; with docs)

Additional Downloads

     * Required patch (for i686 systems):
       [6]http://www.linuxfromscratch.org/patches/blfs/9.1/SDL2-2.0.10-ope
       ngl_include_fix-1.patch

SDL2 Dependencies

Optional

   [7]ALSA-1.2.1, [8]Doxygen-1.8.17 (to create documentation),
   [9]ibus-1.5.21, [10]NASM-2.14.02, [11]PulseAudio-13.0,
   [12]libsamplerate-0.1.9, [13]X Window System, [14]DirectFB, and
   [15]fcitx

   User Notes: [16]http://wiki.linuxfromscratch.org/blfs/wiki/sdl

Installation of SDL2

   First, if you are building on a 32-bit i686 system, apply a patch
   containing a fix for the OpenGL headers and their data types:
case $(uname -m) in
   i?86) patch -Np1 -i ../SDL2-2.0.10-opengl_include_fix-1.patch ;;
esac

   Install SDL2 by running the following commands:
./configure --prefix=/usr &&
make

   If you have [17]Doxygen-1.8.17 installed and want to build the html
   documentation, run the following commands:
pushd docs  &&
  doxygen   &&
popd

   [Note]

Note

   If you wish to build and run the package regression tests, do not
   delete the static libraries below until after the tests are built.

   Now, as the root user:
make install              &&
rm -v /usr/lib/libSDL2*.a

   If you built the documentation, install it as the root user:
install -v -m755 -d        /usr/share/doc/SDL2-2.0.10/html &&
cp -Rv  docs/output/html/* /usr/share/doc/SDL2-2.0.10/html

Command Explanations

   rm -v /usr/lib/libSDL2*.a: Normally static libraries can be disabled
   with a --disable-static option to configure, but that breaks the build
   in this package.

   --disable-alsa-shared: This switch disables dynamically loading ALSA
   shared libraries.

   --disable-sdl-dlopen: This switch disables using dlopen for shared
   object loading. Loading image backend libraries like libpng dynamically
   on the fly does not work.

   --disable-x11-shared: This switch disables dynamically loading X11
   shared libraries.

Configuring SDL2

Configuration Information

   As with most libraries, there is no configuration to do, save that the
   library directory, i.e., /opt/lib or /usr/local/lib should appear in
   /etc/ld.so.conf so that ldd can find the shared libraries. After
   checking that this is the case, /sbin/ldconfig should be run while
   logged in as root.

Contents

   Installed Program: sdl2-config
   Installed Libraries: libSDL2.so
   Installed Directories: /usr/include/SDL2 and /usr/share/doc/SDL-2.0.10

Short Descriptions

   sdl2-config

   determines the compile and linker flags that should be used to compile
   and link programs that use libSDL2.

   libSDL2.so

   library provides low level access to audio, keyboard, mouse, joystick,
   3D hardware via OpenGL, and 2D frame buffer across multiple platforms.

   Last updated on 2020-02-16 15:15:05 -0800

     * [18]Prev
       SDL-1.2.15
     * [19]Next
       sound-theme-freedesktop-0.8
     * [20]Up
     * [21]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/sdl.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/sound-theme-freedesktop.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libdriv.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. http://www.libsdl.org/release/SDL2-2.0.10.tar.gz
   6. http://www.linuxfromscratch.org/patches/blfs/9.1/SDL2-2.0.10-opengl_include_fix-1.patch
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/alsa.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/doxygen.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/ibus.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/nasm.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/pulseaudio.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libsamplerate.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/installing.html
  14. http://pkgs.fedoraproject.org/repo/pkgs/directfb/
  15. https://fcitx-im.org/
  16. http://wiki.linuxfromscratch.org/blfs/wiki/sdl
  17. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/general/doxygen.html
  18. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/sdl.html
  19. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/sound-theme-freedesktop.html
  20. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libdriv.html
  21. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
