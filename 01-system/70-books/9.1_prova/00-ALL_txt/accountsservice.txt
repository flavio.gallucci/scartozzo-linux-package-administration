Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 12. System Utilities

     * [1]Prev
       System Utilities
     * [2]Next
       acpid-2.0.32
     * [3]Up
     * [4]Home

AccountsService-0.6.55

Introduction to AccountsService

   The AccountsService package provides a set of D-Bus interfaces for
   querying and manipulating user account information and an
   implementation of those interfaces based on the usermod(8), useradd(8)
   and userdel(8) commands.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://www.freedesktop.org/software/accountsservice/accountsser
       vice-0.6.55.tar.xz
     * Download MD5 sum: 6e4c6fbd490260cfe17de2e76f5d803a
     * Download size: 96 KB
     * Estimated disk space required: 10 MB
     * Estimated build time: 0.1 SBU

AccountsService Dependencies

Required

   [6]libgcrypt-1.8.5 and [7]Polkit-0.116

Recommended

   [8]gobject-introspection-1.62.0 and [9]elogind-243.4

Optional

   [10]GTK-Doc-1.32 and [11]xmlto-0.0.28

   User Notes:
   [12]http://wiki.linuxfromscratch.org/blfs/wiki/accountsservice

Installation of AccountsService

   Install AccountsService by running the following commands:
mkdir build &&
cd build &&

meson --prefix=/usr                   \
            -Dadmin_group=adm         \
            -Delogind=true            \
            -Dsystemdsystemunitdir=no \
            .. &&
ninja

   This package does not come with a test suite.

   Now, as the root user:
ninja install

Command Explanations

   -Dadmin_group=adm: This switch sets the group for administrator
   accounts.

   -Ddocbook=true: This switch enables building the D-Bus interface API
   documentation.

Configuring AccountsService

   To allow users in the adm group to be listed as Administrators, execute
   the following commands as the root user:
cat > /etc/polkit-1/rules.d/40-adm.rules << "EOF"
polkit.addAdminRule(function(action, subject) {
   return ["unix-group:adm"];
   });
EOF

Boot Script

   To start the accounts-daemon daemon at boot, install the
   /etc/rc.d/init.d/accounts-daemon init script from the
   [13]blfs-bootscripts-20191204 package by running the following command
   as the root user:
make install-accounts-daemon

Contents

   Installed Programs: accounts-daemon (library executable)
   Installed Libraries: libaccountsservice.so
   Installed Directories: /usr/include/accountsservice-1.0,
   /usr/share/doc/accountsservice,
   /usr/share/gtk-doc/html/libaccountsservice, and
   /var/lib/AccountsService

Short Descriptions

   accounts-daemon

   is the AccountsService daemon.

   libaccountsservice.so

   contains the AccountsService API functions.

   Last updated on 2020-02-19 08:47:37 -0800

     * [14]Prev
       System Utilities
     * [15]Next
       acpid-2.0.32
     * [16]Up
     * [17]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/sysutils.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/acpid.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/sysutils.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://www.freedesktop.org/software/accountsservice/accountsservice-0.6.55.tar.xz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libgcrypt.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/polkit.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gobject-introspection.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/elogind.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gtk-doc.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/pst/xmlto.html
  12. http://wiki.linuxfromscratch.org/blfs/wiki/accountsservice
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/introduction/bootscripts.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/sysutils.html
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/acpid.html
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/sysutils.html
  17. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
