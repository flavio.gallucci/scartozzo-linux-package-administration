Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 5. File Systems and Disk Management

     * [1]Prev
       mdadm-4.1
     * [2]Next
       gptfdisk-1.0.5
     * [3]Up
     * [4]Home

ntfs-3g-2017.3.23

Introduction to Ntfs-3g

   The Ntfs-3g package contains a stable, read-write open source driver
   for NTFS partitions. NTFS partitions are used by most Microsoft
   operating systems. Ntfs-3g allows you to mount NTFS partitions in
   read-write mode from your Linux system. It uses the FUSE kernel module
   to be able to implement NTFS support in user space. The package also
   contains various utilities useful for manipulating NTFS partitions.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://tuxera.com/opensource/ntfs-3g_ntfsprogs-2017.3.23.tgz
     * Download MD5 sum: d97474ae1954f772c6d2fa386a6f462c
     * Download size: 1.2 MB
     * Estimated disk space required: 20 MB
     * Estimated build time: 0.2 SBU

Ntfs-3g Dependencies

Optional

   [6]fuse 2.x (this disables user mounts)

   User Notes: [7]http://wiki.linuxfromscratch.org/blfs/wiki/ntfs-3g

Kernel Configuration

   Enable the following options in the kernel configuration and recompile
   the kernel if necessary:
File systems  --->
  <*/M> FUSE (Filesystem in Userspace) support [CONFIG_FUSE_FS]

Installation of Ntfs-3g

   Install Ntfs-3g by running the following commands:
./configure --prefix=/usr        \
            --disable-static     \
            --with-fuse=internal &&
make

   This package does not come with a test suite.

   Now, as the root user:
make install &&
ln -sv ../bin/ntfs-3g /sbin/mount.ntfs &&
ln -sv ntfs-3g.8 /usr/share/man/man8/mount.ntfs.8

   If you want ordinary users to be able to mount NTFS partitions you'll
   need to set mount.ntfs with the root user ID. Note: it is probably
   unsafe to do this on a computer that needs to be secure (like a
   server). As the root user:
chmod -v 4755 /bin/ntfs-3g

Command Explanations

   --disable-static: This switch prevents installation of static versions
   of the libraries.

   --with-fuse=internal: This switch dynamically forces ntfs-3g to use an
   internal copy of the fuse-2.x library. This is required if you wish to
   allow users to mount NTFS partitions.

   --disable-ntfsprogs: Disables installation of various utilities used to
   manipulate NTFS partitions.

   ln -sv ../bin/ntfs-3g /sbin/mount.ntfs: Creating /sbin/mount.ntfs makes
   mount default to using Ntfs-3g to mount NTFS partitions.

   chmod -v 4755 /bin/ntfs-3g: Making mount.ntfs setuid root allows non
   root users to mount NTFS partitions.

Using Ntfs-3g

   To mount a Windows partition at boot time, put a line like this in
   /etc/fstab:
/dev/sda1 /mnt/windows auto defaults 0 0

   To allow users to mount a usb stick with an NTFS filesystem on it, put
   a line similar to this (change sdc1 to whatever a usb stick would be on
   your system) in /etc/fstab:
/dev/sdc1 /mnt/usb auto user,noauto,umask=0,utf8 0 0

   In order for a user to be able to mount the usb stick, they will need
   to be able to write to /mnt/usb, so as the root user:
chmod -v 777 /mnt/usb

Contents

   Installed Programs: lowntfs-3g, mkfs.ntfs, mkntfs, mount.lowntfs-3g,
   mount.ntfs, mount.ntfs-3g, ntfs-3g, ntfs-3g.probe, ntfs-3g.secaudit,
   ntfs-3g.usermap, ntfscat, ntfsclone, ntfscluster, ntfscmp, ntfscp,
   ntfsfix, ntfsinfo, ntfslabel, ntfsls, ntfsresize and ntfsundelete
   Installed Library: libntfs-3g.so
   Installed Directories: /usr/include/ntfs-3g and /usr/share/doc/ntfs-3g

Short Descriptions

   lowntfs-3g

   is similar to ntfs-3g but uses the Fuse low-level interface.

   mkfs.ntfs

   is a symlink to mkntfs.

   mkntfs

   creates an NTFS file system.

   mount.lowntfs-3g

   is a symlink to lowntfs-3g.

   mount.ntfs

   mounts an NTFS filesystem.

   mount.ntfs-3g

   is a symbolic link to ntfs-3g.

   ntfs-3g

   is an NTFS driver, which can create, remove, rename, move files,
   directories, hard links, and streams. It can also read and write files,
   including streams, sparse files and transparently compressed files. It
   can also handle special files like symbolic links, devices, and FIFOs;
   moreover it provides standard management of file ownership and
   permissions, including POSIX ACLs.

   ntfs-3g.probe

   tests if an NTFS volume is mountable read only or read-write, and exits
   with a status value accordingly. The volume can be a block device or
   image file.

   ntfs-3g.secaudit

   audits NTFS Security Data.

   ntfs-3g.usermap

   creates the file defining the mapping of Windows accounts to Linux
   logins for users who owns files which should be visible from both
   Windows and Linux.

   ntfscluster

   identifies files in a specified region of an NTFS volume

   ntfscp

   copies a file to an NTFS volume.

   ntfsfix

   fixes common errors and forces Windows to check an NTFS partition.

   ntfsls

   lists directory contents on an NTFS filesystem.

   ntfscat

   prints NTFS files and streams on the standard output.

   ntfsclone

   clones an NTFS filesystem.

   ntfscmp

   compares two NTFS filesystems and tells the differences.

   ntfsinfo

   dumps a file's attributes.

   ntfslabel

   displays or changes the label on an ntfs file system.

   ntfsresize

   resizes an NTFS filesystem without data loss.

   ntfsundelete

   recovers a deleted file from an NTFS volume.

   libntfs-3g.so

   contains the Ntfs-3g API functions.

   Last updated on 2020-02-17 13:54:09 -0800

     * [8]Prev
       mdadm-4.1
     * [9]Next
       gptfdisk-1.0.5
     * [10]Up
     * [11]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/mdadm.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gptfdisk.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/filesystems.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://tuxera.com/opensource/ntfs-3g_ntfsprogs-2017.3.23.tgz
   6. https://github.com/libfuse/libfuse
   7. http://wiki.linuxfromscratch.org/blfs/wiki/ntfs-3g
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/mdadm.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gptfdisk.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/filesystems.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
