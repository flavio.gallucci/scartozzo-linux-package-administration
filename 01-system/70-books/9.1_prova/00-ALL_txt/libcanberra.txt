Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 42. Multimedia Libraries and Drivers

     * [1]Prev
       libass-0.14.0
     * [2]Next
       libcddb-1.3.2
     * [3]Up
     * [4]Home

libcanberra-0.30

Introduction to libcanberra

   libcanberra is an implementation of the XDG Sound Theme and Name
   Specifications, for generating event sounds on free desktops, such as
   GNOME.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]http://0pointer.de/lennart/projects/libcanberra/libcanberra-0.30
       .tar.xz
     * Download MD5 sum: 34cb7e4430afaf6f447c4ebdb9b42072
     * Download size: 312 KB
     * Estimated disk space required: 7.5 MB
     * Estimated build time: 0.1 SBU

libcanberra Dependencies

Required

   [6]libvorbis-1.3.6

Recommended

   [7]alsa-lib-1.2.1.2, [8]gstreamer-1.16.2 and [9]GTK+-3.24.13

Optional

   [10]GTK+-2.24.32, [11]PulseAudio-13.0 and [12]tdb

   User Notes: [13]http://wiki.linuxfromscratch.org/blfs/wiki/libcanberra

Installation of libcanberra

   Install libcanberra by running the following commands:
./configure --prefix=/usr --disable-oss &&
make

   This package does not come with a test suite.

   Now, as the root user:
make docdir=/usr/share/doc/libcanberra-0.30 install

Command Explanations

   --disable-oss: disable optional deprecated OSS support

   --disable-gtk: disable optional GTK+ 2 support

   --disable-gtk3: disable optional GTK+ 3 support

Contents

   Installed Programs: canberra-boot and canberra-gtk-play
   Installed Libraries: libcanberra-gtk.so, libcanberra-gtk3.so and
   libcanberra.so
   Installed Directories: /usr/lib/libcanberra-0.30,
   /usr/share/doc/libcanberra-0.30 and /usr/share/gtk-doc/html/libcanberra

Short Descriptions

   canberra-gtk-play

   is an application used for playing sound events.

   libcanberra-gtk.so

   contains the libcanberra bindings for GTK+ 2.

   libcanberra-gtk3.so

   contains the libcanberra bindings for GTK+ 3.

   libcanberra.so

   contains the libcanberra API functions.

   Last updated on 2020-02-17 18:27:03 -0800

     * [14]Prev
       libass-0.14.0
     * [15]Next
       libcddb-1.3.2
     * [16]Up
     * [17]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libass.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libcddb.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libdriv.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. http://0pointer.de/lennart/projects/libcanberra/libcanberra-0.30.tar.xz
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libvorbis.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/alsa-lib.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/gstreamer10.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/gtk3.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/x/gtk2.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/pulseaudio.html
  12. http://tdb.samba.org/
  13. http://wiki.linuxfromscratch.org/blfs/wiki/libcanberra
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libass.html
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libcddb.html
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/libdriv.html
  17. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
