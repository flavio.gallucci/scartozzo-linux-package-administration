Beyond Linux^® From Scratch (System V Edition) - Version 9.1

Chapter 45. CD/DVD-Writing Utilities

     * [1]Prev
       Cdrdao-1.2.4
     * [2]Next
       dvd+rw-tools-7.1
     * [3]Up
     * [4]Home

Cdrtools-3.02a09

Introduction to Cdrtools

   The Cdrtools package contains CD recording utilities. These are useful
   for reading, creating or writing (burning) CDs, DVDs, and Blu-ray
   discs.

   This package is known to build and work properly using an LFS-9.1
   platform.

Package Information

     * Download (HTTP):
       [5]https://downloads.sourceforge.net/cdrtools/cdrtools-3.02a09.tar.
       bz2
     * Download MD5 sum: 1e224a6adbbe4ad40047b9fddbb0e60c
     * Download size: 2.1 MB
     * Estimated disk space required: 32 MB
     * Estimated build time: 0.7 SBU

   User Notes: [6]http://wiki.linuxfromscratch.org/blfs/wiki/Cdrtools

Installation of Cdrtools

   [Note]

Note

   This package does not support parallel build.

   Install Cdrtools by running the following commands:
export GMAKE_NOWARN=true &&
make -j1 INS_BASE=/usr DEFINSUSR=root DEFINSGRP=root

   This package does not come with a test suite.

   Now, as the root user:
export GMAKE_NOWARN=true &&
make INS_BASE=/usr DEFINSUSR=root DEFINSGRP=root install &&
install -v -m755 -d /usr/share/doc/cdrtools-3.02a09 &&
install -v -m644 README* ABOUT doc/*.ps \
                    /usr/share/doc/cdrtools-3.02a09

Command Explanations

   export GMAKE_NOWARN=true: This variable avoids a warning when using GNU
   make.

   INS_BASE=/usr: This parameter moves the install directory from
   /opt/schily to /usr.

   DEFINSUSR=root DEFINSGRP=root: These parameters install all programs
   with root:root ownership instead of the default bin:bin.

Contents

   Installed Programs: btcflash, cdda2mp3, cdda2ogg, cdda2wav, cdrecord,
   devdump, isodebug, isodump, isoinfo, isovfy, mkhybrid, mkisofs, readcd,
   rscsi, scgcheck, scgskeleton
   Installed Libraries: libcdrdeflt.a, libdeflt.a, libedc_ecc.a,
   libedc_ecc_dec.a, libfile.a, libfind.a, libhfs.a, libmdigest.a,
   libparanoia.a, librscg.a, libscg.a, libscgcmd.a, libschily.a,
   libsiconv.a
   Installed Directories: /usr/lib/{profiled,siconv},
   /usr/include/{scg,schily},
   /usr/share/doc/cdda2wav,cdrecord,libparanoia,mkisofs,rscsi}

Short Descriptions

   btcflash

   flashes the firmware on BTC DRW1008 DVD+/-RW recorder. Please exercise
   care with this program.

   cdda2wav

   converts Compact Disc audio into WAV sound files.

   cdrecord

   records audio or data Compact Discs.

   devdump

   is a diagnostic program used to dump an ISO-9660 device or file in hex.

   isodebug

   is used to display the command-line parameters used to create an
   ISO-9660 image.

   isodump

   is a diagnostic program used to dump a device or file based on
   ISO-9660.

   isoinfo

   is used to analyze or list an ISO-9660 image.

   isovfy

   is used to verify an ISO-9660 image.

   mkhybrid

   is a symbolic link to mkisofs used to create ISO-9660/HFS hybrid
   filesystem images.

   mkisofs

   is used to create ISO-9660/JOLIET/HFS filesystem images, optionally
   with Rock Ridge attributes.

   readcd

   reads or writes Compact Discs.

   rscsi

   is a remote SCSI manager.

   scgcheck

   is used to check and verify the Application Binary Interface of libscg.

   libscg.a

   is a highly portable SCSI transport library.

   Last updated on 2020-02-18 14:50:03 -0800

     * [7]Prev
       Cdrdao-1.2.4
     * [8]Next
       dvd+rw-tools-7.1
     * [9]Up
     * [10]Home

Riferimenti

   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/cdrdao.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/dvd-rw-tools.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/cdwriteutils.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. https://downloads.sourceforge.net/cdrtools/cdrtools-3.02a09.tar.bz2
   6. http://wiki.linuxfromscratch.org/blfs/wiki/Cdrtools
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/cdrdao.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/dvd-rw-tools.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/cdwriteutils.html
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
