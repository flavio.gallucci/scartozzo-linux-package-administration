   The Xorg font packages provide some scalable fonts and supporting
   packages for Xorg applications. Many people will want to install other
   TTF or OTF fonts in addition to, or instead of, these. Some are listed
   at [5]the section called “TTF and OTF fonts”.
