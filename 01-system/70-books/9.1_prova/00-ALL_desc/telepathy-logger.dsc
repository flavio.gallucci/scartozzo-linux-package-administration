   The Telepathy Logger package is a headless observer client that logs
   information received by the Telepathy framework. It features pluggable
   backends to log different sorts of messages in different formats.
