   Graphite2 is a rendering engine for graphite fonts. These are TrueType
   fonts with additional tables containing smart rendering information and
   were originally developed to support complex non-Roman writing systems.
   They may contain rules for e.g. ligatures, glyph substitution, kerning,
   justification - this can make them useful even on text written in Roman
   writing systems such as English. Note that firefox by default provides
   an internal copy of the graphite engine and cannot use a system version
   (although it can now be patched to use it), but it too should benefit
   from the availability of graphite fonts.
