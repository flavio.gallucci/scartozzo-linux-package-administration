   The volume_key package provides a library for manipulating storage
   volume encryption keys and storing them separately from volumes to
   handle forgotten passphrases.
