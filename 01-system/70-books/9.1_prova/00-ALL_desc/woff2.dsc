   WOFF2 is a library for converting fonts from the TTF format to the WOFF
   2.0 format. It also allows decompression from WOFF 2.0 to TTF. The WOFF
   2.0 format uses the Brotli compression algorithm to compress fonts
   suitable for downloading in CSS @font-face rules.
