   The ssh-askpass is a generic executable name for many packages, with
   similar names, that provide a interactive X service to grab password
   for packages requiring administrative privileges to be run. It prompts
   the user with a window box where the necessary password can be
   inserted. Here, we choose Damien Miller's package distributed in the
   OpenSSH tarball.
