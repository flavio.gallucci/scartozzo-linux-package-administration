   The FriBidi package is an implementation of the [5]Unicode
   Bidirectional Algorithm (BIDI). This is useful for supporting Arabic
   and Hebrew alphabets in other packages.
