   The xine User Interface package contains a multimedia player. It plays
   back CDs, DVDs and VCDs. It also decodes multimedia files like AVI,
   MOV, WMV, MPEG and MP3 from local disk drives, and displays multimedia
   streamed over the Internet.
