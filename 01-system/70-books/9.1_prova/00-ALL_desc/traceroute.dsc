   The Traceroute package contains a program which is used to display the
   network route that packets take to reach a specified host. This is a
   standard network troubleshooting tool. If you find yourself unable to
   connect to another system, traceroute can help pinpoint the problem.
   [Note]
Note
