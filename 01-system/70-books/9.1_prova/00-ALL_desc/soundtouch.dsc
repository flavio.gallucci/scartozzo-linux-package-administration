   The SoundTouch package contains an open-source audio processing library
   that allows changing the sound tempo, pitch and playback rate
   parameters independently from each other.
