   The libnftnl library provides a low-level netlink programming interface
   (API) to the in-kernel nf_tables subsystem.
