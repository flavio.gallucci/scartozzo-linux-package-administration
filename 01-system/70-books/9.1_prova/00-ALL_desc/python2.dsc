   The Python 2 package contains the Python development environment. It is
   useful for object-oriented programming, writing scripts, prototyping
   large programs or developing entire applications. This version is for
   backward compatibility with other dependent packages.
   [Note]
Note
   Python2 has been deprecated by the upstream developers. Support for
   Python2 was discontinued on January 1st, 2020.
   BLFS is attempting to use Python3 as much as possible, but some
   packages have not been updated to support Python3.
