   The libao package contains a cross-platform audio library. This is
   useful to output audio on a wide variety of platforms. It currently
   supports WAV files, OSS (Open Sound System), ESD (Enlighten Sound
   Daemon), ALSA (Advanced Linux Sound Architecture), NAS (Network Audio
   system), aRTS (analog Real-Time Synthesizer), and PulseAudio (next
   generation GNOME sound architecture).
