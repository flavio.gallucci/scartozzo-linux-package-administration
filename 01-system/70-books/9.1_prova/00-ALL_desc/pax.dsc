   pax is an archiving utility created by POSIX and defined by the
   POSIX.1-2001 standard. Rather than sort out the incompatible options
   that have crept up between tar and cpio, along with their
   implementations across various versions of UNIX, the IEEE designed a
   new archive utility. The name “pax” is an acronym for portable archive
   exchange. Furthermore, “pax” means “peace” in Latin, so its name
   implies that it shall create peace between the tar and cpio format
   supporters. The command invocation and command structure is somewhat a
   unification of both tar and cpio.
   pax has been required to be present in LSB conformant systems since LSB
   version 3.0.
