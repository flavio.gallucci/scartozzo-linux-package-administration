   Cogl is a modern 3D graphics API with associated utility APIs designed
   to expose the features of 3D graphics hardware using a direct state
   access API design, as opposed to the state-machine style of OpenGL.
