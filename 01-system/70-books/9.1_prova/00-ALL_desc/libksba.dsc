   The Libksba package contains a library used to make X.509 certificates
   as well as making the CMS (Cryptographic Message Syntax) easily
   accessible by other applications. Both specifications are building
   blocks of S/MIME and TLS. The library does not rely on another
   cryptographic library but provides hooks for easy integration with
   Libgcrypt.
