   Grantlee is a set of free software libraries written using the Qt
   framework. Currently two libraries are shipped with Grantlee: Grantlee
   Templates and Grantlee TextDocument. The goal of Grantlee Templates is
   to make it easier for application developers to separate the structure
   of documents from the data they contain, opening the door for theming.
