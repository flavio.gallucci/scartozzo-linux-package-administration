   The libpwquality package provides common functions for password quality
   checking and also scoring them based on their apparent randomness. The
   library also provides a function for generating random passwords with
   good pronounceability.
