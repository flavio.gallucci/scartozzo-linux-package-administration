   Telepathy Mission Control is an account manager and channel dispatcher
   for the Telepathy framework, allowing user interfaces and other clients
   to share connections to real-time communication services without
   conflicting.
