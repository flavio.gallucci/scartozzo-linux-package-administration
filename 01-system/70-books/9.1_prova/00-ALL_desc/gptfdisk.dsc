   The gptfdisk package is a set of programs for creation and maintenance
   of GUID Partition Table (GPT) disk drives. A GPT partitioned disk is
   required for drives greater than 2 TB and is a modern replacement for
   legacy PC-BIOS partitioned disk drives that use a Master Boot Record
   (MBR). The main program, gdisk, has an inteface similar to the classic
   fdisk program.
