   The time utility is a program that measures many of the CPU resources,
   such as time and memory, that other programs use. The GNU version can
   format the output in arbitrary ways by using a printf-style format
   string to include various resource measurements.
   Although the shell has a builtin command providing similar
   functionalities, this utility is required by the LSB.
