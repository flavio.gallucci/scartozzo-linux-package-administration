   The AccountsService package provides a set of D-Bus interfaces for
   querying and manipulating user account information and an
   implementation of those interfaces based on the usermod(8), useradd(8)
   and userdel(8) commands.
