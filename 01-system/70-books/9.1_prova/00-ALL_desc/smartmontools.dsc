   The smartmontools package contains utility programs (smartctl, smartd)
   to control/monitor storage systems using the Self-Monitoring, Analysis
   and Reporting Technology System (S.M.A.R.T.) built into most modern ATA
   and SCSI disks.
