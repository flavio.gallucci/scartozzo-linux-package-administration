   Colord is a system service that makes it easy to manage, install, and
   generate color profiles. It is used mainly by GNOME Color Manager for
   system integration and use when no users are logged in.
