   Algorithm::Diff computes 'intelligent' differences between two files or
   lists.
   Alien::Build provides tools for building external (non-CPAN)
   dependencies for CPAN.
   Alien::Libxml2 is designed to allow modules to install the C libxml2
   library on your system. In BLFS, it uses pkg-config to find how to link
   to the installed [108]libxml2-2.9.10.
   B::Hooks::EndOfScope allows you to execute code when perl finished
   compiling the surrounding scope.
   Business-ISBN-Data is a data pack for Business::ISBN.
   The Capture::Tiny module captures STDOUT and STDERR from Perl, XS
   (eXternal Subroutine, i.e. written in C or C++) or external programs.
   Class::Data::Inheritable is for creating accessor/mutators to class
   data. That is, if you want to store something about your class as a
   whole (instead of about a single object).
   Class::Inspector allows you to get information about a loaded class.
   A Singleton describes an object class that can have only one instance
   in any system, such as a print spooler. This module implements a
   Singelton class from which other classes can be derived.
   Class:Tiny offers a minimalist class construction kit.
   Clone recursively copies perl datatypes.
   The Config::AutoConf module implements some of the AutoConf macros
   (detecting a command, detecting a library, etc.) in pure perl.
   CPAN::Meta::Check verifies if requirements described in a CPAN::Meta
   object are present.
   DateTime is a date and time object for perl.
   DateTime::Format::Strptime implements most of strptime(3), i.e. it
   takes a string and a pattern and returns a DateTime object.
   DateTime::Locale provides localization support for [138]DateTime-1.51.
   This class is the base class for all time zone objects. A time zone is
   represented internally as a set of observances, each of which describes
   the offset from GMT for a given time period.
   Devel::StackTrace provides an object representing a stacvk trace.
   Dist::CheckConflicts declares version conflicts for a distribution, to
   support post-install updates of dependant distributions.
   Encode::Locale determines the locale encoding.
   Eval::Closure safely and cleanly creates closures via string eval.
   Exception::Class allows you to declare real exception classes in Perl.
   Exporter::Tiny is an exporter with the features of Sub::Exporter but
   only core dependencies.
   ExtUtils::LibBuilder is a tool to build C libraries.
   FFI::CheckLib checks whether a particular dynamic library is available
   for FFI (Foreign Function Interface) to use.
   File::chdir provides a more sensible way to change directories.
   Perl's chdir() has the unfortunate problem of being very, very, very
   global. If any part of your program calls chdir() or if any library you
   use calls chdir(), it changes the current working directory for the
   *whole* program. File::chdir gives you an alternative, $CWD and @CWD.
   This module copies and moves directories recursively (or single files),
   to an optional depth and attempts to preserve each file or directory's
   mode.
   File::Find::Rule is a friendlier interface to File::Find. It allows you
   to build rules which specify the desired files and directories.
   File::Listing parses a directory listing.
   File::ShareDir allows you to access data files which have been
   installed by File::ShareDir::Install.
   File::ShareDir::Install allows you to install read-only data files from
   a distribution.
   HTML::Tagset provides several data tables useful in parsing HTML.
   HTTP::Cookies provides a class for objects that represent a "cookie
   jar" -- that is, a database of all the HTTP cookies that a given
   LWP::UserAgent (from [190]libwww-perl-6.43) object knows about.
   HTTP::Date provides functions to deal with the date formats used by the
   HTTP protocol and also with some other date formats.
   HTTP::Message provides a base class for HTTP style message objects.
   HTTP::Negotiate provides a complete implementation of the HTTP content
   negotiation algorithm.
   Importer provides an alternative but compatible interface to modules
   that export symbols.
   IO::HTML opens an HTML file with automatic character set detection.
   IPC::System::Simple takes the hard work out of calling external
   commands and producing detailed diagnostics.
   The libwww-perl collection is a set of Perl modules which provides a
   simple and consistent application programming interface (API) to the
   World-Wide Web. The main focus of the library is to provide classes and
   functions that allow you to write WWW clients. The library also
   contains modules that are of more general use and even classes that
   help you implement simple HTTP servers.
   List::MoreUtils::XS is a compiled backend for List::MoreUtils
   List::SomeUtils provides the stuff misisng in List::Util.
   List::SomeUtils::XS is a (faster) XS (eXternal Subroutine)
   implementation for List::SomeUtils.
   List::UtilsBy provides a number of higher-order list utility functions.
   LWP::MediaTypes guesses the media type (i.e. the MIME Type) for a file
   or URL.
   MIME::Charset provides information about character sets used for MIME
   messages o nthe internet, such as their encodings.
   Module::Implementation loads one of several alternate underlying
   implementations of a module (e.g. eXternal Subroutine or pure Perl, or
   an implementation for a given OS).
   Module::Pluggable provides a way of having 'plugins' for your module.
   Module::Runtime provides functions to deal with runtime handling of
   Perl modules.
   The "mro" namespace provides several utilities for dealing with method
   resolution order and method caching in general in Perl 5.9.5 and
   higher. This module provides those interfaces for earlier versions of
   Perl.
   This module is very similar to namespace::clean, except it will clean
   all imported functions, no matter if you imported them before or after
   you used the pragma. It will also not touch anything that looks like a
   method.
   The Net::HTTP class is a low level HTTP client. An instance of the
   class represents a connection to an HTTP server.
   Net::SSLeay is a PERL extension for using OpenSSL.
   Number::Compare compiles a simple comparison to an anonymous
   subroutine, which you can call with a value to be tested against. It
   understands IEC standard magnitudes (k, ki, m, mi, g, gi).
   Manipulating stashes (Perl's symbol tables) is occasionally necessary,
   but incredibly messy, and easy to get wrong. This module hides all of
   that behind a simple API.
   Params::Validate allows you to validate method or function call
   parameters to an arbitrary level of specificity.
   Params::ValidationCompiler builds an optimized subroutine parameter
   validator.
   Path::Tiny provides a small fast utility for working with file paths.
   Role::Tiny is a minimalist role composition tool.
   Scope::Guard provides a convenient way to perform cleanup or other
   forms of resource management at the end of a scope.
   Specio provides classes for representing type constraints and coercion,
   along with syntax sugar for declaring them.
   Sub::Exporter::Progressive is a wrapper for Sub::Exporter.
   Sub::Identify allows you to retrieve the real name of code references.
   It is encouraged to migrate to Sub::Util (a core module) when possible.
   Sub::Info is a tool for inspecting subroutines.
   Sub::Quote provides ways to generate subroutines from strings.
   Sub::Uplevel allows you to fool a caller that it is running in a higher
   stack frame.
   SUPER provides easier methods to dispatch control to the superclass
   (when subclassing a class).
   Term::Table formats a header and rows into a table. This is used by
   some failing tests to provide diagnostics about what has goen wrong.
   Test::Deep gives you very flexible ways to check that the result you
   got is the result you were expecting.
   Test::Exception provides convenience methods for testing exception
   based code.
   The Test::Fatal module provides simple helpers for testing code which
   throws exceptions.
   Test::File provides a collection of test utilities for file attributes.
   Test::File::ShareDir is some low level plumbing to enable a
   distribution to perform tests while consuming its own share directories
   in a manner similar to how they will be once installed. This allows
   [301]File-ShareDir-1.116 to see the latest version of content instead
   of whatever is installed on the target system where you are testing.
   Test::LeakTrace traces memory leaks.
   Test::MockModule lets you temporarily redefine subroutines in other
   packages for the purposes of unit testing.
   Test::Needs skips tests if a requested module is not present.
   The Test::Requires module checks if another (optional) module can be
   loaded, and if not it skips all the current tests.
   Test::RequiresInternet is intended to easily test network connectivity
   before functional tests begin to non-local Internet resources.
   Test::utf8 is a collection of tests useful for dealing with utf8
   strings in Perl.
   Test::Warnings tests for warnings and the lack of them.
   This module allows you to deliberately hide modules from a program even
   though they are installed. This is mostly useful for testing modules
   that have a fallback when a certain dependency module is not installed.
   Test2::Plugin::NoWarnings causes tests to fail if there are any
   warnings while they run.
   Test2::Suite is a distribution with a rich set of tools built upon the
   Test2 framework.
   Text::CSV_XS provides facilities for the composition and decomposition
   of comma-separated values.
   Text::Diff performs diffs on files and record sets.
   Text::Glob implements glob(3) style matching that can be used to match
   against text, rather than fetching names from a filesystem.
   You use Tie::Cycle to go through a list over and over again. Once you
   get to the end of the list, you go back to the beginning. You don't
   have to worry about any of this since the magic of tie does that for
   you.
   TimeDate provides miscellaneous timezone manipulation routines.
   Try::Tiny provides try and catch to expect and handle exceptional
   conditions, avoiding quirks in Perl and common mistakes.
   Magic is Perl's way of enhancing variables. With this module, you can
   add your own magic to any variable.
   WWW::RobotRules parses robots.txt files, creating a WWW::RobotRules
   object with methods to check if access to a given URL is prohibited.
   XML::LibXML is a perl binding for [339]libxml2-2.9.10.
   XML::NamespaceSupport offers a simple way to process namespaced XML
   names from within any application that may need them.
   XML::SAX is a SAX parser access API for Perl. It includes classes and
   APIs required for implementing SAX drivers, along with a factory class
   for returning any SAX parser installed on the user's system.
   This module has a very simple task - to be a base class for PerlSAX
   drivers and filters.
   XML::SAX::Expat is an implementation of a SAX2 driver sitting on top of
   Expat (XML::Parser).
