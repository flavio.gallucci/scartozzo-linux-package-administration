   The GNU Scientific Library (GSL) is a numerical library for C and C++
   programmers. It provides a wide range of mathematical routines such as
   random number generators, special functions and least-squares fitting.
