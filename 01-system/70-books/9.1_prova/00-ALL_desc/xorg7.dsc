   Xorg is a freely redistributable, open-source implementation of the X
   Window System. This system provides a client/server interface between
   display hardware (the mouse, keyboard, and video displays) and the
   desktop environment, while also providing both the windowing
   infrastructure and a standardized application interface (API).
   User Notes: [5]http://wiki.linuxfromscratch.org/blfs/wiki/Xorg7
Xorg Download and Installation Instructions
   Xorg-7.0 introduced a completely auto-tooled, modular build system.
   With the new modular build system, it is no longer possible to download
   the entire package in a single file. In fact, there will be well over
   100 packages that need to be fetched from the download location. To
   assist with such a large task, installing [6]Wget-1.20.3 is strongly
   recommended for downloading the needed files. A complete wget file list
   is provided for each page that includes multiple packages.
   Given the number of packages available, deciding which packages you
   need to install for your particular setup may seem a bit overwhelming
   at first. Take a look at [7]this page and [8]this thread to get an idea
   of what you will need. If you are unsure, you should install all
   packages at the cost of extra disk space.
   [Note]
Note
   Even if you intend to download only the necessary packages, you should
   download the wget file lists. The list of files are ordered by
   dependency, and the package versions listed in the files are known to
   work well with each other. Further, the wget file lists contain
   comments for specific packages that are deprecated or are not
   recommended to install. Newer packages are likely intended for the next
   release of Xorg and have already proved to be incompatible with current
   versions of software installed in BLFS. The installed size of Xorg can
   be reduced considerably by installing only the packages that you will
   need and use, however, the BLFS book cannot account for all
   dependencies and build options for the individual Xorg packages. The
   instructions assume that all packages have been built. A [9]wiki page
   containing dependency information is under development. You are
   encouraged to add to these pages if you discover additional information
   that may be helpful to other users who selectively install individual
   packages.
   Additionally, because of the large number of repetitive commands, you
   are encouraged to partially automate the build. Instructions have been
   given that utilize the [10]Sudo-1.8.31 package. It is recommended that
   you use the :NOPASSWD configuration option for the user that will be
   building the xorg packages.
Setting up the Xorg Build Environment
   [Note]
Note
   The following instructions assume that the shell startup files have
   been set up as described in [11]The Bash Shell Startup Files.
   First, you'll need to create a working directory:
mkdir xc &&
cd xc
   As with previous releases of the X Window System, it may be desirable
   to install Xorg into an alternate prefix. This is no longer common
   practice among Linux distributions. The common installation prefix for
   Xorg on Linux is /usr. There is no standard alternate prefix, nor is
   there any exception in the current revision of the Filesystem Hierarchy
   Standard for Release 7 of the X Window System. Alan Coopersmith of Sun
   Microsystems, once stated "At Sun, we were using /usr/X11 and plan to
   stick with it." Only the /opt/* prefix or the /usr prefix adhere to the
   current FHS guidelines.
   Choose your installation prefix, and set the XORG_PREFIX variable with
   the following command:
export XORG_PREFIX="<PREFIX>"
   Throughout these instructions, you will use the following configure
   switches for all of the packages. Create the XORG_CONFIG variable to
   use for this parameter substitution:
export XORG_CONFIG="--prefix=$XORG_PREFIX --sysconfdir=/etc \
    --localstatedir=/var --disable-static"
   Create an /etc/profile.d/xorg.sh configuration file containing these
   variables as the root user:
cat > /etc/profile.d/xorg.sh << EOF
XORG_PREFIX="$XORG_PREFIX"
XORG_CONFIG="--prefix=\$XORG_PREFIX --sysconfdir=/etc --localstatedir=/var --dis
able-static"
export XORG_PREFIX XORG_CONFIG
EOF
chmod 644 /etc/profile.d/xorg.sh
   [Note]
Note
   There is some confustion about the above 'here' document. The backslash
   in front of the dollar sign is correct. Bash will remove it when
   creating /etc/profile.d/xorg.sh. However, if you are creating the file
   with an editor, a copy and paste operation will not remove the
   baskslash. It must then be removed manually.
   If you've installed [12]Sudo-1.8.31, ensure that XORG_PREFIX and
   XORG_CONFIG are available in the sudo environment. As the root user,
   run the following command:
cat > /etc/sudoers.d/xorg << EOF
Defaults env_keep += XORG_PREFIX
Defaults env_keep += XORG_CONFIG
EOF
   [Note]
Note
   If you've decided to use the standard /usr prefix, you can omit the
   remainder of this page and continue at [13]util-macros-1.19.2.
   If you've decided to not use the standard prefix, be sure to add
   $XORG_PREFIX/bin to your PATH environment variable, and
   $XORG_PREFIX/lib/pkgconfig and $XORG_PREFIX/share/pkgconfig to your
   PKG_CONFIG_PATH variable. It is also helpful to specify additional
   search paths for gcc and an include directory for the aclocal program.
   Issue the following commands as the root user:
cat >> /etc/profile.d/xorg.sh << "EOF"
pathappend $XORG_PREFIX/bin             PATH
pathappend $XORG_PREFIX/lib/pkgconfig   PKG_CONFIG_PATH
pathappend $XORG_PREFIX/share/pkgconfig PKG_CONFIG_PATH
pathappend $XORG_PREFIX/lib             LIBRARY_PATH
pathappend $XORG_PREFIX/include         C_INCLUDE_PATH
pathappend $XORG_PREFIX/include         CPLUS_INCLUDE_PATH
ACLOCAL="aclocal -I $XORG_PREFIX/share/aclocal"
export PATH PKG_CONFIG_PATH ACLOCAL LIBRARY_PATH C_INCLUDE_PATH CPLUS_INCLUDE_PA
TH
EOF
   The script above needs to be activated. Normally it will be automatic
   at login, but to activate it now, as a regular user, run:
source /etc/profile.d/xorg.sh
   You should also add $XORG_PREFIX/lib to the /etc/ld.so.conf file.
   Again, as the root user, issue the following command:
echo "$XORG_PREFIX/lib" >> /etc/ld.so.conf
   You should also modify /etc/man_db.conf, adding appropriate
   MANDATORY_MANPATH, MANPATH_MAP, and MANDB_MAP entries following the
   examples for /usr/X11R6. Issue the following command as the root user:
sed "s@/usr/X11R6@$XORG_PREFIX@g" -i /etc/man_db.conf
   Some applications look for shared files in /usr/share/X11. Create a
   symbolic link to the proper location as the root user:
ln -svf $XORG_PREFIX/share/X11 /usr/share/X11
   If building KDE, some cmake files look for Xorg in places other than
   $XORG_PREFIX. Allow cmake to find Xorg with:
ln -svf $XORG_PREFIX /usr/X11R6
   Last updated on 2020-01-31 22:10:49 -0800
     * [14]Prev
       X Window System Environment
     * [15]Next
       util-macros-1.19.2
     * [16]Up
     * [17]Home
Riferimenti
   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/installing.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/util-macros.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/installing.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. http://wiki.linuxfromscratch.org/blfs/wiki/Xorg7
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/basicnet/wget.html
   7. http://wiki.x.org/wiki/ModuleDescriptions
   8. http://lists.x.org/archives/xorg-modular/2005-November/000801.html
   9. http://wiki.linuxfromscratch.org/blfs/wiki/Xorg7
  10. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/sudo.html
  11. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/profile.html
  12. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/postlfs/sudo.html
  13. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/util-macros.html
  14. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/installing.html
  15. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/util-macros.html
  16. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/installing.html
  17. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
