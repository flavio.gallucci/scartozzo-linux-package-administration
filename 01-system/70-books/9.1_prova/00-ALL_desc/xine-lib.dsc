   The Xine Libraries package contains xine libraries. These are useful
   for interfacing with external plug-ins that allow the flow of
   information from the source to the audio and video hardware.
