   The Perl module packages (also referred to as Distributions, because
   each can contain multiple modules) add useful objects to the Perl
   language. The packages listed on this page are required or recommended
   for other packages in the book. If they have dependent modules, those
   are either on this page or else on the next page ([5]Perl Module
   Dependencies).
   In many cases, only the required or recommended dependencies are listed
   - there might be other modules which allow more tests to be run, but
   omitting them will still allow the tests to PASS.
   For a few modules, the BLFS editors have determined that other modules
   still listed as prerequisites are not required, and omitted them.
   Where an alphabetically-earlier dependency of the same module pulls in
   a dependency, it is not mentioned for the later dependencies of the
   same module. You should build the listed dependencies in order.
   It is generally worth running the tests for perl modules, they often
   can show problems such as missing dependencies which are required to
   use the module. Here, the editors have attempted to separate those
   dependencies which are only required for running testsuites, but they
   will not be mentioned for a module where one of its dependencies uses
   that module for its own testsuite. That is to say, if you intend to run
   the testsuites, please run them for each dependency of the module.
   It is possible to automatically install the current versions of a
   module and all missing or too-old dependencies recommended by upstream
   using CPAN. See [6]CPAN automated install of perl modules at the end of
   this page.
   Most of these modules only install files below
   /usr/lib/perl5/site_perl/5.xx.y and those will not be documented. One
   or two install programs (mostly, perl scripts), or a library, into
   /usr/bin/ or /usr/lib/ and these are documented.
   User Notes: [7]http://wiki.linuxfromscratch.org/blfs/wiki/perl-modules
     * [8]Archive-Zip-1.67
     * [9]autovivification-0.18
     * [10]Business-ISBN-3.005
     * [11]Business-ISMN-1.201
     * [12]Business-ISSN-1.003
     * [13]Class-Accessor-0.51
     * [14]Data-Compare-1.27
     * [15]Data-Dump-1.23
     * [16]Data-Uniqid-0.12
     * [17]DateTime-Calendar-Julian-0.102
     * [18]DateTime-Format-Builder-0.82
     * [19]Encode-EUCJPASCII-0.03
     * [20]Encode-HanExtra-0.23
     * [21]Encode-JIS2K-0.03
     * [22]File-Slurper-0.012
     * [23]File-Which-1.23
     * [24]HTML-Parser-3.72
     * [25]HTTP-Daemon-6.06
     * [26]IO-Socket-SSL-2.067
     * [27]IO-String-1.08
     * [28]IPC-Run3-0.048
     * [29]Lingua-Translit-0.28
     * [30]List-AllUtils-0.15
     * [31]List-MoreUtils-0.428
     * [32]Log-Log4perl-1.49
     * [33]LWP-Protocol-https-6.07
     * [34]Module-Build-0.4231
     * [35]Net-DNS-1.22
     * [36]Parse-RecDescent-1.967015
     * [37]Parse-Yapp-1.21
     * [38]PerlIO-utf8_strict-0.007
     * [39]Regexp-Common-2017060201
     * [40]SGMLSpm-1.1
     * [41]Sort-Key-1.33
     * [42]Test-Command-0.11
     * [43]Test-Differences-0.67
     * [44]Text-BibTeX-0.88
     * [45]Text-CSV-2.00
     * [46]Text-Roman-3.5
     * [47]Unicode-Collate-1.27
     * [48]Unicode-LineBreak-2019.001
     * [49]URI-1.76
     * [50]XML-LibXML-Simple-1.01
     * [51]XML-LibXSLT-1.99
     * [52]XML-Simple-2.25
     * [53]XML-Writer-0.625
     * [54]CPAN automated install of perl modules
Archive::Zip-1.67
   The Archive::Zip module allows a Perl program to create, manipulate,
   read, and write Zip archive files.
   This module allows you disable autovivification (the automatic creation
   and population of new arrays and hashes whenever undefined variables
   are dereferenced), and optionally throw a warning or an error when it
   would have occurred.
   The Business::ISBN module is for work with International Standard Book
   Numbers.
   The Business::ISMN module is for work with International Standard Music
   Numbers.
   The Business::ISSN module is for work with International Standard
   Serial Numbers.
   Class::Accessor generates accessors/mutators for your class.
   The Data::Compare module compares two perl data structures.
   Data::Dump provides pretty printing of data structures.
   Data::Uniqid provides three simple routines for generating unique IDs.
   DateTime::Calendar::Julian implements the Julian Calendar.
   DateTime::Format::Builder created DateTime parser classes and objects.
   Encode::EUCJPASCII provides an eucJP-open (Extended Unix Code,
   Japanese) mapping.
   The Encode::HanExtra module provides extra sets of Chinese Encodings
   which are not included in the core Encode module because of size
   issues.
   The Encode::JIS2K module provides JIS X 0212 (aka JIS 2000) Encodings.
   File::Slurper is a simple, sane and efficient module to slurp a file.
   File::Which provides a portable implementation of the 'which' utility.
   The HTML::Parser distribution is a collection of modules that parse and
   extract information from HTML documents.
   Instances of the HTTP::Daemon class are HTTP/1.1 servers that listen on
   a socket for incoming requests. The HTTP::Daemon is a subclass of
   IO::Socket::INET, so you can perform socket operations directly on it
   too.
   IO::Socket::SSL makes using SSL/TLS much easier by wrapping the
   necessary functionality into the familiar IO::Socket interface and
   providing secure defaults whenever possible.
   IO::String - Emulate file interface for in-core strings.
   IPC::Run3 is used to run a subprocess with input/ouput redirection.
   Lingua::Translit and its translit program transliterate text between
   writing systems.
   The List::Allutils module combines List::Util and List::MoreUtils in
   one bite-sized package.
   List::MoreUtils provides the stuff missing in List::Util.
   Log::Log4perl provides a Log4j implementation for perl.
   LWP::Protocol::https provides https support for LWP::UserAgent (i.e.
   [100]libwww-perl-6.43). Once the module is installed LWP is able to
   access sites using HTTP over SSL/TLS.
Additional Download
     * Required patch:
       [101]http://www.linuxfromscratch.org/patches/blfs/9.1/LWP-Protocol-
       https-6.07-system_certs-1.patch
   Module::Build allows perl modules to be built without a make command
   being present.
   Net::DNS is a DNS resolver implemented in Perl. It can be used to
   perform nearly any type of DNS query from a Perl script.
   Parse::RecDescent incrementally generates top-down recursive-descent
   text parsers from simple yacc-like grammar specifications.
   Parse::Yapp is a Perl extension for generating and using LALR parsers.
   PerlIO::utf8_strict provides a fast and correct UTF-8 PerlIO layer.
   Unlike perl's default :utf8 layer it checks the input for correctness.
   Regexp::Common provides commonly requested regular expressions.
   The SGMLSpm module is a Perl library used for parsing the output from
   James Clark's SGMLS and NSGMLS parsers.
   Sort::Key provides a set of functions to sort lists of values by some
   calculated key value.
   Test::Command tests the exit status, STDOUT, or STDERR, of an external
   command.
   Test::Differences tests strings and data structures and shows the
   differences if they do not match.
   Text::BibTeX provides an interface to read and parse BibTeX files.
   Text::CSV is a comma-separated values manipulator, using XS (eXternal
   Subroutine - for subroutines written in C or C++) or pure perl.
   Text::Roman allows conversion between Roman and Arabic algorisms
   (number systems, e.g. MCMXLV and 1945).
   Unicode::Collate provides a Unicode collation algorithm.
   [Note]
Note
   This is a core module. If you are using perl-5.28.0 or later, its
   version is good enough for [128]biber-2.14 and you do not need to
   reinstall this module.
   Unicode::LineBreak provides a UAX #14 Unicode Line Breaking Algorithm.
   This module implements the URI class. Objects of this class represent
   "Uniform Resource Identifier references" as specified in RFC 2396 (and
   updated by RFC 2732). A Uniform Resource Identifier is a compact string
   of characters that identifies an abstract or physical resource. A
   Uniform Resource Identifier can be further classified as either a
   Uniform Resource Locator (URL) or a Uniform Resource Name (URN). The
   distinction between URL and URN does not matter to the URI class
   interface. A "URI-reference" is a URI that may have additional
   information attached in the form of a fragment identifier.
   The XML::LibXML::Simple module is a rewrite of XML::Simple to use the
   XML::LibXML parser for XML structures,instead of the plain Perl or SAX
   parsers.
   XML-LibXSLT provides an interface to [138]libxslt-1.1.34
   XML::Simple provides an easy API to read and write XML (especially
   config files). It is deprecated and its use is discouraged.
   XML::Writer provides a Perl extension for writing XML documents.
