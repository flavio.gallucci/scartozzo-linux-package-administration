   The Doxygen package contains a documentation system for C++, C, Java,
   Objective-C, Corba IDL and to some extent PHP, C# and D. It is useful
   for generating HTML documentation and/or an off-line reference manual
   from a set of documented source files. There is also support for
   generating output in RTF, PostScript, hyperlinked PDF, compressed HTML,
   and Unix man pages. The documentation is extracted directly from the
   sources, which makes it much easier to keep the documentation
   consistent with the source code.
   You can also configure Doxygen to extract the code structure from
   undocumented source files. This is very useful to quickly find your way
   in large source distributions. Used along with Graphviz, you can also
   visualize the relations between the various elements by means of
   include dependency graphs, inheritance diagrams, and collaboration
   diagrams, which are all generated automatically.
