   The Vim package, which is an abbreviation for VI IMproved, contains a
   vi clone with extra features as compared to the original vi.
   The default LFS instructions install vim as a part of the base system.
   If you would prefer to link vim against X, you should recompile vim to
   enable GUI mode. There is no need for special instructions since X
   support is automatically detected.
   [Note]
Note
   The version of vim changes daily. The get the latest version, go to
   [5]https://github.com/vim/vim/releases.
