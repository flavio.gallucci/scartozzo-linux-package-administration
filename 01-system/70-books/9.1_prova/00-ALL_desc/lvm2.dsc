   The LVM2 package is a set of tools that manage logical partitions. It
   allows spanning of file systems across multiple physical disks and disk
   partitions and provides for dynamic growing or shrinking of logical
   partitions, mirroring and low storage footprint snapshots.
