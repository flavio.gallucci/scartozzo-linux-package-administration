   QtWebEngine integrates chromium's web capabilities into Qt. It ships
   with its own copy of ninja which it uses for the build if it cannot
   find a system copy, and various copies of libraries from ffmpeg, icu,
   libvpx, and zlib (including libminizip) which have been forked by the
   chromium developers.
