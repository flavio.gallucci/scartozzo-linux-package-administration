   Yasm is a complete rewrite of the [5]NASM-2.14.02 assembler. It
   supports the x86 and AMD64 instruction sets, accepts NASM and GAS
   assembler syntaxes and outputs binary, ELF32 and ELF64 object formats.
