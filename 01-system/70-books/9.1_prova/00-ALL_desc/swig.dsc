   SWIG (Simplified Wrapper and Interface Generator) is a compiler that
   integrates C and C++ with languages including Perl, Python, Tcl, Ruby,
   PHP, Java, C#, D, Go, Lua, Octave, R, Scheme, and Ocaml. SWIG can also
   export its parse tree into Lisp s-expressions and XML.
   SWIG reads annotated C/C++ header files and creates wrapper code (glue
   code) in order to make the corresponding C/C++ libraries available to
   the listed languages, or to extend C/C++ programs with a scripting
   language.
