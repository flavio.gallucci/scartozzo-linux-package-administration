   The Xorg Drivers page contains the instructions for building Xorg
   drivers that are necessary in order for Xorg Server to take advantage
   of the hardware that it is running on. At least one input and one video
   driver are required for Xorg Server to start.
   On machines using KMS, the modesetting driver is provided by
   xorg-server and can be used instead of the video driver for the
   specific hardware, but with reduced performance. It can also be used
   (without hardware acceleration) in virtual machines running under qemu.
   [Note]
Note
   If you are unsure which video hardware you have, you can use lspci from
   [5]pciutils-3.6.4 to find out which video hardware you have and then
   look at the descriptions of the packages in order to find out which
   driver you need.
   [Note]
Note
   In addition to the drivers listed below, there are several other
   drivers for very old hardware that may still be relevant. The latest
   versions of these drivers may be downloaded from
   [6]https://www.x.org/archive/individual/driver. Instructions for
   building these now intermittently maintained drivers may be found in a
   prior version of BLFS:
   [7]http://www.linuxfromscratch.org/blfs/view/7.6/x/x7driver.html
Xorg Input Drivers
     * [8]libevdev-1.8.0
     * [9]Xorg Evdev Driver-2.10.6
     * [10]libinput-1.15.1
     * [11]Xorg Libinput-0.29.0
     * [12]Xorg Synaptics Driver-1.9.1
     * [13]Xorg Wacom Driver-0.39.0
Xorg Video Drivers
     * [14]Xorg AMDGPU Driver-19.1.0
     * [15]Xorg ATI Driver-19.1.0
     * [16]Xorg Fbdev Driver-0.5.0
     * [17]Xorg Intel Driver-20200218
     * [18]Xorg Nouveau Driver-1.0.16
     * [19]Xorg VMware Driver-13.3.0
Hardware Video Acceleration
     * [20]libva-2.6.1
     * [21]libvdpau-1.3
     * [22]libvdpau-va-gl-0.4.0
libevdev 1.8.0
   The libevdev package contains common functions for Xorg input drivers.
   The Xorg Evdev Driver package contains a Generic Linux input driver for
   the Xorg X server. It handles keyboard, mouse, touchpads and wacom
   devices, though for touchpad and wacom advanced handling, additional
   drivers are required.
   libinput is a library that handles input devices for display servers
   and other applications that need to directly deal with input devices.
   The X.Org Libinput Driver is a thin wrapper around libinput and allows
   for libinput to be used for input devices in X. This driver can be used
   as as drop-in replacement for evdev and synaptics.
   The Xorg Synaptics Driver package contains the X.Org Input Driver,
   support programs and SDK for Synaptics touchpads. Even though the evdev
   driver can handle touchpads very well, this driver is required if you
   want to use advanced features like multi tapping, scrolling with
   touchpad, turning the touchpad off while typing, etc.
   The Xorg Wacom Driver package contains the X.Org X11 driver and SDK for
   Wacom and Wacom-like tablets. It is not required to use a Wacom tablet,
   the xf86-input-evdev driver can handle these devices without problems.
   The Xorg AMDGPU Driver package contains the X.Org Video Driver for
   newer AMD Radeon video cards starting from Volcanic Islands. It can
   also be used for Southern and Sea Islands if the experimental support
   was enabled in the kernel.
   The Xorg ATI Driver package contains the X.Org Video Driver for ATI
   Radeon video cards including all chipsets ranging from R100 to the
   "Volcanic Islands" chipsets.
   The Xorg Fbdev Driver package contains the X.Org Video Driver for
   framebuffer devices. This driver is often used as fallback driver if
   the hardware specific and VESA drivers fail to load or are not present.
   If this driver is not installed, Xorg Server will print a warning on
   startup, but it can be safely ignored if hardware specific driver works
   well.
   The Xorg Intel Driver package contains the X.Org Video Driver for Intel
   integrated video chips including 8xx, 9xx, Gxx, Qxx, HD, Iris, and Iris
   Pro graphics processors.
   The Xorg Nouveau Driver package contains the X.Org Video Driver for
   NVidia Cards including RIVA TNT, RIVA TNT2, GeForce 256, QUADRO,
   GeForce2, QUADRO2, GeForce3, QUADRO DDC, nForce, nForce2, GeForce4,
   QUADRO4, GeForce FX, QUADRO FX, GeForce 6XXX and GeForce 7xxx chipsets.
   The Xorg VMware Driver package contains the X.Org Video Driver for
   VMware SVGA virtual video cards.
   The libva package contains a library which provides access to hardware
   accelerated video processing, using hardware to accelerate video
   processing in order to offload the central processing unit (CPU) to
   decode and encode compressed digital video. The VA API video
   decode/encode interface is platform and window system independent
   targeted at Direct Rendering Infrastructure (DRI) in the X Window
   System however it can potentially also be used with direct framebuffer
   and graphics sub-systems for video output. Accelerated processing
   includes support for video decoding, video encoding, subpicture
   blending, and rendering.
   The libvdpau package contains a library which implements the VDPAU
   library.
   VDPAU (Video Decode and Presentation API for Unix) is an open source
   library (libvdpau) and API originally designed by Nvidia for its
   GeForce 8 series and later GPU hardware targeted at the X Window System
   This VDPAU API allows video programs to offload portions of the video
   decoding process and video post-processing to the GPU video-hardware.
   Currently, the portions capable of being offloaded by VDPAU onto the
   GPU are motion compensation (mo comp), inverse discrete cosine
   transform (iDCT), VLD (variable-length decoding) and deblocking for
   MPEG-1, MPEG-2, MPEG-4 ASP (MPEG-4 Part 2), H.264/MPEG-4 AVC and VC-1,
   WMV3/WMV9 encoded videos. Which specific codecs of these that can be
   offloaded to the GPU depends on the version of the GPU hardware;
   specifically, to also decode MPEG-4 ASP (MPEG-4 Part 2), Xvid/OpenDivX
   (DivX 4), and DivX 5 formats, a GeForce 200M (2xxM) Series (the
   eleventh generation of Nvidia's GeForce graphics processing units) or
   newer GPU hardware is required.
   The libvdpau-va-gl package contains a library which implements the
   VDPAU library. Libvdpau_va_gl uses OpenGL under the hood to accelerate
   drawing and scaling and the VA-API (if available) to accelerate video
   decoding. For now VA-API is available on some Intel chips, and on some
   AMD video adapters with the help of the libvdpau driver.
