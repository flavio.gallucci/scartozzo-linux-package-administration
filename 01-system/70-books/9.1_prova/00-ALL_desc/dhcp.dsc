   The ISC DHCP package contains both the client and server programs for
   DHCP. dhclient (the client) is used for connecting to a network which
   uses DHCP to assign network addresses. dhcpd (the server) is used for
   assigning network addresses on private networks.
