   The FontForge package contains an outline font editor that lets you
   create your own postscript, truetype, opentype, cid-keyed,
   multi-master, cff, svg and bitmap (bdf, FON, NFNT) fonts, or edit
   existing ones.
