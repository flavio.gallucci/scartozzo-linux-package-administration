   MC (Midnight Commander) is a text-mode full-screen file manager and
   visual shell. It provides a clear, user-friendly, and somewhat
   protected interface to a Unix system while making many frequent file
   operations more efficient and preserving the full power of the command
   prompt.
