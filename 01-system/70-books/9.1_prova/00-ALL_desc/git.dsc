   Git is a free and open source, distributed version control system
   designed to handle everything from small to very large projects with
   speed and efficiency. Every Git clone is a full-fledged repository with
   complete history and full revision tracking capabilities, not dependent
   on network access or a central server. Branching and merging are fast
   and easy to do. Git is used for version control of files, much like
   tools such as [5]Mercurial-5.3, Bazaar, [6]Subversion-1.13.0, [7]CVS,
   Perforce, and Team Foundation Server.
