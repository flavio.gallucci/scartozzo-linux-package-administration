   The Python 3 package contains the Python development environment. This
   is useful for object-oriented programming, writing scripts, prototyping
   large programs or developing entire applications.
   [Note]
Note
   Python 3 was installed in LFS. The only reason to rebuild it here is if
   optional modules are needed.
