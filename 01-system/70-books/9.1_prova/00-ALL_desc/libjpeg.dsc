   libjpeg-turbo is a fork of the original IJG libjpeg which uses SIMD to
   accelerate baseline JPEG compression and decompression. libjpeg is a
   library that implements JPEG image encoding, decoding and transcoding.
