   Mercurial is a distributed source control management tool similar to
   Git and Bazaar. Mercurial is written in Python and is used by projects
   such as Mozilla and Vim.
