   The Gtkmm package provides a C++ interface to GTK+ 2. It can be
   installed alongside [5]Gtkmm-3.24.2 (the GTK+ 3 version) with no
   namespace conflicts.
