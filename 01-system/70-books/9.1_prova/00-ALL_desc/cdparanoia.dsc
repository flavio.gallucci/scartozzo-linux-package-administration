   The CDParanoia package contains a CD audio extraction tool. This is
   useful for extracting .wav files from audio CDs. A CDDA capable CDROM
   drive is needed. Practically all drives supported by Linux can be used.
