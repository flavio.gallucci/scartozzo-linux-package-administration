   liboauth is a collection of POSIX-C functions implementing the OAuth
   Core RFC 5849 standard. Liboauth provides functions to escape and
   encode parameters according to OAuth specification and offers
   high-level functionality to sign requests or verify OAuth signatures as
   well as perform HTTP requests.
