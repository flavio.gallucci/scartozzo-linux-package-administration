   The Power Management Utilities provide simple shell command line tools
   to suspend and hibernate the computer. They can be used to run user
   supplied scripts on suspend and resume.
