   The LLVM package contains a collection of modular and reusable compiler
   and toolchain technologies. The Low Level Virtual Machine (LLVM) Core
   libraries provide a modern source and target-independent optimizer,
   along with code generation support for many popular CPUs (as well as
   some less common ones!). These libraries are built around a well
   specified code representation known as the LLVM intermediate
   representation ("LLVM IR").
   The optional Clang and Compiler RT packages provide new C, C++,
   Objective C and Objective C++ front-ends and runtime libraries for the
   LLVM and are required by some packages which use Rust, for example
   firefox.
