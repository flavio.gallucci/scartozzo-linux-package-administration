   SeaMonkey is a browser suite, the Open Source sibling of Netscape. It
   includes the browser, composer, mail and news clients, and an IRC
   client. It is the follow-on to the Mozilla browser suite.
   The Mozilla project also hosts two subprojects that aim to satisfy the
   needs of users who don't need the complete browser suite or prefer to
   have separate applications for browsing and e-mail. These subprojects
   are [5]Firefox-68.5.0 and [6]Thunderbird-68.5.0. Both are based on the
   Mozilla source code.
