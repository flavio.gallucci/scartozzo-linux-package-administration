Introduction
   Window Managers and Desktop Environments are the primary user
   interfaces into the X Window System. A window manager is a program that
   controls the appearance of windows and provides the means by which the
   user can interact with them. A Desktop Environment provides a more
   complete interface to the operating system, and provides a range of
   integrated utilities and applications.
   There are many Window Managers available. Some of the more well known
   ones include fvwm2, Window Maker, AfterStep, Enlightenment, Sawfish,
   and Blackbox.
   The Desktop Environments available for Linux are GNOME, KDE, and XFce.
   Choosing a Window Manager or Desktop Environment is highly subjective.
   The choice depends on the look and feel of the packages, the resources
   (RAM, disk space) required, and the utilities included. One web site
   that provides a very good summary of what is available, screenshots,
   and their respective features is [5]Window Managers for X.
   In this chapter, the installation instructions of several Window
   Managers and one lightweight Desktop Environment are presented. Later
   in the book, both KDE and GNOME have their own sections.
   Last updated on 2013-03-08 15:46:06 -0800
     * [6]Prev
       Window Managers
     * [7]Next
       Fluxbox-1.3.7
     * [8]Up
     * [9]Home
Riferimenti
   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/wm.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/fluxbox.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/wm.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. http://xwinman.org/
   6. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/wm.html
   7. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/fluxbox.html
   8. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/wm.html
   9. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
