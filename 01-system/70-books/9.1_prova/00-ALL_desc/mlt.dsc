   MLT package is the Media Lovin Toolkit. It is an open source multimedia
   framework, designed and developed for television broadcasting. It
   provides a toolkit for broadcasters, video editors, media players,
   transcoders, web streamers and many more types of applications.
