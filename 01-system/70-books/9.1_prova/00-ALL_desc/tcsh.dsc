   The Tcsh package contains “an enhanced but completely compatible
   version of the Berkeley Unix C shell (csh)”. This is useful as an
   alternative shell for those who prefer C syntax to that of the bash
   shell, and also because some programs require the C shell in order to
   perform installation tasks.
