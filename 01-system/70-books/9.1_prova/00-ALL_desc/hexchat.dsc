   HexChat is an IRC chat program. It allows you to join multiple IRC
   channels (chat rooms) at the same time, talk publicly, have private
   one-on-one conversations, etc. File transfers are also possible.
