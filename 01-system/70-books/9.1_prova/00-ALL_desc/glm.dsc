   OpenGL Mathematics (GLM) is a header-only C++ mathematics library for
   graphics software based on the OpenGL Shading Language (GLSL)
   specifications. An extension system provides extended capabilities such
   as matrix transformations and quaternions.
