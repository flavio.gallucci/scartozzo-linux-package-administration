   The GMime package contains a set of utilities for parsing and creating
   messages using the Multipurpose Internet Mail Extension (MIME) as
   defined by the applicable RFCs. See the [5]GMime web site for the RFCs
   resourced. This is useful as it provides an API which adheres to the
   MIME specification as closely as possible while also providing
   programmers with an extremely easy to use interface to the API
   functions.
