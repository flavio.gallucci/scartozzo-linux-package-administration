   DejaGnu is a framework for running test suites on GNU tools. It is
   written in expect, which uses Tcl (Tool command language). It was
   installed by LFS in the temporary /tools directory. These instructions
   install it permanently.
