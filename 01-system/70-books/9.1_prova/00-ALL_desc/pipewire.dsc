   The pipewire package contains a server and user-space API to handle
   multimedia pipelines. This includes a universal API to connect to
   multimedia devices, as well as sharing multimedia files between
   applications.
