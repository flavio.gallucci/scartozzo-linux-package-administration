   The mtdev package contains Multitouch Protocol Translation Library
   which is used to transform all variants of kernel MT (Multitouch)
   events to the slotted type B protocol.
