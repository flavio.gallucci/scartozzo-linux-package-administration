   Xorg's ancestor (X11R1, in 1987) at first only provided bitmap fonts,
   with a tool (bdftopcf) to assist in their installation. With the
   introduction of xorg-server-1.19.0 and libXfont2 many people will not
   need them. There are still a few old packages which might require, or
   benefit from, these deprecated fonts and so the following packages are
   shown here.
   [Note]
Note
   The font-adobe-100dpi package installs 100 dots per inch versions of
   Courier, Helvetica, New Century Schoolbook and Times fonts.
   The remaining font packages here provide 75dpi versions of those fonts,
   and various miscellaneous fonts - mostly for legacy encodings. Those
   are primarily to silence warnings when testing Xorg. In previous
   versions of BLFS a lot more legacy fonts were installed.
   Please consult the BLFS-7.10 book at
   [5]http://www.linuxfromscratch.org/blfs/view/7.10/x/x7font.html if you
   wish to install any of those other fonts.
