   The Quasar DV Codec (libdv) is a software CODEC for DV video, the
   encoding format used by most digital camcorders. It can be used to copy
   videos from camcorders using a firewire (IEEE 1394) connection.
