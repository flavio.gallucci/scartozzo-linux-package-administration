   The libquicktime package contains the libquicktime library, various
   plugins and codecs, along with graphical and command line utilities
   used for encoding and decoding QuickTime files. This is useful for
   reading and writing files in the QuickTime format. The goal of the
   project is to enhance, while providing compatibility with the Quicktime
   4 Linux library.
