   Unbound is a validating, recursive, and caching DNS resolver. It is
   designed as a set of modular components that incorporate modern
   features, such as enhanced security (DNSSEC) validation, Internet
   Protocol Version 6 (IPv6), and a client resolver library API as an
   integral part of the architecture.
