   The PIN-Entry package contains a collection of simple PIN or
   pass-phrase entry dialogs which utilize the Assuan protocol as
   described by the [5]Ägypten project. PIN-Entry programs are usually
   invoked by the gpg-agent daemon, but can be run from the command line
   as well. There are programs for various text-based and GUI
   environments, including interfaces designed for Ncurses (text-based),
   and for the common GTK and Qt toolkits.
