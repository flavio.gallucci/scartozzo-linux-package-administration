   The lm_sensors package provides user-space support for the hardware
   monitoring drivers in the Linux kernel. This is useful for monitoring
   the temperature of the CPU and adjusting the performance of some
   hardware (such as cooling fans).
