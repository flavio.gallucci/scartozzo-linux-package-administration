   The libnsl package contains the public client interface for NIS(YP) and
   NIS+. It replaces the NIS library that used to be in glibc.
