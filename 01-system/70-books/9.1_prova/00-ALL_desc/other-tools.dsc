Introduction
   This section is provided to show you some additional programming tools
   for which instructions have not yet been created in the book or for
   those that are not appropriate for the book. Note that these packages
   may not have been tested by the BLFS team, but their mention here is
   meant to be a convenient source of additional information.
   User Notes:
   [5]http://wiki.linuxfromscratch.org/blfs/wiki/OtherProgrammingTools
Programming Frameworks, Languages and Compilers
A+
   A+ is a powerful and efficient programming language. It is freely
   available under the GNU General Public License. It embodies a rich set
   of functions and operators, a modern graphical user interface with many
   widgets and automatic synchronization of widgets and variables,
   asynchronous execution of functions associated with variables and
   events, dynamic loading of user compiled subroutines, and many other
   features. Execution is by a rather efficient interpreter. A+ was
   created at Morgan Stanley. Primarily used in a
   computationally-intensive business environment, many critical
   applications written in A+ have withstood the demands of real world
   developers over many years. Written in an interpreted language, A+
   applications tend to be portable.
     * Project Home Page: [6]http://www.aplusdev.org/
     * Download Location: [7]http://www.aplusdev.org/Download/index.html
ABC
   ABC is an interactive programming language and environment for personal
   computing, originally intended as a good replacement for BASIC. It was
   designed by first doing a task analysis of the programming task. ABC is
   easy to learn (an hour or so for someone who has already programmed),
   and yet easy to use. Originally intended as a language for beginners,
   it has evolved into a powerful tool for beginners and experts alike.
   Some features of the language include: a powerful collection of only
   five data types that easily combines strong typing, yet without
   declarations, no limitations (such as max int), apart from sheer
   exhaustion of memory refinements to support top-down programming,
   nesting by indentation and programs typically are one fourth or one
   fifth the size of the equivalent Pascal or C program.
     * Project Home Page: [8]http://homepages.cwi.nl/~steven/abc/
     * Download Location:
       [9]http://homepages.cwi.nl/~steven/abc/implementations.html
ALF
   ALF is a language which combines functional and logic programming
   techniques. The foundation of ALF is Horn clause logic with equality
   which consists of predicates and Horn clauses for logic programming,
   and functions and equations for functional programming. The ALF system
   is an efficient implementation of the combination of resolution,
   narrowing, rewriting and rejection. Similarly to Prolog, ALF uses a
   backtracking strategy corresponding to a depth-first search in the
   derivation tree.
     * Project Home Page:
       [10]http://www.informatik.uni-kiel.de/~mh/systems/ALF.html
     * Download Location:
       [11]http://www.informatik.uni-kiel.de/~mh/systems/ALF/
ASM
   ASM is a Java bytecode manipulation framework. It can be used to
   dynamically generate stub classes or other proxy classes, directly in
   binary form, or to dynamically modify classes at load time, i.e., just
   before they are loaded into the Java Virtual Machine. ASM offers
   similar functionalities as BCEL or SERP, but is much smaller (33KB
   instead of 350KB for BCEL and 150KB for SERP) and faster than these
   tools (the overhead of a load time class transformation is of the order
   of 60% with ASM, 700% or more with BCEL, and 1100% or more with SERP).
   Indeed ASM was designed to be used in a dynamic way (though it works
   statically as well) and was therefore designed and implemented to be as
   small and as fast as possible.
     * Project Home Page: [12]http://asm.objectweb.org/
     * Download Location: [13]http://forge.objectweb.org/projects/asm/
BCPL
   BCPL is a simple typeless language that was designed in 1966 by Martin
   Richards and implemented for the first time at MIT in the Spring of
   1967.
     * Project Home Page: [14]http://www.cl.cam.ac.uk/users/mr/BCPL.html
     * Download Location: [15]http://www.cl.cam.ac.uk/users/mr/BCPL/
BETA
   BETA is developed within the Scandinavian School of object-orientation,
   where the first object-oriented language, Simula, was developed. BETA
   is a modern language in the Simula tradition. The resulting language is
   smaller than Simula in spite of being considerably more expressive.
   BETA is a strongly typed language like Simula, Eiffel and C++, with
   most type checking being carried out at compile-time. It is well known
   that it is not possible to obtain all type checking at compile time
   without sacrificing the expressiveness of the language. BETA has
   optimum balance between compile-time type checking and run-time type
   checking.
     * Project Home Page: [16]http://www.daimi.au.dk/~beta/
     * Download Location: [17]ftp://ftp.daimi.au.dk/pub/beta/
<bigwig>
   <bigwig> is a high-level programming language for developing
   interactive Web services. Programs are compiled into a conglomerate of
   lower-level technologies such as C code, HTTP, HTML, JavaScript, and
   SSL, all running on top of a runtime system based on an Apache Web
   server module. It is a descendant of the Mawl project but is a
   completely new design and implementation with vastly expanded
   ambitions. The <bigwig> language is really a collection of tiny
   domain-specific languages focusing on different aspects of interactive
   Web services. These contributing languages are held together by a
   C-like skeleton language. Thus, <bigwig> has the look and feel of
   C-programs but with special data and control structures.
     * Project Home Page: [18]http://www.brics.dk/bigwig/
     * Download Location: [19]http://www.brics.dk/bigwig/download/
Bigloo
   Bigloo is a Scheme implementation devoted to one goal: enabling Scheme
   based programming style where C(++) is usually required. Bigloo
   attempts to make Scheme practical by offering features usually
   presented by traditional programming languages but not offered by
   Scheme and functional programming. Bigloo compiles Scheme modules and
   delivers small and fast stand-alone binary executables. It enables full
   connections between Scheme and C programs, between Scheme and Java
   programs, and between Scheme and C# programs.
     * Project Home Page: [20]http://www-sop.inria.fr/mimosa/fp/Bigloo/
     * Download Location: [21]ftp://ftp-sop.inria.fr/mimosa/fp/Bigloo/
C--
   C-- is a portable assembly language that can be generated by a front
   end and implemented by any of several code generators. It serves as an
   interface between high-level compilers and retargetable, optimizing
   code generators. Authors of front ends and code generators can
   cooperate easily.
     * Project Home Page: [22]http://www.cminusminus.org/
     * Download Location: [23]http://www.cminusminus.org/code.html
Caml
   Caml is a general-purpose programming language, designed with program
   safety and reliability in mind. It is very expressive, yet easy to
   learn and use. Caml supports functional, imperative, and
   object-oriented programming styles. It has been developed and
   distributed by INRIA, France's national research institute for computer
   science, since 1985. The Objective Caml system is the main
   implementation of the Caml language. It features a powerful module
   system and a full-fledged object-oriented layer. It comes with a
   native-code compiler that supports numerous architectures, for high
   performance; a bytecode compiler, for increased portability; and an
   interactive loop, for experimentation and rapid development.
     * Project Home Page: [24]http://caml.inria.fr/
     * Download Location: [25]http://caml.inria.fr/pub/distrib/
Ch
   Ch is an embeddable C/C++ interpreter for cross-platform scripting,
   shell programming, 2D/3D plotting, numerical computing, and embedded
   scripting.
     * Project Home Page: [26]http://www.softintegration.com/
     * Download Location:
       [27]http://www.softintegration.com/products/chstandard/download/
Clean
   Clean is a general purpose, state-of-the-art, pure and lazy functional
   programming language designed for making real-world applications. Clean
   is the only functional language in the world which offers uniqueness
   typing. This type system makes it possible in a pure functional
   language to incorporate destructive updates of arbitrary data
   structures (including arrays) and to make direct interfaces to the
   outside imperative world. The type system makes it possible to develop
   efficient applications.
     * Project Home Page: [28]http://wiki.clean.cs.ru.nl/Clean
     * Download Location: [29]http://wiki.clean.cs.ru.nl/Download_Clean
Cyclone
   Cyclone is a programming language based on C that is safe, meaning that
   it rules out programs that have buffer overflows, dangling pointers,
   format string attacks, and so on. High-level, type-safe languages, such
   as Java, Scheme, or ML also provide safety, but they don't give the
   same control over data representations and memory management that C
   does (witness the fact that the run-time systems for these languages
   are usually written in C.) Furthermore, porting legacy C code to these
   languages or interfacing with legacy C libraries is a difficult and
   error-prone process. The goal of Cyclone is to give programmers the
   same low-level control and performance of C without sacrificing safety,
   and to make it easy to port or interface with legacy C code.
     * Project Home Page: [30]http://cyclone.thelanguage.org/
     * Download Location:
       [31]http://cyclone.thelanguage.org/wiki/Download/
D
   D is a general purpose systems and applications programming language.
   It is a higher level language than C++, but retains the ability to
   write high performance code and interface directly with the operating
   system APIs and with hardware. D is well suited to writing medium to
   large scale million line programs with teams of developers. It is easy
   to learn, provides many capabilities to aid the programmer, and is well
   suited to aggressive compiler optimization technology. D is not a
   scripting language, nor an interpreted language. It doesn't come with a
   VM, a religion, or an overriding philosophy. It's a practical language
   for practical programmers who need to get the job done quickly,
   reliably, and leave behind maintainable, easy to understand code. D is
   the culmination of decades of experience implementing compilers for
   many diverse languages, and attempting to construct large projects
   using those languages. It draws inspiration from those other languages
   (most especially C++) and tempers it with experience and real world
   practicality.
     * Project Home Page: [32]http://www.digitalmars.com/d/
     * Download Location: [33]ftp://ftp.digitalmars.com/
DMDScript
   DMDScript is Digital Mars' implementation of the ECMA 262 scripting
   language. Netscape's implementation is called JavaScript, Microsoft's
   implementation is called JScript. DMDScript is much faster than other
   implementations, which you can verify with the included benchmark.
     * Project Home Page:
       [34]http://www.digitalmars.com/dscript/index.html
     * Download Location: [35]ftp://ftp.digitalmars.com/
DotGNU Portable.NET
   DotGNU Portable.NET goal is to build a suite of free software tools to
   build and execute .NET applications, including a C# compiler,
   assembler, disassembler, and runtime engine. While the initial target
   platform was GNU/Linux, it is also known to run under Windows, Solaris,
   NetBSD, FreeBSD, and MacOS X. The runtime engine has been tested on the
   x86, PowerPC, ARM, Sparc, PARISC, s390, Alpha, and IA-64 processors.
   DotGNU Portable.NET is part of the DotGNU project, built in accordance
   with the requirements of the GNU Project. DotGNU Portable.NET is
   focused on compatibility with the ECMA specifications for CLI. There
   are other projects under the DotGNU meta-project to build other
   necessary pieces of infrastructure, and to explore non-CLI approaches
   to virtual machine implementation.
     * Project Home Page: [36]http://www.gnu.org/software/dotgnu/
     * Download Location:
       [37]http://www.gnu.org/software/dotgnu/pnet-packages.html
Dylan
   Dylan is an advanced, object-oriented, dynamic language which supports
   rapid program development. When needed, programs can be optimized for
   more efficient execution by supplying more type information to the
   compiler. Nearly all entities in Dylan (including functions, classes,
   and basic data types such as integers) are first class objects.
   Additionally, Dylan supports multiple inheritance, polymorphism,
   multiple dispatch, keyword arguments, object introspection, macros, and
   many other advanced features... --Peter Hinely.
     * Project Home Page: [38]http://www.opendylan.org/
     * Download Location: [39]http://opendylan.org/download/index.html
E
   E is a secure distributed Java-based pure-object platform and p2p
   scripting language. It has two parts: ELib and the E Language. Elib
   provides the stuff that goes on between objects. As a pure-Java
   library, ELib provides for inter-process capability-secure distributed
   programming. Its cryptographic capability protocol enables mutually
   suspicious Java processes to cooperate safely, and its event-loop
   concurrency and promise pipelining enable high performance deadlock
   free distributed pure-object computing. The E Language can be used to
   express what happens within an object. It provides a convenient and
   familiar notation for the ELib computational model, so you can program
   in one model rather than two. Under the covers, this notation expands
   into Kernel-E, a minimalist lambda-language much like Scheme or
   Smalltalk. Objects written in the E language are only able to interact
   with other objects according to ELib's semantics, enabling object
   granularity intra-process security, including the ability to safely run
   untrusted mobile code (such as caplets).
     * Project Home Page: [40]http://www.erights.org/
     * Download Location: [41]http://www.erights.org/download/
elastiC
   elastiC is a portable high-level object-oriented interpreted language
   with a C like syntax. Its main characteristics are: open source,
   interpreted, has portable bytecode compilation, dynamic typing,
   automatic real very fast garbage collection, object oriented with
   meta-programming support (a la Smalltalk), functional programming
   support (Scheme-like closures with lexical scoping, and eval-like
   functionality), hierarchical namespaces, a rich set of useful built-in
   types (dynamic arrays, dictionaries, symbols, ...), extensible with C
   (you can add functions, types, classes, methods, packages, ...),
   embeddable in C. elastiC has been strongly influenced by C, Smalltalk,
   Scheme and Python and tries to merge the best characteristics of all
   these languages, while still coherently maintaining its unique
   personality.
     * Project Home Page: [42]http://www.elasticworld.org/
     * Download Location: [43]http://www.elasticworld.org/download.html
Erlang/OTP
   Erlang/OTP is a development environment based on Erlang. Erlang is a
   programming language which has many features more commonly associated
   with an operating system than with a programming language: concurrent
   processes, scheduling, memory management, distribution, networking,
   etc. The initial open-source Erlang release contains the implementation
   of Erlang, as well as a large part of Ericsson's middleware for
   building distributed high-availability systems. Erlang is characterized
   by the following features: robustness, soft real-time, hot code
   upgrades and incremental code loading.
     * Project Home Page: [44]http://www.erlang.org/
     * Download Location: [45]http://www.erlang.org/download.html
Euphoria
   Euphoria is a simple, flexible, and easy-to-learn programming language.
   It lets you quickly and easily develop programs for Windows, DOS, Linux
   and FreeBSD. Euphoria was first released in 1993. Since then Rapid
   Deployment Software has been steadily improving it with the help of a
   growing number of enthusiastic users. Although Euphoria provides
   subscript checking, uninitialized variable checking and numerous other
   run-time checks, it is extremely fast. People have used it to develop
   high-speed DOS games, Windows GUI programs, and X Window System
   programs. It is also very useful for CGI (Web-based) programming.
     * Project Home Page: [46]http://www.rapideuphoria.com/
     * Download Location: [47]http://www.rapideuphoria.com/v20.htm
Felix
   Felix is an advanced Algol like procedural programming language with a
   strong functional subsystem. It features ML style static typing, first
   class functions, pattern matching, garbage collection, polymorphism,
   and has built in support for high performance microthreading, regular
   expressions and context free parsing. The system provides a scripting
   harness so the language can be used like other scripting languages such
   as Python and Perl, but underneath it generates native code to obtain
   high performance. A key feature of the system is that it uses the C/C++
   object model, and provides an advanced binding sublanguage to support
   integration with C/C++ at both the source and object levels, both for
   embedding C/C++ data types and functions into Felix, and for embedding
   Felix into existing C++ architectures. The Felix compiler is written in
   Objective Caml, and generates ISO C++ which should compile on any
   platform.
     * Project Home Page: [48]http://felix.sourceforge.net/
     * Download Location:
       [49]http://felix-lang.org/$/usr/local/lib/felix/tarballs
ferite
   ferite is a scripting language and engine all in one manageable chunk.
   It is designed to be easily extended in terms of API, and to be used
   within other applications making them more configurable and useful to
   the end user. It has a syntax similar to a number of other languages
   but remains clean and its own language.
     * Project Home Page: [50]http://www.ferite.org/
     * Download Location: [51]http://www.ferite.org/download.html
Forth
   Forth is a stack-based, extensible language without type-checking. It
   is probably best known for its "reverse Polish" (postfix) arithmetic
   notation, familiar to users of Hewlett-Packard calculators. Forth is a
   real-time programming language originally developed to control
   telescopes. Forth has many unique features and applications: it can
   compile itself into a new compiler, reverse-polish coding, edit time
   error checking and compiling (similar to BASIC), extremely efficient
   thread based language, can be used to debug itself, extensible; thus
   can become what ever you need it to be. The links below lead to the
   website of the Forth Interest Group (FIG), a world-wide, non-profit
   organization for education in and the promotion of the Forth computer
   language. Another worthwhile website dedicated to the Forth community
   is [52]http://wiki.forthfreak.net/.
     * Project Home Page: [53]http://www.forth.org/
     * Download Location: [54]http://www.forth.org/compilers.html
GNU Smalltalk
   GNU Smalltalk is a free implementation of the Smalltalk-80 language
   which runs on most versions on Unix and, in general, everywhere you can
   find a POSIX-compliance library. An uncommon feature of it is that it
   is well-versed to scripting tasks and headless processing. See
   [55]http://www.gnu.org/software/smalltalk/manual/html_node/Overview.htm
   l for a more detailed explanation of GNU Smalltalk.
     * Project Home Page: [56]http://smalltalk.gnu.org/
     * Download Location: [57]https://ftp.gnu.org/gnu/smalltalk/
Haskell
   Haskell is a computer programming language. In particular, it is a
   polymorphicly typed, lazy, purely functional language, quite different
   from most other programming languages. The language is named for
   Haskell Brooks Curry, whose work in mathematical logic serves as a
   foundation for functional languages. Haskell is based on lambda
   calculus. There are many implementations of Haskell, among them:
     * GHC: [58]http://www.haskell.org/ghc/
     * Helium: [59]http://www.cs.uu.nl/wiki/bin/view/Helium/WebHome
     * Hugs: [60]http://www.haskell.org/hugs/
     * nhc98: [61]http://www.haskell.org/nhc98/
HLA (High Level Assembly)
   The HLA language was developed as a tool to help teach assembly
   language programming and machine organization to University students at
   the University of California, Riverside. The basic idea was to teach
   students assembly language programming by leveraging their knowledge of
   high level languages like C/C++ and Pascal/Delphi. At the same time,
   HLA was designed to allow advanced assembly language programmers write
   more readable and more powerful assembly language code.
     * Project Home Page:
       [62]http://www.plantation-productions.com/Webster/HighLevelAsm/inde
       x.html
     * Download Location:
       [63]http://www.plantation-productions.com/Webster/HighLevelAsm/dnld
       .html
Icon
   Icon is a high-level, general-purpose programming language with a large
   repertoire of features for processing data structures and character
   strings. It is an imperative, procedural language with a syntax
   reminiscent of C and Pascal, but with semantics at a much higher level.
     * Project Home Page: [64]http://www.cs.arizona.edu/icon/
     * Download Location: [65]ftp://ftp.cs.arizona.edu/icon/
Io
   Io is a small, prototype-based programming language. The ideas in Io
   are mostly inspired by Smalltalk (all values are objects), Self
   (prototype-based), NewtonScript (differential inheritance), Act1
   (actors and futures for concurrency), LISP (code is a runtime
   inspectable/modifiable tree) and Lua (small, embeddable).
     * Project Home Page: [66]http://iolanguage.org
     * Download Location: [67]http://iobin.suspended-chord.info/
J
   J is a modern, high-level, general-purpose, high-performance
   programming language. It is portable and runs on Windows, Unix, Mac,
   and PocketPC handhelds, both as a GUI and in a console. True 64-bit J
   systems are available for XP64 or Linux64, on AMD64 or Intel EM64T
   platforms. J systems can be installed and distributed for free.
     * Project Home Page: [68]http://www.jsoftware.com/
     * Download Location: [69]http://www.jsoftware.com/stable.htm
Jamaica
   Jamaica, the JVM Macro Assembler, is an easy-to-learn and easy-to-use
   assembly language for JVM bytecode programming. It uses Java syntax to
   define a JVM class except for the method body that takes bytecode
   instructions, including Jamaica's built-in macros. In Jamaica, bytecode
   instructions use mnemonics and symbolic names for all variables,
   parameters, data fields, constants and labels.
     * Project Home Page: [70]http://judoscript.org/jamaica.html
     * Download Location: [71]http://judoscript.org/download.html
Joy
   Joy is a purely functional programming language. Whereas all other
   functional programming languages are based on the application of
   functions to arguments, Joy is based on the composition of functions.
   All such functions take a stack as an argument and produce a stack as a
   value. Consequently much of Joy looks like ordinary postfix notation.
   However, in Joy a function can consume any number of parameters from
   the stack and leave any number of results on the stack. The
   concatenation of appropriate programs denotes the composition of the
   functions which the programs denote.
     * Project Home Page:
       [72]http://www.latrobe.edu.au/humanities/research/research-projects
       /past-projects/joy-programming-language
Judo
   Judo is a practical, functional scripting language. It is designed to
   cover the use cases of not only
   algorithmic/object-oriented/multi-threaded programming and Java
   scripting but also a number of major application domain tasks, such as
   scripting for JDBC, WSDL, ActiveX, OS, multiple file/data formats, etc.
   Despite its rich functionality, the base language is extremely simple,
   and domain support syntax is totally intuitive to domain experts, so
   that even though you have never programmed in Judo, you would have
   little trouble figuring out what the code does.
     * Project Home Page: [73]http://judoscript.org/judo.html
     * Download Location: [74]http://judoscript.org/download.html
JWIG
   JWIG is a Java-based high-level programming language for development of
   interactive Web services. It contains an advanced session model, a
   flexible mechanism for dynamic construction of XML documents, in
   particular XHTML, and a powerful API for simplifying use of the HTTP
   protocol and many other aspects of Web service programming. To support
   program development, JWIG provides a unique suite of highly specialized
   program analysers that at compile time verify for a given program that
   no runtime errors can occur while building documents or receiving form
   input, and that all documents being shown are valid according to the
   document type definition for XHTML 1.0. The main goal of the JWIG
   project is to simplify development of complex Web services, compared to
   alternatives, such as, Servlets, JSP, ASP, and PHP. JWIG is a
   descendant of the <bigwig> research language.
     * Project Home Page: [75]http://www.brics.dk/JWIG/
     * Download Location: [76]http://www.brics.dk/JWIG/download.html
Lava
   Lava is a name unfortunately chosen for several unrelated software
   development languages/projects. So it doesn't appear as though BLFS has
   a preference for one over another, the project web sites are listed
   below, without descriptions of the capabilities or features for any of
   them.
     * Project Home Page: [77]http://lavape.sourceforge.net/index.htm
     * Project Home Page: [78]http://mathias.tripod.com/IavaHomepage.html
Mercury
   Mercury is a new logic/functional programming language, which combines
   the clarity and expressiveness of declarative programming with advanced
   static analysis and error detection features. Its highly optimized
   execution algorithm delivers efficiency far in excess of existing logic
   programming systems, and close to conventional programming systems.
   Mercury addresses the problems of large-scale program development,
   allowing modularity, separate compilation, and numerous
   optimization/time trade-offs.
     * Project Home Page: [79]http://mercurylang.org/
     * Download Location: [80]http://mercurylang.org/download.html
Mono
   Mono provides the necessary software to develop and run .NET client and
   server applications on Linux, Solaris, Mac OS X, Windows, and Unix.
   Sponsored by Novell, the Mono open source project has an active and
   enthusiastic contributing community and is positioned to become the
   leading choice for development of Linux applications.
     * Project Home Page: [81]http://www.mono-project.com/Main_Page
     * Download Location: [82]http://ftp.novell.com/pub/mono/archive/
MPD
   MPD is a variant of the SR programming language. SR has a Pascal-like
   syntax and uses guarded commands for control statements. MPD has a
   C-like syntax and C-like control statements. However, the main
   components of the two languages are the same: resources, globals,
   operations, procs, procedures, processes, and virtual machines.
   Moreover, MPD supports the same variety of concurrent programming
   mechanisms as SR: co statements, semaphores, call/send/forward
   invocations, and receive and input statements.
     * Project Home Page: [83]http://www.cs.arizona.edu/mpd/
     * Download Location: [84]http://www.cs.arizona.edu/mpd/download/
Nemerle
   Nemerle is a high-level statically-typed programming language for the
   .NET platform. It offers functional, object-oriented and imperative
   features. It has a simple C#-like syntax and a powerful
   meta-programming system. Features that come from the functional land
   are variants, pattern matching, type inference and parameter
   polymorphism (aka generics). The meta-programming system allows great
   compiler extensibility, embedding domain specific languages, partial
   evaluation and aspect-oriented programming.
     * Project Home Page: [85]http://nemerle.org/About
     * Download Location: [86]http://nemerle.org/Downloads
Octave
   GNU Octave is a high-level language, primarily intended for numerical
   computations. It provides a convenient command line interface for
   solving linear and nonlinear problems numerically, and for performing
   other numerical experiments using a language that is mostly compatible
   with Matlab. It may also be used as a batch-oriented language. Octave
   has extensive tools for solving common numerical linear algebra
   problems, finding the roots of nonlinear equations, integrating
   ordinary functions, manipulating polynomials, and integrating ordinary
   differential and differential-algebraic equations. It is easily
   extensible and customizable via user-defined functions written in
   Octave's own language, or using dynamically loaded modules written in
   C++, C, Fortran, or other languages.
     * Project Home Page: [87]http://www.gnu.org/software/octave/
     * Download Location:
       [88]http://www.gnu.org/software/octave/download.html
OO2C (Optimizing Oberon-2 Compiler)
   OO2C is an Oberon-2 development platform. It consists of an optimizing
   compiler, a number of related tools, a set of standard library modules
   and a reference manual. Oberon-2 is a general-purpose programming
   language in the tradition of Pascal and Modula-2. Its most important
   features are block structure, modularity, separate compilation, static
   typing with strong type checking (also across module boundaries) and
   type extension with type-bound procedures. Type extension makes
   Oberon-2 an object-oriented language.
     * Project Home Page: [89]http://sourceforge.net/projects/ooc/
     * Download Location: [90]https://downloads.sourceforge.net/ooc/
Ordered Graph Data Language (OGDL)
   OGDL is a structured textual format that represents information in the
   form of graphs, where the nodes are strings and the arcs or edges are
   spaces or indentation.
     * Project Home Page: [91]http://ogdl.sourceforge.net/
     * Download Location: [92]https://downloads.sourceforge.net/ogdl/
Pike
   Pike is a dynamic programming language with a syntax similar to Java
   and C. It is simple to learn, does not require long compilation passes
   and has powerful built-in data types allowing simple and really fast
   data manipulation. Pike is released under the GNU GPL, GNU LGPL and
   MPL.
     * Project Home Page: [93]http://pike.ida.liu.se/
     * Download Location: [94]http://pike.ida.liu.se/download/pub/pike
Pyrex
   Pyrex is a language specially designed for writing Python extension
   modules. It's designed to bridge the gap between the nice, high-level,
   easy-to-use world of Python and the messy, low-level world of C. Pyrex
   lets you write code that mixes Python and C data types any way you
   want, and compiles it into a C extension for Python.
     * Project Home Page:
       [95]http://www.cosc.canterbury.ac.nz/greg.ewing/python/Pyrex/
Q
   Q is a functional programming language based on term rewriting. Thus, a
   Q program or “script” is simply a collection of equations which are
   used to evaluate expressions in a symbolic fashion. The equations
   establish algebraic identities and are interpreted as rewriting rules
   in order to reduce expressions to “normal forms”.
     * Project Home Page: [96]http://q-lang.sourceforge.net/
     * Download Location: [97]https://downloads.sourceforge.net/q-lang/
R
   R is a language and environment for statistical computing and graphics.
   It is a GNU project similar to the S language and environment which was
   developed at Bell Laboratories (formerly AT&T, now Lucent Technologies)
   by John Chambers and colleagues. R can be considered as a different
   implementation of S. There are some important differences, but much
   code written for S runs unaltered under R. R provides a wide variety of
   statistical (linear and nonlinear modelling, classical statistical
   tests, time-series analysis, classification, clustering, ...) and
   graphical techniques, and is highly extensible. The S language is often
   the vehicle of choice for research in statistical methodology, and R
   provides an Open Source route to participation in that activity.
     * Project Home Page: [98]http://www.r-project.org/
     * Download Location: [99]http://cran.r-project.org/mirrors.html
Regina Rexx
   Regina is a Rexx interpreter that has been ported to most Unix
   platforms (Linux, FreeBSD, Solaris, AIX, HP-UX, etc.) and also to OS/2,
   eCS, DOS, Win9x/Me/NT/2k/XP, Amiga, AROS, QNX4.x, QNX6.x BeOS, MacOS X,
   EPOC32, AtheOS, OpenVMS, SkyOS and OpenEdition. Rexx is a programming
   language that was designed to be easy to use for inexperienced
   programmers yet powerful enough for experienced users. It is also a
   language ideally suited as a macro language for other applications.
     * Project Home Page: [100]http://regina-rexx.sourceforge.net/
     * Download Location:
       [101]https://downloads.sourceforge.net/regina-rexx
Small Device C Compiler (SDCC)
   SDCC is a Freeware, retargetable, optimizing ANSI-C compiler that
   targets the Intel 8051, Maxim 80DS390 and the Zilog Z80 based MCUs.
   Work is in progress on supporting the Motorola 68HC08 as well as
   Microchip PIC16 and PIC18 series. The entire source code for the
   compiler is distributed under GPL.
     * Project Home Page: [102]http://sdcc.sourceforge.net/
     * Download Location: [103]http://sdcc.sourceforge.net/snap.php#Source
SmartEiffel (The GNU Eiffel Compiler)
   SmartEiffel claims to be “the fastest and the slimmest multi-platform
   Eiffel compiler on Earth”. Eiffel is an object-oriented programming
   language which emphasizes the production of robust software. Its syntax
   is keyword-oriented in the ALGOL and Pascal tradition. Eiffel is
   strongly statically typed, with automatic memory management (typically
   implemented by garbage collection). Distinguishing characteristics of
   Eiffel include Design by contract (DbC), liberal use of inheritance
   including multiple inheritance, a type system handling both value and
   reference semantics, and generic classes. Eiffel has a unified type
   system—all types in Eiffel are classes, so it is possible to create
   subclasses of the basic classes such as INTEGER. Eiffel has operator
   overloading, including the ability to define new operators, but does
   not have method overloading.
     * Project Home Page: [104]http://smarteiffel.loria.fr/
     * Download Location: [105]https://gforge.inria.fr/frs/?group_id=184
Squeak
   Squeak is an open, highly-portable Smalltalk implementation whose
   virtual machine is written entirely in Smalltalk, making it easy to
   debug, analyze, and change. To achieve practical performance, a
   translator produces an equivalent C program whose performance is
   comparable to commercial Smalltalks. Other noteworthy aspects of Squeak
   include: real-time sound and music synthesis written entirely in
   Smalltalk, extensions of BitBlt to handle color of any depth and
   anti-aliased image rotation and scaling, network access support that
   allows simple construction of servers and other useful facilities, it
   runs bit-identical on many platforms (Windows, Mac, Unix, and others),
   a compact object format that typically requires only a single word of
   overhead per object and a simple yet efficient incremental garbage
   collector for 32-bit direct pointers efficient bulk-mutation of
   objects.
     * Project Home Page: [106]http://www.squeak.org/
     * Download Location: [107]http://www.squeak.org/Download/
SR (Synchronizing Resources)
   SR is a language for writing concurrent programs. The main language
   constructs are resources and operations. Resources encapsulate
   processes and variables they share; operations provide the primary
   mechanism for process interaction. SR provides a novel integration of
   the mechanisms for invoking and servicing operations. Consequently, all
   of local and remote procedure call, rendezvous, message passing,
   dynamic process creation, multicast, and semaphores are supported. SR
   also supports shared global variables and operations.
     * Project Home Page: [108]http://www.cs.arizona.edu/sr/index.html
     * Download Location: [109]ftp://ftp.cs.arizona.edu/sr/
Standard ML
   Standard ML is a safe, modular, strict, functional, polymorphic
   programming language with compile-time type checking and type
   inference, garbage collection, exception handling, immutable data types
   and updatable references, abstract data types, and parametric modules.
   It has efficient implementations and a formal definition with a proof
   of soundness. There are many implementations of Standard ML, among
   them:
     * ML Kit: [110]http://www.it-c.dk/research/mlkit/
     * MLton: [111]http://mlton.org/
     * Poly/ML: [112]http://www.polyml.org/
     * Standard ML of New Jersey: [113]http://www.smlnj.org/
Steel Bank Common Lisp (SBCL)
   SBCL is an open source (free software) compiler and runtime system for
   ANSI Common Lisp. It provides an interactive environment including an
   integrated native compiler, a debugger, and many extensions. SBCL runs
   on a number of platforms.
     * Project Home Page: [114]http://www.sbcl.org/
     * Download Location: [115]https://downloads.sourceforge.net/sbcl/
Tiny C Compiler (TCC)
   Tiny C Compiler is a small C compiler that can be used to compile and
   execute C code everywhere, for example on rescue disks (about 100KB for
   x86 TCC executable, including C preprocessor, C compiler, assembler and
   linker). TCC is fast. It generates optimized x86 code, has no byte code
   overhead and compiles, assembles and links several times faster than
   GCC. TCC is versatile, any C dynamic library can be used directly. It
   is heading toward full ISOC99 compliance and can compile itself. The
   compiler is safe as it includes an optional memory and bound checker.
   Bound checked code can be mixed freely with standard code. TCC compiles
   and executes C source directly. No linking or assembly necessary. A
   full C preprocessor and GNU-like assembler is included. It is C script
   supported; just add “#!/usr/local/bin/tcc -run” on the first line of
   your C source, and execute it directly from the command line. With
   libtcc, you can use TCC as a backend for dynamic code generation.
     * Project Home Page: [116]http://bellard.org/tcc/
     * Download Location:
       [117]http://download.savannah.gnu.org/releases-noredirect/tinycc/
TinyCOBOL
   TinyCOBOL is a COBOL compiler being developed by members of the free
   software community. The mission is to produce a COBOL compiler based on
   the COBOL 85 standards. TinyCOBOL is available for the Intel
   architecture (IA32) and compatible processors on the following
   platforms: BeOS, FreeBSD, Linux and MinGW on Windows.
     * Project Home Page: [118]http://sourceforge.net/projects/tiny-cobol/
     * Download Location:
       [119]https://downloads.sourceforge.net/tiny-cobol/
Yorick
   Yorick is an interpreted programming language, designed for
   postprocessing or steering large scientific simulation codes. Smaller
   scientific simulations or calculations, such as the flow past an
   airfoil or the motion of a drumhead, can be written as standalone
   yorick programs. The language features a compact syntax for many common
   array operations, so it processes large arrays of numbers very
   efficiently. Unlike most interpreters, which are several hundred times
   slower than compiled code for number crunching, Yorick can approach to
   within a factor of four or five of compiled speed for many common
   tasks. Superficially, Yorick code resembles C code, but Yorick
   variables are never explicitly declared and have a dynamic scoping
   similar to many Lisp dialects. The “unofficial” home page for Yorick
   can be found at [120]http://www.maumae.net/yorick.
     * Project Home Page: [121]http://yorick.sourceforge.net/index.php
     * Download Location:
       [122]http://sourceforge.net/projects/yorick/files/
ZPL
   ZPL is an array programming language designed from first principles for
   fast execution on both sequential and parallel computers. It provides a
   convenient high-level programming medium for supercomputers and
   large-scale clusters with efficiency comparable to hand-coded message
   passing. It is the perfect alternative to using a sequential language
   like C or Fortran and a message passing library like MPI.
     * Project Home Page:
       [123]http://www.cs.washington.edu/research/zpl/home/index.html
     * Download Location:
       [124]http://www.cs.washington.edu/research/zpl/download/download.ht
       ml
Programming Libraries and Bindings
Byte Code Engineering Library (BCEL)
   BECL is intended to give users a convenient possibility to analyze,
   create, and manipulate (binary) Java class files (those ending with
   .class). Classes are represented by objects which contain all the
   symbolic information of the given class: methods, fields and byte code
   instructions, in particular. Such objects can be read from an existing
   file, be transformed by a program (e.g., a class loader at run-time)
   and dumped to a file again. An even more interesting application is the
   creation of classes from scratch at run-time. The Byte Code Engineering
   Library may be also useful if you want to learn about the Java Virtual
   Machine (JVM) and the format of Java .class files. BCEL is already
   being used successfully in several projects such as compilers,
   optimizers, obfuscators, code generators and analysis tools.
     * Project Home Page: [125]http://jakarta.apache.org/bcel/index.html
     * Download Location:
       [126]http://archive.apache.org/dist/jakarta/bcel/
Choco
   Choco is a Java library for constraint satisfaction problems (CSP),
   constraint programming (CP) and explanation-based constraint solving
   (e-CP). It is built on a event-based propagation mechanism with
   backtrackable structures.
     * Project Home Page: [127]http://sourceforge.net/projects/choco/
     * Download Location: [128]http://choco.sourceforge.net/download.html
GOB (GObject Builder)
   GOB (GOB2 anyway) is a preprocessor for making GObjects with inline C
   code so that generated files are not edited. Syntax is inspired by Java
   and Yacc or Lex. The implementation is intentionally kept simple, and
   no C actual code parsing is done.
     * Project Home Page: [129]http://www.5z.com/jirka/gob.html
     * Download Location: [130]http://ftp.5z.com/pub/gob/
GTK+/GNOME Language Bindings (wrappers)
   GTK+/GNOME language bindings allow GTK+ to be used from other
   programming languages, in the style of those languages.
     * Project Home Page: [131]http://www.gtk.org/language-bindings.php
Java-GNOME
   Java-GNOME is a set of Java bindings for the GNOME and GTK+ libraries
   that allow GNOME and GTK+ applications to be written in Java. The
   Java-GNOME API has been carefully designed to be easy to use,
   maintaining a good OO paradigm, yet still wrapping the entire
   functionality of the underlying libraries. Java-GNOME can be used with
   the Eclipse development environment and Glade user interface designer
   to create applications with ease.
     * Project Home Page: [132]http://java-gnome.sourceforge.net/4.0/
     * Download Location: [133]http://java-gnome.sourceforge.net/4.0/get/
gtk2-perl
   gtk2-perl is the collective name for a set of Perl bindings for GTK+
   2.x and various related libraries. These modules make it easy to write
   GTK and GNOME applications using a natural, Perlish, object-oriented
   syntax.
     * Project Home Page: [134]http://gtk2-perl.sourceforge.net/
     * Download Location: [135]https://downloads.sourceforge.net/gtk2-perl
KDE Language Bindings
   KDE and most KDE applications are implemented using the C++ programming
   language, however there are number of bindings to other languages are
   available. These include scripting languages like Perl, Python and
   Ruby, and systems programming languages such as Java and C#.
     * Project Home Page:
       [136]http://techbase.kde.org/Development/Languages
Numerical Python (Numpy)
   Numerical Python adds a fast array facility to the Python language.
     * Project Home Page: [137]http://numeric.scipy.org/
     * Download Location: [138]https://downloads.sourceforge.net/numpy/
Perl Scripts and Additional Modules
   There are many Perl scripts and additional modules located on the
   Comprehensive Perl Archive Network (CPAN) web site. Here you will find
   “All Things Perl”.
     * Project Home Page: [139]http://cpan.org/
Integrated Development Environments
A-A-P
   A-A-P makes it easy to locate, download, build and install software. It
   also supports browsing source code, developing programs, managing
   different versions and distribution of software and documentation. This
   means that A-A-P is useful both for users and for developers.
     * Project Home Page: [140]http://www.a-a-p.org/index.html
     * Download Location: [141]http://www.a-a-p.org/download.html
Anjuta
   Anujuta is a versatile Integrated Development Environment (IDE) for C
   and C++ on GNU/Linux. It has been written for GTK/GNOME and features a
   number of advanced programming facilities. These include project
   management, application wizards, an on-board interactive debugger, and
   a powerful source editor with source browsing and syntax highlighting.
     * Project Home Page:
       [142]http://projects.gnome.org/anjuta/index.shtml
     * Download Location:
       [143]http://projects.gnome.org/anjuta/downloads.html
Eclipse
   Eclipse is an open source community whose projects are focused on
   providing an extensible development platform and application frameworks
   for building software. Eclipse contains many projects, including an
   Integrated Development Environment (IDE) for Java.
     * Project Home Page: [144]http://www.eclipse.org/
     * Download Location: [145]http://www.eclipse.org/downloads/
Mozart
   The Mozart Programming System is an advanced development platform for
   intelligent, distributed applications. Mozart is based on the Oz
   language, which supports declarative programming, object-oriented
   programming, constraint programming, and concurrency as part of a
   coherent whole. For distribution, Mozart provides a true network
   transparent implementation with support for network awareness,
   openness, and fault tolerance. Security is upcoming. It is an ideal
   platform for both general-purpose distributed applications as well as
   for hard problems requiring sophisticated optimization and inferencing
   abilities.
     * Project Home Page: [146]http://mozart.github.io/
     * Download Location: [147]https://github.com/mozart/mozart2#downloads
Other Development Tools
cachecc1
   cachecc1 is a GCC cache. It can be compared with the well known ccache
   package. It has some unique features including the use of an
   LD_PRELOADed shared object to catch invocations to cc1, cc1plus and as,
   it transparently supports all build methods, it can cache GCC
   bootstraps and it can be combined with distcc to transparently
   distribute compilations.
     * Project Home Page: [148]http://cachecc1.sourceforge.net/
     * Download Location: [149]https://downloads.sourceforge.net/cachecc1
ccache
   ccache is a compiler cache. It acts as a caching pre-processor to C/C++
   compilers, using the -E compiler switch and a hash to detect when a
   compilation can be satisfied from cache. This often results in 5 to 10
   times faster speeds in common compilations.
     * Project Home Page: [150]http://ccache.samba.org/
     * Download Location: [151]http://samba.org/ftp/ccache/
DDD (GNU Data Display Debugger)
   GNU DDD is a graphical front-end for command-line debuggers such as
   GDB, DBX, WDB, Ladebug, JDB, XDB, the Perl debugger, the Bash debugger,
   or the Python debugger. Besides “usual” front-end features such as
   viewing source texts, DDD has an interactive graphical data display,
   where data structures are displayed as graphs..
     * Project Home Page: [152]http://www.gnu.org/software/ddd/
     * Download Location: [153]https://ftp.gnu.org/gnu/ddd/
distcc
   distcc is a program to distribute builds of C, C++, Objective C or
   Objective C++ code across several machines on a network. distcc should
   always generate the same results as a local build, is simple to install
   and use, and is usually much faster than a local compile. distcc does
   not require all machines to share a filesystem, have synchronized
   clocks, or to have the same libraries or header files installed. They
   can even have different processors or operating systems, if
   cross-compilers are installed.
     * Project Home Page: [154]http://distcc.samba.org/
     * Download Location: [155]http://distcc.samba.org/download.html
Exuberant Ctags
   Exuberant Ctags generates an index (or tag) file of language objects
   found in source files that allows these items to be quickly and easily
   located by a text editor or other utility. A tag signifies a language
   object for which an index entry is available (or, alternatively, the
   index entry created for that object). Tag generation is supported for
   the following languages: Assembler, AWK, ASP, BETA, Bourne/Korn/Zsh
   Shell, C, C++, COBOL, Eiffel, Fortran, Java, Lisp, Lua, Make, Pascal,
   Perl, PHP, Python, REXX, Ruby, S-Lang, Scheme, Tcl, Vim, and YACC. A
   list of editors and tools utilizing tag files may be found at
   [156]http://ctags.sourceforge.net/tools.html.
     * Project Home Page: [157]http://ctags.sourceforge.net/
     * Download Location: [158]https://downloads.sourceforge.net/ctags/
gocache (GNU Object Cache)
   ccache is a clone of ccache, with the goal of supporting compilers
   other than GCC and adding additional features. Embedded compilers will
   especially be in focus.
     * Project Home Page: [159]http://sourceforge.net/projects/gocache/
     * Download Location: [160]https://downloads.sourceforge.net/gocache/
OProfile
   OProfile is a system-wide profiler for Linux systems, capable of
   profiling all running code at low overhead. OProfile is released under
   the GNU GPL. It consists of a kernel driver and a daemon for collecting
   sample data, and several post-profiling tools for turning data into
   information. OProfile leverages the hardware performance counters of
   the CPU to enable profiling of a wide variety of interesting
   statistics, which can also be used for basic time-spent profiling. All
   code is profiled: hardware and software interrupt handlers, kernel
   modules, the kernel, shared libraries, and applications. OProfile is
   currently in alpha status; however it has proven stable over a large
   number of differing configurations. It is being used on machines
   ranging from laptops to 16-way NUMA-Q boxes.
     * Project Home Page: [161]http://oprofile.sourceforge.net/news/
     * Download Location: [162]http://oprofile.sourceforge.net/download/
strace
   strace is a system call tracer, i.e., a debugging tool which prints out
   a trace of all the system calls made by another process or program.
     * Project Home Page: [163]http://sourceforge.net/projects/strace/
     * Download Location: [164]https://downloads.sourceforge.net/strace/
   Last updated on 2017-08-15 11:49:48 -0700
     * [165]Prev
       yasm-1.3.0
     * [166]Next
       Java-12.0.2
     * [167]Up
     * [168]Home
Riferimenti
   1. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/yasm.html
   2. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/java.html
   3. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/prog.html
   4. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
   5. http://wiki.linuxfromscratch.org/blfs/wiki/OtherProgrammingTools
   6. http://www.aplusdev.org/
   7. http://www.aplusdev.org/Download/index.html
   8. http://homepages.cwi.nl/~steven/abc/
   9. http://homepages.cwi.nl/~steven/abc/implementations.html
  10. http://www.informatik.uni-kiel.de/~mh/systems/ALF.html
  11. http://www.informatik.uni-kiel.de/~mh/systems/ALF/
  12. http://asm.objectweb.org/
  13. http://forge.objectweb.org/projects/asm/
  14. http://www.cl.cam.ac.uk/users/mr/BCPL.html
  15. http://www.cl.cam.ac.uk/users/mr/BCPL/
  16. http://www.daimi.au.dk/~beta/
  17. ftp://ftp.daimi.au.dk/pub/beta/
  18. http://www.brics.dk/bigwig/
  19. http://www.brics.dk/bigwig/download/
  20. http://www-sop.inria.fr/mimosa/fp/Bigloo/
  21. ftp://ftp-sop.inria.fr/mimosa/fp/Bigloo/
  22. http://www.cminusminus.org/
  23. http://www.cminusminus.org/code.html
  24. http://caml.inria.fr/
  25. http://caml.inria.fr/pub/distrib/
  26. http://www.softintegration.com/
  27. http://www.softintegration.com/products/chstandard/download/
  28. http://wiki.clean.cs.ru.nl/Clean
  29. http://wiki.clean.cs.ru.nl/Download_Clean
  30. http://cyclone.thelanguage.org/
  31. http://cyclone.thelanguage.org/wiki/Download/
  32. http://www.digitalmars.com/d/
  33. ftp://ftp.digitalmars.com/
  34. http://www.digitalmars.com/dscript/index.html
  35. ftp://ftp.digitalmars.com/
  36. http://www.gnu.org/software/dotgnu/
  37. http://www.gnu.org/software/dotgnu/pnet-packages.html
  38. http://www.opendylan.org/
  39. http://opendylan.org/download/index.html
  40. http://www.erights.org/
  41. http://www.erights.org/download/
  42. http://www.elasticworld.org/
  43. http://www.elasticworld.org/download.html
  44. http://www.erlang.org/
  45. http://www.erlang.org/download.html
  46. http://www.rapideuphoria.com/
  47. http://www.rapideuphoria.com/v20.htm
  48. http://felix.sourceforge.net/
  49. http://felix-lang.org/$/usr/local/lib/felix/tarballs
  50. http://www.ferite.org/
  51. http://www.ferite.org/download.html
  52. http://wiki.forthfreak.net/
  53. http://www.forth.org/
  54. http://www.forth.org/compilers.html
  55. http://www.gnu.org/software/smalltalk/manual/html_node/Overview.html
  56. http://smalltalk.gnu.org/
  57. https://ftp.gnu.org/gnu/smalltalk/
  58. http://www.haskell.org/ghc/
  59. http://www.cs.uu.nl/wiki/bin/view/Helium/WebHome
  60. http://www.haskell.org/hugs/
  61. http://www.haskell.org/nhc98/
  62. http://www.plantation-productions.com/Webster/HighLevelAsm/index.html
  63. http://www.plantation-productions.com/Webster/HighLevelAsm/dnld.html
  64. http://www.cs.arizona.edu/icon/
  65. ftp://ftp.cs.arizona.edu/icon/
  66. http://iolanguage.org/
  67. http://iobin.suspended-chord.info/
  68. http://www.jsoftware.com/
  69. http://www.jsoftware.com/stable.htm
  70. http://judoscript.org/jamaica.html
  71. http://judoscript.org/download.html
  72. http://www.latrobe.edu.au/humanities/research/research-projects/past-projects/joy-programming-language
  73. http://judoscript.org/judo.html
  74. http://judoscript.org/download.html
  75. http://www.brics.dk/JWIG/
  76. http://www.brics.dk/JWIG/download.html
  77. http://lavape.sourceforge.net/index.htm
  78. http://mathias.tripod.com/IavaHomepage.html
  79. http://mercurylang.org/
  80. http://mercurylang.org/download.html
  81. http://www.mono-project.com/Main_Page
  82. http://ftp.novell.com/pub/mono/archive/
  83. http://www.cs.arizona.edu/mpd/
  84. http://www.cs.arizona.edu/mpd/download/
  85. http://nemerle.org/About
  86. http://nemerle.org/Downloads
  87. http://www.gnu.org/software/octave/
  88. http://www.gnu.org/software/octave/download.html
  89. http://sourceforge.net/projects/ooc/
  90. https://downloads.sourceforge.net/ooc/
  91. http://ogdl.sourceforge.net/
  92. https://downloads.sourceforge.net/ogdl/
  93. http://pike.ida.liu.se/
  94. http://pike.ida.liu.se/download/pub/pike
  95. http://www.cosc.canterbury.ac.nz/greg.ewing/python/Pyrex/
  96. http://q-lang.sourceforge.net/
  97. https://downloads.sourceforge.net/q-lang/
  98. http://www.r-project.org/
  99. http://cran.r-project.org/mirrors.html
 100. http://regina-rexx.sourceforge.net/
 101. https://downloads.sourceforge.net/regina-rexx
 102. http://sdcc.sourceforge.net/
 103. http://sdcc.sourceforge.net/snap.php#Source
 104. http://smarteiffel.loria.fr/
 105. https://gforge.inria.fr/frs/?group_id=184
 106. http://www.squeak.org/
 107. http://www.squeak.org/Download/
 108. http://www.cs.arizona.edu/sr/index.html
 109. ftp://ftp.cs.arizona.edu/sr/
 110. http://www.it-c.dk/research/mlkit/
 111. http://mlton.org/
 112. http://www.polyml.org/
 113. http://www.smlnj.org/
 114. http://www.sbcl.org/
 115. https://downloads.sourceforge.net/sbcl/
 116. http://bellard.org/tcc/
 117. http://download.savannah.gnu.org/releases-noredirect/tinycc/
 118. http://sourceforge.net/projects/tiny-cobol/
 119. https://downloads.sourceforge.net/tiny-cobol/
 120. http://www.maumae.net/yorick
 121. http://yorick.sourceforge.net/index.php
 122. http://sourceforge.net/projects/yorick/files/
 123. http://www.cs.washington.edu/research/zpl/home/index.html
 124. http://www.cs.washington.edu/research/zpl/download/download.html
 125. http://jakarta.apache.org/bcel/index.html
 126. http://archive.apache.org/dist/jakarta/bcel/
 127. http://sourceforge.net/projects/choco/
 128. http://choco.sourceforge.net/download.html
 129. http://www.5z.com/jirka/gob.html
 130. http://ftp.5z.com/pub/gob/
 131. http://www.gtk.org/language-bindings.php
 132. http://java-gnome.sourceforge.net/4.0/
 133. http://java-gnome.sourceforge.net/4.0/get/
 134. http://gtk2-perl.sourceforge.net/
 135. https://downloads.sourceforge.net/gtk2-perl
 136. http://techbase.kde.org/Development/Languages
 137. http://numeric.scipy.org/
 138. https://downloads.sourceforge.net/numpy/
 139. http://cpan.org/
 140. http://www.a-a-p.org/index.html
 141. http://www.a-a-p.org/download.html
 142. http://projects.gnome.org/anjuta/index.shtml
 143. http://projects.gnome.org/anjuta/downloads.html
 144. http://www.eclipse.org/
 145. http://www.eclipse.org/downloads/
 146. http://mozart.github.io/
 147. https://github.com/mozart/mozart2#downloads
 148. http://cachecc1.sourceforge.net/
 149. https://downloads.sourceforge.net/cachecc1
 150. http://ccache.samba.org/
 151. http://samba.org/ftp/ccache/
 152. http://www.gnu.org/software/ddd/
 153. https://ftp.gnu.org/gnu/ddd/
 154. http://distcc.samba.org/
 155. http://distcc.samba.org/download.html
 156. http://ctags.sourceforge.net/tools.html
 157. http://ctags.sourceforge.net/
 158. https://downloads.sourceforge.net/ctags/
 159. http://sourceforge.net/projects/gocache/
 160. https://downloads.sourceforge.net/gocache/
 161. http://oprofile.sourceforge.net/news/
 162. http://oprofile.sourceforge.net/download/
 163. http://sourceforge.net/projects/strace/
 164. https://downloads.sourceforge.net/strace/
 165. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/yasm.html
 166. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/java.html
 167. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/00-ALL/prog.html
 168. file:///mnt/sdc1/pack/06-slbook/9.1/PROVA/index.html
