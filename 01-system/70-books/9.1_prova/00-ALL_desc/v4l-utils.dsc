   v4l-utils provides a series of utilities for media devices, allowing to
   handle the proprietary formats available at most webcams (libv4l), and
   providing tools to test V4L devices.
