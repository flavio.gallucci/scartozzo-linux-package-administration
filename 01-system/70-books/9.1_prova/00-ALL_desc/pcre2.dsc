   The PCRE2 package contains a new generation of the Perl Compatible
   Regular Expression libraries. These are useful for implementing regular
   expression pattern matching using the same syntax and semantics as
   Perl.
