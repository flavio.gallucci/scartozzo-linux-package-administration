   Whois is a client-side application which queries the whois directory
   service for information pertaining to a particular domain name. This
   package will install two programs by default: whois and mkpasswd. The
   mkpasswd command is also installed by the [5]Expect-5.45.4 package.
