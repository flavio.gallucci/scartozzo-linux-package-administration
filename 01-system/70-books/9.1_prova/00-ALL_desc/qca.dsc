   Qca aims to provide a straightforward and cross-platform crypto API,
   using Qt datatypes and conventions. Qca separates the API from the
   implementation, using plugins known as Providers.
