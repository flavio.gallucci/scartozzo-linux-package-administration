   The Bogofilter application is a mail filter that classifies mail as
   spam or ham (non-spam) by a statistical analysis of the message's
   header and content (body).
