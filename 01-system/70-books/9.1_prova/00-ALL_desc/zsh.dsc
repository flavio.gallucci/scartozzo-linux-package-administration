   The zsh package contains a command interpreter (shell) usable as an
   interactive login shell and as a shell script command processor. Of the
   standard shells, zsh most closely resembles ksh but includes many
   enhancements.
