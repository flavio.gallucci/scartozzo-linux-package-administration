   The GPGME package is a C library that allows cryptography support to be
   added to a program. It is designed to make access to public key crypto
   engines like GnuPG or GpgSM easier for applications. GPGME provides a
   high-level crypto API for encryption, decryption, signing, signature
   verification and key management.
