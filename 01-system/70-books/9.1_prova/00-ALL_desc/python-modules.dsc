   The Python module packages add useful objects to the Python language.
   Modules utilized by packages throughout BLFS are listed here, along
   with their dependencies.
     * [5]D-Bus Python-1.2.16
     * [6]decorator-4.4.1
     * [7]docutils-0.16
     * [8]PyAtSpi2-2.34.0
     * [9]PyCairo-1.19.0
     * [10]PyCairo-1.18.2
     * [11]PyCryptodome-3.9.6
     * [12]dbusmock-0.19
     * [13]Pygments-2.5.2
     * [14]PyGObject-2.28.7
     * [15]PyGObject-3.34.0
     * [16]PyGTK-2.24.0
     * [17]PyXDG-0.25
     * [18]libxml2-2.9.10 (for Python2)
     * [19]lxml-4.5.0
     * [20]MarkupSafe-1.1.1
     * [21]Jinja2-2.11.1
     * [22]Mako-1.1.1
     * [23]python-slip-0.6.5
     * [24]Scour-0.37
     * [25]six-1.14.0
     * [26]PyYAML-5.3
D-Bus Python-1.2.16
   D-Bus Python provides Python bindings to the D-Bus API interface.
   The goal of the decorator module is to make it easy to define
   signature-preserving function decorators and decorator factories.
   docutils is a set of Python modules and programs for processing
   plaintext docs into formats such as HTML, XML, or LaTeX.
   The PyAtSpi2 package contains Python bindings for the core components
   of the GNOME Accessibility.
   PyCairo provides Python bindings to Cairo.
   This version of PyCairo provides Python2 bindings to Cairo.
   PyCryptodome is a collection of both secure hash functions (such as
   SHA256 and RIPEMD160), and various encryption algorithms (AES, DES,
   RSA, ElGamal, etc.), and is a drop-in replacement for PyCrypto.
   dbusmock a Python library useful for writing tests for software which
   talks to D-Bus services.
   Pygments is a general syntax highlighter written in Python, for more
   than 300 languages.
   PyGObject-2.28.7 provides Python 2 bindings to the GObject class from
   GLib.
   PyGObject3 provides Python bindings to the GObject class from GLib.
   PyGTK lets you to easily create programs with a graphical user
   interface using the Python programming language.
   PyXDG is a Python library to access freedesktop.org standards.
   These instructions provide the Python2 bindings for libxml2: the
   Python3 bindings are built as part of [107]libxml2-2.9.10 and most
   packages in the book do not use these bindings for the older Python2.
   By building these bindings after [108]libxml2-2.9.10 has been
   installed, there is no need to rebuild the time-consuming (if all
   dependencies are used) main part of that package.
   lxml provides Python bindings for [114]libxslt-1.1.34 and
   [115]libxml2-2.9.10.
   MarkupSafe is a Python module that implements a XML/HTML/XHTML Markup
   safe string.
   Jinja2 is a Python module that implements a simple pythonic template
   language.
   Mako is a Python module that implements hyperfast and lightweight
   templating for the Python platform.
   python-slip provides convenience, extension and workaround code for
   Python and some Python modules.
   PyYAML is a Python module that implements the next generation YAML
   parser and emitter.
   Scour is an SVG (Scalable Vector Graphics) optimizer/cleaner that
   reduces their size by optimizing structure and removing unnecessary
   data.
   Six is a Python 2 and 3 compatibility library.
