   The Gtk VNC package contains a VNC viewer widget for GTK+. It is built
   using coroutines allowing it to be completely asynchronous while
   remaining single threaded.
