   Transcode was a fast, versatile and command-line based audio/video
   everything to everything converter primarily focussed on producing AVI
   video files with MP3 audio, but also including a program to read all
   the video and audio streams from a DVD.
   Although outdated and no longer maintained, it can still be used to
   extract items from a DVD using only the required and recommended
   dependencies, or to recode to AVI files.
   Subjects to the comments below, this package is known to build and
   minimally work using an LFS-8.3 platform.
