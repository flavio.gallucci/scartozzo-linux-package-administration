   Bluefish is a GTK+ text editor targeted towards programmers and web
   designers, with many options to write websites, scripts and programming
   code. Bluefish supports many programming and markup languages, and it
   focuses on editing dynamic and interactive websites.
