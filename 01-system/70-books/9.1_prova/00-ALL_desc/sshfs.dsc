   The Sshfs package contains a filesystem client based on the SSH File
   Transfer Protocol. This is useful for mounting a remote computer that
   you have ssh access to as a local filesystem. This allows you to drag
   and drop files or run shell commands on the remote files as if they
   were on your local computer.
