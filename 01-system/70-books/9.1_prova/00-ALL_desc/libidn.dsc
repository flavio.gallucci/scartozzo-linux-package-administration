   libidn is a package designed for internationalized string handling
   based on the [5]Stringprep, [6]Punycode and [7]IDNA specifications
   defined by the Internet Engineering Task Force (IETF) Internationalized
   Domain Names (IDN) working group, used for internationalized domain
   names. This is useful for converting data from the system's native
   representation into UTF-8, transforming Unicode strings into ASCII
   strings, allowing applications to use certain ASCII name labels
   (beginning with a special prefix) to represent non-ASCII name labels,
   and converting entire domain names to and from the ASCII Compatible
   Encoding (ACE) form.
