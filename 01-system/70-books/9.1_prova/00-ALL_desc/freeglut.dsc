   Freeglut is intended to be a 100% compatible, completely opensourced
   clone of the GLUT library. GLUT is a window system independent toolkit
   for writing OpenGL programs, implementing a simple windowing API, which
   makes learning about and exploring OpenGL programming very easy.
