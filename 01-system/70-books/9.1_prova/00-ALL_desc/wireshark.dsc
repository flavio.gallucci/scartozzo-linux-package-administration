   The Wireshark package contains a network protocol analyzer, also known
   as a “sniffer”. This is useful for analyzing data captured “off the
   wire” from a live network connection, or data read from a capture file.
   Wireshark provides both a graphical and a TTY-mode front-end for
   examining captured network packets from over 500 protocols, as well as
   the capability to read capture files from many other popular network
   analyzers.
