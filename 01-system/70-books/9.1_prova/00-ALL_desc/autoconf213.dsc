   Autoconf2.13 is an old version of Autoconf . This old version accepts
   switches which are not valid in more recent versions. Now that firefox
   has started to use python2 for configuring, this old version is
   required even if configure files have not been changed.
