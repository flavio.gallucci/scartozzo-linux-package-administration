   Bubblewrap is a setuid implementation of user namespaces, or
   sandboxing, that provides access to a subset of kernel user namespace
   features. Bubblewrap allows user owned processes to run in an isolated
   environment with limited access to the underlying filesystem.
