   liba52 is a free library for decoding ATSC A/52 (also known as AC-3)
   streams. The A/52 standard is used in a variety of applications,
   including digital television and DVD.
