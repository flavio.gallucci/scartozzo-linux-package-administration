   BIND Utilities is not a separate package, it is a collection of the
   client side programs that are included with [5]BIND-9.14.10. The BIND
   package includes the client side programs nslookup, dig and host. If
   you install BIND server, these programs will be installed
   automatically. This section is for those users who don't need the
   complete BIND server, but need these client side applications.
