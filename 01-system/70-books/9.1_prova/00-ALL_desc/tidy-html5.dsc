   The Tidy HTML5 package contains a command line tool and libraries used
   to read HTML, XHTML and XML files and write cleaned up markup. It
   detects and corrects many common coding errors and strives to produce
   visually equivalent markup that is both W3C compliant and compatible
   with most browsers.
