   Brotli provides a general-purpose lossless compression algorithm that
   compresses data using a combination of a modern variant of the LZ77
   algorithm, Huffman coding and 2nd order context modeling. Its libraries
   are particularly used for WOFF2 fonts on webpages.
