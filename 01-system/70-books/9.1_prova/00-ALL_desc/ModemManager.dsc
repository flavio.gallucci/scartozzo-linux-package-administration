   ModemManager provides a unified high level API for communicating with
   mobile broadband modems, regardless of the protocol used to communicate
   with the actual device.
