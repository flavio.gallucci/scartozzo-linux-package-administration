   The stunnel package contains a program that allows you to encrypt
   arbitrary TCP connections inside SSL (Secure Sockets Layer) so you can
   easily communicate with clients over secure channels. stunnel can be
   used to add SSL functionality to commonly used Inetd daemons such as
   POP-2, POP-3, and IMAP servers, along with standalone daemons such as
   NNTP, SMTP, and HTTP. stunnel can also be used to tunnel PPP over
   network sockets without changes to the server package source code.
